<?php
/**
 * Copyright 2019 aheadWorks. All rights reserved.
See LICENSE.txt for license details.
 */

namespace Aheadworks\Followupemail2\Model\Template;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Quote\Model\Quote\Item as QuoteItem;
use Magento\Sales\Model\Order\Item as OrderItem;
use Aheadworks\Followupemail2\Model\Template\Resolver\Order\Item as TemplateOrderItemResolver;
use Aheadworks\Followupemail2\Model\Template\Resolver\Quote\Item as TemplateQuoteItemResolver;

/**
 * Class Filter
 * @package Aheadworks\Followupemail2\Model\Template
 * @codeCoverageIgnore
 */
class Filter extends \Magento\Newsletter\Model\Template\Filter
{
    /**
     * For pattern
     */
    const CONSTRUCTION_FOR_PATTERN = '/{{for\s*(.*?)\s*in\s*(.*?)}}(.*?){{\\/for\s*}}/si';

    /**
     * Thumbnail directive parameter names
     */
    const THUMBNAIL_SOURCE              = 'source';
    const THUMBNAIL_WIDTH               = 'width';
    const THUMBNAIL_HEIGHT              = 'height';

    /**#@+
     * Names of parameters for productUrl directive
     */
    const PRODUCT_URL_SOURCE            = 'source';
    /**#@-*/

    /**
     * @var TemplateOrderItemResolver
     */
    private $templateOrderItemResolver;

    /**
     * @var TemplateQuoteItemResolver
     */
    private $templateQuoteItemResolver;

    /**
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\View\Asset\Repository $assetRepo
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Variable\Model\VariableFactory $coreVariableFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\View\LayoutInterface $layout
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     * @param \Magento\Framework\App\State $appState
     * @param \Magento\Framework\UrlInterface $urlModel
     * @param \Pelago\Emogrifier $emogrifier
     * @param \Magento\Variable\Model\Source\Variables $configVariables
     * @param \Magento\Widget\Model\ResourceModel\Widget $widgetResource
     * @param \Magento\Widget\Model\Widget $widget
     * @param TemplateOrderItemResolver $templateOrderItemResolver
     * @param TemplateQuoteItemResolver $templateQuoteItemResolver
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\Stdlib\StringUtils $string,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Variable\Model\VariableFactory $coreVariableFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\LayoutInterface $layout,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Framework\App\State $appState,
        \Magento\Framework\UrlInterface $urlModel,
        \Pelago\Emogrifier $emogrifier,
        \Magento\Variable\Model\Source\Variables $configVariables,
        \Magento\Widget\Model\ResourceModel\Widget $widgetResource,
        \Magento\Widget\Model\Widget $widget,
        TemplateOrderItemResolver $templateOrderItemResolver,
        TemplateQuoteItemResolver $templateQuoteItemResolver
    ) {
        $this->templateOrderItemResolver = $templateOrderItemResolver;
        $this->templateQuoteItemResolver = $templateQuoteItemResolver;
        parent::__construct(
            $string,
            $logger,
            $escaper,
            $assetRepo,
            $scopeConfig,
            $coreVariableFactory,
            $storeManager,
            $layout,
            $layoutFactory,
            $appState,
            $urlModel,
            $emogrifier,
            $configVariables,
            $widgetResource,
            $widget
        );
    }

    /**
     * Filter the string as template
     *
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        $this->_modifiers['formatPrice'] = [$this, 'modifierFormatPrice'];
        $this->_modifiers['formatDecimal'] = [$this, 'modifierFormatDecimal'];
        try {
            $value = $this->process($value);
        } catch (\Exception $e) {
            // Since a single instance of this class can be used to filter content multiple times, reset callbacks to
            // prevent callbacks running for unrelated content (e.g., email subject and email body)
            $this->resetAfterFilterCallbacks();

            if ($this->_appState->getMode() == \Magento\Framework\App\State::MODE_DEVELOPER) {
                $value = sprintf(__('Error filtering template: %s'), $e->getMessage());
            } else {
                $value = __("We're sorry, an error has occurred while generating this email.");
            }
            $this->_logger->critical($e);
        }
        return $value;
    }

    /**
     * Process string
     *
     * @param string $value
     * @return string
     * @throws \Exception
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    private function process($value)
    {
        // "depend", "if", "template" and "for" directives should be first
        $directives = [
            self::CONSTRUCTION_DEPEND_PATTERN => 'dependDirective',
            self::CONSTRUCTION_IF_PATTERN => 'ifDirective',
            self::CONSTRUCTION_TEMPLATE_PATTERN => 'templateDirective',
            self::CONSTRUCTION_FOR_PATTERN => 'forDirective',
        ];
        foreach ($directives as $pattern => $directive) {
            if (preg_match_all($pattern, $value, $constructions, PREG_SET_ORDER)) {
                foreach ($constructions as $construction) {
                    $callback = [$this, $directive];
                    if (!is_callable($callback)) {
                        continue;
                    }
                    try {
                        $replacedValue = call_user_func($callback, $construction);
                    } catch (\Exception $e) {
                        throw $e;
                    }
                    $value = str_replace($construction[0], $replacedValue, $value);
                }
            }
        }

        if (preg_match_all(self::CONSTRUCTION_PATTERN, $value, $constructions, PREG_SET_ORDER)) {
            foreach ($constructions as $construction) {
                $callback = [$this, $construction[1] . 'Directive'];
                if (!is_callable($callback)) {
                    continue;
                }
                try {
                    $replacedValue = call_user_func($callback, $construction);
                } catch (\Exception $e) {
                    throw $e;
                }
                $value = str_replace($construction[0], $replacedValue, $value);
            }
        }

        $value = $this->afterFilter((string) $value);
        return $value;
    }

    /**
     * forDirective
     *
     * @param string[] $construction
     * @return string
     */
    public function forDirective($construction)
    {
        $content = '';
        if (count($this->templateVars) == 0) {
            $content = $construction[0];
        }
        $iterated = $this->getVariable($construction[2], []);
        if (is_array($iterated) || $iterated instanceof \IteratorAggregate) {
            foreach ($iterated as $variable) {
                $this->templateVars[$construction[1]] = $variable;
                $content .= $this->process($construction[3]);
            }
        }
        return $content;
    }

    /**
     * widgetDirective
     *
     * @param \string[] $construction
     * @return string
     */
    public function widgetDirective($construction)
    {
        $params = $this->getParameters($construction[2]);
        $params['area'] = 'frontend';

        // Determine what name block should have in layout
        $name = null;
        if (isset($params['name'])) {
            $name = $params['name'];
        }
        // validate required parameter type or id
        if (!empty($params['type'])) {
            $type = $params['type'];
        } elseif (!empty($params['id'])) {
            $preConfigured = $this->_widgetResource->loadPreconfiguredWidget($params['id']);
            $type = $preConfigured['widget_type'];
            $params = $preConfigured['parameters'];
        } else {
            return '';
        }
        // we have no other way to avoid fatal errors for type like 'cms/widget__link', '_cms/widget_link' etc.
        $xml = $this->_widget->getWidgetByClassType($type);
        if ($xml === null) {
            return '';
        }
        // define widget block and check the type is instance of Widget Interface
        $widget = $this->_layout->createBlock($type, $name, ['data' => $params]);
        if (!$widget instanceof \Magento\Widget\Block\BlockInterface) {
            return '';
        }
        return $widget->toHtml();
    }

    /**
     * modifierFormatPrice
     *
     * @param string $value
     * @return mixed
     */
    public function modifierFormatPrice($value)
    {
        if (isset($this->templateVars['store'])) {
            $value = $this->templateVars['store']->getCurrentCurrency()->format($value);
        }
        return $value;
    }

    /**
     * modifierFormatDecimal
     *
     * @param string $value
     * @return string
     */
    public function modifierFormatDecimal($value)
    {
        if (is_numeric($value)) {
            $params = func_get_args();
            array_shift($params);
            if (!count($params)) {
                $value = number_format($value);
            } elseif (count($params) == 1) {
                $value = number_format($value, $params[0]);
            } elseif (count($params) == 3) {
                $value = number_format($value, $params[0], $params[1], $params[2]);
            }
        }
        return $value;
    }

    /**
     * thumbnailDirective
     * Returns link to product thumbnail
     * Usage: {{thumbnail source="" width="" height=""}}
     *
     * @param string[] $construction
     * @return string
     * @throws \Exception
     */
    public function thumbnailDirective($construction)
    {
        $params = $this->getParameters($construction[2]);

        if (isset($params[self::THUMBNAIL_SOURCE]) && $params[self::THUMBNAIL_SOURCE]) {
            $source = $params[self::THUMBNAIL_SOURCE];
            if (is_object($source)) {
                if ($source instanceof ProductInterface) {
                    /** @var ProductInterface $product */
                    $product = $source;
                    $productId = $product->getId();
                } else {
                    throw new \Exception(__('The object specified is not a product to take a thumbnail from'));
                }
            } elseif (is_scalar($source)) {
                $productId = $source;
            } else {
                throw new \Exception(__('Wrong object type'));
            }
        } else {
            throw new \Exception(__('No source parameter is specified; there is nowhere to take the thumbnail from'));
        }

        $imgWidth = isset($params[self::THUMBNAIL_WIDTH]) ?
            $params[self::THUMBNAIL_WIDTH] :
            null;
        $imgHeight = isset($params[self::THUMBNAIL_HEIGHT]) ?
            $params[self::THUMBNAIL_HEIGHT] :
            null;

        if (isset($product)) {
            $this->urlModel->setScope($this->_storeManager->getStore($product->getStoreId()));
        }
        return $this->urlModel
            ->getUrl(
                'aw_followupemail2/product/image',
                [
                    'product_id' => $productId,
                    'width'  => $imgWidth,
                    'height' => $imgHeight,
                    '_scope_to_url' => true,
                ]
            );
    }

    /**
     * productUrlDirective
     * Returns link to product
     * Usage:
     * {{productUrl source="$productId"}}
     * {{productUrl source="$product"}}
     * {{productUrl source="$quoteItem"}}
     * {{productUrl source="$orderItem"}}
     *
     * @param string[] $construction
     * @return string
     * @throws \Exception
     */
    public function productUrlDirective($construction)
    {
        $parameters = $this->getParameters($construction[2]);
        $productId = null;
        $storeId = null;

        if (isset($parameters[self::PRODUCT_URL_SOURCE])
            && $parameters[self::PRODUCT_URL_SOURCE]
        ) {
            $source = $parameters[self::PRODUCT_URL_SOURCE];
            if (is_object($source)) {
                if ($source instanceof ProductInterface) {
                    $productId = $source->getId();
                    $storeId = $source->getStoreId();
                } elseif ($source instanceof QuoteItem) {
                    $productId = $this->templateQuoteItemResolver->getProductIdForUrl($source);
                    $storeId = $source->getStoreId();
                } elseif ($source instanceof OrderItem) {
                    $productId = $this->templateOrderItemResolver->getProductIdForUrl($source);
                    $storeId = $source->getStoreId();
                } else {
                    throw new \Exception(__('Wrong object type to take a product URL from'));
                }
            } elseif (is_scalar($source)) {
                $productId = $source;
            } else {
                throw new \Exception(__('Wrong object type to take a product URL from'));
            }
        } else {
            throw new \Exception(__('No source parameter is specified; there is nowhere to take a product URL from'));
        }

        if (empty($productId)) {
            throw new \Exception(__('Incorrect source to take a product URL from'));
        }

        if (isset($storeId)) {
            $this->urlModel->setScope($this->_storeManager->getStore($storeId));
        }

        return $this->urlModel
            ->getUrl(
                'catalog/product/view',
                [
                    'id' => $productId,
                    '_scope_to_url' => true,
                ]
            );
    }
}
