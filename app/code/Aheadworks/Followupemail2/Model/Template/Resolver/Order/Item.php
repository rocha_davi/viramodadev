<?php
/**
 * Copyright 2019 aheadWorks. All rights reserved.
See LICENSE.txt for license details.
 */

namespace Aheadworks\Followupemail2\Model\Template\Resolver\Order;

use Magento\Sales\Model\Order\Item as OrderItem;

/**
 * Class OrderItem
 *
 * @package Aheadworks\Followupemail2\Model\Template\Resolver\Order
 */
class Item
{
    /**
     * Retrieve product id for frontend URL generation
     *
     * @param OrderItem $orderItem
     * @return int|null
     */
    public function getProductIdForUrl($orderItem)
    {
        $productId = $orderItem->getProductId();

        $superProductConfig = $orderItem->getProductOptionByCode('super_product_config');
        if (is_array($superProductConfig) && isset($superProductConfig['product_id'])) {
            $productId = $superProductConfig['product_id'];
        }

        if ($orderItem->getParentItem()) {
            $productId = $orderItem->getParentItem()->getProductId();
        }

        return $productId;
    }
}
