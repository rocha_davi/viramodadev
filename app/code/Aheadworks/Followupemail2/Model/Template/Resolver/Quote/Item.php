<?php
/**
 * Copyright 2019 aheadWorks. All rights reserved.
See LICENSE.txt for license details.
 */

namespace Aheadworks\Followupemail2\Model\Template\Resolver\Quote;

use Magento\Quote\Model\Quote\Item as QuoteItem;

/**
 * Class Item
 *
 * @package Aheadworks\Followupemail2\Model\Template\Resolver\Quote
 */
class Item
{
    /**
     * Retrieve product id for frontend URL generation
     *
     * @param QuoteItem $quoteItem
     * @return int|null
     */
    public function getProductIdForUrl($quoteItem)
    {
        $productId = $quoteItem->getData('product_id');

        $productTypeOption = $quoteItem->getOptionByCode('product_type');
        if ($productTypeOption) {
            $productId = $productTypeOption->getProductId();
        }

        if ($quoteItem->getParentItem()) {
            $productId = $quoteItem->getParentItem()->getData('product_id');
        }

        return $productId;
    }
}
