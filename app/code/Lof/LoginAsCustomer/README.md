# [Magento 2 Login As Customer](https://landofcoder.com/login-as-customer-magento-2-extension.html) by Landofcoder

## Overview

There are situations, where customers have issues in the "My Account" area or on the checkout pages. In such cases, they need your help. Now merchants have a simple and fast solution to check the problems within a specific customer account. This extension allows the admin user to log into a customer's account in one click and without using a password or changing authentication data. After entering to customer account, the admin can perform tests and detect any issues within the “My Account” area. Login as Customer extension is also useful for sale managers, it can helps to accompany customer during the checkout process.

## Features

1. Compatible with Magento 2 Guest to Customer

Login as Customer extension is fully compatible with Magento 2 Guest to Customer Extension by Magefan, that allows you to convert guest orders to customers easily. Together, these Magento 2 plugins are a great combination you can use for your benefit. Converting guest order to customer admin user could simultaneously log in as customer and help the visitors with their issues.

2. Enter customer account using Admin Panel

The checkout and order placing processes are easy to complete, however, sometimes there may appear some issues. Also, customers may face the issue in “My Account’ area, and you have no way to detect bugs other than login to customer profile using customer login and password. Fortunately, this process can be done easily and quickly from the admin panel with the Magento 2 Login As Customer extension. It allows you to log into customer’s account in seconds and correspondingly solve the problem faster.

3. Login As Customer Button

For your convenience, in Magento 2 Login as Customer module, there have been created a couple of ways the admin user could have entered the customer’s profile through. Thus, you can find the “Login As Customer” buttons in All Orders and All Customers grids as well as on the Order and Customer Pages. Pressing them will momentarily get you to the My Account area where you will be able to conduct the analysis of the issues.

4. Entrance log

If your store is managed by several admin users responsible for various sometimes similar tasks, it is extremely important to keep track of all the changes made in customer accounts by these admins. Since all the information about who, when and which customer’s account entered is stored in Magento 2 Login as Customer extension entrance log you can access that data anytime to be aware of all logging actions.

5. Restrict Login Permissions

In order to avoid the duplication of configurations by admins in the admin panel restrict their permissions using User Roles managing the Magento 2 Login as Customer extension. This way you will differentiate admins’ roles and be sure every admin user is responsible just for the field he was assigned to.

6. Page Cache Disabling

In order to solve the issue admin user sometimes has to conduct a lot of changes and disable or enable a few options. Page cache may be an obstacle in this process. Magento 2 Login as Customer extension allows you to Disable Page Cache for admin user when login as customer.

7. Guest Shopping Cart Items

There is a feature in Magento according to which products you add to the cart while surfing through the website as a guest are automatically merged with the customer shopping cart during the login. Login as Customer plugin has the option to disable this behavior for admin login ONLY, so products added by the admin while testing won’t be added to the shopping cart of the customer.