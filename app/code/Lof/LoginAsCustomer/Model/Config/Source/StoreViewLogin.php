<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * https://landofcoder.com/end-user-license-agreement
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_LoginAsCustomer
 * @copyright  Copyright (c) 2019 Landofcoder (https://landofcoder.com/)
 * @license    https://landofcoder.com/end-user-license-agreement
 */

namespace Lof\LoginAsCustomer\Model\Config\Source;

/**
 * Class StoreViewLogin
 * @package Lof\LoginAsCustomer\Model\Config\Source
 */
class StoreViewLogin implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @const int
     */
    const AUTODETECT = 0;

    /**
     * @const int
     */
    const MANUAL = 1;
    
    /**
     * Options int
     *
     * @return array
     */
    public function toOptionArray()
    {
        return  [
            ['value' => self::AUTODETECT, 'label' => __('Auto-Detection (default)')],
            ['value' => self::MANUAL, 'label' => __('Manual Choose')],
        ];
    }
}
