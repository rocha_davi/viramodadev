<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Landofcoder
 * @package    Lof_MarketPlace
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */
namespace Lof\MarketPlace\Model;

use Magento\Catalog\Api\Data\CategorySearchResultsInterface;
use Lof\MarketPlace\Helper\Data;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Seller Model
 */
class SellerProduct extends \Magento\Framework\Model\AbstractModel implements \Lof\MarketPlace\Api\SellerProductsRepositoryInterface
{
    /**
     * Seller's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    const STATUS_PENDING = 2;


    /** @var StoreManagerInterface */
    protected $_storeManager;

    /**
     * URL Model instance
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;

    protected $_scopeConfig;
    protected $helper;
    /**
     * @var CollectionFactory
     */
    protected $_productCollectionFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;
    /**
     * @var JoinProcessorInterface
     */
    private $extensionAttributesJoinProcessor;
    /**
     * @var SellerProductSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;


    /**
     * @param Context $context
     * @param Registry $registry
     * @param ResourceModel\SellerProduct $resource
     * @param CollectionFactory $productCollectionFactory
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Framework\UrlInterface $url
     * @param ScopeConfigInterface $scopeConfig
     * @param Data $helper
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param CategorySearchResultsInterface $searchResultsFactory
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ResourceModel\SellerProduct $resource,
        CollectionFactory $productCollectionFactory,
        StoreManagerInterface $storeManager,
        \Magento\Framework\UrlInterface $url,
        ScopeConfigInterface $scopeConfig,
        Data $helper,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        CategorySearchResultsInterface $searchResultsFactory,
        AbstractDb $resourceCollection = null,
        array $data = []
        ) {
        $this->_storeManager = $storeManager;
        $this->_url = $url;
        $this->helper = $helper;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }



    /**
     * Initialize customer model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('Lof\MarketPlace\Model\ResourceModel\SellerProduct');
    }

    /**
     * Prepare page's statuses.
     * Available event cms_page_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [0 => __('Not Submited'),1 => __('Pending'), 2 => __('Approved'),3 => __('Unapproved'),];
    }


    public function getProducts($seller_id, $entity_id = null, $arttributeToSelect='*')
    {
        $data = [];
        $collection = $this->_productCollectionFactory->create();
        $collection->addFieldToFilter('seller_id', $seller_id);
        if($entity_id){
            $collection->addFieldToFilter('entity_id', $entity_id);
        }
        $collection->addAttributeToSelect($arttributeToSelect);
        $baseUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        foreach ($collection as $k => $product) {
            $product_data = $product->getData();
            if($entity_id){
                $data = $product_data;
            }else{
                $data[] = $product_data;
            }
        }
        return $data;
    }
    public function getSellerProducts($seller_id)
    {
       $products = $this->getProducts($seller_id);
       $count = count($products);
        return [
            'total_count' => $count,
            'items'       => $products,
        ];
    }

    public function getListSellersProduct($id)
    {
        $res = [
                "code" => 0,
                "message" => "Get data success",
                "result" => [
                        "products" => $this->getProducts($id)
                    ]
            ];
        header('Content-Type: application/json');
        echo json_encode($res, JSON_PRETTY_PRINT);
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->_productCollectionFactory->create();
        $collection->addFieldToFilter('seller_id', ['neq' => 0]);
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Magento\Catalog\Api\Data\ProductInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory;
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getData();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }
}
