<?php

namespace Lof\MarketPlace\Model;

use Lof\MarketPlace\Api\Data\SellersSearchResultsInterfaceFactory;
use Lof\MarketPlace\Api\SellersFrontendRepositoryInterface;
use Lof\MarketPlace\Model\ResourceModel\Seller\CollectionFactory;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class SellersFrontendRepository
 * @package Lof\MarketPlace\Model
 */
class SellersFrontendRepository implements SellersFrontendRepositoryInterface
{
    /**
     * @var Seller
     */
    protected  $_seller;

    /**
     * @var SellersSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;
    /**
     * @var SellerProduct
     */
    protected $sellerProduct;
    /**
     * @var JoinProcessorInterface
     */
    protected $extensionAttributesJoinProcessor;
    /**
     * @var ResourceModel\Seller\CollectionFactory
     */
    protected $_collection;
    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * SellersFrontendRepository constructor.
     * @param Seller $seller
     * @param CollectionFactory $collection
     * @param StoreManagerInterface $storeManager
     * @param ObjectManagerInterface $objectManager
     * @param SellersSearchResultsInterfaceFactory $searchResultsFactory
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param CollectionProcessorInterface $collectionProcessor
     * @param SellerProduct $sellerProduct
     */
    public function __construct(
        Seller $seller,
        CollectionFactory $collection,
        StoreManagerInterface $storeManager,
        ObjectManagerInterface $objectManager,
        SellersSearchResultsInterfaceFactory $searchResultsFactory,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        CollectionProcessorInterface $collectionProcessor,

        SellerProduct $sellerProduct
        ) {
        $this->_seller = $seller;
        $this->_collection = $collection;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->_storeManager = $storeManager;
        $this->_objectManager = $objectManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->sellerProduct = $sellerProduct;
    }

    /**
     * @param string $seller_id
     * @return array|\Lof\MarketPlace\Api\Data\SellerInterface|mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($seller_id){
        $data = [];
        if($seller_id){
            $collection = $this->_collection->create();
            $collection->addFieldToFilter("seller_id", $seller_id);
            if($collection->getSize() > 0){
                $model = $collection->getLastItem();
                $data = $model->load($model->getSellerId())->getData();
            }
        }
        $customerId['customer_id'] = $data['customer_id'];
        return [$customerId];
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \Lof\MarketPlace\Api\Data\SellerInterface|\Lof\MarketPlace\Api\Data\SellersSearchResultsInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getListSellers(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->_collection->create();
        $this->collectionProcessor->process($criteria, $collection);
        $searchResults = $this->searchResultsFactory->create();
        $items = [];
        foreach ($collection as $val) {
            $data = $val->getData();
            if(isset($data['image']) && $data['image']){
                $data["image"] = $this->_storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                ) . $data["image"];

                $data["thumbnail"] = $this->_storeManager->getStore()->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                ) . $data["thumbnail"];
            }
            if(isset($data['store_id']) && $data['store_id']) {
                $data['store_id'] = implode(',',$data['store_id']);
            }
            $items[] = $data;
        }
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }
    /**
     * get seller review
     * @param string $seller_id
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getSellersReviews($seller_id)
    {
        $sellerreviews = $this->_objectManager->get("Lof\MarketPlace\Model\Review");
        $data = $sellerreviews->getCollection()->addFieldToFilter('seller_id',$seller_id)->getData();
        foreach ($data as &$value) {
            $value["reviewseller_id"] = (int)$value["reviewseller_id"];
            $value["type"]            = (int)$value["type"];
            $value["seller_id"]       = (int)$value["seller_id"];
            $value["customer_id"]     = (int)$value["customer_id"];
            $value["review_id"]       = (int)$value["review_id"];
            $value["product_id"]      = (int)$value["product_id"];
            $value["rating"]          = (int)$value["rating"];
            $value["order_id"]        = (int)$value["order_id"];
            $value["is_public"]       = (int)$value["is_public"];
            $value["status"]          = (int)$value["status"];
        }
        $res = [
            "code" => 0,
            "message" => "Get data success"
        ];
        $res["result"] = [
            "reviews" => $data
        ];
        return $res;
    }
    /**
     * get seller rating
     * @param string $seller_idgetSellersRating
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getSellersRating($seller_id)
    {
        $sellerrating = $this->_objectManager->get("Lof\MarketPlace\Model\Rating");
        $data = $sellerrating->getCollection()->addFieldToFilter('seller_id',$seller_id)->getData();
        foreach ($data as &$value) {
            $value["rating_id"]         = (int)$value["rating_id"];
            $value["seller_id"]         = (int)$value["seller_id"];
            $value["customer_id"]       = (int)$value["customer_id"];
            $value["rate1"]             = (int)$value["rate1"];
            $value["rate2"]             = (int)$value["rate2"];
            $value["rate3"]             = (int)$value["rate3"];
            $value["rating"]            = (int)$value["rating"];
        }
        $res = [
            "total_count" => count($data),
            "items" => $data
        ];
       return $res;
    }
    /**
     * @inheritDoc
     */
    public function getSellersbyProductID($product_id)
    {
        $this->_objectManager->get("Lof\MarketPlace\Model\Rating");
        $seller = $this->_objectManager->create ('Lof\MarketPlace\Model\SellerProduct')->load ($product_id, 'product_id' );
        if($seller->getData("seller_id")){
            return $this->getById($seller->getData("seller_id"));
        }
    }

    /**
     * @inheritDoc
     */
    public function getSellersProducts($seller_id)
    {
         return $this->sellerProduct->getSellerProducts($seller_id);
    }
}
