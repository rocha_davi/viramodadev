<?php

namespace Lof\MarketPlace\Api\Data;

interface SellerProductSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{


    /**
     * Get seller list.
     * @return \Lof\MarketPlace\Api\Data\ProductInterface[]
     */
    public function getItems();

    /**
     * Set seller list.
     * @param \Lof\MarketPlace\Api\Data\ProductInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
