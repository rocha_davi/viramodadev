<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Gdpr
 */


namespace Amasty\Gdpr\Block;

use Amasty\Gdpr\Model\Consent\DataProvider\PrivacySettingsDataProviderFactory;
use Amasty\Gdpr\Model\ConsentLogger;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Data\Form\FormKey as FormKey;
use Amasty\Gdpr\Model\Config;

class Settings extends Template
{
    const DOWNLOAD_DATA_BLOCK_SHORT_NAME = 'download';
    const ANONYMISE_DATA_BLOCK_SHORT_NAME = 'anonymise';
    const DELETE_ACCOUT_BLOCK_SHORT_NAME = 'delete';
    const CONSENT_OPTING_BLOCK_SHORT_NAME = 'consent_opting';
    const VISIBLE_BLOCK_LAYOUT_VARIABLE_NAME = 'visible_block';
    const NEED_PASSWORD_LAYOUT_VARIABLE_NAME = 'need_password';
    const IS_ORDER_LAYOUT_VARIABLE_NAME = 'is_order';

    /**
     * @var string
     */
    protected $_template = 'settings.phtml';

    /**
     * @var FormKey
     */
    protected $formKey;

    /**
     * @var Config
     */
    private $configProvider;
    /**
     * @var PrivacySettingsDataProviderFactory
     */
    private $privacySettingsDataProviderFactory;

    public function __construct(
        Template\Context $context,
        \Magento\Framework\Registry $registry,
        FormKey $formKey,
        Config $configProvider,
        PrivacySettingsDataProviderFactory $privacySettingsDataProviderFactory,
        array $data = []
    ) {
        parent::__construct($context);
        $this->formKey = $formKey;
        $this->registry = $registry;
        $this->configProvider = $configProvider;
        $this->privacySettingsDataProviderFactory = $privacySettingsDataProviderFactory;
    }

    /**
     * @return array
     */
    public function getPrivacySettings()
    {
        return $this->getAvailableBlocks();
    }

    /**
     * @return array
     */
    private function getPrivacyBlocks()
    {
        $result = [];

        if ($this->configProvider->isModuleEnabled()) {
            if ($this->configProvider->isAllowed(Config::DOWNLOAD)) {
                $result[self::DOWNLOAD_DATA_BLOCK_SHORT_NAME] = [
                    'title' => __('Download Informações Pessoais'),
                    'content' => __(
                        'Aqui você pode baixar uma cópia do seu
                        dados que armazenamos para sua conta em formato CSV.'
                    ),
                    'hasCheckbox' => false,
                    'checkboxText' => '',
                    'hidePassword' => false,
                    'needPassword' => $this->isNeedPassword(),
                    'submitText' => __('Download'),
                    'action' => $this->getUrl('gdpr/customer/downloadCsv'),
                    'actionCode' => Config::DOWNLOAD
                ];
            }

            if ($this->configProvider->isAllowed(Config::ANONYMIZE)) {
                $result[self::ANONYMISE_DATA_BLOCK_SHORT_NAME] = [
                    'title' => __('Anonimizar dados pessoais'),
                    'content' => __(
                        'Tornar seus dados pessoais anônimos significa que eles serão substituídos
                        com informações anônimas não pessoais e antes disso você receberá seu
                        novo login para seu endereço de e-mail (sua senha permanecerá a mesma). Após este processo,
                        seu endereço de e-mail e todos os outros dados pessoais serão removidos do site.'
                    ),
                    'hasCheckbox' => true,
                    'checkboxText' => __('Eu concordo e quero continuar'),
                    'hidePassword' => true,
                    'needPassword' => $this->isNeedPassword(),
                    'submitText' => __('Continuar'),
                    'action' => $this->getUrl('gdpr/customer/anonymise'),
                    'actionCode' => Config::ANONYMIZE
                ];
            }

            if ($this->configProvider->isAllowed(Config::DELETE)) {
                $result[self::DELETE_ACCOUT_BLOCK_SHORT_NAME] = [
                    'title' => __('Apagar Conta'),
                    'content' => __(
                        'Solicite a remoção de sua conta, juntamente com todos os seus dados pessoais,
                        será processado por nossa equipe. <br> A exclusão de sua conta removerá todas as compras
                        histórico, descontos, pedidos, faturas e todas as outras informações que possam estar relacionadas ao seu
                        conta ou suas compras. <br> Todos os seus pedidos e informações semelhantes serão
                        perdido. <br> Você não poderá restaurar o acesso à sua conta após
                        aprovamos sua solicitação de remoção.'
                    ),
                    'checked' => true,
                    'hasCheckbox' => true,
                    'checkboxText' => __('Eu entendo e quero deletar minha conta'),
                    'hidePassword' => true,
                    'needPassword' => $this->isNeedPassword(),
                    'submitText' => __('Enviar Pedido'),
                    'action' => $this->getUrl('gdpr/customer/addDeleteRequest'),
                    'actionCode' => Config::DELETE
                ];
            }
            $privacySettingsProvider = $this->privacySettingsDataProviderFactory->create();

            if ($this->configProvider->isAllowed(Config::CONSENT_OPTING)
                && count($privacySettingsProvider->getData(ConsentLogger::FROM_PRIVACY_SETTINGS)) > 0
            ) {
                $checkboxBlock = $this->getLayout()->createBlock(
                    AccountCheckbox::class,
                    'opting_consents_block',
                    [
                        'dataProvider' => $privacySettingsProvider,
                        'scope' => ConsentLogger::FROM_PRIVACY_SETTINGS
                    ]
                );
                $result[self::CONSENT_OPTING_BLOCK_SHORT_NAME] = [
                    'title' => __('Dado o consentimento'),
                    'content' => __('Aqui você pode optar por receber ou não consentimentos que foram dados anteriormente.'),
                    'hasCheckbox' => false,
                    'checkboxText' => '',
                    'additionalBlock' => $checkboxBlock,
                    'hidePassword' => true,
                    'needPassword' => false,
                    'submitText' => __('Salvar'),
                    'action' => $this->getUrl('gdpr/customer/saveConsentChanges'),
                    'actionCode' => Config::DOWNLOAD
                ];
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    private function getAvailableBlocks()
    {
        $result = [];
        $allBlocks = $this->getPrivacyBlocks();
        $visibleBlocks = $this->getData(self::VISIBLE_BLOCK_LAYOUT_VARIABLE_NAME)
            ? explode(',', $this->getData(self::VISIBLE_BLOCK_LAYOUT_VARIABLE_NAME)) : [];

        if (!$visibleBlocks) {
            return $allBlocks;
        }

        foreach ($visibleBlocks as $blockName) {
            if (array_key_exists($blockName, $allBlocks)) {
                $result[$blockName] = $allBlocks[$blockName];
            }
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function isNeedPassword()
    {
        return $this->getData(self::NEED_PASSWORD_LAYOUT_VARIABLE_NAME);
    }

    /**
     * @return int
     */
    public function isOrder()
    {
        return (int) $this->getData(self::IS_ORDER_LAYOUT_VARIABLE_NAME);
    }

    /**
     * @return string|int
     */
    public function getCurrentOderIncrementId()
    {
        $currentOrder = $this->registry->registry('current_order');

        return $currentOrder ? $currentOrder->getIncrementId() : null;
    }

    /**
     * Retrieve Session Form Key
     *
     * @return string
     */
    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }
}
