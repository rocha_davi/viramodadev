<?php
/**
 * Neoretail E-comm
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2021 Neoretail E-comm. (https://www.neoretail.com)
 *
 * @author Neoretail E-comm <contato@neoretail.com>
 */

namespace Neoretail\CustomerPersonType\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Neoretail\Taxvat\Model\Validate as ValidateTaxvat;

/**
 * Class CustomerSaveBeforeObserver
 * @package Neoretail\CustomerPersonType\Observer
 */
class CustomerSaveBeforeObserver implements ObserverInterface
{
    /**
     * @inheritDoc
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        $customer = $observer->getData('customer');

        if (!ValidateTaxvat::isValidCpfOrCnpj($customer->getTaxvat())) {
            throw new LocalizedException(__('Document Invalid'));
        }

        return;
    }
}
