# Módulo Customer Person Type
Módulo adiciona o atributo person type aos atributos do _customer_ . 
O módulo adiciona também toda a parte de frontend para esse novo atributo.
Caso selecionado Pessoa Jurídica eles exibe outros atributos específicos. 

## Atributos criados
- Company Name (Nome da empresa)
- Exempted Customer (Isento de inscrição)
- Person Type (Tipo de pessoa)
- State Registration (Inscrição estadual)
- Trade Name (Razão Social)

## Alterações que foram feitas nas configurações
- Alteração do número de linhas para o endereço
- Habilita a visibilidade do campo taxvat