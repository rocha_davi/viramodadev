<?php
/**
 * Neoretail E-comm
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2021 Neoretail E-comm. (https://www.neoretail.com)
 *
 * @author Neoretail E-comm <contato@neoretail.com>
 */

namespace Neoretail\CustomerPersonType\Block\Widget;

use Exception;
use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Block\Widget\Taxvat;
use Magento\Customer\Helper\Address;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class PersonType
 * @package Neoretail\CustomerPersonType\Block\Widget
 */
class PersonType extends Taxvat
{
    /**
     * XML path for customer restricition module enabled
     */
    const XML_PATH_PERSON_TYPE_ENABLED = 'neoretail_person_type/general/enabled';

    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * PersonType constructor.
     * @param Context $context
     * @param Address $addressHelper
     * @param CustomerMetadataInterface $customerMetadata
     * @param CustomerRepositoryInterface $customerRepository
     * @param ScopeConfigInterface $scopeConfig
     * @param Session $customerSession
     * @param StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        Context $context,
        Address $addressHelper,
        CustomerMetadataInterface $customerMetadata,
        CustomerRepositoryInterface $customerRepository,
        ScopeConfigInterface $scopeConfig,
        Session $customerSession,
        StoreManagerInterface $storeManager,
        array $data = []
    ) {
        parent::__construct($context, $addressHelper, $customerMetadata, $data);
        $this->customerRepository = $customerRepository;
        $this->customerSession = $customerSession;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('Neoretail_CustomerPersonType::widget/person_type.phtml');
    }

    /**
     * Check if person type is enabled for store
     * @return mixed
     */
    public function isPersonTypeEnabled()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_PERSON_TYPE_ENABLED,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
    }

    /**
     * @return bool
     */
    public function isCompany()
    {
        $personType = 1;
        if ($this->getCustomer()) {
            $personType = $this->getCustomAttribute('person_type');
        }
        return $personType == 2;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->getCustomAttribute('company_name');
    }

    /**
     * @return string
     */
    public function getTradeName()
    {
        return $this->getCustomAttribute('trade_name');
    }

    /**
     * @return bool
     */
    public function isExempted()
    {
        $exempted = $this->getCustomAttribute('exempted');

        return $exempted == 1;
    }

    /**
     * @param $attributeCode
     * @return string
     */
    public function getCustomAttribute($attributeCode)
    {
        try {
            $customer = $this->getCustomer();
            if ($customer) {
                $attr = $customer->getCustomAttribute($attributeCode);
                if ($attr == null) {
                    return '';
                }

                return $attr->getValue();
            }
        } catch (Exception $e) {
            return '';
        }
        return '';
    }

    /**
     * Get current customer from session
     * @return CustomerInterface|null
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomer()
    {
        $customerId = $this->customerSession->getCustomerId();
        if ($customerId) {
            try {
                return $this->customerRepository->getById($customerId);
            } catch (\Exception $exception) {
                return null;
            }
        }
        return null;
    }

    /**
     * @return int
     */
    public function getPersonTypeValue()
    {
        if ($this->isCompany()) {
            return \Neoretail\CustomerPersonType\Model\Customer\Attribute\Source\PersonType::COMPANY;
        }
        return \Neoretail\CustomerPersonType\Model\Customer\Attribute\Source\PersonType::PERSON;
    }
}
