<?php
/**
 * Neoretail E-comm
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2021 Neoretail E-comm. (https://www.neoretail.com)
 *
 * @author Neoretail E-comm <contato@neoretail.com>
 */

namespace Neoretail\CustomerPersonType\Model\Customer\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

/**
 * Class PersonType
 * @package Neoretail\CustomerPersonType\Model\Customer\Attribute\Source
 */
class PersonType extends AbstractSource
{
    public const PERSON = 1;
    public const COMPANY = 2;

    /**
     * @var array
     */
    private $options;

    /**
     * @inheritDoc
     */
    public function getAllOptions()
    {
        if ($this->options === null) {
            $this->options = [
                ['value' => self::PERSON, 'label' => __('Person')],
                ['value' => self::COMPANY, 'label' => __('Company')]
            ];
        }
        return $this->options;
    }
}
