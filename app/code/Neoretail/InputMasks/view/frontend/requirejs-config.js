var config = {
    deps: [
        "Neoretail_InputMasks/js/main",
    ],
    paths: {
        "jquery.mask.neoretail": "Neoretail_InputMasks/js/jquery.mask"
    },
    shim: {
        'jquery.mask.neoretail': {
            'deps': ['jquery']
        }
    }
}
