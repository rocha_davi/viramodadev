define([
    'jquery',
    'jquery.mask.neoretail',
    'jquery/validate',
    'mage/translate'
],
function($) {
    "use strict";

    $(document).ready(function () {
        var SPMaskBehaviorNeoretail = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehaviorNeoretail.apply({}, arguments), options);
            }
        };

        $('input[name=postcode]').mask('00000-000');
        $('input[name=telephone]').mask(SPMaskBehaviorNeoretail, spOptions);
        $('input[name=fax]').mask(SPMaskBehaviorNeoretail, spOptions);
        $('input[name=taxvat]').mask('999.999.999-99');
        $('input[name=vat_id]').mask('999.999.999-99');
        $('input[name=dob]').mask('99/99/9999');
    });
});
