<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\OrderExtensionFactory;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;
use Neoretail\Danfe\Api\DanfeRepositoryInterface;
use Neoretail\Danfe\Model\CustomAttributes;

/**
 * Class SalesOrderLoadAfter
 * @package Neoretail\Danfe\Observer
 */
class SalesOrderLoadAfter implements ObserverInterface
{

    /**
     * @var array
     */
    private $danfeItems;

    /**
     * @var DanfeRepositoryInterface
     */
    private $danfeRepository;

    /**
     * @var OrderExtensionFactory
     */
    private $orderExtensionFactory;

    /**
     * SalesOrderLoadAfter constructor.
     * @param DanfeRepositoryInterface $danfeRepository
     * @param OrderExtensionFactory $orderExtensionFactory
     */
    public function __construct(
        DanfeRepositoryInterface $danfeRepository,
        OrderExtensionFactory $orderExtensionFactory
    ){
        $this->danfeRepository = $danfeRepository;
        $this->orderExtensionFactory = $orderExtensionFactory;
    }

    /**
     * Add custom attributes to order api
     * @param Observer $observer
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getOrder();
        $extensionAttributes = $order->getExtensionAttributes();
        $orderExtension = $extensionAttributes ? $extensionAttributes : $this->orderExtensionFactory->create();
        $orderExtension->setData(CustomAttributes::DANFE_RESEND_COUNT, $order->getData(CustomAttributes::DANFE_RESEND_COUNT));
        $orderExtension->setData(CustomAttributes::DANFE_RECEIVED_COUNT, $order->getData(CustomAttributes::DANFE_RECEIVED_COUNT));
        $orderExtension->setDanfes($this->getDanfes($order));
        $order->setExtensionAttributes($extensionAttributes);

    }

    /**
     * Get danfes from order id
     * @param OrderInterface $order
     * @return array|\Magento\Framework\Api\ExtensibleDataInterface[]|\Neoretail\Danfe\Api\Data\DanfeInterface[]
     * @throws LocalizedException
     */
    private function getDanfes(OrderInterface $order)
    {
        if ($this->danfeItems === null) {
            $this->danfeItems = $this->danfeRepository->getDanfesByOrderId($order->getEntityId());
            if (!$this->danfeItems) {
                return [];
            }
        }
        return $this->danfeItems;
    }
}