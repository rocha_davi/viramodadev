<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future If you wish to customize Magento for your
 * needs please refer to https://neoretail.com.br for more information
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento (https://neoretail.com.br)
 *
 * @author Neoretail Core Team <thiago.covre@neocantra.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Controller\Order;

use Magento\Sales\Controller\OrderInterface;

/**
 * Class Danfe
 * @package Neoretail\Danfe\Controller\Order
 */
class Danfe extends \Magento\Sales\Controller\AbstractController\Invoice implements OrderInterface
{
    
}