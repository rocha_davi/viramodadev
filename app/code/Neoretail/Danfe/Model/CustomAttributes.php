<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Model;

/**
 * Class CustomAttributes
 * @package Neoretail\Danfe\Model
 */
class CustomAttributes
{
    const DANFE_RESEND_COUNT = 'danfe_resend_count';
    const DANFE_RECEIVED_COUNT = 'danfe_received_count';
    const DANFES = 'danfes';

}