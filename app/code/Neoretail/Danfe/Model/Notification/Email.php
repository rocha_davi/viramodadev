<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Model\Notification;

use Magento\Framework\Escaper;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Store\Api\Data\StoreConfigInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Stdlib\DateTime\DateTimeFormatter;
use Magento\Framework\Stdlib\DateTime\Timezone;
use Magento\Framework\Translate\Inline\StateInterface as InlineTranslator;
use Magento\Sales\Model\OrderRepository;
use Magento\Store\Model\StoreManagerInterface;
use Neoretail\Danfe\Api\DanfeRepositoryInterface;
use Neoretail\Danfe\Model\Logger\Logger;
use Neoretail\Danfe\Helper\Data as Helper;

/**
 * Class Email
 * @package Neoretail\Danfe\Model\Notification
 */
class Email
{

    const BCC_COPY_METHOD = 'bcc';
    const XML_PATH_EMAIL_CONFIG_IS_SEND_ENABLED = 'neoretail_danfe/email_config/enabled';
    const XML_PATH_EMAIL_CONFIG_SENDER = 'neoretail_danfe/email_config/sender';
    const XML_PATH_EMAIL_CONFIG_EMAIL_SUBJECT = 'neoretail_danfe/email_config/subject';
    const XML_PATH_EMAIL_CONFIG_EMAIL_TEMPLATE = 'neoretail_danfe/email_config/template';
    const XML_PATH_EMAIL_CONFIG_COPY_TO = 'neoretail_danfe/email_config/copy_to';
    const XML_PATH_EMAIL_CONFIG_COPY_METHOD = 'neoretail_danfe/email_config/copy_method';

    /**
     * @var DanfeRepositoryInterface
     */
    private $danfeRepository;

    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * @var DateTimeFormatter
     */
    private $datetimeFormatter;

    /**
     * @var Escaper
     */
    private $escaper;

    /**
     * @var Helper
     */
    private $helper;

    /**
     * @var InlineTranslator
     */
    private $inlineTranslation;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var StoreConfigInterface
     */
    private $storeConfig;

    /**
     * @var Timezone
     */
    private $timezone;

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;


    public function __construct(
        DanfeRepositoryInterface $danfeRepository,
        Datetime $dateTime,
        DateTimeFormatter $dateTimeFormatter,
        Escaper $escaper,
        Helper $helper,
        InlineTranslator $inlineTranslation,
        Logger $logger,
        OrderRepository $orderRepository,
        StoreConfigInterface $storeConfig,
        StoreManagerInterface $storeManager,
        Timezone $timezone,
        TransportBuilder $transportBuilder
    ) {
        $this->danfeRepository = $danfeRepository;
        $this->dateTime = $dateTime;
        $this->datetimeFormatter = $dateTimeFormatter;
        $this->escaper = $escaper;
        $this->helper = $helper;
        $this->inlineTranslation = $inlineTranslation;
        $this->logger = $logger;
        $this->orderRepository = $orderRepository;
        $this->storeConfig = $storeConfig;
        $this->storeManager = $storeManager;
        $this->timezone = $timezone;
        $this->transportBuilder = $transportBuilder;
    }


    /**
     * @param $danfeId
     * @return bool
     * @throws MailException
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function sendMail($danfeId)
    {
        $return = false;
        if (!$this->isSendEnabled()) {
            return $return;
        }

        /** @var \Neoretail\Danfe\Api\Data\DanfeInterface $danfe */
        $danfe = $this->danfeRepository->getById($danfeId);

        /** @var \Magento\Sales\Api\Data\OrderInterface $order */
        $order = $this->orderRepository->get($danfe->getParentId());

        // Disable inline translator
        $this->inlineTranslation->suspend();

        $sender = $this->escaper->escapeHtml($this->getEmailSender());
        $copyTo = $this->getEmailCopyTo();
        if ($copyTo) {
            $copyTo = explode(',', $copyTo);
        }

        if ($copyTo && $this->getEmailCopyMethod() == self::BCC_COPY_METHOD) {
            foreach ($copyTo as $email) {
                $this->transportBuilder->addBcc($email);
            }
        }

        $customerName = $order->getCustomerFirstname(). ' '.$order->getCustomerLastname();

        $customerEmail = $order->getCustomerEmail();

        $emailSubject = $this->getEmailSubject();

        $this->transportBuilder->setTemplateIdentifier(
            $this->getEmailTemplate()
        )->setTemplateOptions(
            [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $this->storeManager->getStore()->getId(),
            ]
        )->setFromByScope(
            $sender, $order->getStoreId()
        )->setTemplateVars(
            [
                'customer_name' => $customerName,
                'email' => $customerEmail,
                'email_subject' => $emailSubject,
                'danfe_link' => $danfe->getBinary(),
                'danfe_key' => $danfe->getKey(),
                'order' => $order,
                'store' => $order->getStore()
            ]
        )->addTo(
            $customerEmail,
            $customerName
        );
        $transport = $this->transportBuilder->getTransport();

        try {
            $transport->sendMessage();
            $this->inlineTranslation->resume();
            $this->logger->log('Email sent');
            // Send Email Times
            $return = true;
        } catch (MailException $exception) {
            $this->logger->log('Email NOT sent to');
            $this->setErrorMessage($exception->getMessage());
            $this->setSendStatus(false);
        }
        return $return;
    }

    /**
     * Get copy method
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getEmailCopyMethod()
    {
        return (string) $this->helper->getConfig(self::XML_PATH_EMAIL_CONFIG_COPY_METHOD);
    }

    /**
     * Get list of emails who will be copied separated by comma
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getEmailCopyTo()
    {
        return $this->helper->getConfig(self::XML_PATH_EMAIL_CONFIG_COPY_TO);
    }

    /**
     * Get email sender
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getEmailSender()
    {
        return (string) $this->helper->getConfig(self::XML_PATH_EMAIL_CONFIG_SENDER);
    }

    /**
     * Email subject
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getEmailSubject()
    {
        return (string) $this->helper->getConfig(self::XML_PATH_EMAIL_CONFIG_EMAIL_SUBJECT);
    }

    /**
     * Get selected template
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function getEmailTemplate()
    {
        return (string) $this->helper->getConfig(self::XML_PATH_EMAIL_CONFIG_EMAIL_TEMPLATE);
    }

    /**
     * Is sending email enabled
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function isSendEnabled()
    {
        return (bool) $this->helper->getConfig(self::XML_PATH_EMAIL_CONFIG_IS_SEND_ENABLED);
    }

}
