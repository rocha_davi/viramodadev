<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Model;

use Magento\Framework\Model\AbstractModel;
use Neoretail\Danfe\Api\Data\DanfeTypeExtensionAttributes;
use Neoretail\Danfe\Api\Data\DanfeTypeInterface;
use Neoretail\Danfe\Model\ResourceModel\DanfeType as DanfeTypeResource;

/**
 * Class Danfe
 * @package Neoretail\Danfe\Model
 */
class DanfeType extends AbstractModel implements DanfeTypeInterface
{

    /**
     *
     */
    protected function _construct()
    {
        $this->_init(DanfeTypeResource::class);
    }

    /**
     * @return int|mixed|null
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * @return int|mixed
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }

    /**
     * @param $type
     * @return string|void
     */
    public function setType($type)
    {
        $this->setData(self::TYPE, $type);
    }
}