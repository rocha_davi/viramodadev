<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 * @author Maico da Silva <silva.maico@neoretail.com.br>
 */

namespace Neoretail\Danfe\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Neoretail\Danfe\Api\Data\DanfeTypeInterface;

/**
 * Class DanfeType
 * @package Neoretail\Danfe\Model\ResourceModel
 */
class DanfeType extends AbstractDb
{
    /**
     * Initialize resource model
     * @return void
     */
    protected function _construct()
    {
        $this->_init('neoretail_sales_order_danfe_type', DanfeTypeInterface::ENTITY_ID);
    }
}
