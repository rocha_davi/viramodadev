<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 * @author Maico da Silva <silva.maico@neoretail.com.br>
 */

namespace Neoretail\Danfe\Model\ResourceModel\DanfeType;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Neoretail\Danfe\Model\DanfeType;
use Neoretail\Danfe\Model\ResourceModel\DanfeType as DafeTypeResource;

/**
 * Class Collection
 * @package Neoretail\Danfe\Model\ResourceModel\DanfeType
 */
class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            DanfeType::class,
            DafeTypeResource::class
        );
    }
}
