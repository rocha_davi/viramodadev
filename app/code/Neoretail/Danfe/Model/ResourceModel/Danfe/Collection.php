<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 * @author Maico da Silva <silva.maico@neoretail.com.br>
 */

namespace Neoretail\Danfe\Model\ResourceModel\Danfe;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Neoretail\Danfe\Model\Danfe;
use Neoretail\Danfe\Model\ResourceModel\Danfe as DanfeResource;

/**
 * Class Collection
 * @package Neoretail\Danfe\Model
 */
class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            Danfe::class,
            DanfeResource::class
        );
    }


    public function _initSelect()
    {
        $this->getSelect()
            ->from(['main_table' => $this->getMainTable()])
            ->join('neoretail_sales_order_danfe_type',
                'main_table.danfe_type_id = neoretail_sales_order_danfe_type.entity_id',
                [
                    'type',
                ]);
        return $this;

    }
}
