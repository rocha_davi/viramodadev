<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 * @author Maico da Silva <silva.maico@neoretail.com.br>
 */

namespace Neoretail\Danfe\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Neoretail\Danfe\Api\Data\DanfeInterface;

/**
 * Class Danfe
 * @package Neoretail\Danfe\Model\ResourceModel
 */
class Danfe extends AbstractDb
{
    /**
     * Initialize resource model
     * @return void
     */
    protected function _construct()
    {
        $this->_init('neoretail_sales_order_danfe', DanfeInterface::ENTITY_ID);
    }

    /**
     * @param $orderId
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getIdByOrderId($orderId)
    {
        $connection = $this->getConnection();

        $select = $connection->select()
            ->from($this->getMainTable(), DanfeInterface::ENTITY_ID)
            ->where(DanfeInterface::PARENT_ID .' = :orderId');

        $bind = [
            ':orderId' => $orderId,
        ];

        return $connection->fetchOne($select, $bind);
    }

    /**
     * @param $orderId
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getIdsByOrderId($orderId)
    {
        $connection = $this->getConnection();

        $select = $connection->select()
            ->from($this->getMainTable(), DanfeInterface::ENTITY_ID)
            ->where(DanfeInterface::PARENT_ID .' = :orderId');

        $bind = [
            ':orderId' => $orderId,
        ];
        $result = $connection->fetchAll($select, $bind);
        $ids = [];
        foreach ($result as $value) {
            $ids[] = $value['entity_id'];
        }
        return $ids;
    }

    /**
     * @param int $typeId
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function filterByTypeId($typeId)
    {
        $connection = $this->getConnection();

        $select = $connection->select()
            ->from($this->getMainTable(), DanfeInterface::ENTITY_ID)
            ->where(DanfeInterface::TYPE .' = :typeId');

        $bind = [
            ':danfe_type_id' => $typeId,
        ];
        $result = $connection->fetchAll($select, $bind);
        $ids = [];
        foreach ($result as $value) {
            $ids[] = $value['entity_id'];
        }
        return $ids;
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return \Magento\Framework\DB\Select
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $field = $this->getConnection()->quoteIdentifier(sprintf('%s.%s', $this->getMainTable(), $field));
        $select = $this->getConnection()
            ->select()
            ->from($this->getMainTable())
            ->where($field . '=?', $value)
            ->join('neoretail_sales_order_danfe_type',
                'neoretail_sales_order_danfe.danfe_type_id = neoretail_sales_order_danfe_type.entity_id',
                [
                    'type',
                ]);
        return $select;
    }
}
