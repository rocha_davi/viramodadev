<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface as CollectionProcessor;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory as SearchResultsFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Neoretail\Danfe\Api\DanfeTypeRepositoryInterface;
use Neoretail\Danfe\Api\Data\DanfeTypeInterface;
use Neoretail\Danfe\Model\ResourceModel\DanfeType as DanfeTypeResource;
use Neoretail\Danfe\Model\ResourceModel\DanfeType\CollectionFactory as DanfeTypeCollectionFactory;

/**
 * Class DanfeTypeRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class DanfeTypeRepository implements DanfeTypeRepositoryInterface
{
    /**
     * @var CollectionProcessor
     */
    private $collectionProcessor;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    private $dataObjectProcessor;

    /**
     * @var DanfeTypeResource
     */
    private $resource;

    /**
     * @var SearchResultsFactory
     */
    private $searchResultsFactory;

    /**
     * @var DanfeTypeFactory
     */
    private $danfeTypeFactory;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var DanfeTypeCollectionFactory
     */
    private $danfeCollectionFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @param DanfeTypeResource $resource
     * @param DanfeTypeFactory $danfeTypeFactory
     * @param DanfeTypeCollectionFactory $DanfeTypeCollectionFactory
     * @param SearchResultsFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param CollectionProcessor $collectionProcessor
     */
    public function __construct(
        DanfeTypeResource $resource,
        DanfeTypeFactory $danfeTypeFactory,
        DanfeTypeCollectionFactory $danfeCollectionFactory,
        FilterBuilder $filterBuilder,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        CollectionProcessor $collectionProcessor,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SearchResultsFactory $searchResultsFactory
    )
    {
        $this->resource = $resource;
        $this->danfeTypeFactory = $danfeTypeFactory;
        $this->danfeCollectionFactory = $danfeCollectionFactory;
        $this->filterBuilder = $filterBuilder;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function get($danfetypeId)
    {
        $danfeType = $this->danfeTypeFactory->create();
        $this->resource->load($danfeType, $danfetypeId);
        if (!$danfeType->getId()) {
            throw new NoSuchEntityException(
                __('The danfe type with the "%1" ID doesn\'t exist.', $danfetypeId)
            );
        }
        return $danfeType;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(DanfeTypeInterface $entity)
    {
        try {
            $this->resource->delete($entity);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($danfetypeId)
    {
        return $this->delete($this->getById($danfetypeId));
    }

    /**
     * {@inheritdoc}
     */
    public function save(DanfeTypeInterface $danfeType)
    {
        try {
            $this->resource->save($danfeType);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $danfeType;
    }

    /**
     * {@inheritDoc}
     */
    public function getTypes()
    {
        $collection = $this->danfeCollectionFactory->create();
        $result = [];
        if ($collection->getSize()>0) {
            foreach ($collection->getItems() as $item) {
                $result[] = $item;
            }
        }
        return $result;
    }
}
