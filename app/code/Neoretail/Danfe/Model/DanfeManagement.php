<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Sales\Model\OrderRepository;
use Neoretail\Danfe\Api\DanfeManagementInterface;
use Neoretail\Danfe\Api\DanfeRepositoryInterface;
use Neoretail\Danfe\Model\Logger\Logger;

/**
 * Class DanfeManagement
 * @package Neoretail\Danfe\Model
 */
class DanfeManagement implements DanfeManagementInterface
{

    /**
     * @var \Neoretail\Danfe\Model\DanfeFactory
     */
    private $danfeFactory;

    /**
     * @var array
     */
    private $danfeItems;


    /**
     * @var DanfeRepositoryInterface
     */
    private $danfeRepository;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var OrderRepository
     */
    private $orderRepository;


    /**
     * DanfeManagement constructor.
     * @param \Neoretail\Danfe\Model\DanfeFactory $danfeFactory
     * @param DanfeRepositoryInterface $danfeRepository
     * @param Logger $logger
     * @param OrderRepository $orderRepository
     */
    public function __construct(
        DanfeFactory $danfeFactory,
        DanfeRepositoryInterface $danfeRepository,
        Logger $logger,
        OrderRepository $orderRepository
    )
    {
        $this->danfeFactory = $danfeFactory;
        $this->danfeRepository = $danfeRepository;
        $this->logger = $logger;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param $orderId
     * @return mixed|void
     * @throws InputException
     */
    public function getDanfes($orderId)
    {
        try {
            $order = $this->orderRepository->get($orderId);
            return $order->getExtensionAttributes()->getDanfes();
        } catch (NoSuchEntityException $noSuchEntityException) {
            $this->logger->error($noSuchEntityException->getMessage());
        }
        return [];
    }

    /**
     * Get danfe by danfe Id
     * @param int $danfeId
     * @return \Neoretail\Danfe\Api\Data\DanfeInterface
     */
    public function getDanfe($danfeId)
    {
        try {
            return $this->danfeRepository->getById($danfeId);
        } catch (NotFoundException $exception) {
            $this->logger->addError($exception->getMessage());
        }
        return null;
    }


    /**
     * @param int $orderId
     * @param string $key
     * @param string $description
     * @param int $typeId
     * @param bool $isResend
     * @param string $binaryDanfe
     * @param string $downloadLink
     * @return mixed
     */
    public function saveDanfe($orderId, $key, $description, $typeId, $isResend = false, $binaryDanfe = '', $downloadLink = '')
    {
        try {
            /** @var Danfe $danfe */
            $danfe = $this->danfeFactory->create();
            $danfe->setParentId($orderId);
            $danfe->setKey($key);
            $danfe->setDescription($description);
            $danfe->setIsResend($isResend);
            $danfe->setBinary($binaryDanfe);
            $danfe->setDownloadLink($downloadLink);
            if ($downloadLink) {
                $danfe->setIsFileGenerated(true);
            }
            $danfe->setTypeId($typeId);
            $this->danfeRepository->save($danfe);
            return $danfe->getId();
        } catch (CouldNotSaveException $couldNotSaveException) {
            $this->logger->log($couldNotSaveException->getMessage());
        }
    }

    public function getDanfesByOrderId($orderId)
    {
        if ($this->danfeItems === null) {
            $this->danfeItems = $this->danfeRepository->getDanfesByOrderId($orderId);
        }
        return $this->danfeItems;
    }
}