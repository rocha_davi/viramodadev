<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface as CollectionProcessor;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory as SearchResultsFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Neoretail\Danfe\Api\DanfeRepositoryInterface;
use Neoretail\Danfe\Api\Data\DanfeInterface as Danfe;
use Neoretail\Danfe\Api\Data\DanfeInterfaceFactory as DanfeFactory;
use Neoretail\Danfe\Model\ResourceModel\Danfe as DanfeResource;
use Neoretail\Danfe\Model\ResourceModel\Danfe\CollectionFactory as DanfeCollectionFactory;

/**
 * Class DanfeRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class DanfeRepository implements DanfeRepositoryInterface
{
    /**
     * @var CollectionProcessor
     */
    private $collectionProcessor;

    /**
     * @var DanfeFactory
     */
    private $danfeFactory;

    /**
     * @var DanfeCollectionFactory
     */
    private $danfeCollectionFactory;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    private $dataObjectProcessor;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var DanfeResource
     */
    private $resource;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SearchResultsFactory
     */
    private $searchResultsFactory;

    /**
     * DanfeRepository constructor.
     * @param CollectionProcessor $collectionProcessor
     * @param DanfeCollectionFactory $danfeCollectionFactory
     * @param DanfeFactory $danfeFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param FilterBuilder $filterBuilder
     * @param DanfeResource $resource
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SearchResultsFactory $searchResultsFactory
     */
    public function __construct(
        CollectionProcessor $collectionProcessor,
        DanfeCollectionFactory $danfeCollectionFactory,
        DanfeFactory $danfeFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        FilterBuilder $filterBuilder,
        DanfeResource $resource,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SearchResultsFactory $searchResultsFactory
    )
    {
        $this->collectionProcessor = $collectionProcessor;
        $this->danfeCollectionFactory = $danfeCollectionFactory;
        $this->danfeFactory = $danfeFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->filterBuilder = $filterBuilder;
        $this->resource = $resource;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * Save danfe
     * @param Danfe $danfe
     * @return Danfe
     * @throws CouldNotSaveException
     */
    public function save(Danfe $danfe)
    {
        try {
            $this->resource->save($danfe);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $danfe;
    }

    /**
     * @param int $danfeId
     * @return Danfe
     * @throws NoSuchEntityException
     */
    public function getById($danfeId)
    {
        $danfe = $this->danfeFactory->create();
        $this->resource->load($danfe, $danfeId);
        if (!$danfe->getId()) {
            throw new NoSuchEntityException(
                __('The danfe with the "%1" ID doesn\'t exist.', $danfeId)
            );
        }
        return $danfe;
    }

    /**
     * @param SearchCriteriaInterface $criteria
     * @return SearchResultsInterface|\Neoretail\Danfe\Api\Data\DanfeSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        $collection = $this->danfeCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var SearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults
            ->setSearchCriteria($criteria)
            ->setItems($collection->getItems())
            ->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * @param Danfe $danfe
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Danfe $danfe)
    {
        try {
            $this->resource->delete($danfe);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param $danfeId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($danfeId)
    {
        return $this->delete($this->getById($danfeId));
    }

    /**
     * Get Danfes by Order Id
     * @param int $orderId
     * @return array|\Magento\Framework\Api\ExtensibleDataInterface[]|Danfe[]
     * @throws LocalizedException
     */
    public function getDanfesByOrderId($orderId)
    {
        $danfesIds = $this->resource->getIdsByOrderId($orderId);

        if (!$danfesIds) {
            return [];
        }

        $filter = $this->filterBuilder->create();
        $filter->setConditionType('in')
            ->setField('main_table.entity_id')
            ->setValue($danfesIds);

        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter($filter)
            ->create();

        $danfes = $this->getList($searchCriteria);

        return $danfes->getItems();
    }
}
