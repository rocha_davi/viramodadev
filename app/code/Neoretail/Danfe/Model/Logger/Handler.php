<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Model\Logger;

use Magento\Framework\Filesystem\DriverInterface;
use Magento\Framework\Logger\Handler\Base;
use Monolog\Logger;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Handler
 * @package Neoretail\Danfe\Model\Logger
 */
class Handler extends Base
{
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * File name
     * @var string
     */
    protected $fileName = '';

    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::DEBUG;

    const LOG_DIRECTORY_NAME    = '/var/log/';
    const LOG_FILENAME = 'neoretail_danfe.log';

    /**
     * Handler constructor.
     * @param \Magento\Framework\Filesystem\DriverInterface $filesystem
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param null $filePath
     * @throws \Exception
     */
    public function __construct(DriverInterface $filesystem, ScopeConfigInterface $scopeConfig, $filePath = null)
    {
        $this->_scopeConfig = $scopeConfig;
        $this->fileName = $this->_getFilePath() . $this->_getFilename();
        parent::__construct($filesystem, $filePath);
    }

    /**
     * @return string
     */
    protected function _getFilename()
    {
        return self::LOG_FILENAME;
    }

    /**
     * @return string
     */
    protected function _getFilePath()
    {
        return self::LOG_DIRECTORY_NAME;
    }
}
