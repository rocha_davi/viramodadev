<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Model\Logger;

use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Logger
 * @package Neoretail\Danfe\Model\Logger
 */
class Logger extends \Monolog\Logger
{
    const XML_PATH_LOG_ENABLED = 'neoretail_danfe/logging/enabled';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    public function __construct(
        $name,
        ScopeConfigInterface $scopeConfig,
        array $handlers = array(),
        array $processors = array()
    ) {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($name, $handlers, $processors);
    }


    /**
     * @return bool
     */
    public function isLoggingEnabled()
    {
        return (bool) $this->scopeConfig->getValue(self::XML_PATH_LOG_ENABLED);
    }

    /**
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return bool|void
     */
    public function log($message, $level = Logger::DEBUG, array $context = array())
    {
        if (!$this->isLoggingEnabled()) {
            return;
        }
        return parent::log($level, $message, $context);
    }
}