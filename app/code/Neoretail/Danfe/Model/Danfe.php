<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Model;

use Magento\Framework\Model\AbstractModel;
use Neoretail\Danfe\Api\Data\DanfeExtensionAttributes;
use Neoretail\Danfe\Api\Data\DanfeInterface;
use Neoretail\Danfe\Model\ResourceModel\Danfe as DanfeResource;

/**
 * Class Danfe
 * @package Neoretail\Danfe\Model
 */
class Danfe extends AbstractModel implements DanfeInterface
{

    /**
     *
     */
    protected function _construct()
    {
        $this->_init(DanfeResource::class);
    }

    /**
     * @return mixed|string|null
     */
    public function getBinary()
    {
        return $this->getData(self::BINARY);
    }

    /**
     * @return mixed|string|null
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @return mixed|string
     */
    public function getDescription()
    {
        return $this->getData(self::DESCRIPTION);
    }


    /**
     * @return int|mixed
     */
    public function getDownloadLink()
    {
        return $this->getData(self::DOWNLOAD_LINK);
    }

    /**
     * @return int|mixed
     */
    public function getEmailSentTimes()
    {
        return $this->getData(self::EMAIL_SENT_TIMES);
    }

    /**
     * @return int|mixed|null
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * @return bool|mixed
     */
    public function getIsFileGenerated()
    {
        return $this->getData(self::IS_FILE_GENERATED);
    }

    /**
     * @return bool|mixed
     */
    public function getIsResend()
    {
        return $this->getData(self::IS_RESEND);
    }

    /**
     * @return mixed|string|null
     */
    public function getKey()
    {
        return $this->getData(self::KEY);
    }

    /**
     * @return mixed|string|null
     */
    public function getParentId()
    {
        return $this->getData(self::PARENT_ID);
    }

    /**
     * @return mixed|string|null
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * @return mixed
     */
    public function getTypeId()
    {
        return $this->getData(self::TYPE);
    }


    /**
     * @param string $content
     * @return DanfeInterface|void
     */
    public function setBinary($content)
    {
        $this->setData(self::BINARY, $content);
    }

    /**
     * @param string $createdAt
     * @return DanfeInterface|void
     */
    public function setCreatedAt($createdAt)
    {
        $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * @param $description
     * @return string|void
     */
    public function setDescription($description)
    {
        $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @param $downloadLink
     * @return string|void
     */
    public function setDownloadLink($downloadLink)
    {
        $this->setData(self::DOWNLOAD_LINK, $downloadLink);
    }

    /**
     * @param int $times
     * @return DanfeInterface|void
     */
    public function setEmailSentTimes($times)
    {
        $this->setData(self::EMAIL_SENT_TIMES, $times);
    }

    /**
     * @param $isFileGenerated
     * @return bool|void
     */
    public function setIsFileGenerated($isFileGenerated)
    {
        $this->setData(self::IS_FILE_GENERATED, $isFileGenerated);
    }

    /**
     * @param $isResend
     * @return bool|void
     */
    public function setIsResend($isResend)
    {
        $this->setData(self::IS_RESEND, $isResend);
    }

    /**
     * @param string $key
     * @return DanfeInterface|void
     */
    public function setKey($key)
    {
        $this->setData(self::KEY, $key);
    }

    /**
     * @param int $parentId
     * @return DanfeInterface|void
     */
    public function setParentId($parentId)
    {
        $this->setData(self::PARENT_ID, $parentId);
    }

    /**
     * @param $typeId
     * @return int|void
     */
    public function setTypeId($typeId)
    {
        $this->setData(self::TYPE, $typeId);
    }


    /**
     * @param string $updatedAt
     * @return DanfeInterface|void
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->setData(self::UPDATED_AT, $updatedAt);
    }

    public function isEmailSent()
    {
        if ($this->getEmailSentTimes() > 0) {
            return true;
        }
        return false;
    }


    /**
     * @return mixed|\Neoretail\Danfe\Api\Data\DanfeExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->getData(self::EXTENSION_ATTRIBUTES_KEY);
    }

    /**
     * @param DanfeExtensionAttributes $extensionAttributes
     * @return DanfeInterface|void
     */
    public function setExtensionAttributes(DanfeExtensionAttributes $extensionAttributes)
    {
        $this->setData(self::EXTENSION_ATTRIBUTES_KEY, $extensionAttributes);
    }

}