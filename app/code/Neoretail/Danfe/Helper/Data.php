<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future If you wish to customize Magento for your
 * needs please refer to https://neoretail.com.br for more information
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento (https://neoretail.com.br)
 *
 * @author Neoretail Core Team <thiago.covre@neocantra.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManager;

/**
 * Class Data
 * @package Neoretail\Danfe\Helper
 */
class Data
{
    const XML_PATH_IS_MODULE_ENABLED = 'neoretail_danfe/settings/enabled';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var StoreManager
     */
    private $storeManager;

    /**
     * Data constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManager $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManager $storeManager
    ){
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function isEnabled()
    {
        return (bool) $this->getConfig(self::XML_PATH_IS_MODULE_ENABLED);
    }

    /**
     * Get scope config for xml path
     * @param $xmlPath
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getConfig($xmlPath)
    {
        return $this->scopeConfig->getValue($xmlPath,
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore());
    }

}