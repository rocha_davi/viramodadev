<?php

namespace Neoretail\Danfe\Block\Order;

/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Api\OrderRepositoryInterface;
use Neoretail\Danfe\Api\DanfeRepositoryInterface;
use Magento\Sales\Block\Items\AbstractItems;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Http\Context as HttpContext;

/**
 * Sales order view block
 *
 * @api
 * @since 100.0.2
 */
class Danfe extends AbstractItems
{

    /**
     * Core registry
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;
    
    /**
     * @var string
     */
    protected $_template = 'Neoretail_Danfe::order/danfe.phtml';

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $httpContext;

    /**
     * Danfe constructor.
     * @param Context $context
     * @param HttpContext $httpContext
     * @param Registry $registry
     * @param OrderRepositoryInterface $orderRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        HttpContext $httpContext,
        Registry $registry,
        OrderRepositoryInterface $orderRepository,
        array $data = [])
    {
        $this->_coreRegistry = $registry;
        $this->orderRepository = $orderRepository;
        $this->httpContext = $httpContext;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(__('Order # %1', $this->getOrder()->getRealOrderId()));
    }

    /**
     * Retrieve current order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->_coreRegistry->registry('current_order');
    }

    /**
     * @return \Neoretail\Danfe\Api\Data\DanfeInterface[]|null
     */
    public function getDanfes()
    {
        return $this->getOrder()->getExtensionAttributes()->getDanfes();
    }
}
