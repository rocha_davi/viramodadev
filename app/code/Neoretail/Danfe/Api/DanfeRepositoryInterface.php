<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Api;

/**
 * Interface DanfeRepositoryInterface
 * @package Neoretail\Danfe\Api
 */
interface DanfeRepositoryInterface
{
    /**
     * Lists Danfes that match specified search criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria The search criteria.
     * @return \Neoretail\Danfe\Api\Data\DanfeSearchResultInterface Danfe search results interface.
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Loads a specified Danfe.
     *
     * @param int $id The Danfe ID.
     * @return \Neoretail\Danfe\Api\Data\DanfeInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function getById($id);

    /**
     * Get danfes by order id
     * @param $orderId
     * @return array
     */
    public function getDanfesByOrderId($orderId);

    /**
     * Deletes a specified Danfe.
     *
     * @param \Neoretail\Danfe\Api\Data\DanfeInterface $entity The Danfe.
     * @return bool
     */
    public function delete(\Neoretail\Danfe\Api\Data\DanfeInterface $entity);

    /**
     * Performs persist operations for a specified Danfe.
     *
     * @param \Neoretail\Danfe\Api\Data\DanfeInterface $entity The Danfe.
     * @return \Neoretail\Danfe\Api\Data\DanfeInterface Danfe interface.
     */
    public function save(\Neoretail\Danfe\Api\Data\DanfeInterface $entity);

}
