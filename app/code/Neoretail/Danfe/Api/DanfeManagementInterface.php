<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Api;

use Neoretail\Danfe\Api\Data\DanfeInterface;

/**
 * Interface DanfeInterface
 * @package Neoretail\Danfe\Api
 */
interface DanfeManagementInterface
{

    /**
     * @param int $danfeId
     * @return \Neoretail\Danfe\Api\Data\DanfeInterface
     */
    public function getDanfe($danfeId);

    /**
     * @param int $orderId
     * @return \Neoretail\Danfe\Api\Data\DanfeInterface[]
     */
    public function getDanfes($orderId);

    /**
     * @param int $orderId
     * @param string $key
     * @param string $description
     * @param int $typeId
     * @param bool $isResend
     * @param string $binaryDanfe
     * @param string $downloadLink
     * @return mixed
     */
    public function saveDanfe($orderId, $key, $description, $typeId, $isResend = false, $binaryDanfe = '', $downloadLink = '');

}