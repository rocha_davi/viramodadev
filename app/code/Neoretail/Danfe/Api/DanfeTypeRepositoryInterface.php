<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Api;

/**
 * Interface DanfeTypeRepositoryInterface
 * @package Neoretail\Danfe\Api
 */
interface DanfeTypeRepositoryInterface
{
    /**
     * Deletes a specified DanfeType.
     *
     * @param \Neoretail\Danfe\Api\Data\DanfeTypeInterface $entity The DanfeType.
     * @return bool
     */
    public function delete(\Neoretail\Danfe\Api\Data\DanfeTypeInterface $entity);

    /**
     * Loads a specified DanfeType.
     *
     * @param int $id The DanfeType ID.
     * @return \Neoretail\Danfe\Api\Data\DanfeTypeInterface
     */
    public function get($id);

    /**
     * Lists All DanfeTypes
     * @return \Neoretail\Danfe\Api\Data\DanfeTypeInterface[] DanfeType search results interface.
     */
    public function getTypes();

    /**
     * Performs persist operations for a specified DanfeType.
     *
     * @param \Neoretail\Danfe\Api\Data\DanfeTypeInterface $entity The DanfeType.
     * @return \Neoretail\Danfe\Api\Data\DanfeTypeInterface DanfeType interface.
     */
    public function save(\Neoretail\Danfe\Api\Data\DanfeTypeInterface $entity);

}
