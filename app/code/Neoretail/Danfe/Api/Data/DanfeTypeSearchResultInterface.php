<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface DanfeSearchResultInterface
 * @package Magento\Sales\Api\Data
 */
interface DanfeTypeSearchResultInterface extends SearchResultsInterface
{
    /**
     * Gets collection items.
     *
     * @return \Neoretail\Danfe\Api\Data\DanfeTypeInterface[] Array of collection items.
     */
    public function getItems();

    /**
     * Set collection items.
     *
     * @param \Neoretail\Danfe\Api\Data\DanfeTypeInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
