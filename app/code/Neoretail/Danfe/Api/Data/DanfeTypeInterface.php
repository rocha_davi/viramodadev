<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future If you wish to customize Magento for your
 * needs please refer to https://neoretail.com.br for more information
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento (https://neoretail.com.br)
 *
 * @author Neoretail Core Team <thiago.covre@neocantra.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Api\Data;

/**
 * Interface DanfeTypeInterface
 * @package Neoretail\Danfe\Api\Data
 */
interface DanfeTypeInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    /**#@+
     * Constants for keys of data array Identical to the name of the getter in snake case
     */
    /*
     * Entity ID
     */
    const ENTITY_ID = 'entity_id';
    /*
     * Type
     */
    const TYPE = 'type';

    /**
     * Gets type
     *
     * @return string|null Danfe type
     */
    public function getType();

    /**
     * Gets the ID for the Danfe
     *
     * @return int|null Order ID
     */
    public function getEntityId();

    /**
     * Sets entity ID
     *
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * Sets danfe type
     * @param $type
     * @return string
     */
    public function setType($type);

}
