<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future If you wish to customize Magento for your
 * needs please refer to https://neoretail.com.br for more information
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento (https://neoretail.com.br)
 *
 * @author Neoretail Core Team <thiago.covre@neocantra.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Api\Data;

/**
 * Interface DanfeInterface
 * @package Neoretail\Danfe\Api\Data
 */
interface DanfeInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    /**
     * Entity ID
     */
    const ENTITY_ID = 'entity_id';
    /**
     * Parent ID
     */
    const PARENT_ID = 'parent_id';

    /**
     * Danfe Content
     */
    const BINARY = 'binary';

    /**
    * Created
    */
    const CREATED_AT = 'created_at';

    /**
    * Description
    */
    const DESCRIPTION = 'description';

    /**
    * Danfe Key Number
    */
    const DOWNLOAD_LINK = 'download_link';

    /**
     * Number of times email was sent to customer
     */
    const EMAIL_SENT_TIMES = 'email_sent_times';
    
    /**
    * Danfe Is generated
    */
    const IS_FILE_GENERATED = 'is_file_generated';

    /**
    * Danfe Is resend
    */
    const IS_RESEND = 'is_resend';

    /**
     * Danfe Key Number
     */
    const KEY = 'key';

    /**
    * Danfe Type
    */
    const TYPE = 'danfe_type_id';

    /**
     * Updated
     */
    const UPDATED_AT = 'updated_at';

    /**
     * Gets content of Danfe
     * @return string|null Danfe content
     */
    public function getBinary();

    /**
     * Gets the created-at timestamp for the order
     *
     * @return string|null Created-at timestamp
     */
    public function getCreatedAt();

    /**
     * Gets the download link
     * @return string
     */
    public function getDescription();

    /**
     * Gets the download link
     * @return string|null
     */
    public function getDownloadLink();

    /**
     * Gets the ID for the Danfe
     *
     * @return int Order ID
     */
    public function getEntityId();

    /**
     * Gets the quantity of times email was sent to customer
     *
     * @return int
     */
    public function getEmailSentTimes();

    /**
     * Is danfe a resend ?
     * @return boolean
     */
    public function getIsResend();

    /**
     * Is danfe file already generated ?
     * @return boolean
     */
    public function getIsFileGenerated();

    /**
     * Gets key number of Danfe
     *
     * @return string|null Danfe Number
     */
    public function getKey();

    /**
     * Gets parent id of Danfe
     *
     * @return string|null Danfe Number
     */
    public function getParentId();

    /**
     * Gets the danfe's type
     * @return int
     */
    public function getTypeId();

    /**
     * Gets the updated-at timestamp for the Danfe
     *
     * @return string|null Updated-at timestamp
     */
    public function getUpdatedAt();

    /**
     * Sets content
     *
     * @param string $content
     * @return $this
     */
    public function setBinary($content);

    /**
     * Sets the created-at timestamp for the order
     *
     * @param string $createdAt Danfe number
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Sets description
     * @param $description
     * @return string
     */
    public function setDescription($description);

    /**
     * Sets download link
     * @param $downloadLink
     * @return string
     */
    public function setDownloadLink($downloadLink);

    /**
     * Sets entity ID
     *
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * Sets email sent times
     *
     * @param int $times
     * @return $this
     */
    public function setEmailSentTimes($times);

    /**
     * Set if danfe is resend
     * @param $isResend
     * @return boolean
     */
    public function setIsResend($isResend);

    /**
     * Set if danfe file is generated
     * @param $isFileGenerated
     * @return boolean
     */
    public function setIsFileGenerated($isFileGenerated);

    /**
     * Sets the parent id
     *
     * @param int $parentId
     * @return $this
     */
    public function setParentId($parentId);

    /**
     * Sets danfe type
     * @param $typeId
     * @return int
     */
    public function setTypeId($typeId);

    /**
     * Sets the Danfe key umber
     *
     * @param string $key
     * @return $this
     */
    public function setKey($key);

    /**
     * Sets the updated-at timestamp for the order
     *
     * @param string $updatedAt timestamp
     * @return $this
     */
    public function setUpdatedAt($updatedAt);

    /**
     * @return bool
     */
    public function isEmailSent();

    /**
     * Retrieve existing extension attributes object or create a new one
     *
     * @return \Neoretail\Danfe\Api\Data\DanfeExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object
     *
     * @param \Neoretail\Danfe\Api\Data\DanfeExtensionAttributes $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(DanfeExtensionAttributes $extensionAttributes);
}
