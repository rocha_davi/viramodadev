# Danfe on Order for Magento 2 (Magento 2.3.2)

#### Módulo DANFE

###### Atributos

Dois atributos adicionados à tabela sales_order e sales_order_grid. Os dois contadores estão visíveis no grid para que possam ser filtrados.

1- Atributo danfe_resend_count inicializado com zero na tabela sales_order e será incrementado ao receber uma nota reenviada

2- Atributo danfe_received_count inicializado com zero na tabela sales_order e será incrementado ao receber uma nota recebida

###### API

- A API utiliza a mesma autenticação padrão da API padrão da plataforma;

- A API é acessível pelo endereço **/V1/nfeasus**;

- Disponibilizado na API da Order, todas as danfes para aquele pedido

`PUT {{baseUrl}}/V1/nfeasus`

`GET {{baseUrl}}/V1/nfeasus/danfeId`

`GET {{baseUrl}}/V1/nfeasus/order/orderId`
 
`GET {{baseUrl}}/V1/nfeasus/order/126`

Request

`{
     "orderId": 126,
     "key": "60000000001111111111222222222233333333334444",
     "description": "description",
     "isResend": true,
     "downloadLink": "http://www.nfe.fazenda.gov.br/portal/consultaRecaptcha.aspx?tipoConsulta=resumo&tipoConteudo=d09fwabTnLk=",
     "typeId":2
 }`
- orderId: id do pedido
- key: chave da danfe (valor único)
- description: texto que será adicionado aos comentários do pedido
- isResend: flag indicando se a nota é um reenvio
- downloadLink: link para download da danfe
- typeId: tipo da danfe (envio ou devolução - esses valores se encontram na tabela neoretail_sales_order_danfe_type)

Response
- entity_id: danfe entityId

###### Envio de Emails
- O comprador recebe um e-mail contendo o link da nota fiscal e a chave de acesso assim que ela for disponibilizada;

###### Visualização dos dados
- painel administrativo na visualização do pedido
- painel do cliente na visualização do pedido
- a descrição estará visível nos comentários do pedido 
