<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Plugin\Api;

use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\OrderExtensionFactory;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Neoretail\Danfe\Model\CustomAttributes;
use Neoretail\Danfe\Model\DanfeFactory;
use Neoretail\Danfe\Model\DanfeRepository;

/**
 * Class OrderRepository
 * @package Neoretail\Danfe\Plugin\Api
 */
class OrderRepository
{

    /**
     * @var DanfeFactory
     */
    private $danfeFactory;

    /**
     * @var DanfeRepository
     */
    private $danfeRepository;

    /**
     * Order Extension Attributes Factory
     *
     * @var \Magento\Sales\Api\Data\OrderExtensionFactory
     */
    protected $orderExtensionFactory;

    /**
     * OrderRepositoryPlugin constructor
     *
     * @param \Magento\Sales\Api\Data\OrderExtensionFactory $extensionFactory
     */
    public function __construct(
        DanfeFactory $danfeFactory,
        DanfeRepository $danfeRepository,
        OrderExtensionFactory $orderExtensionFactory
    ){
        $this->danfeFactory = $danfeFactory;
        $this->danfeRepository = $danfeRepository;
        $this->orderExtensionFactory = $orderExtensionFactory;
    }


    /**
     * Add Danfes to Order
     * @param OrderRepositoryInterface $subject
     * @param OrderInterface $order
     * @return OrderInterface
     * @throws LocalizedException
     */
    public function afterGet(OrderRepositoryInterface $subject, OrderInterface $order)
    {

        $danfes = $this->getDanfes($order);
        $extensionAttributes = $order->getExtensionAttributes();
        $orderExtension = $extensionAttributes ? $extensionAttributes : $this->orderExtensionFactory->create();
        $orderExtension->setData(CustomAttributes::DANFE_RESEND_COUNT, $order->getData(CustomAttributes::DANFE_RESEND_COUNT));
        $orderExtension->setData(CustomAttributes::DANFE_RECEIVED_COUNT, $order->getData(CustomAttributes::DANFE_RECEIVED_COUNT));
        $orderExtension->setDanfes($danfes);
        $order->setExtensionAttributes($orderExtension);
        return $order;
    }

    /**
     * @param OrderRepositoryInterface $subject
     * @param OrderSearchResultInterface $searchResult
     * @return OrderSearchResultInterface
     * @throws LocalizedException
     */
    public function afterGetList(OrderRepositoryInterface $subject, OrderSearchResultInterface $searchResult)
    {
        $orders = $searchResult->getItems();
        /** @var OrderInterface $order */
        foreach ($orders as &$order) {
            $danfes = $this->danfeRepository->getDanfesByOrderId($order->getEntityId());
            $extensionAttributes = $order->getExtensionAttributes();
            $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->orderExtensionFactory->create();
            $extensionAttributes->setData(CustomAttributes::DANFE_RECEIVED_COUNT, $order->getData(CustomAttributes::DANFE_RECEIVED_COUNT));
            $extensionAttributes->setData(CustomAttributes::DANFE_RESEND_COUNT, $order->getData(CustomAttributes::DANFE_RESEND_COUNT));
            $extensionAttributes->setDanfes($danfes);
            $order->setExtensionAttributes($extensionAttributes);
        }
        return $searchResult;
    }

    /**
     * Get danfes from order id
     * @param OrderInterface $order
     * @return array|\Magento\Framework\Api\ExtensibleDataInterface[]|\Neoretail\Danfe\Api\Data\DanfeInterface[]
     * @throws LocalizedException
     */
    private function getDanfes(OrderInterface $order)
    {
        $danfes = $this->danfeRepository->getDanfesByOrderId($order->getEntityId());
        if (!$danfes) {
            return [];
        }
        return $danfes;
    }
}
