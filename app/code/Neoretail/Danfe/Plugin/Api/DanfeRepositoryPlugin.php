<?php
/**
 * Neoretail Magento
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://neoretail.com for more information.
 *
 * @category Neoretail
 *
 * @copyright Copyright (c) 2020 Neoretail Magento. (https://neoretail.com)
 *
 * @author Neoretail Core Team <contato@neoretail.com>
 */
declare(strict_types=1);

namespace Neoretail\Danfe\Plugin\Api;

use Magento\Sales\Api\OrderRepositoryInterface;
use Neoretail\Danfe\Api\Data\DanfeInterface;
use Neoretail\Danfe\Model\CustomAttributes;
use Neoretail\Danfe\Model\DanfeFactory;
use Neoretail\Danfe\Model\DanfeRepository;
use Neoretail\Danfe\Model\Logger\Logger;
use Neoretail\Danfe\Model\Notification\Email;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class OrderRepository
 * @package Neoretail\Danfe\Plugin\Api
 */
class DanfeRepositoryPlugin
{

    /**
     * @var DanfeFactory
     */
    private $danfeFactory;

    /**
     * @var DanfeRepository
     */
    private $danfeRepository;

    /**
     * @var Email
     */
    private $email;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * DanfeRepositoryPlugin constructor.
     * @param DanfeFactory $danfeFactory
     * @param DanfeRepository $danfeRepository
     * @param Logger $logger
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        DanfeFactory $danfeFactory,
        DanfeRepository $danfeRepository,
        Email $email,
        Logger $logger,
        OrderRepositoryInterface $orderRepository
    ){
        $this->danfeFactory = $danfeFactory;
        $this->danfeRepository = $danfeRepository;
        $this->email = $email;
        $this->logger = $logger;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Update danfe count on order after save danfe and send email
     * @param DanfeRepository $subject
     * @param DanfeInterface $result
     * @return DanfeRepository
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterSave(DanfeRepository $subject, DanfeInterface $result)
    {
        $orderId = $result->getParentId();
        try {
            $order = $this->orderRepository->get($orderId);
            if ($result->getIsResend()) {
                $order->setData(CustomAttributes::DANFE_RESEND_COUNT, (int) $order->getData(CustomAttributes::DANFE_RESEND_COUNT) + 1);
            } else {
                $order->setData(CustomAttributes::DANFE_RECEIVED_COUNT, (int) $order->getData(CustomAttributes::DANFE_RECEIVED_COUNT) + 1);
            }
            $order->addCommentToStatusHistory(__('Danfe comment:').$result->getDescription());
            $this->orderRepository->save($order);
            $this->email->sendMail($result->getEntityId());
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            $this->logger->log($exception->getMessage());
        }
        return $subject;
    }
}
