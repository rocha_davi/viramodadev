<?php
/**
 * Copyright ©  All rights reserved.
 */

namespace Neoretail\SponsorCode\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Sponsor extends AbstractHelper
{
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Webkul\MLM\Model\SponsorsFactory $sponsors
    ) {
        parent::__construct($context);
        $this->sponsors = $sponsors;
    }

    public function getSponsorCode($url) {
		$parsedUrl = parse_url($url);
		$host = explode('.', $parsedUrl['host']);
		$sponsorName = $host[0];
    	$sponsor = $this->sponsors->create()->getCollection()->addFieldToFilter('sponser_name', $sponsorName)
                    ->getFirstItem();
            if (!$sponsor->getData('sponsor_code')) {
                    return 'wN-J-W-R-o-X-J-z-V-Ok';
            }
        return $sponsor->getData('sponsor_code');
    }
}