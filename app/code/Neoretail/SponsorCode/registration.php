<?php
/**
 * Copyright ©  All rights reserved.
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Neoretail_SponsorCode', __DIR__);

