<?php
/**
 * Neoretail E-comm
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2021 Neoretail E-comm. (https://www.neoretail.com)
 *
 * @author Neoretail E-comm <contato@neoretail.com>
 */

namespace Neoretail\Checkout\Setup\Patch\Data;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Setup\Patch\Data\ConvertValidationRulesFromSerializedToJson;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class ChangeTaxvatVisibilitySetting implements DataPatchInterface
{
    const TAXVAT_VALUE_REQUIRED = 'req';
    const TAXVAT_CONFIG_PATH = 'customer/address/taxvat_show';

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var WriterInterface
     */
    private $writer;
    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CustomerSetupFactory $customerSetupFactory,
        WriterInterface $writer
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->writer = $writer;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {
        $this->writer->save(self::TAXVAT_CONFIG_PATH, self::TAXVAT_VALUE_REQUIRED);

        $customerSetup = $this->customerSetupFactory->create([
            'resourceConnection' => $this->moduleDataSetup
        ]);
        $customerSetup->updateAttribute('customer', 'taxvat', 'is_required', 1);
        $customerSetup->updateAttribute('customer', 'taxvat', 'is_visible', 1);
    }

    public function getAliases()
    {
        return [];
    }

    public static function getDependencies()
    {
        return [
            ConvertValidationRulesFromSerializedToJson::class,
        ];
    }
}
