<?php
/**
 * Neoretail E-comm
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2021 Neoretail E-comm. (https://www.neoretail.com)
 *
 * @author Neoretail E-comm <contato@neoretail.com>
 */

namespace Neoretail\Checkout\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session\Proxy as CustomerSession;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Phrase;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Neoretail\Checkout\Model\Customer\Attribute\Source\PersonType;
use Neoretail\Taxvat\Model\Validate;

/**
 * Class ConfigProvider
 * @package Neoretail\Checkout\Model
 */
class ConfigProvider implements ConfigProviderInterface
{
    /**
     * @var UrlInterface
     */
    private $urlBuilder;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var PersonType
     */
    private $personType;
    /**
     * @var CustomerSession
     */
    private $customerSession;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var FormKey
     */
    private $formKey;

    /**
     * ConfigProvider constructor.
     * @param UrlInterface $urlBuilder
     * @param ScopeConfigInterface $scopeConfig
     * @param PersonType $personType
     * @param CustomerSession $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        UrlInterface $urlBuilder,
        ScopeConfigInterface $scopeConfig,
        PersonType $personType,
        CustomerSession $customerSession,
        CustomerRepositoryInterface $customerRepository,
        FormKey $formKey
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->personType = $personType;
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->formKey = $formKey;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        $config['neoretailcheckout'] = [
            'isActive' => (bool) $this->getScopeConfig('active'),
            'personTypesAvailable' => $this->getPersonTypesAvailable(),
            'accountCreateUrl' => $this->getAccountCreateUrl(),
            'accountEditUrl' => $this->getAccountEditUrl(),
            'customerValidation' => $this->validateCustomer(),
        ];

        return $config;
    }

    /**
     * @return mixed
     */
    private function getAccountCreateUrl()
    {
        return $this->urlBuilder->getUrl('neoretailcheckout/account/create', [
            '_secure' => true,
            'form_key'=>$this->formKey->getFormKey()
        ]);
    }

    /**
     * @return mixed
     */
    private function getAccountEditUrl()
    {
        return $this->urlBuilder->getUrl('neoretailcheckout/account/edit', [
            '_secure' => true,
            'form_key'=>$this->formKey->getFormKey()
        ]);
    }

    /**
     * @return array
     */
    private function getPersonTypesAvailable()
    {
        $typesAvailable = [];
        $available = $this->getScopeConfig('person_types_available');
        foreach ($this->personType->getAllOptions() as $option) {
            if (!in_array($option['value'], explode(',', $available))) {
                continue;
            }

            $typesAvailable[(int) $option['value']] = ($option['label'] instanceof Phrase)
                ? __($option['label']->getText())
                : __($option['label']);
        }

        return $typesAvailable;
    }

    /**
     * @param $field
     * @return mixed
     */
    private function getScopeConfig($field)
    {
        return $this->scopeConfig->getValue(
            'neoretail_checkout/authentication/' . $field,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return array
     */
    public function validateCustomer(): array
    {
        $requiredFields = [];

        try {
            $customer = $this->customerRepository->getById($this->customerSession->getCustomerId());
            $taxvat = $customer->getTaxvat();
            if (!Validate::isValidCpfOrCnpj($taxvat)) {
                $requiredFields[] = 'taxvat';
            }
            if((bool) $this->getScopeConfig('disable_person_type_validation')) {
                $personType = $customer->getCustomAttribute('person_type');
                if (!isset($personType) || empty($personType->getValue())) {
                    $requiredFields[] = 'person_type';
                }
            }
        } catch (NoSuchEntityException $e) {
        } catch (LocalizedException $e) {
        }

        return $requiredFields;
    }
}
