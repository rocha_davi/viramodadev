<?php
/**
 * Neoretail E-comm
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2021 Neoretail E-comm. (https://www.neoretail.com)
 *
 * @author Neoretail E-comm <contato@neoretail.com>
 */

namespace Neoretail\Checkout\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Neoretail\Checkout\Model\Customer\Attribute\Source\PersonType;

/**
 * Class PersonTypesAvailable
 * @package Neoretail\Checkout\Model\Config\Source
 */
class PersonTypesAvailable implements OptionSourceInterface
{
    /**
     * @var PersonType
     */
    private $personType;

    /**
     * PersonTypesAvailable constructor.
     * @param PersonType $personType
     */
    public function __construct(
        PersonType $personType
    ) {
        $this->personType = $personType;
    }

    /**
     * @return array|\Neoretail\Checkout\Model\Customer\Attribute\Source\Array|null
     */
    public function toOptionArray()
    {
        return $this->personType->getAllOptions();
    }
}
