var config = {
    'config': {
        'mixins': {
            'Magento_Checkout/js/view/shipping': {
                'Neoretail_Checkout/js/view/shipping-payment-mixin': true
            },
            'Magento_Checkout/js/view/payment': {
                'Neoretail_Checkout/js/view/shipping-payment-mixin': true
            },
            'Magento_Checkout/js/view/authentication': {
                'Neoretail_Checkout/js/view/authentication-mixin': true
            },
            'Magento_Checkout/js/model/step-navigator': {
                'Neoretail_Checkout/js/model/step-navigator-mixin': true
            }
        }
    }
};