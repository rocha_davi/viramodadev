/**
 * Neoretail E-comm
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2021 Neoretail E-comm. (https://www.neoretail.com)
 *
 * @author Neoretail E-comm <contato@neoretail.com>
 */
/*browser:true*/
/*global define*/
define([
        'jquery',
        'ko',
        'Neoretail_Taxvat/js/validate',
        'Neoretail_Taxvat/js/mask'
    ], function ($, ko, validate) {
        'use strict';

        var label = ko.observable('CPF');
        var cssClass = ko.observable('input-text mask-cpf');

        return {
            label: label,
            cssClass: cssClass,
            validateRemote: validate.url
        };
    }
);
