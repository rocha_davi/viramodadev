/**
 * Neoretail E-comm
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2021 Neoretail E-comm. (https://www.neoretail.com)
 *
 * @author Neoretail E-comm <contato@neoretail.com>
 */
/*browser:true*/
/*global define*/
define([
        'ko',
        'underscore',
        'Neoretail_Checkout/js/model/taxvat'
    ], function (ko, _, taxvat) {
        'use strict';

        var typePerson = '1';
        var typeCompany = '2';

        var personFieldIsVisible = ko.observable(true);
        var value = ko.observable('');

        return {
            personFieldIsVisible: personFieldIsVisible,
            value: value,

            init: function () {
                var self = this;

                value.subscribe(function (personTypeValue) {
                    self._handlePersonTypeChange(personTypeValue);
                });

                this._initPersonField();
            },

            _initPersonField: function () {
                var personTypesAvailableKeys = Object.keys(this.getPersonTypesAvailable());
                if (personTypesAvailableKeys.length === 1) {
                    this.personFieldIsVisible(false);
                    this.value(personTypesAvailableKeys[0]);
                }
            },

            _handlePersonTypeChange: function (personTypeValue) {
                if (personTypeValue === typePerson) {
                    this._handlePersonType();
                } else if (personTypeValue === typeCompany) {
                    this._handleCompanyType();
                }
            },

            _handlePersonType: function () {
                taxvat.label('CPF');
                taxvat.cssClass('input-text mask-cpf');
            },

            _handleCompanyType: function () {
                taxvat.label('CNPJ');
                taxvat.cssClass('input-text mask-cnpj');
            },

            getPersonTypeOptions: function () {
                return _.map(this.getPersonTypesAvailable(), function (value, key) {
                    return {
                        'type': key,
                        'label': value
                    };
                });
            },

            getPersonTypesAvailable: function () {
                return window.checkoutConfig['neoretailcheckout']['personTypesAvailable'];
            },
        };
    }
);
