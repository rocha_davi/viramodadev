/**
 * Neoretail E-comm
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2021 Neoretail E-comm. (https://www.neoretail.com)
 *
 * @author Neoretail E-comm <contato@neoretail.com>
 */
/*browser:true*/
/*global define*/
define([
    'jquery',
    'mage/translate',
    'Magento_Ui/js/form/form',
    'Neoretail_Checkout/js/model/customer',
    'Magento_Customer/js/customer-data',
    'Neoretail_Checkout/js/model/person-type',
    'Neoretail_Checkout/js/model/taxvat',
    'Magento_Ui/js/model/messageList',
    'Magento_Checkout/js/model/full-screen-loader'
], function ($, $t, Component, customer, customerData, personType, taxvat, messageContainer, fullScreenLoader) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Neoretail_Checkout/edit',
            customer: customer,
            personType: personType,
            taxvat: taxvat
        },

        update: function (updateForm) {
            var self = this;

            if ($(updateForm).validation() &&
                $(updateForm).validation('isValid')
            ) {
                fullScreenLoader.startLoader();

                $.post(
                    window.checkoutConfig['neoretailcheckout']['accountEditUrl'],
                    $(updateForm).serializeArray()
                ).done(function (data) {
                    if (data.success) {
                        self._handleSuccessResult();
                    } else {
                        self._handleErrorResult(data);
                    }
                }).fail(function () {
                    messageContainer.addErrorMessage({
                        'message': $t('Could not update account. Please try again later')
                    });
                    fullScreenLoader.stopLoader();
                });
            }
        },

        _handleSuccessResult: function () {
            customerData.invalidate(['customer']);
            location.reload();
        },

        _handleErrorResult: function (data) {
            messageContainer.addErrorMessage({
                'message': data.message
            });
            fullScreenLoader.stopLoader();
        },

        isVisible: function () {
            return !!(customer.isCustomerLoggedIn() && !customer.isValidCustomer());
        }
    });
});
