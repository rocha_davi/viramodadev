/**
 * Neoretail E-comm
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2021 Neoretail E-comm. (https://www.neoretail.com)
 *
 * @author Neoretail E-comm <contato@neoretail.com>
 */
/*browser:true*/
/*global define*/
define(
    [
        'ko',
        'uiComponent',
        'underscore',
        'Magento_Checkout/js/model/step-navigator',
        'Neoretail_Checkout/js/model/customer',
    ],
    function (ko, Component, _, stepNavigator, customer) {
        'use strict';

        var isVisible = ko.observable(false);

        return Component.extend({
            defaults: {
                template: 'Neoretail_Checkout/customer',
                customer: customer,
                isVisible: isVisible
            },

            initialize: function () {
                this._super();

                if (window.checkoutConfig['neoretailcheckout']['isActive']) {
                    stepNavigator.registerStep(
                        'customer',
                        null,
                        'Customer',
                        this.isVisible,
                        _.bind(this.navigate, this),
                        1
                    );
                }

                return this;
            },

            navigate: function () {
                if (this.customer.isCustomerLoggedIn()
                    && this.customer.isValidCustomer()
                ) {
                    this.isVisible(false);
                    stepNavigator.setHash('shipping');
                } else {
                    this.isVisible(true);
                }
            },

            /**
             * @inheritDoc
             */
            navigateToNextStep: function () {
                stepNavigator.next();
            }
        });
    }
);
