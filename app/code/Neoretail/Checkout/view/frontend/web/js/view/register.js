/**
 * Neoretail E-comm
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2021 Neoretail E-comm. (https://www.neoretail.com)
 *
 * @author Neoretail E-comm <contato@neoretail.com>
 */
/*browser:true*/
/*global define*/
define([
    'jquery',
    'ko',
    'mage/translate',
    'Magento_Ui/js/form/form',
    'Magento_Customer/js/model/customer',
    'Magento_Customer/js/customer-data',
    'Neoretail_Checkout/js/model/person-type',
    'Neoretail_Checkout/js/model/taxvat',
    'Magento_Ui/js/model/messageList',
    'Magento_Checkout/js/model/full-screen-loader',
], function ($, ko, $t, Component, customer, customerData, personType, taxvat, messageContainer, fullScreenLoader) {
    'use strict';

    var exempted = ko.observable(0);

    return Component.extend({
        defaults: {
            template: 'Neoretail_Checkout/register',
            personType: personType,
            taxvat: taxvat,
            exempted: exempted,
            availableRequest: true
        },

        initialize: function () {
            this._super();

            personType.init();
        },

        register: function (registerForm) {
            var self = this;

            if ($(registerForm).validation() &&
                $(registerForm).validation('isValid')
            ) {
                fullScreenLoader.startLoader();

                if (self.availableRequest) {
                    self.availableRequest = false;
                    $.post(
                        window.checkoutConfig['neoretailcheckout']['accountCreateUrl'],
                        $(registerForm).serializeArray()
                    ).done(function (data) {
                        if (data.success) {
                            self._handleSuccessResult();
                        } else {
                            self.availableRequest = true;
                            fullScreenLoader.stopLoader();
                            self._handleErrorResult(data);
                        }
                    }).fail(function () {
                        self.availableRequest = true;
                        messageContainer.addErrorMessage({
                            'message': $t('Could not create account. Please try again later')
                        });
                        fullScreenLoader.stopLoader();
                    });
                }
            }
        },

        _handleSuccessResult: function () {
            customerData.invalidate(['customer']);
            location.reload();
        },

        _handleErrorResult: function (data) {
            messageContainer.addErrorMessage({
                'message': data.message
            });
            fullScreenLoader.stopLoader();
        },

        isVisible: function () {
            return !customer.isLoggedIn();
        }
    });
});
