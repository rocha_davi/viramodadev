<?php
/**
 * Neoretail E-comm
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2021 Neoretail E-comm. (https://www.neoretail.com)
 *
 * @author Neoretail E-comm <contato@neoretail.com>
 */

namespace Neoretail\Checkout\Controller\Account;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\Customer\Mapper;
use Magento\Customer\Model\CustomerExtractor;
use Magento\Customer\Model\Session\Proxy as CustomerSession;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Neoretail\Checkout\Model\Customer\Attribute\Source\PersonType;
use Neoretail\Taxvat\Model\Validate;

/**
 * Class Edit
 * @package Neoretail\Checkout\Controller\Account
 */
class Edit extends Action implements CsrfAwareActionInterface, HttpPostActionInterface
{
    private $result = [
        'success' => false,
        'message' => []
    ];

    /**
     * @var CustomerSession
     */
    private $customerSession;
    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    /**
     * @var Escaper
     */
    private $escaper;
    /**
     * @var Validator
     */
    private $formKeyValidator;
    /**
     * @var Mapper
     */
    private $customerMapper;
    /**
     * @var CustomerExtractor
     */
    private $customerExtractor;

    /**
     * Edit constructor.
     * @param Context $context
     * @param CustomerSession $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param Mapper $customerMapper
     * @param CustomerExtractor $customerExtractor
     * @param JsonFactory $resultJsonFactory
     * @param Escaper $escaper
     * @param Validator $formKeyValidator
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        CustomerRepositoryInterface $customerRepository,
        Mapper $customerMapper,
        CustomerExtractor $customerExtractor,
        JsonFactory $resultJsonFactory,
        Escaper $escaper,
        Validator $formKeyValidator
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->customerRepository = $customerRepository;
        $this->escaper = $escaper;
        $this->formKeyValidator = $formKeyValidator;
        $this->customerMapper = $customerMapper;
        $this->customerExtractor = $customerExtractor;
    }

    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $validFormKey = $this->formKeyValidator->validate($this->getRequest());

        if (!$validFormKey || !$this->getRequest()->isPost()) {
            $this->result['message'][] = __('Data error. Please try again.');

            return $resultJson->setData($this->result);
        }

        if (!$this->isAllowedUpdate()) {
            return $resultJson->setData($this->result);
        }

        if (!$this->isValidDocument()) {
            $this->result['message'][] = __(
                '%1 inválido',
                ($this->getRequest()->getParam('person_type') == PersonType::PERSON) ? 'CPF' : 'CNPJ'
            );

            return $resultJson->setData($this->result);
        }

        try {
            $currentCustomerDataObject = $this->customerRepository->getById($this->customerSession->getCustomerId());
            $customerCandidateDataObject = $this->populateNewCustomerDataObject(
                $this->_request,
                $currentCustomerDataObject
            );

            $this->customerRepository->save($customerCandidateDataObject);

            $this->result['success'] = true;
        } catch (NoSuchEntityException $e) {
            $this->handleNoSuchEntityException($e);
        } catch (LocalizedException $e) {
            $this->handleLocalizedException($e);
        }

        return $resultJson->setData($this->result);
    }

    private function isAllowedUpdate(): bool
    {
        return $this->customerSession->isLoggedIn();
    }

    private function isValidDocument(): bool
    {
        $document = $this->getRequest()->getParam('taxvat');
        return Validate::isValidCpfOrCnpj($document);
    }

    /**
     * @param RequestInterface $inputData
     * @param CustomerInterface $currentCustomerData
     * @return mixed
     */
    private function populateNewCustomerDataObject(
        RequestInterface $inputData,
        CustomerInterface $currentCustomerData
    ) {
        $attributeValues = $this->customerMapper->toFlatArray($currentCustomerData);
        $customerDto = $this->customerExtractor->extract(
            'customer_account_edit',
            $inputData,
            $attributeValues
        );
        $customerDto->setId($currentCustomerData->getId());
        $customerDto->setEmail($currentCustomerData->getEmail());
        $customerDto->setFirstname($currentCustomerData->getFirstname());
        $customerDto->setLastname($currentCustomerData->getLastname());
        if (!$customerDto->getAddresses()) {
            $customerDto->setAddresses($currentCustomerData->getAddresses());
        }

        return $customerDto;
    }

    /**
     * @param LocalizedException $e
     */
    private function handleLocalizedException(LocalizedException $e): void
    {
        $this->result['message'][] = $this->escaper->escapeHtml($e->getMessage());
    }

    /**
     * @param NoSuchEntityException $e
     */
    private function handleNoSuchEntityException(NoSuchEntityException $e): void
    {
        $this->result['message'][] = $this->escaper->escapeHtml($e->getMessage());
    }
}
