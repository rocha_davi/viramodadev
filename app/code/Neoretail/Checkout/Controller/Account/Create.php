<?php
/**
 * Neoretail E-comm
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.neoretail.com for more information.
 *
 * @category Neoretail
 * @package base
 *
 * @copyright Copyright (c) 2021 Neoretail E-comm. (https://www.neoretail.com)
 *
 * @author Neoretail E-comm <contato@neoretail.com>
 */

namespace Neoretail\Checkout\Controller\Account;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\CustomerExtractor;
use Magento\Customer\Model\Registration;
use Magento\Customer\Model\Session\Proxy as CustomerSession;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Newsletter\Model\SubscriberFactory;
use Neoretail\Checkout\Model\Customer\Attribute\Source\PersonType;
use Neoretail\Taxvat\Model\Validate;

/**
 * Class Create
 * @package Neoretail\Checkout\Controller\Account
 */
class Create extends AbstractAccount implements CsrfAwareActionInterface, HttpPostActionInterface
{
    private $result = [
        'success' => false,
        'message' => []
    ];

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var Validator
     */
    private $formKeyValidator;

    /**
     * @var CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var AccountRedirect
     */
    private $accountRedirect;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var CustomerSession
     */
    private $customerSession;
    /**
     * @var Registration
     */
    private $registration;
    /**
     * @var CustomerExtractor
     */
    private $customerExtractor;
    /**
     * @var AccountManagementInterface
     */
    private $accountManagement;
    /**
     * @var Escaper
     */
    private $escaper;
    /**
     * @var CookieManagerInterface
     */
    private $cookieManager;
    /**
     * @var SubscriberFactory
     */
    private $subscriberFactory;

    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        ScopeConfigInterface $scopeConfig,
        AccountManagementInterface $accountManagement,
        Registration $registration,
        Escaper $escaper,
        CustomerExtractor $customerExtractor,
        SubscriberFactory $subscriberFactory,
        AccountRedirect $accountRedirect,
        JsonFactory $resultJsonFactory,
        Validator $formKeyValidator = null,
        CookieManagerInterface $cookieManager = null,
        CookieMetadataFactory $cookieMetadataFactory = null
    ) {
        parent::__construct($context);

        $this->resultJsonFactory = $resultJsonFactory;
        $this->accountRedirect = $accountRedirect;
        $this->scopeConfig = $scopeConfig;
        $this->customerSession = $customerSession;
        $this->registration = $registration;
        $this->customerExtractor = $customerExtractor;
        $this->accountManagement = $accountManagement;
        $this->escaper = $escaper;
        $this->cookieManager = $cookieManager ?:
            ObjectManager::getInstance()->get(CookieManagerInterface::class);
        $this->cookieMetadataFactory = $cookieMetadataFactory ?:
            ObjectManager::getInstance()->get(CookieMetadataFactory::class);
        $this->formKeyValidator = $formKeyValidator ?:
            ObjectManager::getInstance()->get(Validator::class);
        $this->subscriberFactory = $subscriberFactory;
    }

    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }

    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $validFormKey = $this->formKeyValidator->validate($this->getRequest());

        if (!$validFormKey || !$this->getRequest()->isPost()) {
            $this->result['message'][] = __('Data error. Please try again.');

            return $resultJson->setData($this->result);
        }

        if (!$this->isAllowedRegister()) {
            return $resultJson->setData($this->result);
        }

        if (!$this->isValidRequest()) {
            $this->result['message'][] = __('Data error. Please try again.');

            return $resultJson->setData($this->result);
        }

        if (!$this->isValidDocument()) {
            $this->result['message'][] = __(
                '%1 inválido',
                ($this->getRequest()->getParam('person_type') == PersonType::PERSON) ? 'CPF' : 'CNPJ'
            );

            return $resultJson->setData($this->result);
        }

        try {
            $this->registerCustomer();
        } catch (StateException $e) {
            $this->handleStateException($e);
        } catch (InputException $e) {
            $this->handleInputException($e);
        } catch (LocalizedException $e) {
            $this->handleLocalizedException($e);
        } catch (Exception $e) {
            $this->handleException();
        }

        return $resultJson->setData($this->result);
    }

    private function isAllowedRegister(): bool
    {
        return !$this->customerSession->isLoggedIn()
            && $this->registration->isAllowed();
    }

    private function isValidRequest(): bool
    {
        return $this->getRequest()->isPost();
    }

    private function isValidDocument(): bool
    {
        $document = $this->getRequest()->getParam('taxvat');
        return Validate::isValidCpfOrCnpj($document);
    }

    /**
     * @throws InputException
     * @throws LocalizedException
     */
    private function registerCustomer(): void
    {
        $customer = $this->customerExtractor->extract('customer_account_create', $this->_request);
        $customer->setAddresses([]);

        $password = $this->getRequest()->getParam('password');
        $confirmation = $this->getRequest()->getParam('password_confirmation');

        $this->checkPasswordConfirmation($password, $confirmation);

        $customer = $this->accountManagement->createAccount($customer, $password);

        if ($this->getRequest()->getParam('is_subscribed', false)) {
            $this->subscriberFactory->create()->subscribeCustomerById($customer->getId());
        }

        $this->_eventManager->dispatch(
            'customer_register_success',
            ['account_controller' => $this, 'customer' => $customer]
        );

        $this->customerSession->setCustomerDataAsLoggedIn($customer);
        $this->customerSession->regenerateId();

        $this->result['success'] = true;
        $this->result['message'][] = __('Create an account successfully.');

        if ($this->cookieManager->getCookie('mage-cache-sessid')) {
            $metadata = $this->cookieMetadataFactory->createCookieMetadata();
            $metadata->setPath('/');
            $this->cookieManager->deleteCookie('mage-cache-sessid', $metadata);
        }
    }

    private function checkPasswordConfirmation($password, $confirmation)
    {
        if ($password != $confirmation) {
            throw new InputException(__('Please make sure your passwords match.'));
        }
    }

    private function handleStateException(StateException $e): void
    {
        $this->result['message'][] = __(
            'There is already an account with this email address. If you are sure that it is your email address'
        );
    }

    private function handleInputException(InputException $e): void
    {
        $this->handleLocalizedException($e);
        foreach ($e->getErrors() as $error) {
            $this->handleLocalizedException($error);
        }
    }

    private function handleLocalizedException(LocalizedException $e): void
    {
        $this->result['message'][] = $this->escaper->escapeHtml($e->getMessage());
    }

    private function handleException(): void
    {
        $this->result['message'][] = __('We can\'t save the customer.');
    }
}
