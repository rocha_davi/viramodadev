# Neoretail_Taxvat

- Validação de CPF e CNPJ
- Máscara 

## Validação no backend

` \Neoretail\Taxvat\Model\Validate::isValidCpfOrCnpj($documento)`

` \Neoretail\Taxvat\Model\Validate::isValidCpf($documento)`

` \Neoretail\Taxvat\Model\Validate::isValidCnpj($documento)`

## Validação no frontend

`<input ... data-validate="{required:true, 'remote':'<?php /* @escapeNotVerified */ echo $this->getUrl('neoretailtaxvat/validate/both', ['_secure' => true]); ?>'}"/>`

## Máscara

Importe via requireJs 'Neoretail_Taxvat/js/mask'

### CPF
`.mask-cpf`

### CNPJ
`.mask-cnpj`