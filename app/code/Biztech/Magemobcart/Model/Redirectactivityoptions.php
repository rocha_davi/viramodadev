<?php
 namespace Biztech\Magemobcart\Model;

class Redirectactivityoptions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options for Type
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            'home' => __('Home'),
            'category' => __('Category'),
            'product' => __('Product')
        ];
    }
}
