<?php

namespace Biztech\Magemobcart\Model;

class Category
{
    public static function getOptionArray()
    {
        // $optionArray = array();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $rootcategory=$objectManager->create('Magento\Catalog\Model\Category')->getCollection()->addAttributeToSelect('*');
        foreach ($rootcategory as $root) {
            $root_id=$root->getEntityId();
            if ($root_id != 1) {
                $optionArray[]=array('label' => $root->getName(),'value' => $root_id);
            }
        }
        $result = array();
        foreach ($optionArray as $key => $value) {
            $result[$value['value']] = $value['label'];
        }
        return $result;
    }
}
