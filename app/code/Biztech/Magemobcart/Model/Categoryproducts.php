<?php

namespace Biztech\Magemobcart\Model;
use Magento\Framework\Option\ArrayInterface;

class Categoryproducts implements ArrayInterface
{

    protected $categoryProducts;

    public function __construct(
        \Magento\Catalog\Model\CategoryFactory $categoryProducts
    ) {
        $this->categoryProducts = $categoryProducts;
    }

    public function toOptionArray($cat_id = null)
    {
        $result = [];
        if (!is_null($cat_id)) {
            $categoryData = $this->categoryProducts->create()->load($cat_id);
    
            $categoryProduct = $categoryData->getProductCollection()->addAttributeToSelect('name');
            $productData = [];
            foreach ($categoryProduct as $product) {
                $result[] = [
                    "label" => $product->getName(),
                    "value" => $product->getId()
                ];
            }
        }
        return $result;
    }
}
