<?php
 namespace Biztech\Magemobcart\Model;

class Newproductstype implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options for Type
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            '0' => __('Default'),
            '1' => __('Category Wise')
        ];
    }
}
