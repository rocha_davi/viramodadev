<?php
 namespace Biztech\Magemobcart\Model;

class Producttypeoptions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options for Type
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            'best_seller' => __('Best Seller Products'),
            'new_products' => __('New Products'),
            'category_products' => __('From a category'),
            'custom_products' => __('Custom Products')
        ];
    }
}
