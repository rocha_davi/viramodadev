<?php
 namespace Biztech\Magemobcart\Model;

class Imagetype implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options for Type
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            '' => __('--Please Select--'),
            'url' => __('URL'),
            'upload_img' => __('Upload Image')
        ];
    }
}
