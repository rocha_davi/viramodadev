<?php

namespace Biztech\Magemobcart\Model\System\Config;

class Themecolor
{
    public function toOptionArray()
    {
        return [
                ['value'=>'240,94,47', 'label'=>__('Orange')],
                ['value'=>'37,188,200', 'label'=>__('Firozi')],
                ['value'=>'146,39,143', 'label'=>__('Purple')],
                ['value'=>'34,81,163', 'label'=>__('Dark Blue')],
                ['value'=>'3,169,244', 'label'=>__('Sky Blue')],
                ['value'=>'227,96,139', 'label'=>__('Pink')],
                ['value'=>'220,85,98', 'label'=>__('Redish')]
            ];
    }
}
