<?php
 namespace Biztech\Magemobcart\Model;

class Imagecontentoptions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options for Type
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            'scaleAspectFill' => __('Scale Aspect Fill'),
            'scaleAspectFit' => __('Scale Aspect Fit')
        ];
    }
}
