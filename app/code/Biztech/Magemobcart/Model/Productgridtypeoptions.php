<?php
 namespace Biztech\Magemobcart\Model;

class Productgridtypeoptions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options for Type
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            'category_products' => __('From a category'),
            'custom_products' => __('Custom Products')
        ];
    }
}
