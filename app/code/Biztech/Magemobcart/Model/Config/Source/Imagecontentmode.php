<?php

namespace Biztech\Magemobcart\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status
 */
class Imagecontentmode implements OptionSourceInterface
{
    public function __construct()
    {
    }
    public function toOptionArray()
    {
        $tags[] = [
            ['value' => 'scaleAspectFill', 'label' => __('Scale Aspect Fill')],
            ['value' => 'scaleAspectFit', 'label' => __('Scale Aspect Fit')]
        ];
        return $tags;
    }
}
