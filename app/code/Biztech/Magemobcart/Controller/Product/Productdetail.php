<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Product;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Action\Context;
use Magento\CatalogInventory\Api\StockRegistryInterface;

class Productdetail extends \Magento\Framework\App\Action\Action
{
    const XML_PATH_LOCALECODE = 'general/locale/code';

    protected $jsonFactory;
    protected $request;
    protected $storeManager;
    protected $productModel;
    protected $cartHelper;
    protected $stockItemRepository;
    protected $imageHelper;
    protected $wishlistHelper;
    protected $reviewSummaryModel;
    protected $helperOutput;
    protected $productFactory;
    protected $swatchHelper;
    protected $downloadableFactory;
    protected $frameworkRegistry;
    protected $formKey;
    protected $stockApiRepository;
    protected $productOptionModel;
    protected $scopeConfig;
    protected $eavConfig;
    protected $localeFormat;

    /**
     * @param Context                                                               $context
     * @param JsonFactory                                                           $jsonFactory
     * @param Http                                                                  $request
     * @param \Magento\Store\Model\StoreManagerInterface                            $storeManager
     * @param \Magento\Catalog\Model\Product                                        $productModel
     * @param \Magento\Catalog\Model\ProductFactory                                 $productFactory
     * @param \Biztech\Magemobcart\Helper\Data                                      $cartHelper
     * @param \Magento\CatalogInventory\Model\Stock\StockItemRepository             $stockItemRepository
     * @param \Magento\Catalog\Helper\Image                                         $imageHelper
     * @param \Magento\Wishlist\Helper\Data                                         $wishlistHelper
     * @param \Magento\Review\Model\Review\Summary                                  $reviewSummaryModel
     * @param \Magento\Catalog\Helper\Output                                        $helperOutput
     * @param \Magento\Swatches\Helper\Data                                         $swatchHelper
     * @param \Magento\Downloadable\Model\ResourceModel\Sample\CollectionFactory    $downloadableFactory
     * @param \Magento\Framework\Registry                                           $frameworkRegistry
     * @param StockRegistryInterface                                                $stockRegistry
     * @param \Magento\Framework\Data\Form\FormKey                                  $formKey
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface                  $stockApiRepository
     * @param \Magento\Catalog\Model\Product\Option                                 $productOptionModel
     * @param \Magento\Framework\App\Config\ScopeConfigInterface                    $scopeConfig
     * @param \Magento\Eav\Model\Config                                             $eavConfig
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        Http $request,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Biztech\Magemobcart\Helper\Data $cartHelper,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Wishlist\Helper\Data $wishlistHelper,
        \Magento\Review\Model\Review\Summary $reviewSummaryModel,
        \Magento\Catalog\Helper\Output $helperOutput,
        \Magento\Swatches\Helper\Data $swatchHelper,
        \Magento\Downloadable\Model\ResourceModel\Sample\CollectionFactory $downloadableFactory,
        \Magento\Framework\Registry $frameworkRegistry,
        StockRegistryInterface $stockRegistry,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockApiRepository,
        \Magento\Catalog\Model\Product\Option $productOptionModel,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Framework\Locale\FormatInterface $localeFormat
    ) {
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_storeManager = $storeManager;
        $this->_productModel = $productModel;
        $this->_productFactory = $productFactory;
        $this->_cartHelper = $cartHelper;
        $this->_stockItemRepository = $stockItemRepository;
        $this->_imageHelper = $imageHelper;
        $this->_wishlistHelper = $wishlistHelper;
        $this->_reviewSummaryModel = $reviewSummaryModel;
        $this->_helperOutput = $helperOutput;
        $this->_swatchHelper = $swatchHelper;
        $this->_downloadableFactory = $downloadableFactory;
        $this->_frameworkRegistry = $frameworkRegistry;
        $this->_stockRegistry = $stockRegistry;
        $this->formKey = $formKey;
        $this->_request->setParam('form_key', $this->formKey->getFormKey());
        $this->_stockApiRepository = $stockApiRepository;
        $this->_productOptionModel = $productOptionModel;
        $this->_scopeConfig = $scopeConfig;
        $this->_eavConfig = $eavConfig;
        $this->_localeFormat = $localeFormat;
        parent::__construct($context);
    }

    /**
     * This function is used for get product details.
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_cartHelper->isEnable()) {
            if (!$this->_cartHelper->getHeaders()) {
                $errorResult = array('status'=> false,'message' => $this->_cartHelper->getHeaderMessage());
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $sessionId = '';
            if (isset($postData['session_id']) && $postData['session_id'] != null) {
                $sessionId = $postData['session_id'];
                if (!$this->_customerSession->isLoggedIn()) {
                    $customerId = explode("_", $sessionId);
                    $this->_cartHelper->relogin($customerId[0]);
                }
            }
            $postData = $this->_request->getParams();
            try {
                $storeId = isset($postData['storeid']) ? $postData['storeid'] : $this->_storeManager->getStore()->getId();
                if (array_key_exists('productid', $postData)) {
                    $productId = $postData['productid'];
                }
                if (array_key_exists('sku', $postData)) {
                    $productSku = $postData['sku'];
                } else {
                    $productSku = "";
                }
                $currencyCode = (isset($postData['currency_code']) && $postData['currency_code'] != '') ? $postData['currency_code'] : $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();

                // Below code is to set requested store and currency
                $currentStoreId = $this->_storeManager->getStore()->getId();
                $currentCurrencyCode = $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();
                $this->_storeManager->setCurrentStore($storeId);
                $this->_storeManager->getStore($storeId)->setCurrentCurrencyCode($currencyCode);

                if (!array_key_exists('productid', $postData)) {
                    if ($productSku != "") {
                        $productId = $this->_productFactory->create()->getIdBySku($productSku);
                    }
                }
                if (isset($productId) && $productId != null) {
                    $product = $this->_productFactory->create()->setStoreId($storeId)->load($productId);
                    if (is_null($product->getId())) {
                        $returnExtensionArray = [
                            'status' => 'message',
                            'message' => __('No product found.')
                        ];
                        $jsonResult->setData($returnExtensionArray);
                        return $jsonResult;
                    }
                    $status = $product->getStatus();
                    $productStockData = $this->_stockApiRepository->getStockItem($productId);

                    $pro_qty = $productStockData->getQty();
                    $isInStock = $productStockData->getIsInStock();
                    $salebleModelData = $this->getSalebleQty($product->getSku());
                    if (!empty($salebleModelData)) {
                        if (array_key_exists('qty', $salebleModelData)) {
                            $pro_qty = $salebleModelData['qty'];
                        } else {
                            $pro_qty = $productStockData->getQty();
                        }
                    } else {
                        $pro_qty = $productStockData->getQty();
                    }
                    $stock_item = $productStockData;
                    if ($stock_item->getBackorders() != 0) {
                        if ($isInStock == 0) {
                            $isInStock = '0';
                        } else {
                            $isInStock = '1';
                        }
                    } else {
                        if ($pro_qty < 0 || $isInStock == 0) {
                            $pro_qty = 'Out of Stock';
                            $isInStock = '0';
                        } else {
                            $isInStock = '1';
                        }
                    }
                    $images = [];
                    $summaryData = $this->getProductRatingSummary($storeId, $productId);
                    $_images = $product->getMediaGalleryImages();
                    if ($_images) {
                        foreach ($_images as $_image) {
                            $label = $_image->getLabel();
                            if (strpos($label, 'swatch') !== false) {
                                continue;
                            } else {
                                $images[] = $this->_imageHelper->init($product, 'product_page_image_large')
                                ->setImageFile($_image->getFile())->keepFrame(false)->setQuality(100)
                                ->getUrl();
                            }
                        }
                    }
                    $associated_products = [];
                    if ($product->getTypeId() == 'grouped') {
                        $associated_products = $product->getTypeInstance(true)->getAssociatedProducts($product);
                    } elseif ($product->getTypeId() == 'configurable') {
                        $associated_products = $product->getTypeInstance()->getUsedProducts($product);
                    } elseif ($product->getTypeId() == 'bundle') {
                        $associated_products = $product->getTypeInstance(true)->getSelectionsCollection($product->getTypeInstance(true)->getOptionsIds($product), $product);
                    }
                    $prices = [];
                    $associated_products_details = [];
                    $associated_products_list = [];
                    $related_products_list = [];
                    foreach ($associated_products as $associated_product1) {
                        $prices[] = $associated_product1->getPrice();
                    }
                    $mainfinalPrice = $product->getPrice();
                    if ($product->getTypeId() == 'configurable') {
                        if (!empty($prices)) {
                            $mainfinalPrice = min($prices);
                        }
                    }
                    if ($product->getTypeId() == 'grouped') {
                        if (!empty($prices)) {
                            $mainfinalPrice = min($prices);
                        }
                    }
                    $associated_products_related = $product->getRelatedProducts();
                    if (!empty($associated_products_related)) {
                        foreach ($associated_products_related as $associated_product) {
                            $associatedPrices = [];
                            $priceAssociateProduct = $this->_productFactory->create()->setStoreId($storeId)->load($associated_product->getId());
                            $finalPrice = $priceAssociateProduct->getPrice();
                            if ($associated_product->getTypeId() == 'configurable') {
                                $associated_products_price = $priceAssociateProduct->getTypeInstance()->getUsedProducts($priceAssociateProduct);
                                foreach ($associated_products_price as $associated_product2) {
                                    $associatedPrices[] = $associated_product2->getFinalPrice();
                                }
                                if (!empty($associatedPrices)) {
                                    $finalPrice = min($associatedPrices);
                                }
                            }
                            if ($associated_product->getTypeId() == 'grouped') {
                                $associated_products_price = $priceAssociateProduct->getTypeInstance(true)->getAssociatedProducts($priceAssociateProduct);
                                foreach ($associated_products_price as $associated_product2) {
                                    $associatedPrices[] = $associated_product2->getFinalPrice();
                                }
                                if (!empty($associatedPrices)) {
                                    $finalPrice = min($associatedPrices);
                                }
                            }
                            $associated_productStockData = $this->_stockApiRepository->getStockItem($associated_product->getId());
                            $qty = $associated_productStockData->getQty();

                            if ($qty < 0 || $associated_productStockData->getIsInStock() == 0) {
                                $qty = 'Out of Stock';
                            } else {
                                $associated_products_details[] = array(
                                'id' => $associated_product->getId(),
                                'sku' => $associated_product->getSku()
                                );
                                $associated_price = $this->_productFactory->create()->load($associated_product->getId())->getPrice();
                                $associated_product2 = $this->_productFactory->create()->load($associated_product->getId());
                                $related_special_price = $associated_product2->getSpecialPrice();
                                $associated_products_list[] = array(
                                'color' => $associated_product->getColor(),
                                'id' => $associated_product->getId(),
                                'sku' => $associated_product->getSku(),
                                'name' => $this->_productModel->load($associated_product->getId())->getName(),
                                // 'image' => $this->_imageHelper->init($associated_product2, 'product_page_image_medium')->setImageFile($_image->getFile())->keepFrame(false)->setQuality(100)->getUrl(),
                                'image' => $this->_imageHelper->init($associated_product2, 'product_page_image_medium')->keepFrame(false)->setQuality(100)->getUrl(),
                                'status' => $status,
                                'qty' => $qty,
                                'default_price' => (string) number_format((float)$this->_cartHelper->getPriceByStoreWithoutCurrency($finalPrice, $storeId, $currencyCode), 2, '.', ''),
                                'price' => $this->_cartHelper->getPriceByStoreWithCurrency($finalPrice, $storeId, $currencyCode),
                                'default_special_price' => (string) number_format((float)$this->_cartHelper->getPriceByStoreWithoutCurrency($related_special_price, $storeId, $currencyCode), 2, '.', ''),
                                'special_price' => $this->_cartHelper->getPriceByStoreWithCurrency($related_special_price, $storeId, $currencyCode),
                                'save_discount' => $this->_cartHelper->getDiscount($associated_product->getId()),
                                );
                                $prices[] = $associated_product->getPrice();
                            }
                        }
                    }
                    $wishlist_detail = $this->checkwishlist($productId);
                    $byiUrl = '';
                    $isByiExists = '';
                    
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $amount = $product->getPrice();
                    if ($product->getTypeId() == 'configurable') {
                        if (!empty($prices)) {
                            $amount = min($prices);
                        }
                    }

                    $additionalInformation = $this->getProductAdditionalInfo($product->getId(), $storeId);

                    $product_details[] = [
                        'id' => $product->getId(),
                        'sku' => $product->getSku(),
                        'name' => $product->getName(),
                        'type' => $product->getTypeId(),
                        'status' => $status,
                        'in_stock' => $isInStock,
                        'weight' => $product->getWeight(),
                        'qty' => (string)$pro_qty,
                        'default_price' => (string) number_format((float)$this->_cartHelper->getPriceByStoreWithoutCurrency($mainfinalPrice, $storeId, $currencyCode), 2, '.', ''),
                        'price' => $this->_cartHelper->getPriceByStoreWithCurrency($mainfinalPrice, $storeId, $currencyCode),
                        'default_special_price' => (string) number_format((float)$this->_cartHelper->getPriceByStoreWithoutCurrency($product->getSpecialPrice(), $storeId, $currencyCode), 2, '.', ''),
                        'special_price' => $this->_cartHelper->getPriceByStoreWithCurrency($product->getSpecialPrice(), $storeId, $currencyCode),
                        'save_discount' => $this->_cartHelper->getDiscount($product->getId()),
                        'description' => $this->_helperOutput->productAttribute($product, nl2br($product->getDescription()), 'description'),
                        'short_description' => $this->_helperOutput->productAttribute($product, nl2br($product->getShortDescription()), 'short_description'),
                        'additional_info' => $additionalInformation,
                        'is_wishlisted' => $wishlist_detail['in_wishlist'],
                        'wishlist_item_id' => $wishlist_detail['wishlist_item_id'],
                        'review_count' => $summaryData['review_count'],
                        'average_rating' => $summaryData['rating_summary'],
                        // 'image' => $this->_imageHelper->init($product, 'product_page_image_medium')->setImageFile($_image->getFile())->keepFrame(false)->setQuality(100)->getUrl(),
                        'image' => $this->_imageHelper->init($product, 'product_page_image_medium')->keepFrame(false)->setQuality(100)->getUrl(),
                        'associated_skus' => $associated_products_details,
                        'all_images' => $images,
                        'product_url' => $product->getProductUrl()."?id=".base64_encode($product->getId()),
                        'price_format' => $this->_localeFormat->getPriceFormat(null, $currencyCode),
                        'byi_url_id' => (isset($isByiExists) && !empty($isByiExists)) ? $isByiExists : "",
                        'byi_url' => (isset($byiUrl) && !empty($byiUrl)) ? $byiUrl : ""
                    ];

                    $config_data = $this->getProductConfig($product->getId(), $associated_products, $storeId, $currencyCode);

                    // Below code is to reset to deafult current store and current currency
                    $this->_storeManager->getStore($storeId)->setCurrentCurrencyCode($currentCurrencyCode);
                    $this->_storeManager->setCurrentStore($currentStoreId);
                    // Above code is to reset to deafult current store and current currency

                    $productResultArray = array('productdata' => $product_details, 'associated_products_list' => $associated_products_list, 'config_data' => $config_data, 'related_products_list' => $related_products_list);
                    $jsonResult->setData($productResultArray);
                    return $jsonResult;
                }
            } catch (\Exception $e) {
                $productResultArr = array(
                    'status' => 'false',
                    'message' => __($e->getMessage())
                );
                $jsonResult->setData($productResultArr);
                return $jsonResult;
            }
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }

    protected function getProductConfig($pid, $associated_products, $storeId, $currencyCode)
    {
        $jsonResult = $this->_jsonFactory->create();
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrencyCode();
            $currentCurrencyCode = $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();
            $product_id = $pid;
            
            $product = $this->_productFactory->create()->setStoreId($storeId)->load($product_id);
            $this->_frameworkRegistry->register('product', $product);
            $config_data = [];
            $linkids = [];
            if ($product->isSaleable()) {
                $status = 1;
            } else {
                $status = 0;
            }
            if ($status == 1) {
                if ($product->getTypeId() == 'grouped') {
                    $associated_products = $product->getTypeInstance(true)->getAssociatedProducts($product);
                    foreach ($associated_products as $_item) {
                        $config_data[] = array(
                            'name' => $_item->getName(),
                            'price' => $this->_cartHelper->getPriceByStoreWithCurrency($_item->getPrice(), $storeId, $currencyCode),
                            'is_in_stock' => ($product->isSaleable()) ? 1 : 0,
                            'product_id' => $_item->getId()
                        );
                    }
                } elseif ($product->getTypeId() == 'simple' || $product->getTypeId() == 'virtual') {
                    foreach ($product->getOptions() as $o) {
                        $optionType = $o->getType();
                        $values = $o->getValues();

                        $config_data[$o->getId()]['title'] = $o->getTitle();
                        if ($o->getTitle() == "Choose Product Color") {
                            $config_data[$o->getId()]['type'] = 'color';
                        } else {
                            $config_data[$o->getId()]['type'] = $optionType;
                        }
                        $config_data[$o->getId()]['is_required'] = $o->getIsRequire();
                        $config_data[$o->getId()]['order'] = $o->getSortOrder();
                        $temp_config_data = [];
                        if (!empty($values) && count($values) > 0) {
                            foreach ($values as $k => $v) {
                                $temp_config_data[] = [
                                    'option_type_id' => $v->getOptionTypeId(),
                                    'option_id' => $v->getOptionId(),
                                    'sku' => $v->getData('sku'),
                                    'default_title' => $v->getData('default_title'),
                                    'title' => $v->getData('title'),
                                    'default_price' => (string) number_format((float)$this->_cartHelper->getPriceByStoreWithoutCurrency($v->getData('default_price'), $storeId, $currencyCode), 2, '.', ''),
                                    'default_price_type' => $v->getData('default_price_type'),
                                    'price' => $this->_cartHelper->getPriceByStoreWithCurrency($v->getData('price'), $storeId, $currencyCode),
                                    'price_type' => $v->getData('price_type'),
                                    'max_characters' => ($v->getData('max_characters') != "") ? (int) $v->getData('max_characters') : 0
                                ];
                            }
                        } else {
                            if ($optionType == 'file') {
                                $temp_config_data[] = [
                                    'option_type_id' => $o->getOptionTypeId(),
                                    'option_id' => $o->getOptionId(),
                                    'sku' => $o->getData('sku'),
                                    'default_title' => $o->getData('default_title'),
                                    'title' => $o->getData('title'),
                                    'default_price' => (string) number_format((float)$this->_cartHelper->getPriceByStoreWithoutCurrency($o->getData('default_price'), $storeId, $currencyCode), 2, '.', ''),
                                    'default_price_type' => $o->getData('default_price_type'),
                                    'price' => $this->_cartHelper->getPriceByStoreWithCurrency($o->getData('price'), $storeId, $currencyCode),
                                    'price_type' => $o->getData('price_type'),
                                    'type' => $optionType,
                                    'max_characters' => ($o->getData('max_characters') != "") ? (int) $o->getData('max_characters') : 0,
                                    'compatible_file_extensions' => $o->getData('file_extension'),
                                    'max_file_width' => $o->getData('image_size_x'),
                                    'max_file_height' => $o->getData('image_size_y')
                                ];
                            } else {
                                $temp_config_data[] = [
                                    'option_type_id' => $o->getOptionTypeId(),
                                    'option_id' => $o->getOptionId(),
                                    'sku' => $o->getData('sku'),
                                    'default_title' => $o->getData('default_title'),
                                    'title' => $o->getData('title'),
                                    'default_price' => (string) number_format((float)$this->_cartHelper->getPriceByStoreWithoutCurrency($o->getData('default_price'), $storeId, $currencyCode), 2, '.', ''),
                                    'default_price_type' => $o->getData('default_price_type'),
                                    'price' => $this->_cartHelper->getPriceByStoreWithCurrency($o->getData('price'), $storeId, $currencyCode),
                                    'price_type' => $o->getData('price_type'),
                                    'type' => $optionType,
                                    'max_characters' => ($o->getData('max_characters') != "") ? (int) $o->getData('max_characters') : 0
                                ];
                            }
                        }
                        $config_data[$o->getId()]['attributes'] = $temp_config_data;
                        unset($temp_config_data);
                    }
                } elseif ($product->getTypeId() == 'downloadable') {
                    $_myLinksCollection = $this->_downloadableFactory->create();
                    $_myLinksCollection->addProductToFilter($product->getId());
                    $i = 0;
                    $linkids = [];
                    if (sizeof($_myLinksCollection) > 0) {
                        $title = $product->getLinksTitle();
                        if (!isset($title)) {
                            $title = "";
                        }
                        $linkids['title'] = $title;
                        $linkids['links_purchased_separately'] = $product->getLinksPurchasedSeparately();
                        foreach ($_myLinksCollection as $_link) {
                            $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                            $mediaUrl = $mediaUrl . 'downloadable/files/links';
                            $mediaUrlSample = $mediaUrl . 'downloadable/files/link_samples';
                            $sample = "";
                            if ($_link->getSample_type() == 'url') {
                                $sample = $_link->getSample_url();
                                if (!isset($sample)) {
                                    $sample = "";
                                }
                            } elseif ($_link->getSample_type() == 'file') {
                                $file = $_link->getSample_file();
                                if (isset($file)) {
                                    $sample = $mediaUrlSample . $_link->getSample_file();
                                } else {
                                    $sample = "";
                                }
                            }
                            $price = $_link->getPrice();
                            $title = $_link->getStore_title();
                            if (!isset($title)) {
                                $title = "";
                            }
                            $linkidsCollection [] = array('id' => $_link->getId(), 'store_title' => "Sample# ".$i, 'is_shareable' => $_link->getIs_shareable(), 'number_of_downloads' => $_link->getNumber_of_downloads(), 'price' => $price, 'sample' => $sample);
                            $i++;
                        }
                    }
                } elseif ($product->getTypeId() == 'configurable') {
                    $config_object = $objectManager->get('Magento\Framework\View\Element\BlockFactory')->createBlock('Magento\ConfigurableProduct\Block\Product\View\Type\Configurable')->setStoreId($storeId);
                    $config_data = json_decode($config_object->getJsonConfig(), true);
                    $configArray = json_decode($config_object->getJsonConfig(), true);
                    $attributeKey = [];

                    $repository = $objectManager->create('Magento\Catalog\Model\ProductRepository');
                    $productdata = $repository->getById($product->getId());
                    $data = $product->getTypeInstance()->getConfigurableOptions($product);
                    $options = [];
                    foreach ($config_data['attributes'] as $attrKey => $attrValue) {
                        $attribute = $this->_eavConfig->getAttribute('catalog_product', $attrValue['code'])->setStoreId($storeId);
                        $attrOptions = $attribute->getSource()->getAllOptions();
                        foreach ($config_data['attributes'][$attrKey]['options'] as $opKey => $opValue) {
                            foreach ($attrOptions as $attrOpKey => $attrOpValue) {
                                if ($attrOpValue['value'] == $opValue['id']) {
                                    $config_data['attributes'][$attrKey]['options'][$opKey]['label'] = $attrOpValue['label'];
                                }
                            }
                        }
                    }
                    foreach($data as $attr){
                        foreach($attr as $atr){
                            $options[$atr['sku']][$atr['attribute_code']] = $atr['option_title'];
                            $associated_product = $repository->get($atr['sku']);
                            $base_price = $this->_cartHelper->getPriceByStoreWithCurrency($associated_product->getPrice(), $storeId, $currencyCode);
                            $special_price = $this->_cartHelper->getPriceByStoreWithCurrency($associated_product->getSpecialPrice(), $storeId, $currencyCode);
                            $save_discount = $this->_cartHelper->getDiscount($associated_product->getId());
                            $config_data['optionPrices'][$associated_product->getId()]['allPrices']['default_price'] = (string) number_format((float)$this->_cartHelper->getPriceByStoreWithoutCurrency($associated_product->getPrice(), $storeId, $currencyCode), 2, '.', '');
                            $config_data['optionPrices'][$associated_product->getId()]['allPrices']['price'] = $base_price;
                            $config_data['optionPrices'][$associated_product->getId()]['allPrices']['default_special_price'] = (string) number_format((float)$this->_cartHelper->getPriceByStoreWithoutCurrency($associated_product->getSpecialPrice(), $storeId, $currencyCode), 2, '.', '');
                            $config_data['optionPrices'][$associated_product->getId()]['allPrices']['special_price'] = $special_price;
                            $config_data['optionPrices'][$associated_product->getId()]['allPrices']['save_discount'] = $save_discount;
                        }
                    }

                    $customOptions = $this->_productOptionModel->getProductOptionCollection($product);

                    if (!empty($customOptions)) {
                        foreach ($customOptions as $option) {
                            
                            $optionValues = $product->getOptionById($option->getId());

                            $optionType = $option->getType();
                            if (!empty($optionValues->getValues())) {
                                $values = $optionValues->getValues();
                            } else {
                                $values = [];
                            }

                            $config_data['custom_option'][$option->getId()]['title'] = $option->getTitle();
                            if ($option->getTitle() == "Choose Product Color") {
                                $config_data['custom_option'][$option->getId()]['type'] = 'color';
                            } else {
                                $config_data['custom_option'][$option->getId()]['type'] = $optionType;
                            }
                            $config_data['custom_option'][$option->getId()]['is_required'] = $option->getIsRequire();
                            $config_data['custom_option'][$option->getId()]['order'] = $option->getSortOrder();
                            $temp_config_data = [];
                            if (!empty($values) && count($values) > 0) {
                                foreach ($values as $key => $value) {
                                    $temp_config_data[] = [
                                        'option_type_id' => $value->getOptionTypeId(),
                                        'option_id' => $value->getOptionId(),
                                        'sku' => $value->getData('sku'),
                                        'default_title' => $value->getData('default_title'),
                                        'title' => $value->getData('title'),
                                        'default_price' => (string) number_format((float)$this->_cartHelper->getPriceByStoreWithoutCurrency($value->getData('default_price'), $storeId, $currencyCode), 2, '.', ''),
                                        'default_price_type' => $value->getData('default_price_type'),
                                        'price' => $this->_cartHelper->getPriceByStoreWithCurrency($value->getData('price'), $storeId, $currencyCode),
                                        'price_type' => $value->getData('price_type'),
                                        'max_characters' => ($value->getData('max_characters') != "") ? (int) $value->getData('max_characters') : 0
                                    ];
                                }
                            } else {
                                if ($optionType == 'file') {
                                    $temp_config_data[] = [
                                        // 'option_type_id' => $option->getOptionTypeId(),
                                        'option_id' => $option->getOptionId(),
                                        'sku' => $option->getData('sku'),
                                        'default_title' => $option->getData('default_title'),
                                        'title' => $option->getData('title'),
                                        'default_price' => (string) number_format((float)$this->_cartHelper->getPriceByStoreWithoutCurrency($option->getData('default_price'), $storeId, $currencyCode), 2, '.', ''),
                                        'default_price_type' => $option->getData('default_price_type'),
                                        'price' => $this->_cartHelper->getPriceByStoreWithCurrency($option->getData('price'), $storeId, $currencyCode),
                                        'price_type' => $option->getData('price_type'),
                                        'type' => $optionType,
                                        'max_characters' => ($option->getData('max_characters') != "") ? (int) $option->getData('max_characters') : 0,
                                        'compatible_file_extensions' => $option->getData('file_extension'),
                                        'max_file_width' => $option->getData('image_size_x'),
                                        'max_file_height' => $option->getData('image_size_y')
                                    ];
                                } else {
                                    $temp_config_data[] = [
                                        // 'option_type_id' => $option->getOptionTypeId(),
                                        'option_id' => $option->getOptionId(),
                                        'sku' => $option->getData('sku'),
                                        'default_title' => $option->getData('default_title'),
                                        'title' => $option->getData('title'),
                                        'default_price' => (string) number_format((float)$this->_cartHelper->getPriceByStoreWithoutCurrency($option->getData('default_price'), $storeId, $currencyCode), 2, '.', ''),
                                        'default_price_type' => $option->getData('default_price_type'),
                                        'price' => $this->_cartHelper->getPriceByStoreWithCurrency($option->getData('price'), $storeId, $currencyCode),
                                        'price_type' => $option->getData('price_type'),
                                        'type' => $optionType,
                                        'max_characters' => ($option->getData('max_characters') != "") ? (int) $option->getData('max_characters') : 0
                                    ];
                                }
                            }
                            $config_data['custom_option'][$option->getId()]['config_option'] = $temp_config_data;
                            unset($temp_config_data);
                        }
                    }

                    foreach ($configArray['attributes'] as $attrKey => $attrArray) {
                        $attributeKey[] = $attrKey;
                        $attributeData = $product->getResource()->getAttribute('color')->getData();
                        if (array_key_exists('swatch_input_type', $attributeData)) {
                            $attributeType = $attributeData['swatch_input_type'];
                        } else {
                            $attributeType = 'drop_down';
                        }
                        $config_data['attributes'][$attrKey]['attribute_type'] = $attributeType;
                        if ($attrArray['code'] == 'color') {
                            
                            $colorOptions = $attrArray['options'];
                            foreach ($colorOptions as $key => $colorcode) {
                                $colorlabel = $colorcode['label'];
                                $colorid = $colorcode['id'];
                                if ($attributeType != 'drop_down') {
                                    $hashcodeData = $this->_swatchHelper->getSwatchesByOptionsId([$colorid]);
                                    $url = $hashcodeData[$colorid]['value'];
                                    $config_data['attributes'][$attrKey]['options'][$key]['image'] = $url;
                                    unset($url);
                                }
                            }
                        }
                    }
                    $config_data['attribute_keys'] = $attributeKey;
                }
            }
            if (isset($linkidsCollection)) {
                $linkids['link_details'] = $linkidsCollection;
            }
            if ($status) {
                $productResultArr = ['status' => $status, 'type' => $product->getTypeId(), 'config' => $config_data, 'links' => $linkids];
            } else {
                $productResultArr = ['status' => $status, 'error' => 'Product is not saleable.', 'type' => $product->getTypeId(), 'config' => $config_data, 'links' => $linkids];
            }
            return $productResultArr;
        } catch (\Exception $e) {
            $productResultArr = [
                'status' => 'false',
                'message' => __($e->getMessage())
            ];
            return $productResultArr;
        }
    }
    protected function checkwishlist($productID)
    {
        $wishlistArray = array('in_wishlist' => false, 'wishlist_item_id' => null);
        if ($this->_wishlistHelper->isAllow()) {
            foreach ($this->_wishlistHelper->getWishlistItemCollection() as $wishlistItem) {
                if ($productID == $wishlistItem->getProduct()->getId()) {
                    $wishlistArray = array('in_wishlist' => true, 'wishlist_item_id' => $wishlistItem->getId());
                    break;
                }
            }
        }
        return $wishlistArray;
    }
    protected function getProductRatingSummary($storeId, $productId)
    {
        $reviewSummary = array('review_count' => null, 'rating_summary' => null);
        $model = $this->_reviewSummaryModel->setStoreId($storeId)->load($productId);
        $reviewSummary = array('review_count' => $model->getReviewsCount(), 'rating_summary' => $model->getRatingSummary());
        return $reviewSummary;
    }
    private function getSalebleQty($productSku)
    {
        $code = 'base';
        $type = 'website';
        $finalSalesData = [];
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $salesChannelId = $objectManager->get('Magento\InventorySales\Model\ResourceModel\StockIdResolver')->resolve($type, $code);
            $stockName = $objectManager->get('Magento\Inventory\Model\Stock')->load($salesChannelId);
            $stockName = $stockName->getName();
            // $allowProduct = $objectManager->get('Magento\InventorySales\Model\IsProductSalableCondition\IsProductSalableConditionChain')->execute($productSku, $salesChannelId);
            $this->_salebleModel = $objectManager->get('Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku');
            $salebleModelData = $this->_salebleModel->execute((string)$productSku);
            foreach ($salebleModelData as $key => $value) {
                if ($value['stock_name'] == $stockName) {
                    $finalSalesData['stock_name'] = $value['stock_name'];
                    $finalSalesData['qty'] = $value['qty'];
                    $finalSalesData['manage_stock'] = $value['manage_stock'];
                }
            }
            return $finalSalesData;
        } catch (\Exception $e) {
            $salebleModelData = [];
            return $salebleModelData;
        }
        return $salebleModelData;
    }

    public function getProductAdditionalInfo($productId, $storeId = 1)
    {
        try {
            $product = $this->_productFactory->create()->setStoreId($storeId)->load($productId);
            $attributes = $product->getAttributes();
            $additionalData = [];
            foreach ($attributes as $attribute) {
                if ($attribute->getIsVisibleOnFront()) {
                    $value = $attribute->getFrontend()->getValue($product);
                    if (!$product->hasData($attribute->getAttributeCode())) {
                        $value = __('N/A');
                    } elseif ((string)$value == '') {
                        $value = __('No');
                    } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
                        $value = $this->priceCurrency->convertAndFormat($value);
                    }
                    if (is_string($value) && strlen($value)) {
                        if ($attribute->getAttributeCode() != "prrmaresolution0") {
                            $additionalData[$attribute->getAttributeCode()] = [
                                'label' => $attribute->getStoreLabel(),
                                'value' => $value,
                                'code' => $attribute->getAttributeCode(),
                            ];
                        }
                    }
                }
            }

            $additionalInfo = "";
            if (count($additionalData) > 0) {
                $additionalInfo .= "<table border='0' cellspacing='10'>";
                foreach ($additionalData as $key => $additionalDatavalue) {
                    $additionalInfo .= "<tr>"."<td><b>".$additionalDatavalue['label']."</b></td>"."<td>".$additionalDatavalue['value']."</td>"."</tr>";
                }
                $additionalInfo .= "</table>";
            }

            return $additionalInfo;
        } catch (\Exception $e) {
            $additionalInfo = __($e->getMessage());
            return $additionalInfo;
        }
    }
}
