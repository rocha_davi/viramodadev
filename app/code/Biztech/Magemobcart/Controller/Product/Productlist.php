<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Product;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Action\Context;

class Productlist extends \Magento\Framework\App\Action\Action
{
    protected $_jsonFactory;
    protected $_request;
    protected $_storeManager;
    protected $_cartHelper;
    protected $_imageHelper;
    protected $_wishlistHelper;
    protected $_reviewSummaryModel;
    protected $_formKey;
    protected $_stockApiRepository;

    protected $_productRepositoryInterface;
    protected $_objectManager;
    protected $_stockFilter;
    protected $_productCollectionFactory;
    protected $_categoryFactory;
    protected $_layerResolver;
    protected $_filterableAttributes;
    protected $_configurableProductModel;

    /**
     * @param Context                                                           $context
     * @param JsonFactory                                                       $jsonFactory
     * @param Http                                                              $request
     * @param \Magento\Store\Model\StoreManagerInterface                        $storeManager
     * @param \Magento\Catalog\Model\Product                                    $productModel
     * @param \Biztech\Magemobcart\Helper\Data                                  $cartHelper
     * @param \Magento\Catalog\Helper\Image                                     $imageHelper
     * @param \Magento\Wishlist\Helper\Data                                     $wishlistHelper
     * @param \Magento\Review\Model\Review\Summary                              $reviewSummaryModel
     * @param \Magento\Framework\Data\Form\FormKey                              $formKey
     * @param \Magento\Catalog\Api\ProductRepositoryInterface                   $productRepositoryInterface
     * @param \Magento\Catalog\Model\ProductFactory                             $productFactory
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface              $stockApiRepository
     * @param \Magento\CatalogInventory\Helper\Stock                            $stockFilter
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory    $productCollectionFactory
     * @param \Magento\Catalog\Model\CategoryFactory                            $categoryFactory
     * @param \Magento\Catalog\Model\Layer\Resolver                             $layerResolver
     * @param \Magento\Catalog\Model\Layer\Category\FilterableAttributeList     $filterableAttributes
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable     $configurableProductModel
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        Http $request,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Biztech\Magemobcart\Helper\Data $cartHelper,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Wishlist\Helper\Data $wishlistHelper,
        \Magento\Review\Model\Review\Summary $reviewSummaryModel,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockApiRepository,
        \Magento\CatalogInventory\Helper\Stock $stockFilter,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        \Magento\Catalog\Model\Layer\Category\FilterableAttributeList $filterableAttributes,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableProductModel
    ) {
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_storeManager = $storeManager;
        $this->_cartHelper = $cartHelper;
        $this->_imageHelper = $imageHelper;
        $this->_wishlistHelper = $wishlistHelper;
        $this->_reviewSummaryModel = $reviewSummaryModel;
        $this->_formKey = $formKey;
        $this->_request->setParam('form_key', $this->_formKey->getFormKey());
        $this->_stockApiRepository = $stockApiRepository;

        $this->_productRepositoryInterface = $productRepositoryInterface;
        $this->_productFactory = $productFactory;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_stockFilter = $stockFilter;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->_layerResolver = $layerResolver;
        $this->_filterableAttributes = $filterableAttributes;
        $this->_configurableProductModel = $configurableProductModel;
        parent::__construct($context);
    }

    /**
     * This function is used for get all categories list with tree structure.
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_cartHelper->isEnable()) {
            if (!$this->_cartHelper->getHeaders()) {
                $errorResult = ['status'=> false,'message' => $this->_cartHelper->getHeaderMessage()];
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $postData = $this->_request->getParams();
            if (array_key_exists("product_name", $postData)) {
                $product_name = $postData['product_name'];
            } else {
                $product_name = "";
            }

            if (array_key_exists('categoryid', $postData)) {
                $catId = $postData['categoryid'];
                $categoryId = $postData['categoryid'];
            } else {
                $catId = null;
                $categoryId = null;
            }

            if (array_key_exists('storeid', $postData) && $postData['storeid'] != '') {
                $storeId = $postData['storeid'];
            } else {
                $errorResult = ['status'=> false,'message' => __("Store id is missing")];
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }

            if (array_key_exists('currency_code', $postData) && $postData['currency_code'] != '') {
                $currencyCode = $postData['currency_code'];
            } else {
                $currencyCode = $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();
            }
            
            // Below code is to set requested store and currency
            $currentStoreId = $this->_storeManager->getStore()->getId();
            $currentCurrencyCode = $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();
            $this->_storeManager->setCurrentStore($storeId);
            $this->_storeManager->getStore($storeId)->setCurrentCurrencyCode($currencyCode);
            
            if (array_key_exists('limit', $postData) && isset($postData['limit']) && !empty($postData['limit'])) {
                $limit = $postData['limit'];
            } else {
                $limit = 10;
            }
            if (array_key_exists('page', $postData) && isset($postData['page']) && !empty($postData['page'])) {
                $page = $postData['page'];
            } else {
                $page = 1;
            }
            
            if (array_key_exists('attributes', $postData)) {
                $attributesFilterBy = json_decode($postData['attributes'], true);
            } else {
                $attributesFilterBy = [];
            }
            
            $optionIds = [];
            
            try {
                if (isset($catId) && $catId != "") {
                    $category = $this->_categoryFactory->create()->setStoreId($storeId)->load($categoryId);

                    foreach ($attributesFilterBy as $key => $value) {
                        $optionIds[$value['attribute_code']] = explode(',', $value['option_id']);
                    }
                    $filters = $optionIds;

                    $productCollection = $this->_productCollectionFactory->create();
                    $productCollection->addStoreFilter($storeId)
                    ->addMinimalPrice()
                    ->addFinalPrice()
                    ->addUrlRewrite()->addAttributeToSelect('*')
                    ->addCategoriesFilter(['in' => [$category->getId()]]);
                    $productCollection->addAttributeToFilter('status', ["eq" => 1]);
                    $productCollection->addAttributeToFilter('type_id', [
                                                                    "in" => [
                                                                        'simple',
                                                                        'grouped',
                                                                        'configurable',
                                                                        'virtual',
                                                                        'downloadable'
                                                                        ]
                                                                ]);
                    $productCollection->addAttributeToFilter('visibility', array('neq' => 1));
                    $configChild = [];
                    foreach ($productCollection as $key => $_product) {
                        if ($_product->getTypeId() == 'configurable') {
                            $configChild[$key] = $_product->getTypeInstance()->getUsedProductIds($_product);
                        }
                    }
                    $prodIds = array();
                    foreach ($configChild as $key => $value) {
                        foreach ($value as $key => $childIds) {
                            $prodIds[] = $childIds;
                        }
                    }
                    $collectionIds = array_merge($productCollection->getAllIds(), $prodIds);

                    $collection = $this->_productCollectionFactory->create()
                                ->addFieldToFilter('entity_id', array('in' => $collectionIds));
                    $collection->addStoreFilter($storeId)
                    ->addMinimalPrice()
                    ->addFinalPrice()
                    ->addUrlRewrite()->addAttributeToSelect('*');
                    $collection->addAttributeToFilter('status', ["eq" => 1]);
                    $collection->addAttributeToFilter('type_id', [
                                                                    "in" => [
                                                                        'simple',
                                                                        'grouped',
                                                                        'configurable',
                                                                        'virtual',
                                                                        'downloadable'
                                                                        ]
                                                                ]);
                    $collection->addAttributeToFilter('visibility', array('neq' => 1));
                    $collection->load();

                    foreach ($filters as $key => $value) {
                        if ($key == "price") {
                            $value = array_map('strval', $value);
                        } else {
                            $value = array_map('intval', $value);
                        }
                        if ($key == 'price') {
                            foreach ($value as $data) {
                                $priceFilter[] = explode('-', $data);
                            }
                            foreach ($priceFilter as $key => $value11) {
                                if ($value11[0] == '') {
                                    $priceFilterArray[$key]['from'] = '0';
                                } else {
                                    $priceFilterArray[$key]['from'] = $value11[0];
                                }
                                if ($value11[1] == '') {
                                } else {
                                    $priceFilterArray[$key]['to'] = $value11[1];
                                }
                            }
                            $priceFilterQuery = '';
                            foreach ($priceFilterArray as $key => $value) {
                                $priceFilterQuery .= '(price_index.min_price >= '.$value["from"];
                                if (array_key_exists('to', $value)) {
                                    $priceFilterQuery .= ' AND price_index.min_price <= '.$value["to"];
                                }
                                $priceFilterQuery .= ') OR ';
                            }
                            $priceFilterQuery = rtrim($priceFilterQuery, " OR  ");
                            $collection->getSelect()->where($priceFilterQuery);
                        } elseif ($key == 'cat') {
                            $collection->addCategoriesFilter(['in' => $value]);
                        } else {
                            $collection->addAttributeToFilter($key, ['in' => $value]);
                        }
                    }
                } else {
                    $collection = $this->_productCollectionFactory->create();
                    $product_name_words = [];
                    if ($product_name != null) {
                        $product_name = str_replace(' ', '%', $product_name);
                        $collection->addStoreFilter($storeId)
                        ->addMinimalPrice()
                        ->addFinalPrice()
                        ->addUrlRewrite()->addAttributeToSelect('*');
                        $collection->addAttributeToFilter('name', ['like' => '%' . $product_name . '%'])->load();
                        $collection->addAttributeToFilter('visibility', array('neq' => 1));
                    }
                }

                $proArray = [];
                foreach ($collection->getData() as $product) {
                    if ($mainProduct = $this->_configurableProductModel->getParentIdsByChild($product['entity_id'])) {
                        $proArray[] = $mainProduct[0];
                    } else {
                        $proArray[] = $product['entity_id'];
                    }
                }

                $newCollectionIds = array_unique($proArray);
                if (empty($newCollectionIds)) {
                    if ($catId) {
                        $result = array('message' => __('No such product found.'));
                        $jsonResult->setData($result);
                        return $jsonResult;
                    }
                }

                $newCollection = $this->_productCollectionFactory->create();
                $newCollection->addFieldToFilter('entity_id', array('in' => $newCollectionIds))
                ->addAttributeToSelect('*')
                ->addStoreFilter($storeId)
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addUrlRewrite()
                ->addAttributeToFilter('status', ["eq" => 1]);

                if (array_key_exists('dir', $postData)) {
                    if (array_key_exists('order', $postData)) {
                        $direction = $postData['dir'];
                        $sort_by = $postData['order'];
                        if ($sort_by != null && $direction != null) {
                            // $collection->setOrder($sort_by, $direction);
                            $newCollection->addAttributeToSort($sort_by, $direction);
                        }
                    }
                }

                /* below code used to sort by stock filter */
                // $collection->getSelect()->joinLeft(
                //  array('_inventory_table' => $collection->getTable('cataloginventory/stock_item')),
                //  "_inventory_table.product_id = e.entity_id",
                //  ['is_in_stock']
                // )
                // ->order('is_in_stock DESC');

                /* another way to filter out_of_stock products */
                /* require : \Magento\CatalogInventory\Helper\Stock::addInStockFilterToCollection*/
                // $this->_stockFilter->addInStockFilterToCollection($collection);

                // $collection1 = $collection;
                // $totalcount = count($collection1->getData());
                $totalcount = $newCollection->getSize();

                if (isset($limit) && !empty($limit)) {
                    $newCollection->setPageSize($limit);
                }
                if (isset($page) && !empty($page)) {
                    $newCollection->setCurPage($page);
                }

                $product_list = [];

                foreach ($newCollection->getData() as $key => $product) {
                    $value = $product['entity_id'];
                    $summaryData = [];
                    $summaryData = $this->getProductRatingSummary($storeId, $value);
                    $wishlist_detail = $this->checkwishlist($value);
                    $product_data = $this->_productFactory->create()->setStoreId($storeId)->load($value);
                    // $product_data = $this->_productRepositoryInterface->getById($value);
                    $status = $product_data->getStatus();
                    $productTypeId = $product_data->getTypeId();

                    $productStockData = $this->_stockApiRepository->getStockItem($product['entity_id']);
                    $pro_qty = $productStockData->getQty();
                    $isInStock = $productStockData->getIsInStock();
                    $salebleModelData = $this->getSalebleQty($product['sku']);
                    if (!empty($salebleModelData)) {
                        if (array_key_exists('qty', $salebleModelData) && $salebleModelData['qty'] != '') {
                            $pro_qty = $salebleModelData['qty'];
                        } else {
                            $pro_qty = $productStockData->getQty();
                        }
                    } else {
                        $pro_qty = $productStockData->getQty();
                    }
                    $stock_item = $productStockData;
                    if ($stock_item->getBackorders() != 0) {
                        if ($isInStock == 0) {
                            $isInStock = '0';
                        } else {
                            $isInStock = '1';
                        }
                    } else {
                        if (($pro_qty < 0 || $isInStock == 0) && $productTypeId != 'configurable') {
                            $pro_qty = 'Out of Stock';
                            $isInStock = '0';
                        } elseif ($productTypeId == 'configurable') {
                            $children = $product_data->getTypeInstance()->getUsedProducts($product_data);
                            foreach ($children as $child) {
                                if ($this->_stockApiRepository->getStockItem($child->getId())->getQty() > 0) {
                                    $isInStock = '1';
                                    $pro_qty = $productStockData->getQty();
                                    break;
                                } else {
                                    $pro_qty = 'Out of Stock';
                                    $isInStock = '0';
                                }
                            }
                        } else {
                            $isInStock = '1';
                        }
                    }

                    // $currentCurrencyCode = $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();
                    if ($productTypeId == 'grouped') {
                        $aProductIds = $product_data->getTypeInstance()->getChildrenIds($product_data->getId());
                        $prices = [];
                        foreach ($aProductIds as $ids) {
                            foreach ($ids as $id) {
                                $aProduct = $this->_productFactory->create()->load($id);
                                $prices[] = $aProduct->getPriceModel()->getPrice($aProduct);
                            }
                        }
                        krsort($prices);
                        $price_array = array_keys($prices);
                        $price = $this->_cartHelper->getPriceByStoreWithCurrency(min($prices), $storeId, $currencyCode);
                    } elseif ($productTypeId == 'configurable') {
                        $price = $this->_cartHelper->getPriceByStoreWithCurrency($product_data->getFinalPrice(), $storeId, $currencyCode);
                    } else {
                        $price = $this->_cartHelper->getPriceByStoreWithCurrency($product_data->getPrice(), $storeId, $currencyCode);
                    }

                    if ($productTypeId == 'simple' || $productTypeId == 'virtual') {
                        if ($product_data->getData('has_options')) {
                            $hasOptions = "1";
                        } else {
                            $hasOptions = "0";
                        }
                    } elseif ($productTypeId == 'configurable' || $productTypeId == 'grouped' || $productTypeId == 'bundle') {
                        $hasOptions = "1";
                    } elseif ($productTypeId == 'downloadable') {
                        $hasOptions = $product_data->getData('links_purchased_separately');
                    }
                    $byiUrl = '';
                    $isByiExists = '';
                    $page = (array_key_exists('page', $postData) && !empty($postData['page'])) ? $postData['page'] : 1;
                    
                    $specialPrice = $this->_cartHelper->getPriceByStoreWithCurrency($product_data->getSpecialPrice(), $storeId, $currencyCode);
                    if ($product_data->getId() != null && $product_data->getId() != '') {
                        $product_list[] = [
                            'id' => $product_data->getId(),
                            'sku' => $product_data->getSku(),
                            'name' => $product_data->getName(),
                            'status' => $status,
                            'qty' => (string)$pro_qty,
                            'in_stock' => $isInStock,
                            'price' => $price,
                            'special_price' => $specialPrice,
                            'image' => $this->_imageHelper->init($product_data, 'product_page_image_medium')->resize(300, 330)->constrainOnly(true)->keepAspectRatio(true)->getUrl(),
                            'type' => $productTypeId,
                            'is_wishlisted' => $wishlist_detail['in_wishlist'],
                            'wishlist_item_id' => $wishlist_detail['wishlist_item_id'],
                            'review_count' => $summaryData['review_count'],
                            'average_rating' => $summaryData['rating_summary'],
                            'has_options' => $hasOptions,
                            // 'byi_url_id' => $isByiExists,
                            // 'byi_url' => $byiUrl,
                            'save_discount' => $this->_cartHelper->getDiscount($product_data->getId())
                        ];
                        if (isset($isByiExists) && !empty($isByiExists)) {
                            $product_list['byi_url_id'] = $isByiExists;
                        }
                        if (isset($byiUrl) && !empty($byiUrl)) {
                            $product_list['byi_url'] = $byiUrl;
                        }
                    }
                }

                if ($page + 1 <= ceil($totalcount / $limit)) {
                    $nextCountPage = $page + 1;
                } else {
                    $nextCountPage = "";
                }

                // Below code is to reset to deafult current store and current currency
                $this->_storeManager->getStore($storeId)->setCurrentCurrencyCode($currentCurrencyCode);
                $this->_storeManager->setCurrentStore($currentStoreId);

                $response = ['productCollection' => $product_list];
                $response['item_count'] = $totalcount;
                $response['current_page'] = $page;
                $response['next_page'] = $nextCountPage;
                $jsonResult->setData($response);
                return $jsonResult;

            } catch (\Exception $e) {
                $productResultArr = [
                    'status' => 'false',
                    'message' => __($e->getMessage())
                ];
                $jsonResult->setData($productResultArr);
                return $jsonResult;
            }
        } else {
            $returnExtensionArray = ['enable' => false];
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }
    protected function checkwishlist($productID)
    {
        $wishlistArray = ['in_wishlist' => false, 'wishlist_item_id' => null];
        if ($this->_wishlistHelper->isAllow()) {
            foreach ($this->_wishlistHelper->getWishlistItemCollection() as $wishlistItem) {
                if ($productID == $wishlistItem->getProduct()->getId()) {
                    $wishlistArray = ['in_wishlist' => true, 'wishlist_item_id' => $wishlistItem->getId()];
                    break;
                }
            }
        }
        return $wishlistArray;
    }
    protected function getProductRatingSummary($storeId, $productId)
    {
        $reviewSummary = ['review_count' => null, 'rating_summary' => null];
        $model = $this->_reviewSummaryModel->setStoreId($storeId)->load($productId);
        $reviewSummary = ['review_count' => $model->getReviewsCount(), 'rating_summary' => $model->getRatingSummary()];
        return $reviewSummary;
    }
    private function getSalebleQty($productSku)
    {
        if (!$this->checkMsiEnabled()) {
            $salebleModelData =array();
            return $salebleModelData;
        }
        try {
            $this->_salebleModel = $this->_objectManager->get('Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku');
            $salebleModelData = $this->_salebleModel->execute((string)$productSku);
            return $salebleModelData;
        } catch (\Exception $e) {
            $salebleModelData = array();
            return $salebleModelData;
        }
        return $salebleModelData;
    }
    private function checkMsiEnabled()
    {
        $allowNormal = false;
        $moduleManager = $this->_objectManager->get('Magento\Framework\Module\Manager');
        if ($moduleManager->isEnabled('Magento_Inventory')) {
            $allowNormal = true;
        }
        return $allowNormal;
    }
}
