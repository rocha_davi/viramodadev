<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Cart;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Context;

class Getcustomershoppinglist extends \Magento\Framework\App\Action\Action
{
    protected $jsonFactory;
    protected $storeManager;
    protected $cartHelper;
    protected $request;
    protected $customerSession;
    protected $cartModel;
    protected $cartSession;
    protected $formKey;

    /**
     * @param Context                                       $context
     * @param JsonFactory                                   $jsonFactory
     * @param \Magento\Store\Model\StoreManagerInterface    $storeManager
     * @param \Magento\Framework\App\Request\Http           $request
     * @param \Biztech\Magemobcart\Helper\Data              $cartHelper
     * @param \Magento\Customer\Model\Session               $customerSession
     * @param \Magento\Checkout\Model\Cart                  $cartModel
     * @param \Magento\Checkout\Model\Session               $cartSession
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Request\Http $request,
        \Biztech\Magemobcart\Helper\Data $cartHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Cart $cartModel,
        \Magento\Checkout\Model\Session $cartSession,
        \Magento\Framework\Data\Form\FormKey $formKey
    ) {
        $this->_jsonFactory = $jsonFactory;
        $this->_storeManager = $storeManager;
        $this->_cartHelper = $cartHelper;
        $this->_request = $request;
        $this->_customerSession = $customerSession;
        $this->_cartModel = $cartModel;
        $this->_cartSession = $cartSession;
        $this->formKey = $formKey;
        $this->_request->setParam('form_key', $this->formKey->getFormKey());
        parent::__construct($context);
    }

    /**
     * This function is used for get customer shopping list.
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_cartHelper->isEnable()) {
            if (!$this->_cartHelper->getHeaders()) {
                $errorResult = array('status'=> false,'message' => $this->_cartHelper->getHeaderMessage());
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $session = $this->_customerSession;
            $responseArr['status'] = 'false';
            $postData = $this->_request->getParams();
            $storeId = (isset($postData['storeid']) && $postData['storeid'] != '') ? $postData['storeid'] : $this->_storeManager->getStore()->getId();
            $currencyCode = (isset($postData['currency_code']) && $postData['currency_code'] != '') ? $postData['currency_code'] : $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();
            $sessionId = '';
            if (isset($postData['session_id']) && $postData['session_id'] != null) {
                $sessionId = $postData['session_id'];
                if (!$this->_customerSession->isLoggedIn()) {
                    $customer_id = explode("_", $sessionId);
                    $this->_cartHelper->relogin($customer_id[0]);
                }
            }
            $cart = $this->_getCart();
            $totals = $this->_cartSession->getQuote()->getTotals();
            $items = $cart->getQuote()->getAllVisibleItems();
            $cartData = $this->_cartHelper->getCartData($items, $totals, 'displaycart', '', $storeId, $currencyCode);
            return $cartData;
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }
    protected function _getCart()
    {
        return $this->_cartModel;
    }
}
