<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * Last updated @18-04-2020.
 */
namespace Biztech\Magemobcart\Controller\Homepage;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;

class Index extends \Magento\Framework\App\Action\Action
{
    const XML_PATH_COLOR = 'magemobcart/themeselection/primary_background';
    const XML_PATH_SECONDARYCOLOR = 'magemobcart/themeselection/secondary_background';
    const XML_PATH_BGCOLOR = 'magemobcart/themeselection/background_color';
    const XML_PATH_BTNTXTCOLOR = 'magemobcart/themeselection/button_text_color';

    protected $jsonFactory;
    protected $request;
    protected $storeManager;
    protected $productModel;
    protected $cartHelper;
    protected $stockItemRepository;
    protected $imageHelper;
    protected $wishlistHelper;
    protected $reviewSummaryModel;
    protected $customerSession;
    protected $cartModel;
    protected $scopeConfig;
    protected $catalogruleModel;
    protected $stockInterface;
    protected $productFactory;
    protected $notificationHistoryModel;
    protected $notificationHelper;
    protected $localeDate;

    protected $_layoutModel;
    protected $_layoutComponentModel;
    protected $_layoutComponentFactory;
    protected $_bannerSliderFactory;
    protected $_productGridFactory;
    protected $_productHorizontalSliderFactory;
    protected $_featuredCategoryBlocksFactory;
    protected $_fcbDisplayTypeFactory;
    protected $_productRepositoryFactory;
    protected $_filesystem;
    protected $formKey;

    /**
     * @param Context                                                       $context
     * @param JsonFactory                                                   $jsonFactory
     * @param Http                                                          $request
     * @param \Magento\Store\Model\StoreManagerInterface                    $storeManager
     * @param \Magento\Catalog\Model\Product                                $productModel
     * @param \Magento\Catalog\Model\ProductFactory                         $productFactory
     * @param \Biztech\Magemobcart\Helper\Data                              $cartHelper
     * @param \Magento\CatalogInventory\Model\Stock\StockItemRepository     $stockItemRepository
     * @param \Magento\Catalog\Helper\Image                                 $imageHelper
     * @param \Magento\Wishlist\Helper\Data                                 $wishlistHelper
     * @param \Magento\Review\Model\Review\Summary                          $reviewSummaryModel
     * @param \Magento\Customer\Model\Session                               $customerSession
     * @param \Magento\Checkout\Model\Cart                                  $cartModel
     * @param \Magento\Framework\App\Config\ScopeConfigInterface            $scopeConfig
     * @param \Magento\CatalogRule\Model\Rule                               $catalogruleModel
     * @param \Magento\CatalogInventory\Api\StockStateInterface             $stockInterface
     * @param \Biztech\Magemobcart\Model\Notificationhistory                $notificationHistoryModel
     * @param \Biztech\Magemobcart\Helper\Notification                      $notificationHelper
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface          $localeDate
     * @param \Biztech\Magemobcart\Model\Layout                             $layoutModel
     * @param \Biztech\Magemobcart\Model\Layoutcomponent                    $layoutComponentModel
     * @param \Biztech\Magemobcart\Model\LayoutcomponentFactory             $layoutComponentFactory
     * @param \Biztech\Magemobcart\Model\Bannerslider1Factory               $bannerSliderFactory
     * @param \Biztech\Magemobcart\Model\ProductgridFactory                 $productGridFactory
     * @param \Biztech\Magemobcart\Model\ProducthorizontalslidingFactory    $productHorizontalSliderFactory
     * @param \Biztech\Magemobcart\Model\FeaturedcategoryblocksFactory      $featuredCategoryBlocksFactory
     * @param \Biztech\Magemobcart\Model\FcbdisplaytypeFactory              $fcbDisplayTypeFactory
     * @param \Magento\Catalog\Model\ProductRepositoryFactory               $productRepositoryFactory
     * @param \Magento\Framework\Filesystem                                 $filesystem
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        Http $request,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Biztech\Magemobcart\Helper\Data $cartHelper,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Wishlist\Helper\Data $wishlistHelper,
        \Magento\Review\Model\Review\Summary $reviewSummaryModel,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Cart $cartModel,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\CatalogRule\Model\Rule $catalogruleModel,
        \Magento\CatalogInventory\Api\StockStateInterface $stockInterface,
        \Biztech\Magemobcart\Model\Notificationhistory $notificationHistoryModel,
        \Biztech\Magemobcart\Helper\Notification $notificationHelper,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Biztech\Magemobcart\Model\Layout $layoutModel,
        \Biztech\Magemobcart\Model\Layoutcomponent $layoutComponentModel,
        \Biztech\Magemobcart\Model\LayoutcomponentFactory $layoutComponentFactory,
        \Biztech\Magemobcart\Model\Bannerslider1Factory $bannerSliderFactory,
        \Biztech\Magemobcart\Model\ProductgridFactory $productGridFactory,
        \Biztech\Magemobcart\Model\ProducthorizontalslidingFactory $productHorizontalSliderFactory,
        \Biztech\Magemobcart\Model\FeaturedcategoryblocksFactory $featuredCategoryBlocksFactory,
        \Biztech\Magemobcart\Model\FcbdisplaytypeFactory $fcbDisplayTypeFactory,
        \Magento\Catalog\Model\ProductRepositoryFactory $productRepositoryFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Data\Form\FormKey $formKey
    ) {
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_storeManager = $storeManager;
        $this->_productModel = $productModel;
        $this->_cartHelper = $cartHelper;
        $this->_stockItemRepository = $stockItemRepository;
        $this->_imageHelper = $imageHelper;
        $this->_wishlistHelper = $wishlistHelper;
        $this->_reviewSummaryModel = $reviewSummaryModel;
        $this->_customerSession = $customerSession;
        $this->_cartModel = $cartModel;
        $this->_scopeConfig = $scopeConfig;
        $this->_catalogruleModel = $catalogruleModel;
        $this->_stockInterface = $stockInterface;
        $this->_productFactory = $productFactory;
        $this->_notificationHistoryModel = $notificationHistoryModel;
        $this->_notificationHelper = $notificationHelper;
        $this->_localeDate = $localeDate;
        $this->_layoutModel = $layoutModel;
        $this->_layoutComponentModel = $layoutComponentModel;
        $this->_layoutComponentFactory = $layoutComponentFactory;
        $this->_bannerSliderFactory = $bannerSliderFactory;
        $this->_productGridFactory = $productGridFactory;
        $this->_productHorizontalSliderFactory = $productHorizontalSliderFactory;
        $this->_featuredCategoryBlocksFactory = $featuredCategoryBlocksFactory;
        $this->_fcbDisplayTypeFactory = $fcbDisplayTypeFactory;
        $this->_productRepositoryFactory = $productRepositoryFactory;
        $this->_filesystem = $filesystem;
        $this->formKey = $formKey;
        $this->_request->setParam('form_key', $this->formKey->getFormKey());
        parent::__construct($context);
    }

    /**
     * This function is used for get application landing page
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_cartHelper->isEnable()) {
            if (!$this->_cartHelper->getHeaders()) {
                $errorResult = ['status'=> false,'message' => $this->_cartHelper->getHeaderMessage()];
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            try {
                $dashboardResponse = array();
                $postData = $this->_request->getParams();
                $mediaPath = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

                if (in_array($postData['storeid'], $this->_cartHelper->getAllWebsites())) {

                    $sessionId = '';
                    if (isset($postData['session_id']) && $postData['session_id'] != null) {
                        $sessionId = $postData['session_id'];
                        if (!$this->_customerSession->isLoggedIn()) {
                            $customerId = explode("_", $sessionId);
                            $this->_cartHelper->relogin($customerId[0]);
                        }
                        if ($this->_customerSession->isLoggedIn()) {
                            $customerId = explode("_", $sessionId);
                        }
                    }
                    $storeId = $postData['storeid'];
                    if ($storeId == "") {
                        $storeId = $this->_storeManager->getStore()->getId();
                    }

                    $currencyCode = (isset($postData['currency_code']) && $postData['currency_code'] != '') ? $postData['currency_code'] : $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();
                    
                    $themePrimarycolor = $this->_scopeConfig->getValue(self::XML_PATH_COLOR, \Magento\Store\Model\ScopeInterface::SCOPE_STORES, $storeId);
                    $dashboardResponse['color'] = $themePrimarycolor;
                    $themeSecondarycolor = $this->_scopeConfig->getValue(self::XML_PATH_SECONDARYCOLOR, \Magento\Store\Model\ScopeInterface::SCOPE_STORES, $storeId);
                    $dashboardResponse['secondary_color'] = $themeSecondarycolor;

                    $themeBackgroundColor = $this->_scopeConfig->getValue(self::XML_PATH_BGCOLOR, \Magento\Store\Model\ScopeInterface::SCOPE_STORES, $storeId);
                    $dashboardResponse['background_color'] = $themeBackgroundColor;
                    $themeButtonTextColor = $this->_scopeConfig->getValue(self::XML_PATH_BTNTXTCOLOR, \Magento\Store\Model\ScopeInterface::SCOPE_STORES, $storeId);
                    $dashboardResponse['button_text_color'] = $themeButtonTextColor;

                    if (isset($postData['customer_id']) && $postData['customer_id'] != "") {
                        $customerId = $postData['customer_id'];
                        $dashboardResponse['notification_count'] = (bool)$this->_notificationHelper->getNotificationCount($customerId);
                    }

                    /*****************Customer cart data *******************/

                    $count = $this->_cartModel->getQuote()->getItemsCount();
                    if (!isset($count)) {
                        $count = "0";
                    } else {
                        $count = $this->_cartModel->getQuote()->getItemsCount();
                    }
                    $dashboardResponse['cart_count'] = $count;


                    /*****************Customer cart data *******************/

                    /**************** Layout component data *******************/

                    /**************** Layout component sequence ***************/

                    $defaultLayoutData = $this->_layoutModel->getCollection()
                                            ->addFieldToFilter('is_default', ['eq' => 1])
                                            ->addFieldToFilter('store_id', ['eq' => $storeId])
                                            ->setPageSize(1)
                                            ->setCurPage(1)
                                            ->load();

                    $defaultLayoutId = 0;
                    $layoutDataId = 0;
                    if (!empty($defaultLayoutData)) { 
                        foreach ($defaultLayoutData as $value) {
                            $defaultLayoutId = $value->getId();
                        }
                    }

                    if ($defaultLayoutId) {
                        $layoutComponentModel = $this->_layoutComponentFactory->create()
                                                    ->getCollection()
                                                    ->addFieldToFilter('layout_id', ['eq' => $defaultLayoutId])
                                                    ->setPageSize(1)
                                                    ->setCurPage(1)
                                                    ->load();
                        if (!empty($layoutComponentModel)) {
                            foreach ($layoutComponentModel as $value) {
                                $layoutDataId = $value->getId();
                            }
                        }
                    }

                    $layoutComponentData = [];
                    if ($layoutDataId) {
                        $layoutComponentData = $this->_layoutComponentModel->load($layoutDataId)->getComponentData();
                        if (!empty(json_decode($layoutComponentData))) {
                            $dashboardResponse['layout_component_data'] = json_decode($layoutComponentData);
                        } else {
                            $dashboardResponse['layout_component_data'] = json_decode($layoutComponentData);
                            $layoutResultArray = [
                                'status' => false,
                                'message' => __('Couldn\'t work for the storeview : '. $this->_storeManager->getStore($postData['storeid'])->getName().' | Please create and set layout as default for this storeview from MageMob App Builder → Manage Home Page Layout.')
                            ];
                            $jsonResult->setData($layoutResultArray);
                            return $jsonResult;
                        }
                    }

                    if (empty($layoutComponentData)) {
                        $dashboardResponse['layout_component_data'] = $layoutComponentData;
                        $layoutResultArray = [
                            'status' => false,
                            'message' => __('Couldn\'t work for the storeview : '. $this->_storeManager->getStore($postData['storeid'])->getName().' | Please create and set layout as default for this storeview from MageMob App Builder → Manage Home Page Layout.')
                        ];
                        $jsonResult->setData($layoutResultArray);
                        return $jsonResult;
                    }

                    /**************** Layout component sequence ***************/
                    
                    /********** Layout featured category blocks data **********/

                    $dashboardResponse['fcb'] = [];
                    if ($defaultLayoutId) {
                        $featuredCategoryBlocksData = $this->_featuredCategoryBlocksFactory->create()
                                                            ->getCollection()
                                                            ->addFieldToFilter('layout_id', ['eq' => $defaultLayoutId])
                                                            ->addFieldToFilter('status', ['eq' => 1])
                                                            ->setOrder('sort_order', 'ASC')
                                                            ->load()
                                                            ->getData();
                        if (!empty($featuredCategoryBlocksData)) {
                            foreach ($featuredCategoryBlocksData as $value) {
                                $fcbData = [
                                    'fcb_id' => $value['fcb_id'],
                                    'title' => $value['category_title'],
                                    'sort_order' => $value['sort_order'],
                                    'category' => $value['category_id']
                                ];
                                if ($value['image_type'] == 'url') {
                                    $fcbData += ['filepath' => $value['image_url']];
                                } else {
                                    $fcbData += ['filepath' => $mediaPath.$value['filepath']];
                                }
                                $dashboardResponse['fcb'][$value['fcb_id']]['fcbdata'][] = $fcbData;
                            }
                        }

                        $fcbDisplayTypeData = $this->_fcbDisplayTypeFactory->create()
                                                    ->getCollection()
                                                    ->addFieldToFilter('layout_id', ['eq' => $defaultLayoutId])
                                                    ->load()
                                                    ->getData();
                        if (!empty($fcbDisplayTypeData)) {
                            foreach ($fcbDisplayTypeData as $value) {
                                $dashboardResponse['fcb'][$value['fcb_id']]['display_type'] = $value['display_type'];
                                if ($value['fcb_title'] == null || $value['fcb_title'] == '') {
                                    $dashboardResponse['fcb'][$value['fcb_id']]['fcb_title'] = "";
                                } else {
                                    $dashboardResponse['fcb'][$value['fcb_id']]['fcb_title'] = $value['fcb_title'];
                                }
                            }
                        }
                    } else {
                        $dashboardResponse['fcb'] = (object)[];
                    }

                    /********** Layout featured category blocks data **********/
                    
                    /***************** Layout product grid data ***************/

                    $dashboardResponse['productgrid'] = [];
                    if ($defaultLayoutId) {

                        $productGridData = $this->_productGridFactory->create()
                                                ->getCollection()
                                                ->addFieldToFilter('layout_id', ['eq' => $defaultLayoutId])
                                                ->load()
                                                ->getData();
                        if (!empty($productGridData)) {
                            foreach ($productGridData as $value) {
                                $dashboardResponse['productgrid'][$value['productgrid_id']]['component_title'] = $value['component_title'];
                                if ($value['product_type'] == 'category_products') {
                                    $dashboardResponse['productgrid'][$value['productgrid_id']]['productdata'] = [];
                                    $pgItemCount = count(json_decode($value['category_products']));
                                    if ($pgItemCount > 4) {
                                        $dashboardResponse['productgrid'][$value['productgrid_id']]['is_viewmore'] = true;
                                    } else {
                                        $dashboardResponse['productgrid'][$value['productgrid_id']]['is_viewmore'] = false;
                                    }
                                    $itemCount = 1;
                                    foreach (json_decode($value['category_products']) as $prodId) {
                                        if ($itemCount <= 4) {
                                            $productRepoModel = $this->_productRepositoryFactory->create()
                                                                    ->getById($prodId);
                                            $productId = $productRepoModel->getId();
                                            $productFactoryData = $this->_productFactory->create()->setStoreId($storeId)->load($productId);
                                            $productPrice = ($productRepoModel->getTypeId() != 'simple') ? $productRepoModel->getFinalPrice() : $productRepoModel->getPrice();
                                            $productSpecialPrice = $productRepoModel->getSpecialPrice();
                                            $productImage = $productRepoModel->getData('small_image');
                                            $productData = [
                                                'id' => $productId,
                                                'sku' => $productFactoryData->getSku(),
                                                'name' => $productFactoryData->getName(),
                                                'status' => $productFactoryData->getStatus(),
                                                'price' => $this->_cartHelper->getPriceByStoreWithCurrency($productPrice, $storeId, $currencyCode),
                                                'special_price' => $this->_cartHelper->getPriceByStoreWithCurrency($productSpecialPrice, $storeId, $currencyCode),
                                                'save_discount' => $this->_cartHelper->getDiscount($productId),
                                                'image' => $mediaPath . 'catalog/product' . $productImage
                                            ];
                                            array_push($dashboardResponse['productgrid'][$value['productgrid_id']]['productdata'], $productData);
                                            $itemCount++;
                                        }
                                    }
                                } elseif ($value['product_type'] == 'custom_products') {
                                    $dashboardResponse['productgrid'][$value['productgrid_id']]['productdata'] = [];
                                    $pgItemCount = sizeof(json_decode($value['product_list']));
                                    if ($pgItemCount > 4) {
                                        $dashboardResponse['productgrid'][$value['productgrid_id']]['is_viewmore'] = true;
                                    } else {
                                        $dashboardResponse['productgrid'][$value['productgrid_id']]['is_viewmore'] = false;
                                    }
                                    $itemCount = 1;
                                    if ($itemCount <= 4) {
                                        foreach (json_decode($value['product_list']) as $prodId) {
                                            $productRepoModel = $this->_productRepositoryFactory->create()
                                                                    ->getById($prodId);
                                            $productId = $productRepoModel->getId();
                                            $productFactoryData = $this->_productFactory->create()->setStoreId($storeId)->load($productId);
                                            $productPrice = ($productRepoModel->getTypeId() != 'simple') ? $productRepoModel->getFinalPrice() : $productRepoModel->getPrice();
                                            $productSpecialPrice = $productRepoModel->getSpecialPrice();
                                            $productImage = $productRepoModel->getData('small_image');
                                            $productData = [
                                                'id' => $productId,
                                                'sku' => $productFactoryData->getSku(),
                                                'name' => $productFactoryData->getName(),
                                                'status' => $productFactoryData->getStatus(),
                                                'price' => $this->_cartHelper->getPriceByStoreWithCurrency($productPrice, $storeId, $currencyCode),
                                                'special_price' => $this->_cartHelper->getPriceByStoreWithCurrency($productSpecialPrice, $storeId, $currencyCode),
                                                'save_discount' => $this->_cartHelper->getDiscount($productId),
                                                'image' => $mediaPath . 'catalog/product' . $productImage
                                            ];
                                            array_push($dashboardResponse['productgrid'][$value['productgrid_id']]['productdata'], $productData);
                                            $itemCount++;
                                        }
                                    }
                                } else {
                                    $dashboardResponse['productgrid'][$value['productgrid_id']] = $value;
                                }
                            }
                        } else {
                            $dashboardResponse['productgrid'] = (object)[];
                        }
                    } else {
                        $dashboardResponse['productgrid'] = (object)[];
                    }

                    /***************** Layout product grid data ***************/

                    /************ Layout product horizontal slider ************/

                    $dashboardResponse['phs'] = [];
                    if ($defaultLayoutId) {

                        $productHorizontalSliderData = $this->_productHorizontalSliderFactory->create()
                                                            ->getCollection()
                                                            ->addFieldToFilter('layout_id', ['eq' => $defaultLayoutId])
                                                            ->load()
                                                            ->getData();
                        if (!empty($productHorizontalSliderData)) {
                            foreach ($productHorizontalSliderData as $value) {
                                $dashboardResponse['phs'][$value['phs_id']]['component_title'] = $value['component_title'];
                                $dashboardResponse['phs'][$value['phs_id']]['product_type'] = $value['phs_product_type'];
                                if ($value['phs_product_type'] == 'category_products') {
                                    $dashboardResponse['phs'][$value['phs_id']]['productdata'] = [];
                                    $phsItemCount = sizeof(json_decode($value['phs_category_products']));
                                    if ($phsItemCount > 5) {
                                        $dashboardResponse['phs'][$value['phs_id']]['is_viewmore'] = true;
                                    } else {
                                        $dashboardResponse['phs'][$value['phs_id']]['is_viewmore'] = false;
                                    }
                                    $itemCount = 1;
                                    foreach (json_decode($value['phs_category_products']) as $prodId) {
                                        if ($itemCount <= 5) {
                                            $productRepoModel = $this->_productRepositoryFactory->create()
                                                                    ->getById($prodId);
                                            $productId = $productRepoModel->getId();
                                            $productFactoryData = $this->_productFactory->create()->setStoreId($storeId)->load($productId);
                                            $productPrice = ($productRepoModel->getTypeId() != 'simple') ? $productRepoModel->getFinalPrice() : $productRepoModel->getPrice();
                                            $productSpecialPrice = $productRepoModel->getSpecialPrice();
                                            $productImage = $productRepoModel->getData('small_image');
                                            $productData = [
                                                'id' => $productId,
                                                'sku' => $productFactoryData->getSku(),
                                                'name' => $productFactoryData->getName(),
                                                'status' => $productFactoryData->getStatus(),
                                                'price' => $this->_cartHelper->getPriceByStoreWithCurrency($productPrice, $storeId, $currencyCode),
                                                'special_price' => $this->_cartHelper->getPriceByStoreWithCurrency($productSpecialPrice, $storeId, $currencyCode),
                                                'save_discount' => $this->_cartHelper->getDiscount($productId),
                                                'image' => $mediaPath . 'catalog/product' . $productImage
                                            ];
                                            array_push($dashboardResponse['phs'][$value['phs_id']]['productdata'], $productData);
                                            $itemCount++;
                                        }
                                    }
                                } elseif ($value['phs_product_type'] == 'custom_products') {
                                    $dashboardResponse['phs'][$value['phs_id']]['productdata'] = [];
                                    $phsItemCount = sizeof(json_decode($value['phs_product_list']));
                                    if ($phsItemCount > 5) {
                                        $dashboardResponse['phs'][$value['phs_id']]['is_viewmore'] = true;
                                    } else {
                                        $dashboardResponse['phs'][$value['phs_id']]['is_viewmore'] = false;
                                    }
                                    $itemCount = 1;
                                    foreach (json_decode($value['phs_product_list']) as $prodId) {
                                        if ($itemCount <= 5) {
                                            $productRepoModel = $this->_productRepositoryFactory->create()
                                                                    ->getById($prodId);
                                            $productId = $productRepoModel->getId();
                                            $productFactoryData = $this->_productFactory->create()->setStoreId($storeId)->load($productId);
                                            $productPrice = ($productRepoModel->getTypeId() != 'simple') ? $productRepoModel->getFinalPrice() : $productRepoModel->getPrice();
                                            $productSpecialPrice = $productRepoModel->getSpecialPrice();
                                            $productImage = $productRepoModel->getData('small_image');

                                            $productData = [
                                                'id' => $productId,
                                                'sku' => $productFactoryData->getSku(),
                                                'name' => $productFactoryData->getName(),
                                                'status' => $productFactoryData->getStatus(),
                                                'price' => $this->_cartHelper->getPriceByStoreWithCurrency($productPrice, $storeId, $currencyCode),
                                                'special_price' => $this->_cartHelper->getPriceByStoreWithCurrency($productSpecialPrice, $storeId, $currencyCode),
                                                'save_discount' => $this->_cartHelper->getDiscount($productId),
                                                'image' => $mediaPath . 'catalog/product' . $productImage
                                            ];
                                            array_push($dashboardResponse['phs'][$value['phs_id']]['productdata'], $productData);
                                            $itemCount++;
                                        }
                                    }
                                } elseif ($value['phs_product_type'] == 'best_seller') {
                                    $bestsellerProducts = $this->getBestSellerProducts($storeId, $currencyCode);
                                    if (empty($bestsellerProducts)) {
                                        $bestsellerProducts = array("item_count" => null,"productCollection" => null);
                                    }
                                    if (($bestsellerProducts['item_count'] != null) && ($bestsellerProducts['item_count'] > 5)) {
                                        $dashboardResponse['phs'][$value['phs_id']]['is_viewmore'] = true;
                                    } else {
                                        $dashboardResponse['phs'][$value['phs_id']]['is_viewmore'] = false;
                                    }
                                    $dashboardResponse['phs'][$value['phs_id']]['best_seller'] = $bestsellerProducts['productCollection'];
                                } elseif ($value['phs_product_type'] == 'new_products') {
                                    $newProducts = $this->getAllNewProducts($storeId, $currencyCode, $value['phs_id']);
                                    if (empty($newProducts)) {
                                        $newProducts = array("item_count" => null,"productCollection" => null);
                                    }
                                    if (($newProducts['item_count'] != null) && ($newProducts['item_count'] > 5)) {
                                        $dashboardResponse['phs'][$value['phs_id']]['is_viewmore'] = true;
                                    } else {
                                        $dashboardResponse['phs'][$value['phs_id']]['is_viewmore'] = false;
                                    }
                                    $dashboardResponse['phs'][$value['phs_id']]['new_products'] = $newProducts['productCollection'];
                                } else {
                                    $dashboardResponse['phs'][$value['phs_id']] = $value;
                                }
                            }
                        } else {
                            $dashboardResponse['phs'] = (object)[];
                        }
                    } else {
                        $dashboardResponse['phs'] = (object)[];
                    }

                    /************ Layout product horizontal slider ************/

                    /****************** Layout banner slider ******************/

                    $dashboardResponse['bannerslider'] = [];
                    if ($defaultLayoutId) {
                        $bannerSliderData = $this->_bannerSliderFactory->create()
                                                            ->getCollection()
                                                            ->addFieldToFilter('layout_id', ['eq' => $defaultLayoutId])
                                                            ->addFieldToFilter('status', ['eq' => 1])
                                                            ->load()
                                                            ->getData();
                        if (!empty($bannerSliderData)) {
                            foreach ($bannerSliderData as $value) {
                                $bannerData = [
                                    'bannerslider_id' => $value['bannerslider_id'],
                                    'title' => $value['component_title'],
                                    'type' => $value['redirect_activity'],
                                    'url' => $value['offer_link'],
                                    'sort_order' => $value['sort_order'],
                                    'category' => $value['category_id'],
                                    'product_id' => $value['product_id']
                                ];
                                if ($value['image_type'] == 'url') {
                                    $bannerData += ['filepath' => $value['image_url']];
                                } else {
                                    $bannerData += ['filepath' => $mediaPath.$value['filepath']];
                                }
                                $dashboardResponse['bannerslider'][$value['bannerslider_id']]['bannerdata'][] = $bannerData;
                            }
                        } else {
                            $dashboardResponse['bannerslider'] = (object)[];
                        }
                    } else {
                        $dashboardResponse['bannerslider'] = (object)[];
                    }

                    /****************** Layout banner slider ******************/

                    /****************** Layout component data *****************/
                    
                    $session = $this->_customerSession;
                    
                    $dashboardResponse['magento_version'] = false;
                    $jsonResult->setData($dashboardResponse);
                    return $jsonResult;
                } else {
                    $returnExtensionArray = [
                        'status' => false,
                        'message' => __('Couldn\'t work for the storeview : '. $this->_storeManager->getStore($postData['storeid'])->getName().' | Please enable Magemob Appbuilder for this storeview from Stores → Configuration → AppJetty → Magemob App Builder → Magemob App Builder Activation.')
                    ];
                    $jsonResult->setData($returnExtensionArray);
                    return $jsonResult;
                }
            } catch (\Exception $e) {
                $dashboardResponse = [
                    'status' => 'false',
                    'message' => $e->getMessage()
                ];
            }
            $jsonResult->setData($dashboardResponse);
            return $jsonResult;
        } else {
            $returnExtensionArray = ['enable' => false];
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }

    /**
     * This function is used for get best seller product list
     * @param  int $storeId
     * @param  int $seeall
     * @param  int $page
     * @param  int $limit
     * @return Array
     */
    public function getBestSellerProducts($storeId, $currencyCode, $seeall = null, $page = null, $limit = null)
    {
        $jsonResult = $this->_jsonFactory->create();
        try{
            $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrencyCode();
            $currentCurrencyCode = $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();
            $currencyCode = ($currencyCode != '') ? $currencyCode : $currentCurrencyCode;
            
            $product_list = array();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productCollection = $objectManager->create('Magento\Reports\Model\ResourceModel\Report\Collection\Factory');
            $bestSelProducts = $productCollection->create('Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection');
            $bestSelProducts->setPeriod('month');
            
            $total_collection = count($bestSelProducts);

            $bestSelProducts->getSelect()->limit(5, 0);

            foreach ($bestSelProducts->getData() as $product) {
                $configurableTypeIds = $objectManager->get('Magento\ConfigurableProduct\Model\Product\Type\Configurable')->getParentIdsByChild($product['product_id']);
                $parentId = array_shift($configurableTypeIds);
                if (isset($parentId)) {
                    $productFinalId = $parentId;
                } else {
                    $productFinalId = $product['product_id'];
                }
                $product_data = $this->_productFactory->create()->setStoreId($storeId)->load($product['product_id']);
                $productSku = (string)$product_data->getSku();
                $allow = $this->allowProduct($productSku);
                if ($allow == "") {
                    continue;
                }
                if ($product_data->getTypeId() == 'simple' || $product_data->getTypeId() == 'virtual') {
                    if ($product_data->getData('has_options')) {
                        $hasOptions = "1";
                    } else {
                        $hasOptions = "0";
                    }
                } elseif ($product_data->getTypeId() == 'configurable' || $product_data->getTypeId() == 'grouped' || $product_data->getTypeId() == 'bundle') {
                    $hasOptions = "1";
                } elseif ($product_data->getTypeId() == 'downloadable') {
                    $hasOptions = $product_data->getData('links_purchased_separately');
                }
                $status = $product_data->getStatus();
                $stockState = $this->_stockInterface;
                
                
                $qty = $stockState->getStockQty($product_data->getId(), $product_data->getStore()->getWebsiteId());
                $summaryData = $this->getProductRatingSummary($storeId, $product_data->getId());
                $wishlist_detail = $this->checkwishlist($product_data->getId());
                
                $stock_item = $this->_stockItemRepository->get($product_data->getId());
                if ($stock_item->getBackorders() != 0) {
                    if ($stock_item->getIsInStock() == 0) {
                        $isInStock = '0';
                    } else {
                        $isInStock = '1';
                    }
                } else {
                    if ($qty < 0 || $stock_item->getIsInStock() == 0) {
                        $qty = 'Out of Stock';
                        $isInStock = '0';
                    } else {
                        $isInStock = '1';
                    }
                }
                if ($this->_catalogruleModel->calcProductPriceRule($product_data, $product_data->getPrice())) {
                    $specialPrice = $this->_catalogruleModel->calcProductPriceRule($product_data, $product_data->getPrice());
                } else {
                    $specialPrice = $product_data->getSpecialPrice();
                }
                $associated_products = array();
                if ($product_data->getTypeId() == 'grouped') {
                    $associated_products = $product_data->getTypeInstance(true)->getAssociatedProducts($product_data);
                } elseif ($product_data->getTypeId() == 'configurable') {
                    $associated_products = $product_data->getTypeInstance()->getUsedProducts($product_data);
                } elseif ($product_data->getTypeId() == 'bundle') {
                    $associated_products = $product_data->getTypeInstance(true)->getSelectionsCollection($product_data->getTypeInstance(true)->getOptionsIds($product_data), $product_data);
                }
                $prices = array();
                $associated_products_details = array();
                $associated_products_list = array();
                $related_products_list = array();

                foreach ($associated_products as $associated_product) {
                    $associated_productStockData = $this->_stockItemRepository->get($associated_product->getId());
                    $qty = $associated_productStockData->getQty();

                    if ($qty < 0 || $associated_productStockData->getIsInStock() == 0) {
                        $qty = 'Out of Stock';
                    } else {
                        $associated_products_details[] = array(
                            'id' => $associated_product->getId(),
                            'sku' => $associated_product->getSku()
                        );
                        $associated_price = $this->_productFactory->create()->setStoreId($storeId)->load($associated_product->getId())->getPrice();
                        $associated_products_list[] = array(
                            'color' => $associated_product->getColor(),
                            'id' => $associated_product->getId(),
                            'sku' => $associated_product->getSku(),
                            'name' => $this->_productModel->setStoreId($storeId)->load($associated_product->getId())->getName(),
                            'image' => $this->_imageHelper->init($associated_product, 'product_page_image_medium')->resize(300, 330)->constrainOnly(true)->keepAspectRatio(true)->getUrl(),
                            'status' => $status,
                            'qty' => $qty,
                            'price' => $this->_cartHelper->getPriceByStoreWithCurrency($associated_price, $storeId, $currencyCode),
                        );
                        $prices[] = $associated_product->getPrice();
                    }
                }
                $amount = $product_data->getPrice();
                if ($product_data->getTypeId() == 'configurable') {
                    $amount = min($prices);
                }
                $product_list[] = array(
                    'id' => $productFinalId,
                    'sku' => $product_data->getSku(),
                    'name' => $product_data->getName(),
                    'status' => $status,
                    'qty' => $qty,
                    'in_stock' => $isInStock,
                    'price' => $this->_cartHelper->getPriceByStoreWithCurrency($amount, $storeId, $currencyCode),
                    'special_price' => $this->_cartHelper->getPriceByStoreWithCurrency($specialPrice, $storeId, $currencyCode),
                    'image' => $this->_imageHelper->init($product_data, 'product_page_image_medium')->resize(300, 330)->constrainOnly(true)->keepAspectRatio(true)->getUrl(),
                    'type' => $product_data->getTypeId(),
                    'is_wishlisted' => $wishlist_detail['in_wishlist'],
                    'wishlist_item_id' => $wishlist_detail['wishlist_item_id'],
                    'review_count' => $summaryData['review_count'],
                    'average_rating' => $summaryData['rating_summary'],
                    'has_options' => $hasOptions,
                    'save_discount' => $this->_cartHelper->getDiscount($product_data->getId()),
                );
            }
            $responseArr['item_count'] = $total_collection;
            $responseArr['productCollection'] = $product_list;
            $responseArr['current_page'] = $page;
            if (isset($limit)) {
                if ($page + 1 <= ceil($total_collection / $limit)) {
                    $responseArr['next_page'] = $page + 1;
                }
            }
            return $responseArr;
        } catch (\Exception $e) {
            $responseArr = [
                'item_count' => null,
                'productCollection' => '',
                'current_page' => ''
            ];
            return $responseArr;
        }
    }

    /**
     * This function is used for get new product list
     * @param  int $storeId
     * @param  int $seeall
     * @param  int $page
     * @param  int $limit
     * @return Array
     */
    public function getAllNewProducts($storeId, $currencyCode, $componentId = null, $seeall = null, $page = null, $limit = null)
    {
        $jsonResult = $this->_jsonFactory->create();
        try {
            $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrencyCode();
            $currentCurrencyCode = $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();
            $currencyCode = ($currencyCode != '') ? $currencyCode : $currentCurrencyCode;

            $todayStartOfDayDate = $this->_localeDate->date()->setTime(0, 0, 0)->format('Y-m-d H:i:s');
            $todayEndOfDayDate = $this->_localeDate->date()->setTime(23, 59, 59)->format('Y-m-d H:i:s');
            $displayProduct = '0';
            $product_list = [];

            if ($componentId != null) {
                $phsData = $this->_productHorizontalSliderFactory->create()
                                        ->getCollection()
                                        ->addFieldToFilter("phs_id", $componentId)
                                        ->setPageSize(1)
                                        ->setCurPage(1)
                                        ->getData();
                $displayProduct = ($phsData[0]['new_arrival_product_type']) ? $phsData[0]['new_arrival_product_type'] : '0';
                if ($displayProduct == '1') {
                    $displayType = ($phsData[0]['phs_display_category']) ? $phsData[0]['phs_display_category'] : '0';
                }
            }

            $todayDate = date('Y-m-d');
            if ($displayProduct == '0') {
                $products = $this->_productModel->getCollection()
                ->setStoreId($storeId)
                ->addAttributeToFilter('status', array("eq" => 1))
                ->addAttributeToSort('news_from_date', 'desc')
                ->addAttributeToSort('created_at', 'desc')->addAttributeToFilter('visibility', array("neq" => 1));
                $products->addAttributeToFilter(
                    'news_from_date',
                    [
                        'or' => [
                            0 => ['date' => true, 'to' => $todayEndOfDayDate],
                            1 => ['is' => new \Zend_Db_Expr('null')],
                        ]
                    ]
                )->addAttributeToFilter(
                    [
                        ['attribute' => 'news_from_date', 'is' => new \Zend_Db_Expr('not null')],
                    ]
                );
            }
            if ($displayProduct == '1') {
                $products = $this->_productModel->getCollection()->setStoreId($storeId)
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('status', array("eq" => 1))
                ->addAttributeToFilter('visibility', array("neq" => 1))
                ->addCategoriesFilter(array('in' => $displayType));
                $products->getSelect()->group('e.entity_id');
                $products->setOrder('entity_id', 'ASC');
                $products->addAttributeToFilter(
                    'news_from_date',
                    [
                        'or' => [
                            0 => ['date' => true, 'to' => $todayStartOfDayDate],
                            1 => ['is' => new \Zend_Db_Expr('null')],
                        ]
                    ]
                )->addAttributeToFilter(
                    [
                        ['attribute' => 'news_from_date', 'is' => new \Zend_Db_Expr('not null')],
                    ]
                );
            }

            $total_collection = count($products);
            
            $products->getSelect()->limit(5, 0);
            
            foreach ($products->getData() as $product) {
                $product_data = $this->_productFactory->create()->setStoreId($storeId)->load($product['entity_id']);
                if ($product_data->getTypeId() == 'simple' || $product_data->getTypeId() == 'virtual') {
                    if ($product_data->getData('has_options')) {
                        $hasOptions = "1";
                    } else {
                        $hasOptions = "0";
                    }
                } elseif ($product_data->getTypeId() == 'configurable' || $product_data->getTypeId() == 'grouped' || $product_data->getTypeId() == 'bundle') {
                    $hasOptions = "1";
                } elseif ($product_data->getTypeId() == 'downloadable') {
                    $hasOptions = $product_data->getData('links_purchased_separately');
                }
                /*****************************Product type wise price*********************************/
                $finalPrice = $product_data->getPrice();
                $associated_products = array();
                if ($product_data->getTypeId() == 'grouped') {
                    $associated_products = $product_data->getTypeInstance(true)->getAssociatedProducts($product_data);
                } elseif ($product_data->getTypeId() == 'configurable') {
                    $associated_products = $product_data->getTypeInstance()->getUsedProducts($product_data);
                }
                $prices = array();
                foreach ($associated_products as $associated_product1) {
                    $priceAssociateProduct = $this->_productFactory->create()->setStoreId($storeId)->load($associated_product1->getId());
                    $prices[] = $priceAssociateProduct->getFinalPrice();
                }
                if (!empty($prices)) {
                    $finalPrice = min($prices);
                }


                /*************************************************************************************/
                $status = $product_data->getStatus();

                $stockState = $this->_stockItemRepository->get($product_data->getId(), $product_data->getStore()->getWebsiteId());
                $qty = $stockState->getQty();
                if ($stockState->getBackorders() != 0) {
                    if ($stockState->getIsInStock() == 0) {
                        $isInStock = '0';
                    } else {
                        $isInStock = '1';
                    }
                } else {
                    if ($qty < 0 || $stockState->getIsInStock() == 0) {
                        $qty = 'Out of Stock';
                        $isInStock = '0';
                    } else {
                        $isInStock = '1';
                    }
                }
                $summaryData = $this->getProductRatingSummary($storeId, $product_data->getId());
                $wishlist_detail = $this->checkwishlist($product_data->getId());
                if ($qty == 0 && $stockState->getIsInStock() == 0 && $product_data->getTypeId() == "simple") {
                    $qty = 'Out of Stock';
                }
                if ($this->_catalogruleModel->calcProductPriceRule($product_data, $product_data->getPrice())) {
                    $specialPrice = $this->_catalogruleModel->calcProductPriceRule($product_data, $product_data->getPrice());
                } else {
                    $specialPrice = $product_data->getSpecialPrice();
                }
                $product_list[] = array(
                    'id' => $product_data->getId(),
                    'sku' => $product_data->getSku(),
                    'name' => $product_data->getName(),
                    'status' => $status,
                    'qty' => $qty,
                    'in_stock' => $isInStock,
                    'price' => $this->_cartHelper->getPriceByStoreWithCurrency($finalPrice, $storeId, $currencyCode),
                    'special_price' => $this->_cartHelper->getPriceByStoreWithCurrency($specialPrice, $storeId, $currencyCode),
                    'image' => $this->_imageHelper->init($product_data, 'product_page_image_medium')->resize(300, 330)->constrainOnly(true)->keepAspectRatio(true)->getUrl(),
                    'type' => $product_data->getTypeId(),
                    'is_wishlisted' => $wishlist_detail['in_wishlist'],
                    'wishlist_item_id' => $wishlist_detail['wishlist_item_id'],
                    'review_count' => $summaryData['review_count'],
                    'average_rating' => $summaryData['rating_summary'],
                // 'has_options' => $hasOptions,
                    'save_discount' => $this->_cartHelper->getDiscount($product_data->getId()),
                );
            }
            $responseArr['item_count'] = $total_collection;
            $responseArr['productCollection'] = $product_list;
            $responseArr['current_page'] = $page;
            if (isset($limit)) {
                if ($page + 1 <= ceil($total_collection / $limit)) {
                    $responseArr['next_page'] = $page + 1;
                }
            }
            return $responseArr;
        } catch (\Exception $e) {
            $responseArr = [
                'item_count' => null,
                'productCollection' => '',
                'current_page' => ''
            ];
            return $responseArr;
        }
    }
    /**
     * This function is used for get the particular product rating summary
     * @param  int $storeId
     * @param  int $productId
     * @return Array
     */
    protected function getProductRatingSummary($storeId, $productId)
    {
        $reviewSummaryArray = array('review_count' => null, 'rating_summary' => null);
        $reviewModelData = $this->_reviewSummaryModel->setStoreId($storeId)->load($productId);
        $reviewSummaryArray = array('review_count' => $reviewModelData->getReviewsCount(), 'rating_summary' => $reviewModelData->getRatingSummary());
        return $reviewSummaryArray;
    }

    /**
     * This function is used for the get product is in wishlist or not
     * @param  int $productId
     * @return Array
     */
    protected function checkwishlist($productId)
    {
        $wishlistArray = array('in_wishlist' => false, 'wishlist_item_id' => null);
        if ($this->_wishlistHelper->isAllow()) {
            foreach ($this->_wishlistHelper->getWishlistItemCollection() as $wishlistItem) {
                if ($productId == $wishlistItem->getProduct()->getId()) {
                    $wishlistArray = array('in_wishlist' => true, 'wishlist_item_id' => $wishlistItem->getId());
                    break;
                }
            }
        }
        return $wishlistArray;
    }
    private function getSalebleQty($productSku)
    {
        $code = 'base';
        $type = 'website';
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $salesChannelId = $objectManager->get('Magento\InventorySales\Model\ResourceModel\StockIdResolver')->resolve($type, $code);
            $stockName = $objectManager->get('Magento\Inventory\Model\Stock')->load($salesChannelId);
            $stockName = $stockName->getName();
            $allowProduct = $objectManager->get('Magento\InventorySales\Model\IsProductSalableCondition\IsProductSalableConditionChain')->execute($productSku, $salesChannelId);
            $this->_salebleModel = $objectManager->get('Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku');
            $salebleModelData = $this->_salebleModel->execute((string)$productSku);
            foreach ($salebleModelData as $key => $value) {
                if ($value['stock_name'] == $stockName) {
                    $finalSalesData['stock_name'] = $value['stock_name'];
                    $finalSalesData['qty'] = $value['qty'];
                    $finalSalesData['manage_stock'] = $value['manage_stock'];
                }
            }
            return $finalSalesData;
        } catch (\Exception $e) {
            $salebleModelData = array();
            return $salebleModelData;
        }
        return $salebleModelData;
    }
    private function allowProduct($productSku)
    {
        $code = 'base';
        $type = 'website';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $salesChannelId = $objectManager->get('Magento\InventorySales\Model\ResourceModel\StockIdResolver')->resolve($type, $code);
        $allowProduct = $objectManager->get('Magento\InventorySales\Model\IsProductSalableCondition\IsProductSalableConditionChain')->execute($productSku, $salesChannelId);
        return $allowProduct;
    }
}
