<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Homepage;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Action\Context;

class Getcomponentproducts extends \Magento\Framework\App\Action\Action
{
    protected $jsonFactory;
    protected $request;
    protected $productModel;
    protected $_productFactory;
    protected $_productCollectionFactory;
    protected $cartHelper;
    protected $_productGridFactory;
    protected $_productHorizontalSliderFactory;
    protected $imageHelper;
    protected $wishlistHelper;
    protected $reviewSummaryModel;
    protected $customerSession;
    protected $scopeConfig;
    protected $stockInterface;
    protected $catalogruleModel;
    protected $stockItemRepository;
    protected $_stockApiRepository;
    protected $_storeManager;
    protected $_localeDate;
    protected $formKey;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        Http $request,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Biztech\Magemobcart\Helper\Data $cartHelper,
        \Biztech\Magemobcart\Model\Productgrid $productGridModel,
        \Biztech\Magemobcart\Model\Producthorizontalsliding $productHorizontalSliderModel,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Wishlist\Helper\Data $wishlistHelper,
        \Magento\Review\Model\Review\Summary $reviewSummaryModel,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\CatalogRule\Model\Rule $catalogruleModel,
        \Magento\CatalogInventory\Api\StockStateInterface $stockInterface,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockApiRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Data\Form\FormKey $formKey
    ) {
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_productModel = $productModel;
        $this->_productFactory = $productFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_cartHelper = $cartHelper;
        $this->_productGridModel = $productGridModel;
        $this->_productHorizontalSliderModel = $productHorizontalSliderModel;
        $this->_imageHelper = $imageHelper;
        $this->_wishlistHelper = $wishlistHelper;
        $this->_reviewSummaryModel = $reviewSummaryModel;
        $this->_customerSession = $customerSession;
        $this->_scopeConfig = $scopeConfig;
        $this->_stockInterface = $stockInterface;
        $this->_catalogruleModel = $catalogruleModel;
        $this->_stockItemRepository = $stockItemRepository;
        $this->_stockApiRepository = $stockApiRepository;
        $this->_storeManager = $storeManager;
        $this->_localeDate = $localeDate;
        $this->formKey = $formKey;
        $this->_request->setParam('form_key', $this->formKey->getFormKey());
        parent::__construct($context);
    }

    /**
     * This function is used for get all best seller products list
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        $postData = $this->_request->getParams();

        if ($this->_cartHelper->isEnable()) {
            if (in_array($postData['storeid'], $this->_cartHelper->getAllWebsites())) {
                if (!$this->_cartHelper->getHeaders()) {
                    $errorResult = array('status'=> false,'message' => $this->_cartHelper->getHeaderMessage());
                    $jsonResult->setData($errorResult);
                    return $jsonResult;
                }
                $sessionId = '';
                if (isset($postData['session_id']) && $postData['session_id'] != null) {
                    $sessionId = $postData['session_id'];
                    if (!$this->_customerSession->isLoggedIn()) {
                        $customer_id = explode("_", $sessionId);
                        $this->_cartHelper->relogin($customer_id[0]);
                    }
                }
                $storeId = (isset($postData['storeid']) && $postData['storeid'] != '') ? $postData['storeid'] : $this->_storeManager->getStore()->getId();
                $seeAll = $postData['see_all'];
                if (isset($postData['component_id']) && !empty($postData['component_id'])) {
                    $componentId = $postData['component_id'];
                } elseif (isset($postData['category_id']) && !empty($postData['category_id'])) {
                    $componentId = $postData['category_id'];
                } elseif (isset($postData['categoryid']) && !empty($postData['categoryid'])) {
                    $componentId = $postData['categoryid'];
                } else {
                    $componentId = '';
                }
                $categoryType = $postData['category_type'] ? $postData['category_type'] : '';
                $page = $postData['page'] ? $postData['page'] : 1;
                $limit = $postData['limit'] ? $postData['limit'] : 10;
                $currencyCode = (isset($postData['currency_code']) && $postData['currency_code'] != '') ? $postData['currency_code'] : $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();

                try {
                    if ($categoryType == 'category_products' || $categoryType == 'custom_products' || $categoryType == 'productgrid') {
                        $moreProdcuts = $this->getMoreProducts($storeId, $componentId, $seeAll, $page, $limit, $currencyCode);
                    } elseif ($categoryType == 'best_seller') {
                        $moreProdcuts = $this->getBestSellerProducts($storeId, $seeAll, $page, $limit, $currencyCode);
                    } elseif ($categoryType == 'new_products') {
                        $moreProdcuts = $this->getAllNewProducts($storeId, $componentId, $seeAll, $page, $limit, $currencyCode);
                    } else {
                        $moreProdcuts = ['item_count' => null, 'productCollection' => [], 'current_page' => null];
                    }
                    
                    $jsonResult->setData($moreProdcuts);
                    return $jsonResult;
                } catch (\Exception $e) {
                    $moreProdcuts = array(
                    'status' => 'false',
                    'message' => $e->getMessage()
                    );
                    $jsonResult->setData($moreProdcuts);
                    return $jsonResult;
                }
            } else {
                $returnExtensionArray = [
                    'status' => false,
                    'message' => __('Couldn\'t work for the storeview : '. $this->_storeManager->getStore($postData['storeid'])->getName().' | Please enable Magemob Appbuilder for this storeview from Stores → Configuration → AppJetty → Magemob App Builder → Magemob App Builder Activation.')
                ];
                $jsonResult->setData($returnExtensionArray);
                return $jsonResult;
            }
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }

    /**
     * This function is used for get best seller product list
     * @param  int $storeId
     * @param  varchare $componentId
     * @param  int $seeall
     * @param  int $page
     * @param  int $limit
     * @return Array
     */
    public function getMoreProducts($storeId, $componentId, $seeall = null, $page = null, $limit = null, $currencyCode)
    {
        $jsonResult = $this->_jsonFactory->create();
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productCollection = $objectManager->create('Magento\Reports\Model\ResourceModel\Report\Collection\Factory');
            $productIds = [];
            $product_list = [];
            if (explode("_",$componentId)[0] == 'productgrid') {
                $productGridData = $this->_productGridModel->getCollection()
                                        ->addFieldToFilter("productgrid_id", $componentId)
                                        ->setPageSize(1)
                                        ->setCurPage(1)
                                        ->load()->getData();
                
                if ($productGridData[0]['product_type'] == 'category_products') {
                    $products = $productGridData[0]['category_products'];
                    $productIds = json_decode($products);
                } elseif ($productGridData[0]['product_type'] == 'custom_products') {
                    $products = $productGridData[0]['product_list'];
                    $productIds = json_decode($products);
                }
            } elseif (explode("_",$componentId)[0] == 'phs') {
                $phsData = $this->_productHorizontalSliderModel->getCollection()
                                        ->addFieldToFilter("phs_id", $componentId)
                                        ->setPageSize(1)
                                        ->setCurPage(1)
                                        ->load()->getData();
                if ($phsData[0]['phs_product_type'] == 'category_products') {
                    $products = $phsData[0]['phs_category_products'];
                    $productIds = json_decode($products);
                } elseif ($phsData[0]['phs_product_type'] == 'custom_products') {
                    $products = $phsData[0]['phs_product_list'];
                    $productIds = json_decode($products);
                }
            }
            $productCollection = $this->_productCollectionFactory->create()
                                        ->addFieldToFilter('entity_id', array('in' => $productIds))
                                        ->addAttributeToSelect('*')
                                        // ->addStoreFilter()
                                        // ->setStoreId($storeId);
                                        ->addStoreFilter($storeId);
                
            $total_collection = count($productCollection);
            
            if ($seeall != true) {
                $productCollection->getSelect()->limit(5, 0);
            }

            $productCollection->setCurPage($page);

            if ($limit) {
                $productCollection->setPageSize($limit);
            }
            $productData = $productCollection->getData();
            foreach ($productData as $product) {
                $product_data = $this->_productFactory->create()->setStoreId($storeId)->load($product['entity_id']);
                $productSku = (string)$product_data->getSku();
                // $allow = $this->allowProduct($productSku);
                // if ($allow == "") {
                //     continue;
                // }
                if ($product_data->getTypeId() == 'simple' || $product_data->getTypeId() == 'virtual') {
                    if ($product_data->getData('has_options')) {
                        $hasOptions = "1";
                    } else {
                        $hasOptions = "0";
                    }
                } elseif ($product_data->getTypeId() == 'configurable' || $product_data->getTypeId() == 'grouped' || $product_data->getTypeId() == 'bundle') {
                    $hasOptions = "1";
                } elseif ($product_data->getTypeId() == 'downloadable') {
                    $hasOptions = $product_data->getData('links_purchased_separately');
                }
                $status = $product_data->getStatus();
                $stockState = $this->_stockInterface;
                $qty = $stockState->getStockQty($product_data->getId(), $product_data->getStore()->getWebsiteId());
                $summaryData = $this->getProductRatingSummary($storeId, $product_data->getId());
                $wishlist_detail = $this->checkwishlist($product['entity_id']);
                // $stock_item = $this->_stockItemRepository->get($product_data->getId());

                if ($product_data->getTypeId() != 'configurable') {
                    $stock_item = $this->_stockApiRepository->getStockItem($product_data->getId());
                    if ($stock_item->getBackorders() != 0) {
                        if ($stock_item->getIsInStock() == 0) {
                            $in_stock = '0';
                        } else {
                            $in_stock = '1';
                        }
                    } else {
                        if ($stock_item->getManageStock() == 0) {
                            if ($stock_item->getIsInStock() == true) {
                                $in_stock = '1';
                            } else {
                                $in_stock = '0';
                            }
                        } else {
                            $in_stock = strval((int)$stock_item->getIsInStock());
                        }
                    }
                    if ($qty < 0 || $stock_item->getIsInStock() == 0) {
                        $qty = 'Out of Stock';
                    }
                } elseif ($product_data->getTypeId() == 'configurable') {
                    $children = $product_data->getTypeInstance()->getUsedProducts($product_data);
                    foreach ($children as $child) {
                        if ($this->_stockApiRepository->getStockItem($child->getId())->getQty() > 0 && $this->_stockApiRepository->getStockItem($child->getId())->getIsInStock() == 1) {
                            $in_stock = '1';
                            $qty = $stockState->getStockQty($product_data->getId(), $product_data->getStore()->getWebsiteId());
                            break;
                        } else {
                            $qty = 'Out of Stock';
                            $in_stock = '0';
                        }
                    }
                } else {
                    $in_stock = '1';
                }

                if ($this->_catalogruleModel->calcProductPriceRule($product_data, $product_data->getPrice())) {
                    $specialPrice = $this->_catalogruleModel->calcProductPriceRule($product_data, $product_data->getPrice());
                } else {
                    $specialPrice = $product_data->getSpecialPrice();
                }
                $product_list[] = array(
                'id' => $product_data->getId(),
                'sku' => $product_data->getSku(),
                'name' => $product_data->getName(),
                'status' => $status,
                'qty' => $qty,
                'in_stock' => $in_stock,
                'price' => $this->_cartHelper->getPriceByStoreWithCurrency($product_data->getFinalPrice(), $storeId, $currencyCode),
                'special_price' => $this->_cartHelper->getPriceByStoreWithCurrency($specialPrice, $storeId, $currencyCode),
                'image' => $this->_imageHelper->init($product_data, 'product_page_image_medium')->resize(300, 330)->constrainOnly(true)->keepAspectRatio(true)->getUrl(),
                'type' => $product_data->getTypeId(),
                'is_wishlisted' => $wishlist_detail['in_wishlist'],
                'wishlist_item_id' => $wishlist_detail['wishlist_item_id'],
                'review_count' => $summaryData['review_count'],
                'average_rating' => $summaryData['rating_summary'],
                'has_options' => $hasOptions,
                'save_discount' => $this->_cartHelper->getDiscount($product_data->getId()),
                );
            }
            $responseArr['item_count'] = $total_collection;
            $responseArr['productCollection'] = $product_list;
            $responseArr['current_page'] = $page;
            if (isset($limit)) {
                if ($page + 1 <= ceil($total_collection / $limit)) {
                    $responseArr['next_page'] = $page + 1;
                }
            }
            return $responseArr;
        } catch (\Exception $e) {
            $returnExtensionArray = [
                'status' => false,
                'message' => __('Something went wrong while getting product data.')
            ];
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }
    /**
     * This function is used for get best seller product list
     * @param  int $storeId
     * @param  int $seeall
     * @param  int $page
     * @param  int $limit
     * @return Array
     */
    public function getBestSellerProducts($storeId, $seeall = null, $page = null, $limit = null, $currencyCode)
    {
        $jsonResult = $this->_jsonFactory->create();
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $productCollection = $objectManager->create('Magento\Reports\Model\ResourceModel\Report\Collection\Factory');
            $bestSelProducts = $productCollection->create('Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection');
            $bestSelProducts->setPeriod('month');
            
            if ($seeall != true) {
                $bestSelProducts->getSelect()->limit(5, 0);
            }
            $total_collection = count($bestSelProducts);

            $bestSelProducts->setCurPage($page);

            if ($limit) {
                $bestSelProducts->setPageSize($limit);
            }
            foreach ($bestSelProducts->getData() as $product) {
                $product_data = $this->_productModel->setStoreId($storeId)->load($product['product_id']);
                $productSku = (string)$product_data->getSku();
                $allow = $this->allowProduct($productSku);
                if ($allow == "") {
                    continue;
                }
                if ($product_data->getTypeId() == 'simple' || $product_data->getTypeId() == 'virtual') {
                    if ($product_data->getData('has_options')) {
                        $hasOptions = "1";
                    } else {
                        $hasOptions = "0";
                    }
                } elseif ($product_data->getTypeId() == 'configurable' || $product_data->getTypeId() == 'grouped' || $product_data->getTypeId() == 'bundle') {
                    $hasOptions = "1";
                } elseif ($product_data->getTypeId() == 'downloadable') {
                    $hasOptions = $product_data->getData('links_purchased_separately');
                }
                $status = $product_data->getStatus();
                $stockState = $this->_stockInterface;
                $qty = $stockState->getStockQty($product_data->getId(), $product_data->getStore()->getWebsiteId());
                $summaryData = $this->getProductRatingSummary($storeId, $product_data->getId());
                $wishlist_detail = $this->checkwishlist($product_data->getId());
                $stock_item = $this->_stockItemRepository->get($product_data->getId());
                if ($stock_item->getBackorders() != 0) {
                    if ($stock_item->getIsInStock() == 0) {
                        $in_stock = '0';
                    } else {
                        $in_stock = '1';
                    }
                } else {
                    if ($stock_item->getManageStock() == 0) {
                        if ($stock_item->getIsInStock() == true) {
                            $in_stock = '1';
                        } else {
                            $in_stock = '0';
                        }
                    } else {
                        $in_stock = strval((int)$stock_item->getIsInStock());
                    }
                }
                if ($qty < 0 || $stock_item->getIsInStock() == 0) {
                    $qty = 'Out of Stock';
                }
                if ($this->_catalogruleModel->calcProductPriceRule($product_data, $product_data->getPrice())) {
                    $specialPrice = $this->_catalogruleModel->calcProductPriceRule($product_data, $product_data->getPrice());
                } else {
                    $specialPrice = $product_data->getSpecialPrice();
                }
                $product_list[] = array(
                'id' => $product_data->getId(),
                'sku' => $product_data->getSku(),
                'name' => $product_data->getName(),
                'status' => $status,
                'qty' => $qty,
                'in_stock' => $in_stock,
                'price' => $this->_cartHelper->getPriceByStoreWithCurrency($product_data->getPrice(), $storeId, $currencyCode),
                'special_price' => $this->_cartHelper->getPriceByStoreWithCurrency($specialPrice, $storeId, $currencyCode),
                'image' => $this->_imageHelper->init($product_data, 'product_page_image_medium')->resize(300, 330)->constrainOnly(true)->keepAspectRatio(true)->getUrl(),
                'type' => $product_data->getTypeId(),
                'is_wishlisted' => $wishlist_detail['in_wishlist'],
                'wishlist_item_id' => $wishlist_detail['wishlist_item_id'],
                'review_count' => $summaryData['review_count'],
                'average_rating' => $summaryData['rating_summary'],
                'has_options' => $hasOptions,
                'save_discount' => $this->_cartHelper->getDiscount($product_data->getId()),
                );
            }
            $responseArr['item_count'] = $total_collection;
            $responseArr['productCollection'] = $product_list;
            $responseArr['current_page'] = $page;
            if (isset($limit)) {
                if ($page + 1 <= ceil($total_collection / $limit)) {
                    $responseArr['next_page'] = $page + 1;
                }
            }
            return $responseArr;
        } catch (\Exception $e) {
            $returnExtensionArray = [
                'status' => false,
                'message' => __('Something went wrong while getting product data.')
            ];
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }
    /**
     * This function is used for get best seller product list
     * @param  int $storeId
     * @param  int $seeall
     * @param  int $page
     * @param  int $limit
     * @return Array
     */
    public function getAllNewProducts($storeId, $componentId = null, $seeall = null, $page = null, $limit = null, $currencyCode)
    {
        $jsonResult = $this->_jsonFactory->create();
        
        $todayStartOfDayDate = $this->_localeDate->date()->setTime(0, 0, 0)->format('Y-m-d H:i:s');
        $todayEndOfDayDate = $this->_localeDate->date()->setTime(23, 59, 59)->format('Y-m-d H:i:s');
        $todayDate = date('Y-m-d');
        $displayProduct = '0';
        
        if ($componentId != null) {
            $phsData = $this->_productHorizontalSliderModel->getCollection()
                                        ->addFieldToFilter("phs_id", $componentId)
                                        ->setPageSize(1)
                                        ->setCurPage(1)
                                        ->load()->getData();
            $displayProduct = ($phsData[0]['new_arrival_product_type']) ? $phsData[0]['new_arrival_product_type'] : '0';
            if ($displayProduct == '1') {
                $displayType = ($phsData[0]['phs_display_category']) ? $phsData[0]['phs_display_category'] : '0';
            }
        }

        if ($displayProduct == '0') {
            $products = $this->_productModel->getCollection()
                            ->setStoreId($storeId)
                            ->addAttributeToFilter('status', array("eq" => 1))
                            ->addAttributeToSort('news_from_date', 'desc')
                            ->addAttributeToSort('created_at', 'desc')->addAttributeToFilter('visibility', array("neq" => 1));
            $products->addAttributeToFilter(
                'news_from_date',
                [
                'or' => [
                0 => ['date' => true, 'to' => $todayEndOfDayDate],
                1 => ['is' => new \Zend_Db_Expr('null')],
                ]
                ]
            )->addAttributeToFilter(
                [
                ['attribute' => 'news_from_date', 'is' => new \Zend_Db_Expr('not null')],
                ]
            );
        }
        $product_list = array();
        if ($displayProduct == '1') {
            $products = $this->_productModel->getCollection()->setStoreId($storeId)
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('status', array("eq" => 1))
                    ->addAttributeToFilter('visibility', array("neq" => 1))
                    ->addCategoriesFilter(array('in' => $displayType));
            $products->setOrder('entity_id', 'ASC');
            $products->addAttributeToFilter(
                'news_from_date',
                [
                'or' => [
                0 => ['date' => true, 'to' => $todayEndOfDayDate],
                1 => ['is' => new \Zend_Db_Expr('null')],
                ]
                ]
            )->addAttributeToFilter(
                [
                ['attribute' => 'news_from_date', 'is' => new \Zend_Db_Expr('not null')],
                ]
            );
        }

        $total_collection = count($products);
        
        if ($seeall != true) {
            $products->getSelect()->limit(5, 0);
        }

        $products->setCurPage($page);

        if ($limit) {
            $products->setPageSize($limit);
        }
        foreach ($products->getData() as $product) {
            $productData = $this->_productFactory->create()->setStoreId($storeId)->load($product['entity_id']);
            if ($productData->getTypeId() == 'simple' || $productData->getTypeId() == 'virtual') {
                if ($productData->getData('has_options')) {
                    $hasOptions = "1";
                } else {
                    $hasOptions = "0";
                }
            } elseif ($productData->getTypeId() == 'configurable' || $productData->getTypeId() == 'grouped' || $productData->getTypeId() == 'bundle') {
                $hasOptions = "1";
            } elseif ($productData->getTypeId() == 'downloadable') {
                $hasOptions = $productData->getData('links_purchased_separately');
            }
            /*****************************Product type wise price*********************************/
            $finalPrice = $productData->getPrice();
            $associated_products = array();
            if ($productData->getTypeId() == 'grouped') {
                $associated_products = $productData->getTypeInstance(true)->getAssociatedProducts($productData);
            } elseif ($productData->getTypeId() == 'configurable') {
                $associated_products = $productData->getTypeInstance()->getUsedProducts($productData);
            }
            $prices = array();
            foreach ($associated_products as $associated_product1) {
                $priceAssociateProduct = $this->_productFactory->create()->setStoreId($storeId)->load($associated_product1->getId());
                $prices[] = $priceAssociateProduct->getFinalPrice();
            }
            if (!empty($prices)) {
                $finalPrice = min($prices);
            }


            /*************************************************************************************/
            $status = $productData->getStatus();

            $stockState = $this->_stockItemRepository->get($productData->getId(), $productData->getStore()->getWebsiteId());
            if ($stockState->getBackorders() != 0) {
                if ($stockState->getIsInStock() == 0) {
                    $in_stock = '0';
                } else {
                    $in_stock = '1';
                }
            } else {
                if ($stockState->getManageStock() == 0) {
                    if ($stockState->getIsInStock() == true) {
                        $in_stock = '1';
                    } else {
                        $in_stock = '0';
                    }
                } else {
                    $in_stock = strval((int)$stockState->getIsInStock());
                }
            }
            $qty = $stockState->getQty();
            $summaryData = $this->getProductRatingSummary($storeId, $productData->getId());
            $wishlist_detail = $this->checkwishlist($productData->getId());
            if ($qty == 0 && $stockState->getIsInStock() == 0 && $productData->getTypeId() == "simple") {
                $qty = 'Out of Stock';
            }
            if ($this->_catalogruleModel->calcProductPriceRule($productData, $productData->getPrice())) {
                $specialPrice = $this->_catalogruleModel->calcProductPriceRule($productData, $productData->getPrice());
            } else {
                $specialPrice = $productData->getSpecialPrice();
            }
            $product_list[] = array(
            'id' => $productData->getId(),
            'sku' => $productData->getSku(),
            'name' => $productData->getName(),
            'status' => $status,
            'qty' => $qty,
            'in_stock' => $in_stock,
            'price' => $this->_cartHelper->getPriceByStoreWithCurrency($finalPrice, $storeId, $currencyCode),
            'special_price' => $this->_cartHelper->getPriceByStoreWithCurrency($specialPrice, $storeId, $currencyCode),
            'image' => $this->_imageHelper->init($productData, 'product_page_image_medium')->resize(300, 330)->constrainOnly(true)->keepAspectRatio(true)->getUrl(),
            'type' => $productData->getTypeId(),
            'is_wishlisted' => $wishlist_detail['in_wishlist'],
            'wishlist_item_id' => $wishlist_detail['wishlist_item_id'],
            'review_count' => $summaryData['review_count'],
            'average_rating' => $summaryData['rating_summary'],
            'has_options' => $hasOptions,
            // 'save_discount' => $this->_cartHelper->getDiscount($productData->getId()),
            );
        }
        $responseArr['item_count'] = $total_collection;
        $responseArr['productCollection'] = $product_list;
        $responseArr['current_page'] = $page;
        if (isset($limit)) {
            if ($page + 1 <= ceil($total_collection / $limit)) {
                $responseArr['next_page'] = $page + 1;
            }
        }
        return $responseArr;

    }
    /**
     * This function is used for get the particular product rating summary
     * @param  int $storeId
     * @param  int $productId
     * @return Array
     */
    protected function getProductRatingSummary($storeId, $productId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $reviewModelData = array();
        $reviewCount = "";
        $ratingSummary = "";
        $reviewModelData = $objectManager->create('Magento\Review\Model\Review\Summary')->setStoreId($storeId)->load($productId);
        $reviewCount = $reviewModelData->getReviewsCount();
        $ratingSummary = $reviewModelData->getRatingSummary();

        if (array_key_exists('reviews_count', $reviewModelData->getData())) {
            $reviewSummaryArray = array('review_count' => $reviewCount, 'rating_summary' => $ratingSummary);
        } else {
            $reviewSummaryArray = array('review_count' => null, 'rating_summary' => null);
        }
        return $reviewSummaryArray;
    }

    /**
     * This function is used for the get product is in wishlist or not
     * @param  int $productId
     * @return Array
     */
    protected function checkwishlist($productId)
    {
        $wishlistArray = array('in_wishlist' => false, 'wishlist_item_id' => null);
        if ($this->_wishlistHelper->isAllow()) {
            foreach ($this->_wishlistHelper->getWishlistItemCollection() as $wishlistItem) {
                if ($productId == $wishlistItem->getProduct()->getId()) {
                    $wishlistArray = array('in_wishlist' => true, 'wishlist_item_id' => $wishlistItem->getId());
                    break;
                }
            }
        }
        return $wishlistArray;
    }
    private function getSalebleQty($productSku)
    {
        $code = 'base';
        $type = 'website';
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $salesChannelId = $objectManager->get('Magento\InventorySales\Model\ResourceModel\StockIdResolver')->resolve($type, $code);
            $stockName = $objectManager->get('Magento\Inventory\Model\Stock')->load($salesChannelId);
            $stockName = $stockName->getName();
            $allowProduct = $objectManager->get('Magento\InventorySales\Model\IsProductSalableCondition\IsProductSalableConditionChain')->execute($productSku, $salesChannelId);
            $this->_salebleModel = $objectManager->get('Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku');
            $salebleModelData = $this->_salebleModel->execute((string)$productSku);
            foreach ($salebleModelData as $key => $value) {
                if ($value['stock_name'] == $stockName) {
                    $finalSalesData['stock_name'] = $value['stock_name'];
                    $finalSalesData['qty'] = $value['qty'];
                    $finalSalesData['manage_stock'] = $value['manage_stock'];
                }
            }
            return $finalSalesData;
        } catch (\Exception $e) {
            $salebleModelData = array();
            return $salebleModelData;
        }
        return $salebleModelData;
    }
    private function allowProduct($productSku)
    {
        $code = 'base';
        $type = 'website';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $salesChannelId = $objectManager->get('Magento\InventorySales\Model\ResourceModel\StockIdResolver')->resolve($type, $code);
        $allowProduct = $objectManager->get('Magento\InventorySales\Model\IsProductSalableCondition\IsProductSalableConditionChain')->execute($productSku, $salesChannelId);
        return $allowProduct;
    }
}
