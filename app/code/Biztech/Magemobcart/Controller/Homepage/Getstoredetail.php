<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Homepage;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Action\Context;

class Getstoredetail extends \Magento\Framework\App\Action\Action
{
    const XML_PATH_ENABLED = 'contact/contact/enabled';
    const XML_PATH_ALLOWEDCMSPAGES = 'magemobcart/magemobcart_general/allowed_mobile_cms_pages';
    const XML_PATH_BARCODE = 'magemobcart/magemobcart_general/enable_barcode';
    const XML_PATH_COLOR = 'magemobcart/themeselection/primary_background';
    const XML_PATH_SECONDARYCOLOR = 'magemobcart/themeselection/secondary_background';
    const XML_PATH_BGCOLOR = 'magemobcart/themeselection/background_color';
    const XML_PATH_BTNTXTCOLOR = 'magemobcart/themeselection/button_text_color';
    const XML_PATH_LANGUAGECODE = 'general/locale/code';
    const XML_PATH_CURRENCYCODE = 'currency/options/default';
    const XML_PATH_HELPTOLL = 'magemobcart/helpdesk/tollfreenumber';
    const XML_PATH_HELPEMAIL = 'magemobcart/helpdesk/helpemail';
    const XML_PATH_HELPADD = 'magemobcart/helpdesk/helpaddress';
    const XML_PATH_WISHLISTACTIVE = 'wishlist/general/active';
    const XML_PATH_DDSEN = 'deliverydate/deliverydate_general/enabled';
    const XML_PATH_DDSDISPLAYAT = 'deliverydate/deliverydate_general/on_which_page';
    const XML_PATH_ZIPVALIDDATA = 'zipcode_restriction/activation/data';
    const XML_PATH_ZIPVALIDINSTALLED = 'zipcode_restriction/activation/installed';
    const XML_PATH_ZIPVALIDWEBSITES = 'zipcode_restriction/activation/websites';
    const XML_PATH_ZIPCHECKEN = 'zipcode_restriction/general/enable';
    const XML_PATH_ZIPCHECKPLACEHOLDER = 'zipcode_restriction/general/zipcheck_placeholder';
    const XML_PATH_ZIPCHECKBTNTXT = 'zipcode_restriction/general/zipcheck_button_text';
    const XML_PATH_ZIPSHOWEST = 'zipcode_restriction/general/show_estimated_time';
    const XML_PATH_ZIPESTFORMAT = 'zipcode_restriction/general/format_type';
    const XML_PATH_ZIPESTDATEFORMAT = 'zipcode_restriction/general/date_format';
    const XML_PATH_DDSSUPPORTEN = 'magemobcart/magemobcart_dds_support/enabled';
    const XML_PATH_ZIPVALIDSUPPORTEN = 'magemobcart/magemobcart_zipvalidator_support/enabled';

    protected $jsonFactory;
    protected $request;
    protected $storeManager;
    protected $cartHelper;
    protected $customerSession;
    protected $cartModel;
    protected $scopeConfig;
    protected $localeCurrency;
    protected $localeInterface;
    protected $cmspageModel;
    protected $filterProvider;
    protected $encryptor;
    protected $_formkey;

    /**
     * @param Context                                            $context
     * @param JsonFactory                                        $jsonFactory
     * @param \Magento\Store\Model\StoreManagerInterface         $storeManager
     * @param \Biztech\Magemobcart\Helper\Data                   $cartHelper
     * @param \Magento\Customer\Model\Session                    $customerSession
     * @param \Magento\Checkout\Model\Cart                       $cartModel
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Locale\CurrencyInterface        $localeCurrency
     * @param \Magento\Framework\Locale\ResolverInterface        $localeInterface
     * @param \Magento\Framework\Encryption\EncryptorInterface   $encryptor
     * @param \Magento\Cms\Model\PageFactory                     $cmspageModel
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        Http $request,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Biztech\Magemobcart\Helper\Data $cartHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Cart $cartModel,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Locale\CurrencyInterface $localeCurrency,
        \Magento\Framework\Locale\ResolverInterface $localeInterface,
        \Magento\Cms\Model\PageFactory $cmspageModel,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Framework\Data\Form\FormKey $formKey
    ) {
        $this->_jsonFactory = $jsonFactory;
        $this->_request = $request;
        $this->_storeManager = $storeManager;
        $this->_cartHelper = $cartHelper;
        $this->_customerSession = $customerSession;
        $this->_cartModel = $cartModel;
        $this->_scopeConfig = $scopeConfig;
        $this->_localeCurrency = $localeCurrency;
        $this->_localeInterface = $localeInterface;
        $this->_cmspageModel = $cmspageModel;
        $this->_filterProvider = $filterProvider;
        $this->_encryptor = $encryptor;
        $this->_formkey = $formKey;
        $this->_request->setParam('form_key', $this->_formkey->getFormKey());
        parent::__construct($context);
    }

    /**
     * This function is used for get all store details of websites.
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        $postData = $this->_request->getParams();
        $isDefaultStoreId = (isset($postData['storeid']) && $postData['storeid'] != '' && $postData['storeid'] != 0) ? $postData['storeid'] : $this->_storeManager->getStore()->getId();
        $appActiveStores = $this->_cartHelper->getAllWebsites();
        if(!in_array($isDefaultStoreId, $appActiveStores)) {
            $isDefaultStoreId = $appActiveStores[0];
        }

        if ($this->_cartHelper->isEnable($isDefaultStoreId)) {
            if (!$this->_cartHelper->getHeaders()) {
                $errorResult = array('status'=> false,'message' => $this->_cartHelper->getHeaderMessage());
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $sessionId = '';
            if (isset($postData['session_id']) && $postData['session_id'] != null) {
                $sessionId = $postData['session_id'];
                if (!$this->_customerSession->isLoggedIn()) {
                    $customerId = explode("_", $sessionId);
                    $this->_cartHelper->relogin($customerId[0]);
                }
            }
            try {
                $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
                // here we get default locale based on current store
                $defaultLocale = (isset($postData['storeid']) && $postData['storeid'] != '' && $postData['storeid'] != 0) ? $this->_scopeConfig->getValue(self::XML_PATH_LANGUAGECODE, $storeScope, $postData['storeid']) : $this->_localeInterface->getLocale();
                // here we get default currency based on current store
                $currentCurrencyCode = (isset($postData['currency_code']) && $postData['currency_code'] != '') ? $postData['currency_code'] : $this->_scopeConfig->getValue(self::XML_PATH_CURRENCYCODE, $storeScope, $isDefaultStoreId);
                $this->_storeManager->getStore($isDefaultStoreId)->setCurrentCurrencyCode($currentCurrencyCode);

                $isZipValidatorInstalled = ($this->_cartHelper->isModuleEnabled('Biztech_ZipValidator') && (bool)$this->_scopeConfig->getValue(self::XML_PATH_ZIPVALIDSUPPORTEN, $storeScope, $isDefaultStoreId)) ? $this->_cartHelper->isModuleEnabled('Biztech_ZipValidator') : false;
                if ($isZipValidatorInstalled) {
                    $isZipValidatorAvailable = $this->checkZipValidatorEnabled($isDefaultStoreId);
                } else {
                    $isZipValidatorAvailable = false;
                }
                if ($isZipValidatorAvailable) {

                    $showEstimatedDateTime = $this->_scopeConfig->getValue(self::XML_PATH_ZIPSHOWEST, $storeScope, $isDefaultStoreId);
                    if ($showEstimatedDateTime == 1) {
                        $zipEstFormat = $this->_scopeConfig->getValue(self::XML_PATH_ZIPESTFORMAT, $storeScope, $isDefaultStoreId);
                        $estimatedDateTimeFormat = ($zipEstFormat != 'custom') ? $zipEstFormat : $this->_scopeConfig->getValue(self::XML_PATH_ZIPESTDATEFORMAT, $storeScope, $isDefaultStoreId);
                    } else {
                        $estimatedDateTimeFormat = '';
                    }
                }

                foreach ($this->_storeManager->getWebsites() as $website) {
                    $websiteScope = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES;
                    $websiteDefaultStoreId = $this->_storeManager->getWebsite($website->getId())->getDefaultStore()->getId();
                    if (!in_array($websiteDefaultStoreId, $appActiveStores)) {
                        foreach ($appActiveStores as $appActiveStoreId) {
                            $storeWebsiteId = $this->_storeManager->getStore($appActiveStoreId)->getWebsiteId();
                            if ($storeWebsiteId == $website->getId()) {
                                $websiteActiveStoreId = $appActiveStoreId;
                                break;
                            }
                        }
                        if (isset($websiteActiveStoreId)) {
                            $websiteDefaultStoreId = $websiteActiveStoreId;
                            unset($websiteActiveStoreId);
                        } else {
                            continue;
                        }
                    }
                    $storeResponse['website'][] = [
                        'website_id' => $website->getId(), 
                        'website_name' => $website->getName(),
                        'is_default' => ($this->_storeManager->getStore($isDefaultStoreId)->getWebsiteId() == $website->getId()) ? 1 : 0,
                        'default_store_id' => $websiteDefaultStoreId,
                        'is_active_wishlist' => ($this->_scopeConfig->getValue(self::XML_PATH_WISHLISTACTIVE, $websiteScope, $website->getId()) == 1) ? true : false
                    ];

                    foreach ($website->getGroups() as $group) {
                        $stores = $group->getStores();

                        foreach ($stores as $store) {
                            if ($store->getIsActive() == 1) {
                                $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;

                                $currentLocale = $this->_scopeConfig->getValue(self::XML_PATH_LANGUAGECODE, $storeScope, $store->getId());
                                $this->_localeInterface->setLocale($currentLocale);
                                
                                $currencyCode = $this->_scopeConfig->getValue(self::XML_PATH_CURRENCYCODE, $storeScope, $store->getId());
                                $currencySign = $this->_localeCurrency->getCurrency($currencyCode)->getSymbol();
                                if ($currencySign == "") {
                                    $currencySign = $currencyCode;
                                }

                                $allowedCurrencies = [];
                                $allowedCurrencyCodes = $this->_storeManager->getStore($store->getId())->getAvailableCurrencyCodes(true);
                                foreach ($allowedCurrencyCodes as $curCode) {
                                    $curName = $this->_localeCurrency->getCurrency($curCode)->getName();
                                    $curName = ($curName != '') ? $curName : $curCode;
                                    $curSymbol = $this->_localeCurrency->getCurrency($curCode)->getSymbol();
                                    $curSymbol = ($curSymbol != '') ? $curSymbol : $curCode;

                                    $allowedCurrencies[] = [
                                        'label' => $curName,
                                        'code' => $curCode,
                                        'symbol' => $curSymbol,
                                        'is_default' => ($curCode == $currentCurrencyCode) ? 1 : 0
                                    ];
                                }

                                if (in_array($store->getId(), $this->_cartHelper->getAllWebsites()) && $store->getId() == $isDefaultStoreId) {
                                    $languageCode = $this->_scopeConfig->getValue(self::XML_PATH_LANGUAGECODE, $storeScope, $store->getId());
                                    $storeResponse['stores'][] = [
                                        'store_id' => $store->getId(), 
                                        'store_name' => $store->getName(), 
                                        'language_code' => $languageCode, 
                                        'is_default' => 1, 
                                        'currency_code' => $currencyCode, 
                                        'currency_sign' => $currencySign,
                                        'allowed_currencies' => $allowedCurrencies, 
                                        'website_id' => $this->_storeManager->getStore($store->getId())->getWebsiteId(),
                                        'store_base_url' => $this->_storeManager->getStore($store->getId())->getBaseUrl() . "magemobcart/",
                                        'is_active_wishlist' => ($this->_scopeConfig->getValue(self::XML_PATH_WISHLISTACTIVE, $storeScope, $store->getId()) == 1) ? true : false,
                                        'deliverydate_enabled' => $this->_cartHelper->isModuleEnabled('Biztech_Deliverydate') ? (bool)$this->_scopeConfig->getValue(self::XML_PATH_DDSEN, $storeScope, $isDefaultStoreId) : false,
                                        'deliverydate_display_at' => $this->_cartHelper->isModuleEnabled('Biztech_Deliverydate') ? $this->_scopeConfig->getValue(self::XML_PATH_DDSDISPLAYAT, $storeScope, $isDefaultStoreId) : '',
                                        'zip_validator_enable' => ($isZipValidatorAvailable) ? (bool)$this->_scopeConfig->getValue(self::XML_PATH_ZIPCHECKEN, $storeScope, $isDefaultStoreId) : false,
                                        'zip_check_label' => ($isZipValidatorAvailable) ? $this->_scopeConfig->getValue(self::XML_PATH_ZIPCHECKPLACEHOLDER, $storeScope, $isDefaultStoreId) : '',
                                        'zip_check_button_text' => ($isZipValidatorAvailable) ? $this->_scopeConfig->getValue(self::XML_PATH_ZIPCHECKBTNTXT, $storeScope, $isDefaultStoreId) : '',
                                        'show_estimated_time' => ($isZipValidatorAvailable) ? (bool)$showEstimatedDateTime : false,
                                        'estimated_time_format' => ($isZipValidatorAvailable) ? $estimatedDateTimeFormat : ''
                                    ];
                                } else if (in_array($store->getId(), $this->_cartHelper->getAllWebsites())) {
                                    $currencyCode = $this->_scopeConfig->getValue(self::XML_PATH_CURRENCYCODE, $storeScope, $store->getId());
                                    $currencySign = $this->_localeCurrency->getCurrency($currencyCode)->getSymbol();
                                    if ($currencySign == "") {
                                        $currencySign = $currencyCode;
                                    }
                                    $languageCode = $this->_scopeConfig->getValue(self::XML_PATH_LANGUAGECODE, $storeScope, $store->getId());
                                    $storeResponse['stores'][] = [
                                        'store_id' => $store->getId(), 
                                        'store_name' => $store->getName(), 
                                        'language_code' => $languageCode, 
                                        'currency_code' => $currencyCode, 
                                        'currency_sign' => $currencySign, 
                                        'allowed_currencies' => $allowedCurrencies, 
                                        'website_id' => $this->_storeManager->getStore($store->getId())->getWebsiteId(),
                                        'store_base_url' => $this->_storeManager->getStore($store->getId())->getBaseUrl() . "magemobcart/",
                                        'is_active_wishlist' => ($this->_scopeConfig->getValue(self::XML_PATH_WISHLISTACTIVE, $storeScope, $store->getId()) == 1) ? true : false
                                    ];
                                }
                            }
                        }
                    }
                }

                $this->_localeInterface->setLocale($defaultLocale);
                $this->_localeInterface->setDefaultLocale($defaultLocale);

                // *********************** Allowed cms pages content ****************************

                $allowedMobileCmsPagesData = [];
                $allowedCmsForMobile = explode(',', $this->_scopeConfig->getValue(self::XML_PATH_ALLOWEDCMSPAGES, $storeScope, $isDefaultStoreId));
                foreach ($allowedCmsForMobile as $cmsPageKey) {
                    $cmsPageData = $this->_cmspageModel->create()->load($cmsPageKey);
                    $cmsPageTitle = $cmsPageData->getTitle();
                    $cmsPageContent = $cmsPageData->getContent();
                    $htmlCMSPage = $this->_filterProvider->getPageFilter()->filter($cmsPageContent);
                    $allowedMobileCmsPagesData[] = [
                                                    'title' => $cmsPageTitle,
                                                    'content' => $htmlCMSPage
                                                ];
                }

                $storeResponse['cms_pages'] = $allowedMobileCmsPagesData;

                // *********************** Help Desk ****************************
                $storeResponse['help_toll'] = $this->_scopeConfig->getValue(self::XML_PATH_HELPTOLL, $storeScope, $isDefaultStoreId);

                $storeResponse['help_email'] = $this->_scopeConfig->getValue(self::XML_PATH_HELPEMAIL, $storeScope, $isDefaultStoreId);

                $storeResponse['help_address'] = $this->_scopeConfig->getValue(self::XML_PATH_HELPADD, $storeScope, $isDefaultStoreId);

                // *************************************************************************
                $storeResponse['byi_enabled'] = false;

                $enableContactUs = $this->_scopeConfig->getValue(self::XML_PATH_ENABLED, $storeScope, $isDefaultStoreId);
                $storeResponse['enable_contactus'] = (bool)$enableContactUs;

                $themePrimarycolor = $this->_scopeConfig->getValue(self::XML_PATH_COLOR, $storeScope, $isDefaultStoreId);
                $storeResponse['color'] = $themePrimarycolor;
                $themeSecondarycolor = $this->_scopeConfig->getValue(self::XML_PATH_SECONDARYCOLOR, $storeScope, $isDefaultStoreId);
                $storeResponse['secondary_color'] = $themeSecondarycolor;

                $themeBackgroundColor = $this->_scopeConfig->getValue(self::XML_PATH_BGCOLOR, $storeScope, $isDefaultStoreId);
                $storeResponse['background_color'] = $themeBackgroundColor;
                $themeButtonTextColor = $this->_scopeConfig->getValue(self::XML_PATH_BTNTXTCOLOR, $storeScope, $isDefaultStoreId);
                $storeResponse['button_text_color'] = $themeButtonTextColor;

                $enableSku = $this->_scopeConfig->getValue(self::XML_PATH_BARCODE, $storeScope, $isDefaultStoreId);
                $storeResponse['is_barcode'] = $enableSku;
                $storeResponse['magento_version'] = true;
                
                $storeResponse['appstore_link'] = "https://apps.apple.com/us/app/magemob-app-builder/id1039362898";
                $storeResponse['playstore_link'] = "https://play.google.com/store/apps/details?id=com.biztech.magemobcart";
                $storeResponse['inappupdate_link'] = "itms-apps://itunes.apple.com/us/app/magemob-app-builder/id1039362898";

                $storeResponse['dds_support_enable'] = ($this->_cartHelper->isModuleEnabled('Biztech_Magemobdds') && (bool)$this->_scopeConfig->getValue(self::XML_PATH_DDSSUPPORTEN, $storeScope, $isDefaultStoreId)) ? $this->_cartHelper->isModuleEnabled('Biztech_Magemobdds') : false;
                
            } catch (\Exception $e) {
                $storeResponse = array(
                    'status' => 'false',
                    'message' => __($e->getMessage())
                );
            }
            $jsonResult->setData($storeResponse);
            return $jsonResult;
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }

    public function checkZipValidatorEnabled($storeId)
    {
        $value = $this->_scopeConfig->getValue(self::XML_PATH_ZIPVALIDINSTALLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if (!$value) {
            return false;
        }

        $data = $this->_scopeConfig->getValue(self::XML_PATH_ZIPVALIDDATA, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $web = $this->_scopeConfig->getValue(self::XML_PATH_ZIPVALIDWEBSITES, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $websites = explode(',', str_replace($data, '', $this->_encryptor->decrypt($web)));
        $websites = array_diff($websites, ['']);
        return in_array($storeId, $websites);
    }
}
