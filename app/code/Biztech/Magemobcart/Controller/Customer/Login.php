<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Customer;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\EmailNotConfirmedException;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\State\UserLockedException;

class Login extends \Magento\Framework\App\Action\Action
{
    protected $jsonFactory;
    protected $storeManager;
    protected $cartHelper;
    protected $request;
    protected $customerSession;
    protected $cartModel;
    protected $newsletterModel;
    protected $customerModel;
    protected $accountManagementInterface;
    protected $formKey;
    protected $_customerRepositoryInterface;
    protected $_encryption;

    /**
     * @param Context                                           $context
     * @param JsonFactory                                       $jsonFactory
     * @param \Magento\Store\Model\StoreManagerInterface        $storeManager
     * @param \Magento\Framework\App\Request\Http               $request
     * @param \Biztech\Magemobcart\Helper\Data                  $cartHelper
     * @param \Magento\Customer\Model\Session                   $customerSession
     * @param \Magento\Customer\Model\Customer                  $customerModel
     * @param \Magento\Checkout\Model\Cart                      $cartModel
     * @param \Magento\Newsletter\Model\Subscriber              $newsletterModel
     * @param \Magento\Customer\Api\AccountManagementInterface  $accountManagementInterface
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     * @param \Magento\Framework\Encryption\EncryptorInterface  $encryption
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Request\Http $request,
        \Biztech\Magemobcart\Helper\Data $cartHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Customer $customerModel,
        \Magento\Checkout\Model\Cart $cartModel,
        \Magento\Newsletter\Model\Subscriber $newsletterModel,
        \Magento\Customer\Api\AccountManagementInterface $accountManagementInterface,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Framework\Encryption\EncryptorInterface $encryption
    ) {
        $this->_jsonFactory = $jsonFactory;
        $this->_storeManager = $storeManager;
        $this->_cartHelper = $cartHelper;
        $this->_request = $request;
        $this->_customerSession = $customerSession;
        $this->_cartModel = $cartModel;
        $this->_newsletterModel = $newsletterModel;
        $this->_customerModel = $customerModel;
        $this->_accountManagementInterface = $accountManagementInterface;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_encryption = $encryption;
        $this->formKey = $formKey;
        $this->_request->setParam('form_key', $this->formKey->getFormKey());
        parent::__construct($context);
    }

    /**
     * This function is used for customer login
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_cartHelper->isEnable()) {
            if (!$this->_cartHelper->getHeaders()) {
                $errorResult = array('status'=> false,'message' => $this->_cartHelper->getHeaderMessage());
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $postData = $this->_request->getParams();
            $responseArr['status'] = 'false';
            $storeId = (isset($postData['storeid']) && $postData['storeid'] != '') ? $postData['storeid'] : $this->_storeManager->getStore()->getId();
            $currencyCode = (isset($postData['currency_code']) && $postData['currency_code'] != '') ? $postData['currency_code'] : $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();
            $sessionId = '';
            if (isset($postData['session_id']) && $postData['session_id'] != null) {
                $sessionId = $postData['session_id'];
                if (!$this->_customerSession->isLoggedIn()) {
                    $customer_id = explode("_", $sessionId);
                    $this->_cartHelper->relogin($customer_id[0]);
                }
            }
            if (!empty($postData['username']) && !empty($postData['password'])) {
                try {
                    $password = $postData['password'];
                    try {
                        $session = $this->_customerSession;
                        $customer = $this->_accountManagementInterface->authenticate($postData['username'], $postData['password']);
                        $this->_customerSession->setCustomerDataAsLoggedIn($customer);
                        $this->_customerSession->regenerateId();
                    } catch (EmailNotConfirmedException $e) {
                        $value = $this->customerUrl->getEmailConfirmationUrl($login['username']);
                        $message = __('This account is not confirmed.');
                        $responseArr['status'] = 'false';
                        $responseArr['message'] = $message;
                        $jsonResult->setData($responseArr);
                        return $jsonResult;
                    } catch (UserLockedException $e) {
                        $message = __('You did not sign in correctly or your account is temporarily disabled.');
                        $responseArr['status'] = 'false';
                        $responseArr['message'] = $message;
                        $jsonResult->setData($responseArr);
                        return $jsonResult;
                    } catch (AuthenticationException $e) {
                        $message = __('You did not sign in correctly or your account is temporarily disabled.');
                        $responseArr['status'] = 'false';
                        $responseArr['message'] = $message;
                        $jsonResult->setData($responseArr);
                        return $jsonResult;
                    } catch (LocalizedException $e) {
                        $message = __($e->getMessage());
                        $responseArr['status'] = 'false';
                        $responseArr['message'] = $message;
                        $jsonResult->setData($responseArr);
                        return $jsonResult;
                    } catch (\Exception $e) {
                        $message = __('An unspecified error occurred. Please contact us for assistance.');
                        $responseArr['status'] = 'false';
                        $responseArr['message'] = $message;
                        $jsonResult->setData($responseArr);
                        return $jsonResult;
                    }
                    
                    if ($this->_customerSession->isLoggedIn()) {
                        $customer = $this->_customerSession->getCustomer();
                        $customerData = $this->_customerRepositoryInterface->getById($customer->getId());
                        $byi_url_id = '';
                        $responseArr['welcome_text'] = __('Welcome, '. ucfirst($this->_customerSession->getCustomer()->getName()));
                        $wishListItemCollection = $this->_cartHelper->getWishlistData($customer->getId(), $storeId, $currencyCode, $byi_url_id);
                        $responseArr['status'] = 'true';
                        $responseArr['message'] = __('Successfully logged in.');
                        $responseArr['cart_count'] = $this->_cartModel->getQuote()->getItemsCount();
                        $responseArr['wishlist_count'] = count($wishListItemCollection);
                        $responseArr['customer_id'] = $customer->getId();
                        $responseArr['firstname'] = $customer->getFirstname();
                        $responseArr['lastname'] = $customer->getLastname();
                        if (sizeof($customerData->getAddresses()) > 0) {
                            $responseArr['is_address_available'] = true;
                        } else {
                            $responseArr['is_address_available'] = false;
                        }
                        $subscriber_data = $this->_newsletterModel->loadByEmail($postData['username']);
                        if ($subscriber_data->getId()) {
                            if ($subscriber_data->getSubscriberStatus() == 1) {
                                $responseArr['is_subscribed'] = $subscriber_data->getSubscriberStatus();
                            } else {
                                $responseArr['is_subscribed'] = "0";
                            }
                        } else {
                            $responseArr['is_subscribed'] = "0";
                        }

                        $session_id = $customer->getId() . '_' . $this->_encryption->encrypt($postData['username']);

                        if ($session_id) {
                            $responseArr['session_id'] = $session_id;
                            $data = array('username' => $postData['username'], 'devicetoken' => $postData['device_token'], 'customer_id' => $customer->getId(), 'is_logout' => 0,'password' => $postData['password'],'customer_id' => $customer->getId());
                            $this->_cartHelper->create($data);
                        }
                        $responseArr['byi_enabled'] = false;
                    }
                } catch (Exception $e) {
                    $responseArr['message'] = $message;
                    $responseArr['status'] = 'false';
                    $session->setUsername($postData['username']);
                }
            } else {
                $responseArr['message'] = __('Login and password are required.');
                $responseArr['status'] = 'false';
            }
            $jsonResult->setData($responseArr);
            return $jsonResult;
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }
}
