<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Customer;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Context;

class GetCustomerwishlist extends \Magento\Framework\App\Action\Action
{
    protected $jsonFactory;
    protected $storeManager;
    protected $cartHelper;
    protected $request;
    protected $customerSession;
    protected $formKey;
    
    /**
     * @param Context                                       $context
     * @param JsonFactory                                   $jsonFactory
     * @param \Magento\Store\Model\StoreManagerInterface    $storeManager
     * @param \Biztech\Magemobcart\Helper\Data              $cartHelper
     * @param \Magento\Framework\App\Request\Http           $request
     * @param \Magento\Customer\Model\Session               $customerSession
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Biztech\Magemobcart\Helper\Data $cartHelper,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Data\Form\FormKey $formKey
    ) {
        $this->_jsonFactory = $jsonFactory;
        $this->_storeManager = $storeManager;
        $this->_cartHelper = $cartHelper;
        $this->_request = $request;
        $this->_customerSession = $customerSession;
        $this->formKey = $formKey;
        $this->_request->setParam('form_key', $this->formKey->getFormKey());
        parent::__construct($context);
    }

    /**
     * This function is used for get all wishlist product list of customer.
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_cartHelper->isEnable()) {
            if (!$this->_cartHelper->getHeaders()) {
                $errorResult = array('status'=> false,'message' => $this->_cartHelper->getHeaderMessage());
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            try {
                $postData = $this->_request->getParams();
                $sessionId = '';
                if (isset($postData['session_id']) && $postData['session_id'] != null) {
                    $sessionId = $postData['session_id'];
                    if (!$this->_customerSession->isLoggedIn()) {
                        $customerId = explode("_", $sessionId);
                        $this->_cartHelper->relogin($customerId[0]);
                    }
                }
                if (array_key_exists('customer_id', $postData) && array_key_exists('storeid', $postData)) {
                    $customerId = $postData['customer_id'];
                    $storeId = (isset($postData['storeid']) && $postData['storeid'] != '') ? $postData['storeid'] : $this->_storeManager->getStore()->getId();
                    $currencyCode = (isset($postData['currency_code']) && $postData['currency_code'] != '') ? $postData['currency_code'] : $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();
                    if ($customerId != "" && $storeId != "") {
                        // Below code is to set requested store and currency
                        $currentStoreId = $this->_storeManager->getStore()->getId();
                        $currentCurrencyCode = $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();
                        $this->_storeManager->setCurrentStore($storeId);
                        $this->_storeManager->getStore($storeId)->setCurrentCurrencyCode($currencyCode);
                        
                        $wishlistCollection = array();
                        $wishlistCollection = $this->_cartHelper->getWishlistData($customerId, $storeId, $currencyCode);
                        $responseArr['wishlistCollection'] = $wishlistCollection;

                        // Below code is to reset to deafult current store and current currency
                        $this->_storeManager->getStore($storeId)->setCurrentCurrencyCode($currentCurrencyCode);
                        $this->_storeManager->setCurrentStore($currentStoreId);
                        
                        $jsonResult->setData($responseArr);
                        return $jsonResult;
                    } else {
                        $responseArr['status'] = 'false';
                        $responseArr['message'] = __('Customer and Store should not blank');
                        $jsonResult->setData($responseArr);
                        return $jsonResult;
                    }
                } else {
                    $responseArr['status'] = 'false';
                    $responseArr['message'] = __('Store or customer is missing');
                    $jsonResult->setData($responseArr);
                    return $jsonResult;
                }
            } catch (\Exception $e) {
                $responseArr['status'] = 'false';
                $responseArr['message'] = $e->getMessage();
                $jsonResult->setData($responseArr);
                return $jsonResult;
            }
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }
}
