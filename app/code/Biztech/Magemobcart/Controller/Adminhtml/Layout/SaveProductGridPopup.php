<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Adminhtml\Layout;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\Result\JsonFactory;

class SaveProductGridPopup extends \Magento\Backend\App\Action
{
    protected $_featuredCategoryBlockModel;
    protected $_layoutCollectionFactory;
    protected $storeManagerInterface;
    protected $backendSession;
    protected $productGridModel;

    public function __construct(
        Action\Context $context,
        JsonFactory $jsonFactory,
        \Biztech\Magemobcart\Model\ProductgridFactory $productGridModel,
        \Biztech\Magemobcart\Model\ResourceModel\Layout\CollectionFactory $layoutCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Backend\Model\Session $backendSession
    ) {
        $this->_jsonFactory = $jsonFactory;
        $this->_layoutCollectionFactory = $layoutCollectionFactory;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_backendSession = $backendSession;
        $this->productGridModel = $productGridModel;
        parent::__construct($context);
    }
    public function execute()
    {
        $postdata = $this->getRequest()->getPostValue();

        $jsonResult = $this->_jsonFactory->create();
        $result = ['status' => 'error' , 'message' => 'something went wrong while saving'];
        try {
            if ($postdata) {
                $modelData = $this->productGridModel->create()->getCollection()->addFieldToFilter("productgrid_id", $postdata['productgrid_id']);
                $model = $this->productGridModel->create();
                
                if ($postdata['product_type'] == "category_products") {
                    if (isset($postdata['category_products'])) {
                        $postdata['category_products'] = json_encode($postdata['category_products']);
                    }else {
                        $postdata['category_products'] = null;
                    }
                    $postdata['product_list'] = null;
                } else {
                    if (isset($postdata['product_list'])) {
                        $postdata['product_list'] = json_encode($postdata['product_list']);
                    } else {
                        $postdata['product_list'] = null;
                    }
                    $postdata['category_products'] = null;
                }

                if ($modelData->count()) {
                    $model->load($modelData->getLastItem()->getId());
                    $model->setComponentTitle($postdata['component_title']);
                    $model->setProductType($postdata['product_type']);
                    $model->setCategoryId($postdata['category_id']);
                    $model->setCategoryProducts(isset($postdata['category_products']) ? $postdata['category_products'] : null);
                    $model->setProductList(isset($postdata['product_list']) ? $postdata['product_list'] : null);
                } else {
                    $model->setData($postdata);
                }
                $this->messageManager->addSuccess(__('Product grid saved successfully'));
                $model->save();
            }
            $result['status'] = "success";
            $result['message'] = (string) __("Product grid saved successfully");
        } catch (Exception $e) {
            $result['message'] = $e->getMessage();
        }
        $jsonResult->setData($result);
        return $jsonResult;
    }
}
