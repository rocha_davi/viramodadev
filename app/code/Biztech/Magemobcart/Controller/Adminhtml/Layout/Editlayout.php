<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Adminhtml\Layout;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Editlayout extends Action
{
    protected $_resultPageFactory;
    protected $_registry;
    protected $_layoutModel;
    protected $_layoutCollectionFactory;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Biztech\Magemobcart\Model\Layout $layoutModel,
        \Biztech\Magemobcart\Model\ResourceModel\Layout\CollectionFactory $layoutCollectionFactory
    ) {
        parent::__construct($context);
        $this->_resultPageFactory = $resultPageFactory;
        $this->_registry = $registry;
        $this->_layoutModel = $layoutModel;
        $this->_layoutCollectionFactory = $layoutCollectionFactory;
    }
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $storeViewId = $this->getRequest()->getParam('store');
        $model = $this->_layoutModel;
        if (isset($id)) {
            $model->setStoreViewId($storeViewId)->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This layout no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }

        }
        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setLayoutId($data['layout_id']);
        }
        $registryObject = $this->_registry;
        $registryObject->register('magemobcart_edit_layout_data', $model);
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Biztech_Magemobcart::layout');
        $resultPage->getConfig()->getTitle()
                ->prepend($model->getId() ? __('Edit Layout') : __('Add Layout'));
        return $resultPage;
    }
}
