<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Adminhtml\Layout;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\Result\JsonFactory;

class Savelayout extends \Magento\Backend\App\Action
{
    protected $_layoutComponentModelFactory;
    protected $backendSession;
    protected $_jsonFactory;

    public function __construct(
        Action\Context $context,
        JsonFactory $jsonFactory,
        \Biztech\Magemobcart\Model\LayoutcomponentFactory $layoutComponentModelFactory,
        \Magento\Backend\Model\Session $backendSession
    ) {
        $this->_layoutComponentModelFactory = $layoutComponentModelFactory;
        $this->_backendSession = $backendSession;
        $this->_jsonFactory = $jsonFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $postdata = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();

        $_jsonResult = $this->_jsonFactory->create();
        $result = ['status' => 'error' , 'message' => 'something went wrong while saving'];

        if ($postdata) {
            $id = 0;
            $layout_id = 0;
            $component_data = '[]';
            $recent_product_count = 0;

            if (array_key_exists('layout_id', $postdata)) {
                $layout_id = $postdata['layout_id'];
            }
            if (array_key_exists('component_data', $postdata)) {
                $component_data = json_encode($postdata['component_data']);
            }
            if (array_key_exists('recent_product_count', $postdata) && $postdata['recent_product_count'] != 0) {
                $recent_product_count = $postdata['recent_product_count'];
            }
            
            if ($layout_id != 0) {
                try {

                    $model1 = $this->_layoutComponentModelFactory->create()
                    ->getCollection()
                    ->addFieldToFilter('layout_id', ['eq' => $layout_id])
                    ->setPageSize(1)
                    ->setCurPage(1)
                    ->load();
                    foreach ($model1 as $data) {
                        $id = $data->getId();
                    }

                    $model = $this->_layoutComponentModelFactory->create();

                    if ($id) {
                        $model->load($id);
                    }
                    $model->setLayoutId($layout_id);
                    $model->setComponentData($component_data);
                    if ($recent_product_count == 1) {
                        $model->setRecentProductCount($recent_product_count);
                    } else {
                        $model->setRecentProductCount(0);
                    }
                    $model->setComponentData($component_data);
                    $model->save();
                    
                    $result['status'] = "success";
                    $result['message'] = (string) __("Layout Saved Successfully");
                } catch (\Magento\Framework\Exception\LocalizedException $e) {
                    $this->messageManager->addError($e->getMessage());
                    $result['message'] = $e->getMessage();
                } catch (\RuntimeException $e) {
                    $this->messageManager->addError($e->getMessage());
                    $result['message'] = $e->getMessage();
                } catch (\Exception $e) {
                    $this->messageManager->addException($e, __('Something went wrong while saving layout component.'));
                    $result['message'] = __('Something went wrong while saving layout component.');
                }
            }
        }
        $_jsonResult->setData($result);
        return $_jsonResult;
    }
}
