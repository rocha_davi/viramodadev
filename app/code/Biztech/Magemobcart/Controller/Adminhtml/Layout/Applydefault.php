<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Adminhtml\Layout;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Applydefault extends Action
{
    protected $_layoutModel;
    protected $_layoutCollectionFactory;

    public function __construct(
        Context $context,
        \Biztech\Magemobcart\Model\Layout $layoutModel,
        \Biztech\Magemobcart\Model\ResourceModel\Layout\CollectionFactory $layoutCollectionFactory
    ) {
        parent::__construct($context);
        $this->_layoutModel = $layoutModel;
        $this->_layoutCollectionFactory = $layoutCollectionFactory;
    }
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        if (isset($id)) {
            try {
                $model = $this->_layoutModel;
                $model->load($id);
                $store_id = $model->getStoreId();
                $model->setIsDefault(1)->save();

                // Get Collection to reset is_default for other layouts.
                $layoutCollection = $this->_layoutCollectionFactory->create();
                if ($id) {
                    $layoutCollection->addFieldToFilter('layout_id', ['neq' => $id])->addFieldToFilter('store_id', ['eq' => $store_id]);
                } else {
                    
                }
                if (!empty($layoutCollection->getData())) {
                    foreach ($layoutCollection as $item) {
                        $item->setIsDefault(0);
                    }
                    $layoutCollection->save();
                }
                $this->messageManager->addSuccess(__('Layout apply as default successfully'));
            
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the entry.'));
            }
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/');
        }
    }
}
