<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Adminhtml\Layout;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;

class DeleteSlider extends Action
{
    protected $_sliderModel;
    protected $_registry;
    protected $_jsonFactory;
    protected $_storeManager;
    protected $_fileSystem;
    protected $_fileDriver;

    public function __construct(
        Context $context,
        \Biztech\Magemobcart\Model\Bannerslider1 $sliderModel,
        \Magento\Framework\Filesystem $fileSystem,
        \Magento\Framework\Filesystem\Driver\File $fileDriver,
        \Magento\Framework\Registry $registry,
        JsonFactory $jsonFactory,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->_sliderModel = $sliderModel;
        $this->_fileSystem = $fileSystem;
        $this->_fileDriver = $fileDriver;
        $this->_registry = $registry;
        $this->_jsonFactory = $jsonFactory;
        $this->_storeManager = $storeManager;
    }

    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        $result['status'] = 'error';

        $id = $this->getRequest()->getParam('id');
        $model = $this->_sliderModel;
        
        if (isset($id)) {
            try {
                $model->load($id);
                if (!$model->getId()) {
                    $result['status'] = 'error';
                } else {
                    if (($model->getFilename() != null) && ($model->getFilepath() != null)) {
                        $mediaDirectory = $this->_fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->getAbsolutePath('Magemobcart/Banners');
                        $filename = $mediaDirectory . "/" . $model->getFilename();
                        if ($this->_fileDriver->isExists($filename)) {
                            $this->_fileDriver->deleteFile($filename);
                        }
                    }
                    $model->delete();
                    $result['status'] = 'success';
                }
            } catch (\Exception $e) {
                $result['status'] = 'error';
            }
        }

        $jsonResult->setData($result);
        return $jsonResult;
    }
}