<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Adminhtml\Layout;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;

class EditFeaturedCategoryBlocks extends Action
{
    protected $_featuredcategoryblocksModel;
    protected $_registry;
    protected $_jsonFactory;
    protected $_storeManager;

    public function __construct(
        Context $context,
        \Biztech\Magemobcart\Model\Featuredcategoryblocks $featuredcategoryblocksModel,
        \Magento\Framework\Registry $registry,
        JsonFactory $jsonFactory,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->_featuredcategoryblocksModel = $featuredcategoryblocksModel;
        $this->_registry = $registry;
        $this->_jsonFactory = $jsonFactory;
        $this->_storeManager = $storeManager;
    }

    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        $result['status'] = 'error';

        $id = $this->getRequest()->getParam('id');
        $model = $this->_featuredcategoryblocksModel;
        
        if (isset($id)) {
            $model->load($id);
            if (!$model->getId()) {
                $result['status'] = 'error';
            } else {
                $result['status'] = 'success';
                
                $result['category_title'] = $model->getCategoryTitle();
                $result['image_type'] = $model->getImageType();
                $result['filename'] = $model->getFilename();
                $result['filepath'] = ($model->getImageType() == 'image') ? $this->getImageUrl($model->getFilepath()) : $model->getImageUrl();
                $result['image_url'] = $model->getImageUrl();
                $result['category_status'] = $model->getStatus();
                $result['category_id'] = $model->getCategoryId();
                $result['sort_order'] = $model->getSortOrder();
            }
        }

        $jsonResult->setData($result);
        return $jsonResult;
    }

    public function getImageUrl($image)
    {
        $categoryImage = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $categoryImage = $categoryImage.$image;
        return $categoryImage;
    }
}