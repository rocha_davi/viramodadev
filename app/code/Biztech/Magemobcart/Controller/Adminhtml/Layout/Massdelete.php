<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Adminhtml\Layout;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Massdelete extends Action
{
    protected $_layoutModel;

    public function __construct(
        Context $context,
        \Biztech\Magemobcart\Model\Layout $layoutModel
    ) {
        parent::__construct($context);
        $this->_layoutModel = $layoutModel;
    }
    public function execute()
    {
        $layoutIds = $this->getRequest()->getParam('layout');
        if (!is_array($layoutIds)) {
            $this->messageManager->addError(__('Please select layout'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/');
        } else {
            foreach ($layoutIds as $layoutId) {
                $layout = $this->_layoutModel->load($layoutId);
                $layout->delete();
            }
            $this->messageManager->addSuccess(__('Layout removed successfully '));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/');
        }
    }
}
