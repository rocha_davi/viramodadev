<?php
namespace Biztech\Magemobcart\Controller\Adminhtml\Layout;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Backend\App\Action\Context;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Biztech\Magemobcart\Model\FeaturedcategoryblocksFactory;
use Magento\Framework\UrlInterface;
use Biztech\Magemobcart\Helper\Data as MagemobHelper;

class Savefeaturedcategoryblockspopup extends \Magento\Backend\App\Action
{
    protected $jsonFactory;
    protected $formKey;
    protected $request;
    protected $uploaderFactory;
    protected $featuredcategoryblocksModel;
    protected $categoryFactory;
    protected $fileSystem;
    protected $fileDriver;
    protected $urlBuilder;
    protected $helper;
    
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        UploaderFactory $uploaderFactory,
        FeaturedcategoryblocksFactory $featuredcategoryblocksModel,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\Filesystem $fileSystem,
        \Magento\Framework\Filesystem\Driver\File $fileDriver,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Data\Form\FormKey $formKey,
        UrlInterface $urlBuilder,
        MagemobHelper $helper
    ) {
        $this->_jsonFactory = $jsonFactory;
        $this->formKey = $formKey;
        $this->request = $request;
        $this->fileSystem = $fileSystem;
        $this->fileDriver = $fileDriver;
        $this->featuredcategoryblocksModel = $featuredcategoryblocksModel;
        $this->categoryFactory = $categoryFactory;
        $this->uploaderFactory = $uploaderFactory;
        $this->request->setParam('form_key', $this->formKey->getFormKey());
        $this->urlBuilder = $urlBuilder;
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        $result = ['status' => 'success' , 'message' => 'something went wrong while saving data'];

        try{
            $data = $this->request->getParams();
            $model = $this->featuredcategoryblocksModel->create();
            $isError = false;
            $isUpdate = false;
            if (isset($data['featured_category_id']) && !empty($data['featured_category_id'])) {
                $model->load($data['featured_category_id']);
                if (!$model->getId()) {
                    $isError = true;
                    $result = ['status' => 'error' , 'message' => 'The wrong category is specified!'];
                }
                $isUpdate = true;
            }

            if (!$isError) {
                $fcb_id = str_replace("edit_", "", $data['fcb_id']);

                $model->setFcbId($fcb_id)
                    ->setLayoutId($data['layout_id'])
                    ->setCategoryTitle($data['category_title'])
                    ->setImageType($data['cat_image_type'])
                    ->setImageUrl($data['cat_image_url'])
                    ->setStatus($data['status'])
                    ->setSortOrder($data['category_position']);

                if (array_key_exists('cat_category_id', $data)) {
                    $model->setCategoryId($data['cat_category_id']);
                }

                $fileData = $this->request->getFiles('categoryuploadedfile');
                $oldFile = null;
                $file = null;
                $filename = null;

                if ($isUpdate) {
                    $fileDirectory = $this->fileSystem
                                        ->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)
                                        ->getAbsolutePath('Magemobcart/Categories');
                    $oldFile = $fileDirectory . "/" . $model->getFilename();
                }

                if (isset($fileData['name']) && !empty($fileData['name'])) {
                    if ($data['cat_image_type'] == 'image') {
                        if ($oldFile != null && $isUpdate) {
                            if ($this->fileDriver->isExists($oldFile)) {
                                $this->fileDriver->deleteFile($oldFile);
                            }
                        }
                        $uploader = $this->uploaderFactory->create(['fileId' => 'categoryuploadedfile']);

                        $uploader->setFilesDispersion(false);
                        $uploader->setFilenamesCaseSensitivity(false);
                        $uploader->setAllowedExtensions(['png', 'jpg' , 'jpeg' , 'webp' , 'gif']);
                        $uploader->setAllowCreateFolders(true);
                        $uploader->setAllowRenameFiles(true);
                        $mediaDirectory = $this->fileSystem
                        ->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                        $savePath = $mediaDirectory->getAbsolutePath('Magemobcart/Categories');
                        $fileData = $uploader->save($savePath);
                        $file = 'Magemobcart/Categories/'.$fileData['file'];
                        $filename = $fileData['file'];
                    }
                }

                if ($isUpdate && ($fileData['name'] == '')) {
                    $file = $model->getFilepath();
                    $filename = $model->getFilename();
                }

                $model->setFilepath($file)->setFilename($filename);

                if ($model->save()) {
                    $result['status'] = "Success";
                    $result['message'] = "Category Added Successfully";
                    $filePath = ($model->getImageType() == 'image') ? $this->helper->getImageUrl($model->getFilepath()) : $model->getImageUrl();

                    $editUrl = $this->urlBuilder->getUrl('*/*/editFeaturedCategoryBlocks', ['id' => $model->getId()]);
                    $deleteUrl = $this->urlBuilder->getUrl('*/*/deleteFeaturedCategoryBlocks', ['id' => $model->getId()]);

                    $html = "<tr id='category_row_".$model->getId()."'>
                    <td>".$model->getId()."</td>
                    <td>".$model->getCategoryTitle()."</td>
                    <td><img src='".$filePath."' class='category_img'></td>
                    <td>".$model->getCategoryId()."</td>
                    <td>".$this->categoryFactory->create()->load($model->getCategoryId())->getName()."</td>
                    <td>".$model->getSortOrder()."</td>
                    <td>
                    <button type='button' class='edit-fcb-data btn btn-success btn_margin' onclick='editFeaturedCategories(\"".$editUrl."\",\"".$model->getId()."\")' title='Edit'>Edit</button>
                    <button type='button' class='delete-fcb-data btn btn-danger' onclick='deleteFeaturedCategories(\"".$deleteUrl."\",\"".$model->getId()."\")' title='Delete'>Delete</button>  
                    </td>
                    </tr>";

                    $result['html'] = $html;
                    $result['is_update'] = $isUpdate;
                    $result['featured_category_id'] = 'category_row_' . $model->getId();
                }
            }

        } catch (\Exception $e){
            $result = ['status' => 'error' , 'message' => $e->getMessage()];
        }

        $jsonResult->setData($result);
        return $jsonResult;
    }
}
