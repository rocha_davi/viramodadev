<?php
namespace Biztech\Magemobcart\Controller\Adminhtml\Layout;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Backend\App\Action\Context;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Biztech\Magemobcart\Model\ProductgridFactory;
use Biztech\Magemobcart\Model\ProducthorizontalslidingFactory;
use Biztech\Magemobcart\Model\Bannerslider1Factory;
use Biztech\Magemobcart\Model\FeaturedcategoryblocksFactory;
use Biztech\Magemobcart\Model\FcbdisplaytypeFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Exception;

class DeleteComponent extends \Magento\Backend\App\Action
{

    protected $jsonFactory;
    protected $formKey;
    protected $request;
    protected $fileSystem;
    protected $fileDriver;
    protected $productGrid;
    protected $productHorizontalSlider;
    protected $bannerSlider;
    protected $featuredCategoryBlocks;
    protected $fcbDisplayType;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        UploaderFactory $uploaderFactory,
        ProductgridFactory $productGrid,
        ProducthorizontalslidingFactory $productHorizontalSlider,
        Bannerslider1Factory $bannerSlider,
        FeaturedcategoryblocksFactory $featuredCategoryBlocks,
        FcbdisplaytypeFactory $fcbDisplayType,
        \Magento\Framework\Filesystem $fileSystem,
        \Magento\Framework\Filesystem\Driver\File $fileDriver,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Data\Form\FormKey $formKey
    ) {
        $this->_jsonFactory = $jsonFactory;
        $this->formKey = $formKey;
        $this->request = $request;
        $this->fileSystem = $fileSystem;
        $this->fileDriver = $fileDriver;
        $this->uploaderFactory = $uploaderFactory;
        $this->productGrid = $productGrid;
        $this->productHorizontalSlider = $productHorizontalSlider;
        $this->bannerSlider = $bannerSlider;
        $this->featuredCategoryBlocks = $featuredCategoryBlocks;
        $this->fcbDisplayType = $fcbDisplayType;
        $this->request->setParam('form_key', $this->formKey->getFormKey());
        parent::__construct($context);
    }

    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        $result = ['status' => 'success'];
        $data = $this->request->getParams();
        $component_id = str_replace("delete_", "", $data['comonent_id']);
        $component_type = $data['componenttype'];
        switch ($component_type) {
            case 'fcb':
                try {
                    $this->deleteFeaturedCategoryBlocks($component_id);
                } catch (Exception $e) {
                    $result['status'] = "error";
                    $result['message'] = (string)__($e->getMessage());
                }
                break;
            case 'productgrid':
                try {
                    $this->deleteProductGrid($component_id);
                } catch (Exception $e) {
                    $result['status'] = "error";
                    $result['message'] = (string)__($e->getMessage());
                }
                break;
            case 'phs':
                try {
                    $this->deleteProductsHorizontalSlider($component_id);
                } catch (Exception $e) {
                    $result['status'] = "error";
                    $result['message'] = (string)__($e->getMessage());
                }
                break;
            case 'bannerslider':
                try {
                    $this->deleteBannerSlider($component_id);
                } catch (Exception $e) {
                    $result['status'] = "error";
                    $result['message'] = (string)__($e->getMessage());
                }
                break;
            default:
                break;
        }
        $jsonResult->setData($result);
        return $jsonResult;
    }
    public function deleteProductGrid($component_id)
    {
        $collection = $this->productGrid->create()->getCollection()->addFieldToFilter("productgrid_id", $component_id);
        if ($collection->count()) {
            foreach ($collection as $value) {
                $this->productGrid->create()->load($value->getId())->delete();
            }
        }
    }
    public function deleteProductsHorizontalSlider($component_id)
    {
        $collection = $this->productHorizontalSlider->create()->getCollection()->addFieldToFilter("phs_id", $component_id);
        if ($collection->count()) {
            foreach ($collection as $value) {
                $this->productHorizontalSlider->create()->load($value->getId())->delete();
            }
        }
    }
    public function deleteBannerSlider($component_id)
    {
        $collection = $this->bannerSlider->create()->getCollection()->addFieldToFilter("bannerslider_id", $component_id);
        $mediaUrl = $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
        if ($collection->count()) {
            foreach ($collection as $value) {
                if ($value->getFilepath() != null) {
                    $filename = $mediaUrl . $value->getFilepath();
                    if ($this->fileDriver->isExists($filename)) {
                        $this->fileDriver->deleteFile($filename);
                    }
                }
                $this->bannerSlider->create()->load($value->getId())->delete();
            }
        }
    }
    public function deleteFeaturedCategoryBlocks($component_id)
    {
        $collection = $this->featuredCategoryBlocks->create()->getCollection()->addFieldToFilter("fcb_id", $component_id);
        $mediaUrl = $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
        if ($collection->count()) {
            foreach ($collection as $value) {
                if ($value->getFilepath() != null) {
                    $filename = $mediaUrl . $value->getFilepath();
                    if ($this->fileDriver->isExists($filename)) {
                        $this->fileDriver->deleteFile($filename);
                    }
                }
                $this->featuredCategoryBlocks->create()->load($value->getId())->delete();
            }
        }

        $fcbDisplayTypeCollection = $this->fcbDisplayType->create()->getCollection()->addFieldToFilter("fcb_id", $component_id);
        if ($fcbDisplayTypeCollection->count()) {
            foreach ($fcbDisplayTypeCollection as $value) {
                $this->fcbDisplayType->create()->load($value->getId())->delete();
            }
        }
    }
}
