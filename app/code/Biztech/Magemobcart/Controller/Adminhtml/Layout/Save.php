<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Adminhtml\Layout;

use Magento\Backend\App\Action;

class Save extends \Magento\Backend\App\Action
{
    protected $_layoutModel;
    protected $_layoutCollectionFactory;
    protected $storeManagerInterface;
    protected $backendSession;

    public function __construct(
        Action\Context $context,
        \Biztech\Magemobcart\Model\Layout $layoutModel,
        \Biztech\Magemobcart\Model\ResourceModel\Layout\CollectionFactory $layoutCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Backend\Model\Session $backendSession
    ) {
        $this->_layoutModel = $layoutModel;
        $this->_layoutCollectionFactory = $layoutCollectionFactory;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_backendSession = $backendSession;
        parent::__construct($context);
    }
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();

        if (isset($data['store_id']) && !empty($data['store_id'])) {
            $data['store_id'] = $data['store_id'];
        } else {
            $data['store_id'] = 0;
        }

        if (!isset($data['is_default']) || empty($data['is_default'])) {
            $data['is_default'] = 0;
        }

        if ($data) {
            $id = $this->getRequest()->getParam('layout_id');
            
            try {
                // Get Collection to reset is_default for other layouts.
                if(isset($data['is_default']) && $data['is_default'] == 1) {
                    $layoutCollection = $this->_layoutCollectionFactory->create()->addFieldToFilter('store_id', ['eq' => $data['store_id']]);
                    if ($id) {
                        $layoutCollection->addFieldToFilter('layout_id', ['neq' => $id]);
                    }
                    if (!empty($layoutCollection->getData())) {
                        foreach ($layoutCollection as $item) {
                            $item->setIsDefault(0);
                        }
                        $layoutCollection->save();
                    }
                }

                $model = $this->_layoutModel;

                if ($id) {
                    $model->load($id);
                    $model->setData($data);
                } else {
                    $model->setData($data);
                }
                $model->save();

                $this->messageManager->addSuccess(__('Layout has been saved.'));
                $this->_backendSession->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the entry.'));
            }
            
            $this->_getSession()->setFormData($data);
            if ($id) {
                return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
            }
        }
        return $resultRedirect->setPath('*/*/');
    }
}
