<?php
namespace Biztech\Magemobcart\Controller\Adminhtml\Layout;

use Exception;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Backend\App\Action\Context;

class GetCategoryProductData extends \Magento\Backend\App\Action
{

    protected $jsonFactory;
    protected $formKey;
    protected $request;
    protected $_layoutModel;
    protected $categoryProducts;
    protected $productCollectionFactory;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        \Biztech\Magemobcart\Model\Layout $layoutModel,
        \Magento\Catalog\Model\CategoryFactory $categoryProducts,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Data\Form\FormKey $formKey
    ) {
        parent::__construct($context);
        $this->_jsonFactory = $jsonFactory;
        $this->formKey = $formKey;
        $this->request = $request;
        $this->layoutModel = $layoutModel;
        $this->categoryProducts = $categoryProducts;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->request->setParam('form_key', $this->formKey->getFormKey());
    }

    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        $result = ['status' => 'error' , 'message' => 'something went wrong while saving'];
        try {
            $storeId = $this->layoutModel->load($this->request->getParam('layoutId'))->getStoreId();
            $categoryData = $this->categoryProducts->create()->setStoreId($storeId)->load($this->request->getParam('categoryId'));
            $categoryId = $categoryData->getId();
            // $categoryProduct = $categoryData->getProductCollection()->addAttributeToSelect('name');
            $categoryProduct = $this->productCollectionFactory->create()
                                    ->addAttributeToSelect('name')
                                    ->addCategoriesFilter(['in' => [$categoryId]])
                                    ->addAttributeToFilter('visibility', \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH)
                                    ->addAttributeToFilter('status',\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
            $productData = [];
            foreach ($categoryProduct as $product) {
                $productData[$product->getId()] = $product->getName();
            }
            $result['status'] = "success";
            $result['data'] = $productData;
            unset($result['message']);
        } catch (Exception $e) {
            $result['message'] = $e->getMessage();
        }

        $jsonResult->setData($result);
        return $jsonResult;
    }
}
