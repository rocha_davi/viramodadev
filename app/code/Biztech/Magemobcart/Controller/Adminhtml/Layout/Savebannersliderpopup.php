<?php
namespace Biztech\Magemobcart\Controller\Adminhtml\Layout;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Backend\App\Action\Context;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Biztech\Magemobcart\Model\Bannerslider1Factory;
use Magento\Framework\UrlInterface;
use Biztech\Magemobcart\Helper\Data as MagemobHelper;

class Savebannersliderpopup extends \Magento\Backend\App\Action
{
    protected $jsonFactory;
    protected $formKey;
    protected $request;
    protected $uploaderFactory;
    protected $bannerslider1;
    protected $fileSystem;
    protected $fileDriver;
    protected $urlBuilder;
    protected $helper;
    
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        UploaderFactory $uploaderFactory,
        Bannerslider1Factory $bannerslider1,
        \Magento\Framework\Filesystem $fileSystem,
        \Magento\Framework\Filesystem\Driver\File $fileDriver,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Data\Form\FormKey $formKey,
        UrlInterface $urlBuilder,
        MagemobHelper $helper
    ) {
        $this->_jsonFactory = $jsonFactory;
        $this->formKey = $formKey;
        $this->request = $request;
        $this->fileSystem = $fileSystem;
        $this->fileDriver = $fileDriver;
        $this->bannerslider1 = $bannerslider1;
        $this->uploaderFactory = $uploaderFactory;
        $this->request->setParam('form_key', $this->formKey->getFormKey());
        $this->urlBuilder = $urlBuilder;
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        $result = ['status' => 'success' , 'message' => 'something went wrong while saving'];

        try{

            $data = $this->request->getParams();
            $model = $this->bannerslider1->create();

            $isError = false;
            $isUpdate = false;

            if (isset($data['banner_slider_id']) && !empty($data['banner_slider_id'])) {
                $model->load($data['banner_slider_id']);
                if (!$model->getId()) {
                    $isError = true;
                    $result = ['status' => 'error' , 'message' => 'The wrong slider is specified!'];
                }
                $isUpdate = true;
            }

            if (!$isError) {
                $banner_id = str_replace("edit_", "", $data['banner_id']);

                $model->setBannersliderId($banner_id)
                    ->setLayoutId($data['banner_component_id'])
                    ->setComponentTitle($data['banner_title'])
                    ->setImageType($data['image_type'])
                    ->setImageUrl($data['image_url'])
                    ->setStatus($data['status'])
                    ->setRedirectActivity($data['banner_type'])
                    ->setSortOrder($data['banner_position']);
                if (array_key_exists('offer_link_redirect', $data)) {
                    $model->setOfferLink($data['offer_link_redirect']);
                }
                if (array_key_exists('redirect_banner_product_id', $data)) {
                    $model->setProductId($data['redirect_banner_product_id']);
                }
                if (array_key_exists('banner_category_id', $data)) {
                    $model->setCategoryId($data['banner_category_id']);
                }

                $fileData = $this->request->getFiles('slideruploadedfile');
                $oldFile = null;
                $file = null;
                $filename = null;

                if ($isUpdate) {
                    $fileDirectory = $this->fileSystem
                                        ->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)
                                        ->getAbsolutePath('Magemobcart/Banners');
                    $oldFile = $fileDirectory . "/" . $model->getFilename();
                }

                if (isset($fileData['name']) && !empty($fileData['name'])) {
                    if ($oldFile != null && $isUpdate) {
                        if ($this->fileDriver->isExists($oldFile)) {
                            $this->fileDriver->deleteFile($oldFile);
                        }
                    }
                    if ($data['image_type'] == 'image') {
                        $uploader = $this->uploaderFactory->create(['fileId' => 'slideruploadedfile']);

                        $uploader->setFilesDispersion(false);
                        $uploader->setFilenamesCaseSensitivity(false);
                        $uploader->setAllowedExtensions(['png', 'jpg' , 'jpeg' , 'webp' , 'gif']);
                        $uploader->setAllowCreateFolders(true);
                        $uploader->setAllowRenameFiles(true);
                        $mediaDirectory = $this->fileSystem
                        ->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                        $savePath = $mediaDirectory->getAbsolutePath('Magemobcart/Banners');
                        $fileData = $uploader->save($savePath);
                        $file = 'Magemobcart/Banners/'.$fileData['file'];
                        $filename = $fileData['file'];
                    }
                }

                if ($isUpdate && ($fileData['name'] == '')) {
                    $file = $model->getFilepath();
                    $filename = $model->getFilename();
                }

                $model->setFilepath($file)->setFilename($filename);

                if ($model->save()) {
                    $result['status'] = "Success";
                    $result['message'] = "Banner Added Successfully";
                    $filePath = ($model->getImageType() == 'image') ? $this->helper->getImageUrl($model->getFilepath()) : $model->getImageUrl();

                    $editUrl = $this->urlBuilder->getUrl('*/*/editslider', ['id' => $model->getId()]);
                    $deleteUrl = $this->urlBuilder->getUrl('*/*/deleteslider', ['id' => $model->getId()]);

                    $html = "<tr id='slider_row_".$model->getId()."'>
                    <td>".$model->getId()."</td>
                    <td>".$model->getComponentTitle()."</td>
                    <td><img src='".$filePath."' class='slider_img'></td>
                    <td>".$model->getRedirectActivity()."</td>
                    <td>".$model->getCategoryId()."</td>
                    <td>".$model->getProductId()."</td>
                    <td>".$model->getSortOrder()."</td>                            
                    <td>
                    <button type='button' class='edit-banner-offer-data btn btn-success btn_margin' onclick='editSlider(\"".$editUrl."\",\"".$model->getId()."\")' title='Edit'>Edit</button>
                    <button type='button' class='delete-banner-offer-data btn btn-danger' onclick='deleteSlider(\"".$deleteUrl."\",\"".$model->getId()."\")' title='Delete'>Delete</button>  
                    </td>
                    </tr>";

                    $result['html'] = $html;
                    $result['is_update'] = $isUpdate;
                    $result['banner_slider_id'] = 'slider_row_' . $model->getId();
                }
            }

        } catch (\Exception $exception){
            $result = ['status' => 'error' , 'message' => $exception->getMessage()];
        }

        $jsonResult->setData($result);
        return $jsonResult;
    }
}
