<?php
namespace Biztech\Magemobcart\Controller\Adminhtml\Layout;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Backend\App\Action\Context;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Biztech\Magemobcart\Model\ProducthorizontalslidingFactory;

class Saveproducthorizontalsliderpopup extends \Magento\Backend\App\Action
{

    protected $_jsonFactory;
    protected $_productHorizontalSliderFactory;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        ProducthorizontalslidingFactory $productHorizontalSliderFactory
    ) {
        $this->_jsonFactory = $jsonFactory;
        $this->_productHorizontalSliderFactory = $productHorizontalSliderFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        $result = ['status' => 'error' , 'message' => 'something went wrong while saving'];
        $postdata = $this->getRequest()->getPostValue();
        try {
            if ($postdata['phs_product_type'] == "category_products" &&  isset($postdata['phs_category_products']) && !empty($postdata['phs_category_products'])) {
                $postdata['phs_category_products'] = json_encode($postdata['phs_category_products']);
                $postdata['phs_product_list'] = null;
                $postdata['phs_display_category'] = 0;
            }
            if ($postdata['phs_product_type'] == "custom_products" && isset($postdata['phs_product_list']) && !empty($postdata['phs_product_list'])) {
                $postdata['phs_product_list'] = json_encode($postdata['phs_product_list']);
                $postdata['phs_category_id'] = 0;
                $postdata['phs_category_products'] = null;
                $postdata['phs_display_category'] = 0;
            }
            if ($postdata['phs_product_type'] == "new_products" && isset($postdata['new_arrival_product_type']) && !empty($postdata['new_arrival_product_type'])) {
                $postdata['phs_category_id'] = 0;
                $postdata['phs_product_list'] = null;
                $postdata['phs_category_products'] = null;
            }
            
            $collection = $this->_productHorizontalSliderFactory->create()->getCollection()->addFieldToFilter("phs_id", $postdata['phs_id']);
            if ($collection->count()) {
                $phsData = $this->_productHorizontalSliderFactory->create()->load($collection->getLastItem()->getId());
                $phsData->setLayoutId($postdata['layout_id'])
                        ->setComponentTitle($postdata['component_title'])
                        ->setPhsProductType($postdata['phs_product_type']);
                if (isset($postdata['phs_category_id']) && !empty($postdata['phs_category_id'])) {
                    $phsData->setPhsCategoryId($postdata['phs_category_id']);
                }
                if (isset($postdata['phs_display_category']) && !is_null($postdata['phs_display_category'])) {
                    $phsData->setPhsDisplayCategory($postdata['phs_display_category']);
                }
                if (isset($postdata['phs_category_products'])) {
                    $phsData->setPhsCategoryProducts($postdata['phs_category_products']);
                }
                if (isset($postdata['phs_product_list'])) {
                    $phsData->setPhsProductList($postdata['phs_product_list']);
                }
                if (isset($postdata['new_arrival_product_type'])) {
                    $phsData->setNewArrivalProductType($postdata['new_arrival_product_type']);
                }
                if ($phsData->save()) {
                    $result['status'] = "success";
                    $result['message'] = "Product horizontal slider block saved successfully";
                }
            } else {
                $phsData = $this->_productHorizontalSliderFactory->create();
                $phsData->setData($postdata);

                if ($phsData->save()) {
                    $result['status'] = "success";
                    $result['message'] = "Product horizontal slider block saved successfully";
                }
            }
            $jsonResult->setData($result);
            return $jsonResult;
        } catch (\Exception $e) {
            $result['status'] = "error";
            $result['message'] = $e->getMessage();
            $jsonResult->setData($result);
            return $jsonResult;
        }
    }
}
