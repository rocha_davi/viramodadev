<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Controller\Adminhtml\Layout;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Backend\App\Action\Context;
use Biztech\Magemobcart\Model\FcbdisplaytypeFactory;

class Savefcbdisplaytype extends \Magento\Backend\App\Action
{
    protected $_jsonFactory;
    protected $_request;
    protected $_fcbDisplayTypeFactory;
    protected $formKey;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        \Magento\Framework\App\Request\Http $request,
        FcbdisplaytypeFactory $fcbDisplayTypeFactory,
        \Magento\Framework\Data\Form\FormKey $formKey
    ) {
        $this->_jsonFactory = $jsonFactory;
        $this->_request = $request;
        $this->_fcbDisplayTypeFactory = $fcbDisplayTypeFactory;
        $this->formKey = $formKey;
        $this->_request->setParam('form_key', $this->formKey->getFormKey());
        parent::__construct($context);
    }

    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        $result = ['status' => 'error' , 'message' => 'something went wrong while saving'];
        $postdata = $this->_request->getPostValue();
        $fcbId = str_replace("edit_", "", $postdata['fcb_id']);
        try {
            $collection = $this->_fcbDisplayTypeFactory->create()->getCollection()->addFieldToFilter("fcb_id", $fcbId)->addFieldToFilter('layout_id', $postdata['layout_id']);
            if ($collection->count()) {
                $fcbData = $this->_fcbDisplayTypeFactory->create()->load($collection->getLastItem()->getId());
                $fcbData->setLayoutId($postdata['layout_id'])
                        ->setDisplayType($postdata['cat_display_type'])
                        ->setFcbTitle($postdata['fcb_title']);
                if ($fcbData->save()) {
                    $result['status'] = "success";
                    $result['message'] = __("Featured category block saved successfully");
                    $result['display_type'] = $fcbData->getDisplayType();
                    $result['fcb_title'] = $fcbData->getFcbTitle();
                    $result['fcb_id'] = $fcbData->getFcbId();
                }
            } else {
                $fcbData = $this->_fcbDisplayTypeFactory->create();
                $fcbData->setFcbId($fcbId)
                		->setLayoutId($postdata['layout_id'])
                		->setDisplayType($postdata['cat_display_type'])
                		->setFcbTitle($postdata['fcb_title']);

                if ($fcbData->save()) {
                    $result['status'] = "success";
                    $result['message'] = __("Featured category block saved successfully");
                    $result['display_type'] = $fcbData->getDisplayType();
                    $result['fcb_title'] = $fcbData->getFcbTitle();
                    $result['fcb_id'] = $fcbData->getFcbId();
                }
            }
            $jsonResult->setData($result);
            return $jsonResult;
        } catch (\Exception $e) {
            $result['status'] = "error";
            $result['message'] = $e->getMessage();
            $jsonResult->setData($result);
            return $jsonResult;
        }
    }
}
