<?php

namespace Biztech\Magemobcart\Controller\Adminhtml\Notification;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Massstatus extends Action
{
    protected $_notificationModel;
    protected $resultPageFactory;
    protected $registry;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Biztech\Magemobcart\Model\Notification $notificationMode,
        \Magento\Framework\Registry $registry
    ) {
        parent::__construct($context);
        $this->_resultPageFactory = $resultPageFactory;
        $this->_notificationModel = $notificationModel;
        $this->_registry = $registry;
    }
    public function execute()
    {
        $notificationIds = $this->getRequest()->getParam('notification');
        if (!is_array($notificationIds)) {
            $this->messageManager->addError(__('Please select banners'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/');
        } else {
            foreach ($notificationIds as $notificationId) {
                $notification = $this->_notificationModel->load($notificationId);
                $notification->load($notificationId)->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
            }
            $this->messageManager->addSuccess(__('Successfully notification status updated'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/');
        }
    }
}
