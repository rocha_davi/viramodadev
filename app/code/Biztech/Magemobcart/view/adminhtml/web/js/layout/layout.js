/**
 *
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var confirm_msg_txt = "Are you sure, you want to remove this component?";
var edit_component_txt = "Edit Component";
var close_txt = "Close";
var save_txt = "Save";
var min_category_limit = "Need to add value in atleast 4 categories";
var add_component_msg = "Component added successfully";

var add_fcb_msg = "Featured categories block added successfully";
var add_productgrid_msg = "Product grid block added successfully";
var add_productslider_msg = "Product horizontal slider block added successfully";
var add_bannerslider_msg = "Banner slider block added successfully";
var add_recentproducts_msg = "Recently access products block added successfully";

var delete_component_msg = "Component removed successfully";

var delete_fcb_msg = "Featured categories block removed successfully";
var delete_productgrid_msg = "Product grid block removed successfully";
var delete_productslider_msg = "Product horizontal slider block removed successfully";
var delete_bannerslider_msg = "Banner slider block removed successfully";
var delete_recentproducts_msg = "Recently access products block removed successfully";

var error_msg = "Something went wrong";
var delete_component_error = "Can not remove the component";
var save_component_success = "Component layout updated";
var save_component_error = "Can not save the component";

var num_of_component = 0;
var recent_access_product_count = window.recent_product_count;
var new_id = 1;
var fcb_error_count = 0;
var banner_error_count = 0;

var actionUrl = window.editActionUrl;
var editLayoutUrl = window.editLayoutUrl;
var ajaxSaveLayoutUrl = window.saveLayoutUrl;
var ajaxFcbUrl = window.editFeaturedCategoryBlocksUrl;
var saveFcbUrl = window.saveFeaturedCategoriesBlocksUrl;
var ajaxProductGridUrl = window.editProductGridUrl;
var ajaxSaveProductGridUrl = window.saveProductGridUrl;
var ajaxPhsUrl = window.editProductHorizontalSliderUrl;
var ajaxBannerSliderUrl = window.editBannerSliderUrl;
var saveBannerSliderUrl = window.saveBannerSliderUrl;
var getcategoryproductdata = window.getcategoryproductdata;
var deleteComponent = window.deleteComponent;
var ajaxSavePhsUrl = window.savePhsUrl;
var saveFcbDisplayTypeUrl = window.saveFcbDisplayTypeUrl;

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      jQuery("#sliderimage").attr("src", e.target.result);
      jQuery("#categoryimage").attr("src", e.target.result);
    };

    reader.readAsDataURL(input.files[0]);
  }
}
function submitBannerSlider(event) {
  if (validateBannerSlider()) {
    event.preventDefault();
    jQuery.ajax({
      showLoader: true,
      url: saveBannerSliderUrl,
      type: "post",
      processData: false,
      contentType: false,
      data: new FormData(document.getElementById("save_banneroffer_slider")),
      success: function(data) {
        if (data.status == "error") {
          Layout.prototype.errorMessage(data.message);
        } else {
          banner_error_count = 0;
          Layout.prototype.successMessage(data.message);

          if (data.is_update == true) {
            jQuery("#" + data.banner_slider_id).replaceWith(data.html);
          } else {
            if (jQuery("#no_records").length == 1) {
              jQuery("#no_records").remove();
            }

            jQuery("#slider_grid_table tbody").append(data.html);
          }
          if (jQuery("#bannerslider_form_save_type").val() == "save") {
            jQuery("#banner_offer_slider_form").hide();
            jQuery("#add-bannerslider-form-btn").show();
          } else {
            jQuery("#banner_offer_slider_form").show();
            jQuery("#add-bannerslider-form-btn").hide();
          }
          jQuery("#save_banneroffer_slider").trigger("reset");

          jQuery("#banner_slider_id").val("");
          jQuery("#banner_slider_image").val("new");
          jQuery("#image_file_sec").fadeOut();
          jQuery("#image_url_holder").fadeOut();
          jQuery("#image_url_sec").fadeOut();
          jQuery("#offer_links").fadeOut();
          jQuery("#offer_links").val("");
          jQuery("#product_id_holder").fadeOut();
          jQuery("#product_id_holder").val("");
          jQuery("#banner_category_id_holder").fadeOut();
          jQuery("#banner_category_id_holder").val("");
          jQuery("#image_url_holder").hide();
        }
      },
      error: function() {
        Layout.prototype.errorMessage("something went wrong while saving data");
      }
    });
  }
  event.preventDefault();
}

function submitFeaturedCategoryBlocks(event) {
  if (validateFcb()) {
    event.preventDefault();
    jQuery.ajax({
      showLoader: true,
      url: saveFcbUrl,
      type: "post",
      processData: false,
      contentType: false,
      data: new FormData(document.getElementById("save_featuredcategory_blocks")),
      success: function(data) {
        if (data.status == "error") {
          Layout.prototype.errorMessage(data.message);
        } else {
          fcb_error_count == 0;
          Layout.prototype.successMessage(data.message);

          if (data.is_update == true) {
            jQuery("#" + data.featured_category_id).replaceWith(data.html);
          } else {
            if (jQuery("#cat_no_records").length == 1) {
              jQuery("#cat_no_records").remove();
            }

            jQuery("#fcb_grid_table tbody").append(data.html);
          }
          if (jQuery("#fcb_form_save_type").val() == "save") {
            jQuery("#featured_category_form").hide();
            jQuery("#add-fcb-form-btn").show();
          } else {
            jQuery("#featured_category_form").show();
            jQuery("#add-fcb-form-btn").hide();
          }
          jQuery("#save_featuredcategory_blocks").trigger("reset");
          jQuery("#featured_category_id").val("");
          jQuery("#category_image").val("new");
          jQuery("#cat_image_file_sec").fadeOut();
          jQuery("#cat_image_url_holder").fadeOut();
          jQuery("#cat_image_url_holder").hide();
          jQuery("#cat_image_url_sec").fadeOut();
          jQuery("#cat_category_id_holder").val("");
          jQuery("#fcb_form_save_type").val("");
        }
      },
      error: function() {
        Layout.prototype.errorMessage("something went wrong while saving data");
      }
    });
  }
  event.preventDefault();
}

function submitFcbDisplayType(event) {
  if (validateFcbData()) {
    event.preventDefault();
    jQuery.ajax({
      showLoader: true,
      url: saveFcbDisplayTypeUrl,
      type: "post",
      processData: false,
      contentType: false,
      data: new FormData(document.getElementById("save_fcb_display_type")),
      success: function(data) {
        if (data.status == "error") {
          Layout.prototype.errorMessage(data.message);
        } else {
          Layout.prototype.successMessage(data.message);
          jQuery('#cat_display_type').val(data.display_type);
          jQuery('#fcb_title').val(data.fcb_title);
          if (data.display_type == 'circle') {
            jQuery("#" + data.fcb_id + "_img").attr("src", window.featuredCategoriesCircleImgPath);
          } else {
            jQuery("#" + data.fcb_id + "_img").attr("src", window.featuredCategoriesRectImgPath);
          }
        }
      },
      error: function() {
        Layout.prototype.errorMessage("something went wrong while saving data");
      }
    });
  }
  event.preventDefault();
}

var Layout = function() {};
Layout.prototype = {
  initialize: function() {
    Layout.prototype.firstTimeLoad();
  },
  firstTimeLoad: function() {
    var layout_id = jQuery("#layout_id").val();

    jQuery("#featured_categories").click(function() {
        Layout.prototype.addFeaturedCategoryBlock();
    });
    jQuery("#product_grid").click(function() {
      Layout.prototype.addProductGrid();
    });
    jQuery("#product_horizontal_slider").click(function() {
      Layout.prototype.addProductHorizontalSliding();
    });
    jQuery("#banner_slider").click(function() {
      Layout.prototype.addBannerSlider();
    });
    jQuery("#offer_slider").click(function() {
      Layout.prototype.addOfferSlider();
    });
    jQuery("#recently_accessed_products").click(function() {
      if (recent_product_count < 1) {
        Layout.prototype.addProductRecentlyViewed();
      } else {
        Layout.prototype.errorMessage("You can add this component only once");
      }
    });
    jQuery(".add-component").click(function() {
      jQuery(this).attr("id");
      if (jQuery(this).attr("id") == "featured_categories") {
        Layout.prototype.saveLayoutSequence(
          add_fcb_msg,
          recent_product_count
        );
      } else if (jQuery(this).attr("id") == "recently_accessed_products") {
        if (recent_product_count < 1) {
          Layout.prototype.saveLayoutSequence(
            add_recentproducts_msg,
            (recent_product_count += 1)
          );
        }
      } else if (jQuery(this).attr("id") == "product_grid") {
        Layout.prototype.saveLayoutSequence(
          add_productgrid_msg,
          recent_product_count
        );
      } else if (jQuery(this).attr("id") == "product_horizontal_slider") {
        Layout.prototype.saveLayoutSequence(
          add_productslider_msg,
          recent_product_count
        );
      } else if (jQuery(this).attr("id") == "banner_slider") {
        Layout.prototype.saveLayoutSequence(
          add_bannerslider_msg,
          recent_product_count
        );
      } else {
        Layout.prototype.saveLayoutSequence(
          add_component_msg,
          recent_product_count
        );
      }
    });

    jQuery("#sortablelist").sortable({
      placeholder: "drop-placeholder",
      start: function(event, ui) {
        iBefore = ui.item.index();
      },
      stop: function(event, ui) {
        jQuery(this).sortable("serialize");
        Layout.prototype.saveLayoutSequence(
          save_component_success,
          recent_product_count
        );
      },
      update: function(event, ui) {
        iAfter = ui.item.index();
        evictee = jQuery("#iframe_html_mobile li:eq(" + iAfter + ")");
        evictor = jQuery("#iframe_html_mobile li:eq(" + iBefore + ")");

        evictee.replaceWith(evictor);
        if (iBefore > iAfter) evictor.after(evictee);
        else evictor.before(evictee);
      }
    });
    jQuery("#sortable").disableSelection();

    jQuery(document).on("change", "#product_type", function() {
      var productType = jQuery(this).attr("value");
      Layout.prototype.showHideProductType(productType);
    });

    jQuery(document).on("change", "#category_id", function() {
      var categoryId = jQuery(this).attr("value");
      Layout.prototype.getCategoryData(categoryId);
    });

    jQuery(document).on("change", "#phs_product_type", function() {
      var productType = jQuery(this).attr("value");
      Layout.prototype.showHideProductSliderType(productType);
    });
    jQuery(document).on("change", "#phs_category_id", function() {
      var categoryId = jQuery(this).attr("value");
      Layout.prototype.getPhsCategoryData(categoryId);
    });
  },
  getNewestId: function(layoutId) {
    var ids = jQuery("#sortablelist").sortable("toArray");
    if (ids.length != 0) {
      for (var i = ids.length - 1; i >= 0; i--) {
        ids[i] = ids[i].split("_")[2];
        // ids[i] = ids[i].replace('layout_component_' + layoutId + '_', '');
        ids[i] = parseInt(ids[i]);
        if (!Number.isInteger(ids[i])) {
          break;
        }
        if (new_id <= ids[i]) {
          new_id = ids[i] + 1;
        }
      }
      return new_id;
    } else {
      return new_id++;
    }
  },
  addFeaturedCategoryBlock: function() {
    var layout_id = jQuery("#layout_id").val();
    var new_id = Layout.prototype.getNewestId(layout_id);
    var fcb_id = "fcb_" + layout_id + "_" + new_id;

    var circleImgPath = window.featuredCategoriesCircleImgPath;
    var rectImgPath = window.featuredCategoriesRectImgPath;

    var featured_categories_html =
      '<li class="list-group-item" id="' + fcb_id + '">';
    featured_categories_html +=
      '<span class="slideTitle">Featured Categories</span>';
    featured_categories_html +=
      '<span class="edit_component featured-category-block-modal-popup" id="edit_' +
      fcb_id +
      '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
    featured_categories_html +=
      '<span class="trash" id="delete_' +
      fcb_id +
      '" onclick="Layout.prototype.trashFeaturedcategoryblocksComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
    featured_categories_html += "</li>";

    jQuery("#sortablelist").append(featured_categories_html);

    var featured_categories_html_mobile =
      '<li class="slide" id="' + fcb_id + '">';
    featured_categories_html_mobile +=
      '<span class="slideTitle">Featured Categories</span>';
    featured_categories_html_mobile +=
      '<span class="edit_component edit_mobile_component featured-category-block-modal-popup" id="edit_' +
      fcb_id +
      '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
    featured_categories_html_mobile +=
      '<span class="trash" id="delete_' +
      fcb_id +
      '" onclick="Layout.prototype.trashFeaturedcategoryblocksComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
    featured_categories_html_mobile +=
      '<div class="banner_preview layout_div"><img id="' + fcb_id + '_img" class="banner_preview_image" src="' +
      circleImgPath +
      '" /></div>';
    featured_categories_html_mobile += "</li>";

    jQuery("#iframe_html_mobile").append(featured_categories_html_mobile);

    // set default display type for featured category block
    Layout.prototype.setFcbDisplayType(fcb_id, 'circle');
  },
  setFcbDisplayType: function(fcb_id, cat_display_type, fcb_title = '') {
    layout_id = jQuery("#layout_id").val();
    jQuery.ajax({
      showLoader: true,
      url: saveFcbDisplayTypeUrl,
      type: "post",
      data: {
        layout_id: layout_id,
        fcb_id: fcb_id,
        cat_display_type: cat_display_type,
        fcb_title: fcb_title
      },
      success: function(data) {
        if (data != undefined) {
          jQuery('#cat_display_type').val(data.display_type);
          if (data.display_type == 'circle') {
            jQuery("#" + fcb_id + "_img").attr("src", window.featuredCategoriesCircleImgPath);
          } else {
            jQuery("#" + fcb_id + "_img").attr("src", window.featuredCategoriesRectImgPath);
          }
          // message = "Featured Category Block added successfully";
          // Layout.prototype.successMessage(message);
        }
      },
      error: function() {
        Layout.prototype.errorMessage(save_component_error);
      }
    });
  },
  addProductGrid: function() {
    var layout_id = jQuery("#layout_id").val();
    var new_id = Layout.prototype.getNewestId(layout_id);

    var imgPath = window.productGridImgPath;

    var product_grid_html =
      '<li class="list-group-item" id="productgrid_' +
      layout_id +
      "_" +
      new_id +
      '">';
    product_grid_html += '<span class="slideTitle">Product Grid</span>';
    product_grid_html +=
      '<span class="edit_component product-grid-modal-popup" id="edit_productgrid_' +
      layout_id +
      "_" +
      new_id +
      '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
    product_grid_html +=
      '<span class="trash" id="delete_productgrid_' +
      layout_id +
      "_" +
      new_id +
      '" onclick="Layout.prototype.trashProductgridComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
    product_grid_html += "</li>";

    jQuery("#sortablelist").append(product_grid_html);

    var product_grid_html_mobile =
      '<li class="slide" id="productgrid_' + layout_id + "_" + new_id + '">';
    product_grid_html_mobile += '<span class="slideTitle">Product Grid</span>';
    product_grid_html_mobile +=
      '<span class="edit_component edit_mobile_component product-grid-modal-popup" id="edit_productgrid_' +
      layout_id +
      "_" +
      new_id +
      '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
    product_grid_html_mobile +=
      '<span class="trash" id="delete_productgrid_' +
      layout_id +
      "_" +
      new_id +
      '" onclick="Layout.prototype.trashProductgridComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
    product_grid_html_mobile +=
      '<div class="banner_preview layout_div"><img class="banner_preview_image" src="' +
      imgPath +
      '" /></div>';
    product_grid_html_mobile += "</li>";

    jQuery("#iframe_html_mobile").append(product_grid_html_mobile);
  },
  addProductHorizontalSliding: function() {
    var layout_id = jQuery("#layout_id").val();
    var new_id = Layout.prototype.getNewestId(layout_id);

    var imgPath = window.productHorizontalSliderImgPath;

    var product_horizontal_slider_html =
      '<li class="list-group-item" id="phs_' + layout_id + "_" + new_id + '">';
    product_horizontal_slider_html +=
      '<span class="slideTitle">Products Horizontal Slider</span>';
    product_horizontal_slider_html +=
      '<span class="edit_component product-slider-modal-popup" id="edit_phs_' +
      layout_id +
      "_" +
      new_id +
      '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
    product_horizontal_slider_html +=
      '<span class="trash" id="delete_phs_' +
      layout_id +
      "_" +
      new_id +
      '" onclick="Layout.prototype.trashProducthorizontalslidingComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
    product_horizontal_slider_html += "</li>";

    jQuery("#sortablelist").append(product_horizontal_slider_html);

    var product_horizontal_slider_html_mobile =
      '<li class="slide" id="phs_' + layout_id + "_" + new_id + '">';
    product_horizontal_slider_html_mobile +=
      '<span class="slideTitle">Products Horizontal Slider</span>';
    product_horizontal_slider_html_mobile +=
      '<span class="edit_component edit_mobile_component product-slider-modal-popup" id="edit_phs_' +
      layout_id +
      "_" +
      new_id +
      '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
    product_horizontal_slider_html_mobile +=
      '<span class="trash" id="delete_phs_' +
      layout_id +
      "_" +
      new_id +
      '" onclick="Layout.prototype.trashProducthorizontalslidingComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
    product_horizontal_slider_html_mobile +=
      '<div class="banner_preview layout_div"><img class="banner_preview_image" src="' +
      imgPath +
      '" /></div>';
    product_horizontal_slider_html_mobile += "</li>";

    jQuery("#iframe_html_mobile").append(product_horizontal_slider_html_mobile);
  },
  addBannerSlider: function() {
    var layout_id = jQuery("#layout_id").val();
    var new_id = Layout.prototype.getNewestId(layout_id);

    var imgPath = window.bannerSliderImgPath;

    var banner_slider_html =
      '<li class="list-group-item" id="bannerslider_' +
      layout_id +
      "_" +
      new_id +
      '">';
    // banner_slider_html += '<span class="slideTitle">Banner Slider</span>';
    banner_slider_html += '<span class="slideTitle">Banner/Offer Slider</span>';
    banner_slider_html +=
      '<span class="edit_component banner-slider-modal-popup" id="edit_bannerslider_' +
      layout_id +
      "_" +
      new_id +
      '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
    banner_slider_html +=
      '<span class="trash" id="delete_bannerslider_' +
      layout_id +
      "_" +
      new_id +
      '" onclick="Layout.prototype.trashBannersliderComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
    banner_slider_html += "</li>";

    jQuery("#sortablelist").append(banner_slider_html);

    var banner_slider_html_mobile =
      '<li class="slide" id="bannerslider_' + layout_id + "_" + new_id + '">';
    // banner_slider_html_mobile += '<span class="slideTitle">Banner Slider</span>';
    banner_slider_html_mobile +=
      '<span class="slideTitle">Banner/Offer Slider</span>';
    banner_slider_html_mobile +=
      '<span class="edit_component edit_mobile_component banner-slider-modal-popup" id="edit_bannerslider_' +
      layout_id +
      "_" +
      new_id +
      '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
    banner_slider_html_mobile +=
      '<span class="trash" id="delete_bannerslider_' +
      layout_id +
      "_" +
      new_id +
      '" onclick="Layout.prototype.trashBannersliderComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
    banner_slider_html_mobile +=
      '<div class="banner_preview layout_div"><img class="banner_preview_image" src="' +
      imgPath +
      '" /></div>';
    banner_slider_html_mobile += "</li>";

    jQuery("#iframe_html_mobile").append(banner_slider_html_mobile);
  },
  addProductRecentlyViewed: function() {
    var layout_id = jQuery("#layout_id").val();
    var new_id = Layout.prototype.getNewestId(layout_id);

    var imgPath = window.recentlyAccessedProductsImgPath;

    var recently_accessed_products_html =
      '<li class="list-group-item" id="recentproducts_' +
      layout_id +
      "_" +
      new_id +
      '">';
    recently_accessed_products_html +=
      '<span class="slideTitle">Recently Accessed Products</span>';
    recently_accessed_products_html +=
      '<span class="trash" id="delete_recentproducts_' +
      layout_id +
      "_" +
      new_id +
      '" onclick="Layout.prototype.trashProductrecentlyviewedComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
    recently_accessed_products_html += "</li>";

    jQuery("#sortablelist").append(recently_accessed_products_html);

    var recently_accessed_products_html_mobile =
      '<li class="slide" id="recentproducts_' + layout_id + "_" + new_id + '">';
    recently_accessed_products_html_mobile +=
      '<span class="slideTitle">Recently Accessed Products</span>';
    recently_accessed_products_html_mobile +=
      '<span class="trash" id="delete_recentproducts_' +
      layout_id +
      "_" +
      new_id +
      '" onclick="Layout.prototype.trashProductrecentlyviewedComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
    recently_accessed_products_html_mobile +=
      '<div class="banner_preview layout_div"><img class="banner_preview_image" src="' +
      imgPath +
      '" /></div>';
    recently_accessed_products_html_mobile += "</li>";

    jQuery("#iframe_html_mobile").append(
      recently_accessed_products_html_mobile
    );
  },
  changeMobileView: function(data, layoutid) {
    var ids = data.split("&");
    var item_ids = ids;
    for (var i = ids.length - 1; i >= 0; i--) {
      item_ids[i] = ids[i].replace(
        "layout_component_" + layoutid + "[]=",
        "layout_component_" + layoutid + "_"
      );
    }
    /*jQuery("#iframe_html_mobile li").sort(function (a, b) {
		    return parseInt(a.id) < parseInt(b.id);
		}).each(function () {
		    var elem = jQuery(this);
		    elem.remove();
		    jQuery(elem).appendTo("#iframe_html_mobile");
		});*/
  },
  editFeaturedCategories: function () {
    if (
      confirm(
        "This will redirect you to featured category page, Are you sure you want to perform this action?"
      )
    ) {
      window.location.href = ajaxFcbUrl;
    }
  },
  trashFeaturedcategoryblocksComponentFunction: function(link) {
    if (confirm(confirm_msg_txt)) {
      Layout.prototype.deleteLayoutData(
        "fcb",
        link.id,
        link,
        delete_fcb_msg,
        recent_product_count
      );
    }
  },
  trashProductgridComponentFunction: function(link) {
    if (confirm(confirm_msg_txt)) {
      Layout.prototype.deleteLayoutData(
        "productgrid",
        link.id,
        link,
        delete_productgrid_msg,
        recent_product_count
      );
    }
  },
  trashProducthorizontalslidingComponentFunction: function(link) {
    if (confirm(confirm_msg_txt)) {
      Layout.prototype.deleteLayoutData(
        "phs",
        link.id,
        link,
        delete_productslider_msg,
        recent_product_count
      );
    }
  },
  trashBannersliderComponentFunction: function(link) {
    if (confirm(confirm_msg_txt)) {
      Layout.prototype.deleteLayoutData(
        "bannerslider",
        link.id,
        link,
        delete_bannerslider_msg,
        recent_product_count
      );
    }
  },
  trashProductrecentlyviewedComponentFunction: function(link) {
    if (confirm(confirm_msg_txt)) {
      link.parentNode.parentNode.removeChild(link.parentNode);
      jQuery("#" + link.parentNode.id).remove();
      jQuery("#" + link.parentNode.id).remove();
      Layout.prototype.saveLayoutSequence(
        delete_recentproducts_msg,
        (recent_product_count -= 1)
      );
    }
  },
  getCategoryData: function(id) {
    var layout_id = jQuery("#layout_id").val();
    jQuery.ajax({
      showLoader: true,
      url: getcategoryproductdata,
      type: "post",
      data: { layoutId: layout_id, categoryId: id },
      success: function(data) {
        var html = "";
        jQuery.each(data.data, function(index, value) {
          html += "<option value=" + index + "> " + value + " </option>";
        });
        jQuery("#category_products").html(html);
      },
      error: function() {
        Layout.prototype.errorMessage("Can not load category data");
      }
    });
  },
  getPhsCategoryData: function(id) {
    var layout_id = jQuery("#layout_id").val();
    jQuery.ajax({
      showLoader: true,
      url: getcategoryproductdata,
      type: "post",
      data: { layoutId: layout_id, categoryId: id },
      success: function(data) {
        var html = "";
        jQuery.each(data.data, function(index, value) {
          html += "<option value=" + index + "> " + value + " </option>";
        });
        jQuery("#phs_category_products").html(html);
      },
      error: function() {
        Layout.prototype.errorMessage("Can not save category data");
      }
    });
  },
  showHideProductType: function(type) {
    if (type == "category_products") {
      jQuery(".field-product_list").hide();
      jQuery("#product_list").val("");
      jQuery(".field-category_id").fadeIn();
      jQuery(".field-category_id").val();
      jQuery(".field-category_products").fadeIn();
      jQuery("#category_products").val("");

      if (jQuery("#category_id").val() == "") {
        jQuery("#category_products").html("");
      }
    } else if (type == "custom_products") {
      jQuery(".field-product_list").fadeIn();
      jQuery(".field-category_id").hide();
      jQuery("#category_id").val("");
      jQuery(".field-category_products").hide();
      jQuery("#category_products").val("");
    } else {
      jQuery(".field-product_list").hide();
      jQuery("#product_list").val("");
      jQuery(".field-category_id").hide();
      jQuery("#category_id").val("");
      jQuery(".field-category_products").hide();
      jQuery("#category_products").val("");
    }
  },
  showHideProductSliderType: function(type) {
    if (type == "category_products") {
      jQuery(".field-phs_product_list").hide();
      jQuery("#phs_product_list").val("");
      jQuery(".field-phs_category_id").show();
      jQuery(".field-phs_category_id").val();
      jQuery(".field-phs_category_products").show();
      jQuery(".field-new_arrival_product_type").hide();
      jQuery("#new_arrival_product_type").val("");
      jQuery(".field-phs_display_category").hide();
      jQuery("#phs_display_category").val("");
    } else if (type == "custom_products") {
      jQuery(".field-phs_product_list").show();
      jQuery(".field-phs_category_id").hide();
      jQuery("#phs_category_id").val("");
      jQuery(".field-phs_category_products").hide();
      jQuery("#phs_category_products").val("");
      jQuery(".field-new_arrival_product_type").hide();
      jQuery("#new_arrival_product_type").val("");
      jQuery(".field-phs_display_category").hide();
      jQuery("#phs_display_category").val("");
    } else if (type == "new_products") {
      jQuery(".field-new_arrival_product_type").show();
      jQuery(".field-phs_product_list").hide();
      jQuery("#phs_product_list").val("");
      jQuery(".field-phs_category_id").hide();
      jQuery("#phs_category_id").val("");
      jQuery(".field-phs_category_products").hide();
      jQuery("#phs_category_products").val("");
      Layout.prototype.showHideNewProductCategory(jQuery("#new_arrival_product_type").val());
    } else {
      jQuery(".field-new_arrival_product_type").hide();
      jQuery("#new_arrival_product_type").val("");
      jQuery(".field-phs_product_list").hide();
      jQuery("#phs_product_list").val("");
      jQuery(".field-phs_category_id").hide();
      jQuery("#phs_category_id").val("");
      jQuery(".field-phs_category_products").hide();
      jQuery("#phs_category_products").val("");
      jQuery(".field-phs_display_category").hide();
      jQuery("#phs_display_category").val("");
    }
  },
  showHideNewProductCategory: function(type) {
    if (type == 1) {
      jQuery(".field-phs_display_category").show();
    } else {
      jQuery(".field-phs_display_category").hide();
      jQuery("#phs_display_category").val("");
    }
  },
  ProductGridshowHideProductType: function(type) {
    if (type == "category_products") {
      jQuery(".field-product_list").hide();
    } else if (type == "custom_products") {
      jQuery(".field-category_products").hide();
      jQuery(".field-category_id").hide();
    }
  },
  saveLayoutSequence: function(
    message = null,
    recent_product_count = 0
  ) {
    var layout_id = jQuery("#layout_id").val();
    var data = jQuery("#sortablelist").sortable("serialize");
    var component_data = [];
    if (data != "") {
      var comp_data = data.split("&");
      for (let i = 0; i <= comp_data.length - 1; i++) {
        let component = comp_data[i].split("[]=");
        let type = component[0].split("_")[0];
        item = {};
        item[type] = component[0] + "_" + component[1];
        component_data.push(item);
      }
    }
    jQuery.ajax({
      showLoader: true,
      url: ajaxSaveLayoutUrl,
      type: "post",
      data: {
        layout_id: layout_id,
        component_data: component_data,
        recent_product_count: recent_product_count
      },
      success: function(data) {
        if (data != undefined && message != null) {
          Layout.prototype.successMessage(message);
        }
      },
      error: function() {
        Layout.prototype.errorMessage(save_component_error);
      }
    });
  },
  deleteLayoutData: function(
    componenttype,
    comonent_id,
    link,
    delete_component_msg,
    recent_product_count
  ) {
    var result = false;
    jQuery.ajax({
      showLoader: true,
      url: deleteComponent,
      type: "post",
      data: {
        componenttype,
        comonent_id
      },
      success: function(data) {
        if (data != undefined) {
          if (data.status == "error") {
            Layout.prototype.errorMessage(delete_component_error);
          } else {
            link.parentNode.parentNode.removeChild(link.parentNode);
            jQuery("#" + link.parentNode.id).remove();
            jQuery("#" + link.parentNode.id).remove();
            Layout.prototype.saveLayoutSequence(
              delete_component_msg,
              recent_product_count
            );
          }
        }
      },
      error: function() {
        Layout.prototype.errorMessage(delete_component_error);
      }
    });
  },
  successMessage: function(message) {
    jQuery.notiny({
      text: message,
      position: "right-top",
      theme: "success",
      delay: 3000
    });
  },
  errorMessage: function(message) {
    jQuery.notiny({
      text: message,
      position: "right-top",
      theme: "error",
      delay: 3000
    });
  }
};

require(["Magento_Ui/js/modal/modal", "mage/validation"], function(modal) {
  /* below code used for featured category blocks popup model operations */
  jQuery(document).on("click", "#add-fcb-form-btn", function() {
    jQuery("#save_featuredcategory_blocks").trigger("reset");
    jQuery("#featured_category_form").show();
    jQuery("#add-fcb-form-btn").hide();
    jQuery("#featured_category_id").val("");
    jQuery("#category_image").val("new");
    jQuery("#cat_image_file_sec").fadeOut();
    jQuery("#cat_image_url_holder").fadeOut();
    jQuery("#cat_image_url_holder").hide();
    jQuery("#cat_image_url_sec").fadeOut();
    jQuery("#cat_category_id_holder").val("");
  });
  jQuery(document).on("click", "#fcb-save-btn", function() {
    jQuery("#fcb_form_save_type").val("save");
  });
  jQuery(document).on("click", "#fcb-save-add-new-btn", function() {
    jQuery("#fcb_form_save_type").val("save_and_add_new");
  });
  jQuery(document).on("click", ".edit-fcb-data", function() {
    jQuery("#featured_category_form").show();
    jQuery("#add-fcb-form-btn").hide();
  });
  jQuery(document).on("change", "#cat_image_type", function(event) {
    if (event.target.value == "url") {
      jQuery("#cat_image_url_sec").fadeIn();
      jQuery("#cat_image_url_holder").fadeIn();
      jQuery("#cat_image_file_sec").fadeOut();
      if (
        jQuery("#cat_image_url").val() == null ||
        jQuery("#cat_image_url").val() == ""
      ) {
        jQuery("#categoryimage").attr(
          "src",
          jQuery("#cat_placeholder_img").attr("src")
        );
      } else {
        jQuery("#categoryimage").attr("src", jQuery("#cat_image_url").val());
      }
    } else if (event.target.value == "image") {
      jQuery("#cat_image_file_sec").fadeIn();
      jQuery("#cat_image_url_holder").fadeIn();
      jQuery("#cat_image_url_sec").fadeOut();
      jQuery("#categoryimage").attr(
        "src",
        jQuery("#cat_placeholder_img").attr("src")
      );
    } else {
      jQuery("#cat_image_file_sec").fadeOut();
      jQuery("#cat_image_url_holder").fadeOut();
      jQuery("#cat_image_url_sec").fadeOut();
      jQuery("#categoryimage").attr(
        "src",
        jQuery("#cat_placeholder_img").attr("src")
      );
    }
  });

  jQuery(document).on("change", "#cat_image_url", function(event) {
    if (event.target.value.match(/\.(jpeg|jpg|gif|png|webp)$/) != null) {
      jQuery("#categoryimage").attr("src", event.target.value);
    } else {
      jQuery("#cat_image_url").val("");
      jQuery("#categoryimage").attr(
        "src",
        jQuery("#cat_placeholder_img").attr("src")
      );
      Layout.prototype.errorMessage("Please enter a proper image url");
    }
  });

  /* below function is used to open popup modal for featured category blocks */
  jQuery(document).on("click", ".featured-category-block-modal-popup", function() {
    var elementId = jQuery(this).attr("id");
    jQuery.ajax({
      showLoader: true,
      url: ajaxFcbUrl,
      data: {
        form_key: window.FORM_KEY,
        layout_id: jQuery("#layout_id").val(),
        id: elementId
      },
      type: "post",
      success: function(data) {
        fcb_error_count = 0;
        if (data != undefined) {
          jQuery("#save_featured_category_blocks").html(data);
          var options = {
            title: "Edit Featured Category Block",
            data: {
              form_key: window.FORM_KEY
            },
            type: "popup",
            responsive: true,
            innerScroll: true,
            opened: function() {
              jQuery(".modal-footer").hide();
            }
          };

          var popup = modal(options, jQuery("#save_featured_category_blocks"));
          jQuery("#save_featured_category_blocks").modal("openModal");
        }
      },
      error: function() {
        Layout.prototype.errorMessage("something went wrong");
      }
    });
  });
  /* above code used for featured category blocks popup model operations */

  /* below function is used to open popup modal for product grid */
  jQuery(document).on("click", ".product-grid-modal-popup", function() {
    var elementId = jQuery(this).attr("id");
    var layoutId = jQuery("#layout_id").val();
    jQuery.ajax({
      showLoader: true,
      url: ajaxProductGridUrl,
      data: {
        form_key: window.FORM_KEY,
        layout_id: layoutId,
        id: elementId
      },
      type: "post",
      success: function(data) {
        if (data != undefined) {
          jQuery("#save_product_grid").html(data);
          Layout.prototype.ProductGridshowHideProductType(
            jQuery("#product_type").val()
          );

          var options = {
            title: "Edit Product Grid",
            data: {
              form_key: window.FORM_KEY,
              layout_id: layoutId,
              id: elementId
            },
            type: "popup",
            responsive: true,
            innerScroll: true,
            buttons: [
              {
                text: jQuery.mage.__("Save"),
                class:
                  "button_submit_form_component btn btn-success save_product_grid_data " +
                  elementId,
                click: function() {
                  // this.closeModal();
                }
              },
              {
                text: jQuery.mage.__("Close"),
                class: "btn btn-warning banner_popup_close",
                click: function() {
                  this.closeModal();
                }
              }
            ]
          };

          var popup = modal(options, jQuery("#save_product_grid"));
          jQuery("#save_product_grid").modal("openModal");
        }
      }
    });
  });

  /* below function is used to open popup modal for product horizontal sliding */
  jQuery(document).on("click", ".product-slider-modal-popup", function() {
    var elementId = jQuery(this).attr("id");
    var layoutId = jQuery("#layout_id").val();
    jQuery.ajax({
      showLoader: true,
      url: ajaxPhsUrl,
      data: {
        form_key: window.FORM_KEY,
        layout_id: layoutId,
        id: elementId
      },
      type: "post",
      success: function(data) {
        if (data != undefined) {
          jQuery("#save_product_horizontal_slider").html(data);

          Layout.prototype.showHideProductSliderType(
            jQuery("#phs_product_type").val()
          );

          var options = {
            title: "Edit Product Horizontal Slider",
            data: {
              form_key: window.FORM_KEY
            },
            type: "popup",
            responsive: true,
            innerScroll: true,
            buttons: [
              {
                text: jQuery.mage.__("Save"),
                class:
                  "button_submit_form_component btn btn-success save_product_slider_data",
                click: function() {
                  // this.closeModal();
                }
              },
              {
                text: jQuery.mage.__("Close"),
                class: "btn btn-warning banner_popup_close",
                click: function() {
                  this.closeModal();
                }
              }
            ]
          };

          var popup = modal(options, jQuery("#save_product_horizontal_slider"));
          jQuery("#save_product_horizontal_slider").modal("openModal");
        }
      }
    });
  });

  jQuery(document).on("change", "#image_type", function(event) {
    if (event.target.value == "url") {
      jQuery("#image_url_sec").fadeIn();
      jQuery("#image_url_holder").fadeIn();
      jQuery("#image_file_sec").fadeOut();
      if (
        jQuery("#image_url").val() == null ||
        jQuery("#image_url").val() == ""
      ) {
        jQuery("#sliderimage").attr(
          "src",
          jQuery("#placeholder_img").attr("src")
        );
      } else {
        jQuery("#sliderimage").attr("src", jQuery("#image_url").val());
      }
    } else if (event.target.value == "image") {
      jQuery("#image_file_sec").fadeIn();
      jQuery("#image_url_holder").fadeIn();
      jQuery("#image_url_sec").fadeOut();
      jQuery("#sliderimage").attr(
        "src",
        jQuery("#placeholder_img").attr("src")
      );
    } else {
      jQuery("#image_file_sec").fadeOut();
      jQuery("#image_url_holder").fadeOut();
      jQuery("#image_url_sec").fadeOut();
      jQuery("#sliderimage").attr(
        "src",
        jQuery("#placeholder_img").attr("src")
      );
    }
  });

  jQuery(document).on("change", "#new_arrival_product_type", function(event) {
    if (event.target.value == 1) {
      jQuery(".field-phs_display_category").show();
    } else {
      jQuery(".field-phs_display_category").hide();
      jQuery("#phs_display_category").val("");
    }
  });

  jQuery(document).on("change", "#image_url", function(event) {
    if (event.target.value.match(/\.(jpeg|jpg|gif|png|webp)$/) != null) {
      jQuery("#sliderimage").attr("src", event.target.value);
    } else {
      jQuery("#image_url").val("");
      jQuery("#sliderimage").attr(
        "src",
        jQuery("#placeholder_img").attr("src")
      );
      Layout.prototype.errorMessage("Please enter a proper image url");
    }
  });

  jQuery(document).on("change", "#banner_type", function(event) {
    if (event.target.value == "category_offers") {
      jQuery("#banner_category_id_holder").fadeIn();
      jQuery("#product_id_holder").fadeOut();
      jQuery("#product_id_holder").val("");
      jQuery("#offer_links").fadeOut();
      jQuery("#offer_links").val("");
    } else if (event.target.value == "product") {
      jQuery("#product_id_holder").fadeIn();
      jQuery("#banner_category_id_holder").fadeOut();
      jQuery("#banner_category_id_holder").val("");
      jQuery("#offer_links").fadeOut();
      jQuery("#offer_links").val("");
    } else if (event.target.value == "offer") {
      jQuery("#offer_links").fadeIn();
      jQuery("#product_id_holder").fadeOut();
      jQuery("#product_id_holder").val("");
      jQuery("#banner_category_id_holder").fadeOut();
      jQuery("#banner_category_id_holder").val("");
    } else {
      jQuery("#offer_links").fadeOut();
      jQuery("#offer_links").val("");
      jQuery("#product_id_holder").fadeOut();
      jQuery("#product_id_holder").val("");
      jQuery("#banner_category_id_holder").fadeOut();
      jQuery("#banner_category_id_holder").val("");
    }
  });

  /* below function is used to open popup modal for banner slider */
  jQuery(document).on("click", "#add-bannerslider-form-btn", function() {
    jQuery("#save_banneroffer_slider").trigger("reset");
    jQuery("#banner_offer_slider_form").show();
    jQuery("#add-bannerslider-form-btn").hide();
    jQuery("#banner_slider_id").val("");
    jQuery("#banner_slider_image").val("new");
    jQuery("#image_file_sec").fadeOut();
    jQuery("#image_url_holder").fadeOut();
    jQuery("#image_url_sec").fadeOut();
    jQuery("#offer_links").fadeOut();
    jQuery("#offer_links").val("");
    jQuery("#product_id_holder").fadeOut();
    jQuery("#product_id_holder").val("");
    jQuery("#banner_category_id_holder").fadeOut();
    jQuery("#banner_category_id_holder").val("");
    jQuery("#image_url_holder").hide();
  });
  jQuery(document).on("click", "#bannerslider-save-btn", function() {
    jQuery("#bannerslider_form_save_type").val("save");
  });
  jQuery(document).on("click", "#bannerslider-save-add-new-btn", function() {
    jQuery("#bannerslider_form_save_type").val("save_and_add_new");
  });
  jQuery(document).on("click", ".edit-banner-offer-data", function() {
    jQuery("#banner_offer_slider_form").show();
    jQuery("#add-bannerslider-form-btn").hide();
  });

  /* below function is used to open popup modal for banner slider */
  jQuery(document).on("click", ".banner-slider-modal-popup", function() {
    var elementId = jQuery(this).attr("id");
    jQuery.ajax({
      showLoader: true,
      url: ajaxBannerSliderUrl,
      data: {
        form_key: window.FORM_KEY,
        layout_id: jQuery("#layout_id").val(),
        id: elementId
      },
      type: "post",
      success: function(data) {
        banner_error_count = 0;
        if (data != undefined) {
          jQuery("#save_banner_slider").html(data);
          var options = {
            title: "Edit Banner Slider",
            data: {
              form_key: window.FORM_KEY
            },
            type: "popup",
            responsive: true,
            innerScroll: true,
            opened: function() {
              jQuery(".modal-footer").hide();
            }
          };

          var popup = modal(options, jQuery("#save_banner_slider"));
          jQuery("#save_banner_slider").modal("openModal");
        }
      }
    });
  });

  /* below function is used to save data of product slider */
  jQuery(document).on("click", ".save_product_slider_data", function() {
    var dataArr = jQuery("#save_product_horizontal_slider").serializeArray();
    if (validatePhs()) {
      jQuery.ajax({
        showLoader: true,
        url: ajaxSavePhsUrl,
        type: "post",
        data: dataArr,
        success: function(result) {
          if (result.status == "success") {
            Layout.prototype.successMessage(result.message);
          } else if (result.status == "error") {
            Layout.prototype.errorMessage(result.message);
          }
        },
        error: function() {
          Layout.prototype.errorMessage("Something went wront while save data");
        }
      });
    }
  });

  /* below function is used to save data of product grid */
  jQuery(document).on("click", ".save_product_grid_data", function() {
    var dataArr = jQuery("#save_product_grid").serializeArray();
    if (validateProductGrid()) {
      var productgrid_id = dataArr[0]["value"];
      var layout_id = dataArr[1]["value"];
      dataArr.layout_id = layout_id;
      dataArr.form_key = window.FORM_KEY;
      jQuery.ajax({
        showLoader: true,
        url: ajaxSaveProductGridUrl,
        type: "post",
        data: dataArr,
        success: function(data) {
          if (data.status == "success") {
            Layout.prototype.successMessage(data.message);
          }
        },
        error: function() {
          Layout.prototype.errorMessage("something went wrong");
        }
      });
    }
  });
});

function validateFcbData() {
  var valid = true;

  if (jQuery('#cat_display_type').val() == "") {
    // Layout.prototype.errorMessage("Please select display type.");
    jQuery('#cat_display_type').css("border-color", "#D55D5A");
    jQuery('#cat_display_type_error').html("*Please select display type.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery('#cat_display_type').css("border-color", "#ADADAD");
    jQuery('#cat_display_type_error').html("");
  }
  
  return valid;
}
function validateFcb() {
  var valid = true;
  if (jQuery("#category_title").val() == "") {
    // Layout.prototype.errorMessage("Please enter category title.");
    jQuery("#category_title").css("border-color", "#D55D5A");
    jQuery("#category_title_error").html("*Please enter category title.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery("#category_title").css("border-color", "#ADADAD");
    jQuery("#category_title_error").html("");
  }

  if (jQuery("#cat_image_type").val() == "") {
    // Layout.prototype.errorMessage("Please select image type.");
    jQuery("#cat_image_type").css("border-color", "#D55D5A");
    jQuery("#cat_image_type_error").html("*Please select image type.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery("#cat_image_type").css("border-color", "#ADADAD");
    jQuery("#cat_image_type_error").html("");
  }

  if (
    jQuery("#cat_category_id").val() == "" || 
    jQuery("#cat_category_id").val() == null
  ) {
    // Layout.prototype.errorMessage("Please select category.");
    jQuery("#cat_category_id").css("border-color", "#D55D5A");
    jQuery("#cat_category_id_error").html("*Please select category.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery("#cat_category_id").css("border-color", "#ADADAD");
    jQuery("#cat_category_id_error").html("");
  }

  if (
    jQuery("#category_position").val() == "" || 
    jQuery("#category_position").val() == "0"
  ) {
    // Layout.prototype.errorMessage("Please enter valid sort order.");
    jQuery("#category_position").css("border-color", "#D55D5A");
    jQuery("#category_position_error").html("*Please enter valid sort order.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery("#category_position").css("border-color", "#ADADAD");
    jQuery("#category_position_error").html("");
  }

  if (
    jQuery("#cat_image_type").val() == "url" &&
    jQuery("#cat_image_url").val() == ""
  ) {
    // Layout.prototype.errorMessage("Please enter category image url.");
    jQuery("#cat_image_url").css("border-color", "#D55D5A");
    jQuery("#cat_image_url_error").html("*Please enter category image url.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery("#cat_image_url").css("border-color", "#ADADAD");
    jQuery("#cat_image_url_error").html("");
  }

  //need to check few conditions here !
  //categoryuploadedfile
  if (
    jQuery("#cat_image_type").val() == "image" &&
    jQuery("#categoryuploadedfile").val() == "" &&
    jQuery("#category_image").val() == "new"
  ) {
    // Layout.prototype.errorMessage("Please upload category image.");
    jQuery("#categoryuploadedfile").css("border-color", "#D55D5A");
    jQuery("#categoryuploadedfile").attr("placeholder", "Please upload category image.");
    jQuery("#categoryuploadedfile_error").html("*Please upload category image.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery("#categoryuploadedfile").css("border-color", "#ADADAD");
    jQuery("#categoryuploadedfile").attr("placeholder", "");
    jQuery("#categoryuploadedfile_error").html("");
  }

  if (jQuery("#category_title").val() == "") {
    jQuery("#category_title").focus();
  } else if (jQuery("#cat_image_type").val() == "") {
    jQuery("#cat_image_type").focus();
  } else if (
    jQuery("#cat_image_type").val() == "url" &&
    jQuery("#cat_image_url").val() == ""
  ) {
    jQuery("#cat_image_url").focus();
  } else if (
    jQuery("#cat_image_type").val() == "image" &&
    jQuery("#categoryuploadedfile").val() == "" &&
    jQuery("#category_image").val() == "new"
  ) {
    jQuery("#categoryuploadedfile").focus();
  } else if (
    jQuery("#cat_category_id").val() == "" || 
    jQuery("#cat_category_id").val() == null
  ) {
    jQuery("#cat_category_id").focus();
  } else if (
    jQuery("#category_position").val() == "" || 
    jQuery("#category_position").val() == "0"
  ) {
    jQuery("#category_position").focus();
  }

  return valid;
}

function validateProductGrid() {
  var valid = true;

  if (jQuery("#component_title").val() == "") {
    // Layout.prototype.errorMessage("Component title is required");
    if(!jQuery("#component_title_error").parent().length){
      jQuery("#component_title").after("<span id='component_title_error' class='productgrid-error text-danger'><br>*Component title is required.</span>");
    }
    jQuery("#component_title").css("border-color", "#D55D5A");
    valid = false;
  } else {
    if(jQuery("#component_title_error").parent().length){
      jQuery("#component_title_error").remove();
    }
    jQuery("#component_title").css("border-color", "#ADADAD");
  }
  if (
    jQuery("#product_type").val() == "category_products" &&
    jQuery("#category_id").val() == ""
  ) {
    // Layout.prototype.errorMessage("Category selection is required");
    if(!jQuery("#category_id_error").parent().length){
      jQuery("#category_id").after("<span id='category_id_error' class='productgrid-error text-danger'><br>*Category selection is required.</span>");
    }
    jQuery("#category_id").css("border-color", "#D55D5A");
    valid = false;
  } else {
    if(jQuery("#category_id_error").parent().length){
      jQuery("#category_id_error").remove();
    }
    jQuery("#category_id").css("border-color", "#ADADAD");
  }
  if (
    jQuery("#product_type").val() == "category_products" &&
    jQuery("#category_id").val() != "" &&
    jQuery("#category_products").val() == null
  ) {
    // Layout.prototype.errorMessage("Product selection is required");
    if(!jQuery("#category_products_error").parent().length){
      jQuery("#category_products").after("<span id='category_products_error' class='productgrid-error text-danger'><br>*Product selection is required.</span>");
    } else if (jQuery("#category_products_error").parent().length) {
      jQuery("#category_products_error").remove();
      jQuery("#category_products").after("<span id='category_products_error' class='productgrid-error text-danger'><br>*Product selection is required.</span>");
    }
    jQuery("#category_products").css("border-color", "#D55D5A");
    valid = false;
  } else if (
    jQuery("#product_type").val() == "category_products" &&
    jQuery("#category_id").val() != "" &&
    jQuery("#category_products").val().length < 4
  ) {
    // Layout.prototype.errorMessage("Please select atleast 4 products");
    if(!jQuery("#category_products_error").parent().length){
      jQuery("#category_products").after("<span id='category_products_error' class='productgrid-error text-danger'><br>*Please select atleast 4 products.</span>");
    } else if (jQuery("#category_products_error").parent().length) {
      jQuery("#category_products_error").remove();
      jQuery("#category_products").after("<span id='category_products_error' class='productgrid-error text-danger'><br>*Please select atleast 4 products.</span>");
    }
    jQuery("#category_products").css("border-color", "#D55D5A");
    valid = false;
  } else {
    if(jQuery("#category_products_error").parent().length){
      jQuery("#category_products_error").remove();
    }
    jQuery("#category_products").css("border-color", "#ADADAD");
  }
  if (
    jQuery("#product_type").val() == "custom_products" &&
    jQuery("#product_list").val() == null
  ) {
    // Layout.prototype.errorMessage("Product selection is required");
    if(!jQuery("#product_list_error").parent().length){
      jQuery("#product_list").after("<span id='product_list_error' class='productgrid-error text-danger'><br>*Product selection is required.</span>");
    } else if (jQuery("#product_list_error").parent().length) {
      jQuery("#product_list_error").remove();
      jQuery("#product_list").after("<span id='product_list_error' class='productgrid-error text-danger'><br>*Product selection is required.</span>");
    }
    jQuery("#product_list").css("border-color", "#D55D5A");
    valid = false;
  } else if (
    jQuery("#product_type").val() == "custom_products" &&
    jQuery("#product_list").val().length < 4
  ) {
    // Layout.prototype.errorMessage("Please select atleast 4 products");
    if(!jQuery("#product_list_error").parent().length){
      jQuery("#product_list").after("<span id='product_list_error' class='productgrid-error text-danger'><br>*Please select atleast 4 products.</span>");
    } else if (jQuery("#product_list_error").parent().length) {
      jQuery("#product_list_error").remove();
      jQuery("#product_list").after("<span id='product_list_error' class='productgrid-error text-danger'><br>*Please select atleast 4 products.</span>");
    }
    jQuery("#product_list").css("border-color", "#D55D5A");
    valid = false;
  } else {
    if(jQuery("#product_list_error").parent().length){
      jQuery("#product_list_error").remove();
    }
    jQuery("#product_list").css("border-color", "#ADADAD");
  }

  if (jQuery("#component_title").val() == "") {
    jQuery("#component_title").focus();
  } else if (
    jQuery("#product_type").val() == "category_products" &&
    jQuery("#category_id").val() == ""
  ) {
    jQuery("#category_id").focus();
  } else if (
    jQuery("#product_type").val() == "category_products" &&
    jQuery("#category_id").val() != "" &&
    jQuery("#category_products").val() == null
  ) {
    jQuery("#category_products").focus();
  } else if (
    jQuery("#product_type").val() == "category_products" &&
    jQuery("#category_id").val() != "" &&
    jQuery("#category_products").val().length < 4
  ) {
    jQuery("#category_products").focus();
  } else if (
    jQuery("#product_type").val() == "custom_products" &&
    jQuery("#product_list").val() == null
  ) {
    jQuery("#product_list").focus();
  } else if (
    jQuery("#product_type").val() == "custom_products" &&
    jQuery("#product_list").val().length < 4
  ) {
    jQuery("#product_list").focus();
  }
  return valid;
}

function validatePhs() {
  var valid = true;

  if (jQuery("#component_title").val() == "") {
    // Layout.prototype.errorMessage("Component title is required");
    if(!jQuery("#phs_component_title_error").parent().length){
      jQuery("#component_title").after("<span id='phs_component_title_error' class='phs-error text-danger'><br>*Component title is required.</span>");
    }
    jQuery("#component_title").css("border-color", "#D55D5A");
    valid = false;
  } else {
    if(jQuery("#phs_component_title_error").parent().length){
      jQuery("#phs_component_title_error").remove();
    }
    jQuery("#component_title").css("border-color", "#ADADAD");
  }
  if (
    jQuery("#phs_product_type").val() == "category_products" &&
    jQuery("#phs_category_id").val() == ""
  ) {
    // Layout.prototype.errorMessage("Category selection is required");
    if(!jQuery("#phs_category_id_error").parent().length){
      jQuery("#phs_category_id").after("<span id='phs_category_id_error' class='productgrid-error text-danger'><br>*Category selection is required.</span>");
    }
    jQuery("#phs_category_id").css("border-color", "#D55D5A");
    valid = false;
  } else {
    if(jQuery("#phs_category_id_error").parent().length){
      jQuery("#phs_category_id_error").remove();
    }
    jQuery("#phs_category_id").css("border-color", "#ADADAD");
  }
  if (
    jQuery("#phs_product_type").val() == "category_products" &&
    jQuery("#phs_category_id").val() != "" &&
    jQuery("#phs_category_products").val() == null
  ) {
    // Layout.prototype.errorMessage("Product selection is required");
    if(!jQuery("#phs_category_products_error").parent().length){
      jQuery("#phs_category_products").after("<span id='phs_category_products_error' class='productgrid-error text-danger'><br>*Product selection is required.</span>");
    }
    jQuery("#phs_category_products").css("border-color", "#D55D5A");
    valid = false;
  } else {
    if(jQuery("#phs_category_products_error").parent().length){
      jQuery("#phs_category_products_error").remove();
    }
    jQuery("#phs_category_products").css("border-color", "#ADADAD");
  }
  if (
    jQuery("#phs_product_type").val() == "custom_products" &&
    jQuery("#phs_product_list").val() == null
  ) {
    // Layout.prototype.errorMessage("Product selection is required");
    if(!jQuery("#phs_product_list_error").parent().length){
      jQuery("#phs_product_list").after("<span id='phs_product_list_error' class='productgrid-error text-danger'><br>*Product selection is required.</span>");
    } 
    jQuery("#phs_product_list").css("border-color", "#D55D5A");
    valid = false;
  } else {
    if(jQuery("#phs_product_list_error").parent().length){
      jQuery("#phs_product_list_error").remove();
    }
    jQuery("#phs_product_list").css("border-color", "#ADADAD");
  }
  if (
    jQuery("#phs_product_type").val() == "new_products" &&
    jQuery("#new_arrival_product_type").val() == null
  ) {
    // Layout.prototype.errorMessage("Display new products type selection is required");
    if(!jQuery("#new_arrival_product_type_error").parent().length){
      jQuery("#new_arrival_product_type").after("<span id='new_arrival_product_type_error' class='productgrid-error text-danger'><br>*Display new products type selection is required.</span>");
    } 
    jQuery("#new_arrival_product_type").css("border-color", "#D55D5A");
    valid = false;
  } else {
    if(jQuery("#new_arrival_product_type_error").parent().length){
      jQuery("#new_arrival_product_type_error").remove();
    }
    jQuery("#new_arrival_product_type").css("border-color", "#ADADAD");
  }
  if (
    jQuery("#phs_product_type").val() == "new_products" &&
    jQuery("#new_arrival_product_type").val() == 1 &&
    (
      jQuery("#phs_display_category").val() == null ||
      jQuery("#phs_display_category").val() == ""
    )
  ) {
    // Layout.prototype.errorMessage("Display category selection is required");
    if(!jQuery("#phs_display_category_error").parent().length){
      jQuery("#phs_display_category").after("<span id='phs_display_category_error' class='productgrid-error text-danger'><br>*Display category selection is required.</span>");
    } 
    jQuery("#phs_display_category").css("border-color", "#D55D5A");
    valid = false;
  } else {
    if(jQuery("#phs_display_category_error").parent().length){
      jQuery("#phs_display_category_error").remove();
    }
    jQuery("#phs_display_category").css("border-color", "#ADADAD");
  }

  if (jQuery("#component_title").val() == "") {
    jQuery("#component_title").focus();
  } else if (
    jQuery("#phs_product_type").val() == "category_products" &&
    jQuery("#phs_category_id").val() == ""
  ) {
    jQuery("#phs_category_id").focus();
  } else if (
    jQuery("#phs_product_type").val() == "category_products" &&
    jQuery("#phs_category_id").val() != "" &&
    jQuery("#phs_category_products").val() == null
  ) {
    jQuery("#phs_category_products").focus();
  } else if (
    jQuery("#phs_product_type").val() == "custom_products" &&
    jQuery("#phs_product_list").val() == null
  ) {
    jQuery("#phs_product_list").focus();
  } else if (
    jQuery("#phs_product_type").val() == "new_products" &&
    jQuery("#new_arrival_product_type").val() == null
  ) {
    jQuery("#new_arrival_product_type").focus();
  } else if (
    jQuery("#phs_product_type").val() == "new_products" &&
    jQuery("#new_arrival_product_type").val() == 1 &&
    (
      jQuery("#phs_display_category").val() == null ||
      jQuery("#phs_display_category").val() == ""
    )
  ) {
    jQuery("#phs_display_category").focus();
  }
  return valid;
}

function validateBannerSlider() {
  var valid = true;
  if (jQuery("#banner_title").val() == "") {
    // Layout.prototype.errorMessage("Please enter banner title.");
    jQuery("#banner_title").css("border-color", "#D55D5A");
    jQuery("#banner_title_error").html("*Please enter banner title.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery("#banner_title").css("border-color", "#ADADAD");
    jQuery("#banner_title_error").html("");
  }

  if (jQuery("#image_type").val() == "") {
    // Layout.prototype.errorMessage("Please select image type.");
    jQuery("#image_type").css("border-color", "#D55D5A");
    jQuery("#image_type_error").html("*Please select image type.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery("#image_type").css("border-color", "#ADADAD");
    jQuery("#image_type_error").html("");
  }

  if (jQuery("#banner_type").val() == "") {
    // Layout.prototype.errorMessage("Please select banner type.");
    jQuery("#banner_type").css("border-color", "#D55D5A");
    jQuery("#banner_type_error").html("*Please select banner type.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery("#banner_type").css("border-color", "#ADADAD");
    jQuery("#banner_type_error").html("");
  }

  if (
    jQuery("#banner_type").val() == "category_offers" && 
    (
      jQuery("#banner_category_id").val() == "" || 
      jQuery("#banner_category_id").val() == null
    )
  ) {
    // Layout.prototype.errorMessage("Please select category.");
    jQuery("#banner_category_id").css("border-color", "#D55D5A");
    jQuery("#banner_category_id_error").html("*Please select category.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery("#banner_category_id").css("border-color", "#ADADAD");
    jQuery("#banner_category_id_error").html("");
  }

  if (
    jQuery("#banner_type").val() == "product" && 
    (
      jQuery("#redirect_banner_product_id").val() == "" || 
      jQuery("#redirect_banner_product_id").val() == null
    )
  ) {
    // Layout.prototype.errorMessage("Please select product.");
    jQuery("#redirect_banner_product_id").css("border-color", "#D55D5A");
    jQuery("#redirect_banner_product_id_error").html("*Please select product.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery("#redirect_banner_product_id").css("border-color", "#ADADAD");
    jQuery("#redirect_banner_product_id_error").html("");
  }

  if (
    jQuery("#banner_type").val() == "offer" && 
    (
      jQuery("#offer_link_redirect").val() == "" || 
      jQuery("#offer_link_redirect").val() == null
    )
  ) {
    // Layout.prototype.errorMessage("Please enter offer url.");
    jQuery("#offer_link_redirect").css("border-color", "#D55D5A");
    jQuery("#offer_link_redirect_error").html("*Please enter offer url.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery("#offer_link_redirect").css("border-color", "#ADADAD");
    jQuery("#offer_link_redirect_error").html("");
  }

  if (
    jQuery("#banner_position").val() == "" || 
    jQuery("#banner_position").val() == "0"
  ) {
    // Layout.prototype.errorMessage("Please enter valid sort order.");
    jQuery("#banner_position").css("border-color", "#D55D5A");
    jQuery("#banner_position_error").html("*Please enter valid sort order.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery("#banner_position").css("border-color", "#ADADAD");
    jQuery("#banner_position_error").html("");
  }

  if (
    jQuery("#image_type").val() == "url" &&
    jQuery("#image_url").val() == ""
  ) {
    // Layout.prototype.errorMessage("Please enter banner image url.");
    jQuery("#image_url").css("border-color", "#D55D5A");
    jQuery("#image_url_error").html("*Please enter banner image url.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery("#image_url").css("border-color", "#ADADAD");
    jQuery("#image_url_error").html("");
  }

  //need to check few conditions here !
  //slideruploadedfile
  if (
    jQuery("#image_type").val() == "image" &&
    jQuery("#slideruploadedfile").val() == "" &&
    jQuery("#banner_slider_image").val() == "new"
  ) {
    // Layout.prototype.errorMessage("Please upload banner image.");
    jQuery("#slideruploadedfile").css("border-color", "#D55D5A");
    jQuery("#slideruploadedfile_error").html("*Please upload banner image.");
    jQuery(this).focus();
    valid = false;
  } else {
    jQuery("#slideruploadedfile").css("border-color", "#ADADAD");
    jQuery("#slideruploadedfile_error").html("");
  }

  if (jQuery("#banner_title").val() == "") {
    jQuery("#banner_title").focus();
  } else if (jQuery("#image_type").val() == "") {
    jQuery("#image_type").focus();
  } else if (
    jQuery("#image_type").val() == "url" &&
    jQuery("#image_url").val() == ""
  ) {
    jQuery("#image_url").focus();
  } else if (
    jQuery("#image_type").val() == "image" &&
    jQuery("#slideruploadedfile").val() == "" &&
    jQuery("#banner_slider_image").val() == "new"
  ) {
    jQuery("#slideruploadedfile").focus();
  } else if (jQuery("#banner_type").val() == "") {
    jQuery("#banner_type").focus();
  } else if (
    jQuery("#banner_type").val() == "category_offers" && 
    (
      jQuery("#banner_category_id").val() == "" || 
      jQuery("#banner_category_id").val() == null
    )
  ) {
    jQuery("#banner_category_id").focus();
  } else if (
    jQuery("#banner_type").val() == "product" && 
    (
      jQuery("#redirect_banner_product_id").val() == "" || 
      jQuery("#redirect_banner_product_id").val() == null
    )
  ) {
    jQuery("#redirect_banner_product_id").focus();
  } else if (
    jQuery("#banner_type").val() == "offer" && 
    (
      jQuery("#offer_link_redirect").val() == "" || 
      jQuery("#offer_link_redirect").val() == null
    )
  ) {
    jQuery("#offer_link_redirect").focus();
  } else if (
    jQuery("#banner_position").val() == "" || 
    jQuery("#banner_position").val() == "0"
  ) {
    jQuery("#banner_position").focus();
  }

  return valid;
}
