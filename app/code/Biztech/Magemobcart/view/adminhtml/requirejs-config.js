var config = {
  map: {
    "*": {
      layout: "Biztech_Magemobcart/js/layout/layout"
    }
  },
  paths: {
    nicescroll: "Biztech_Magemobcart/js/jquery.nicescroll.min",
    notiny: "Biztech_Magemobcart/js/notiny.min"
  },
  shim: {
    nicescroll: {
      deps: ["jquery"]
    },
    notiny: {
      deps: ["jquery"]
    }
  }
};
