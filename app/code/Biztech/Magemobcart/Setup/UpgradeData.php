<?php
/**
 *
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Setup;

use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements UpgradeDataInterface
{
    private $_customerSetupFactory;

    public function __construct(
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
    ) {
        $this->_customerSetupFactory = $customerSetupFactory;
    }
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ){
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.1.5') < 0) {
            
            $customerSetup = $this->_customerSetupFactory->create(['setup' => $setup]);
            $customerSetup->addAttribute(
                Customer::ENTITY,
                'apple_id',
                [
                    'label' => 'Apple Id',
                    'input' => 'text',
                    'required' => false,
                    'sort_order' => 40,
                    'visible' => false,
                    'system' => false,
                    'is_used_in_grid' => false,
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'is_searchable_in_grid' => false,
                    'type' => 'varchar'
                ]
            );
            $attribute = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'apple_id');
            $attribute->save();

        }

        $setup->endSetup();
    }
}
