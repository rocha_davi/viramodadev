<?php
/**
 *
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        if (!$installer->tableExists('magemob_notification_history')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('magemob_notification_history')
            )
            ->addColumn(
                'notification_history_id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'notification_history_id'
            )
            ->addColumn(
                'type',
                Table::TYPE_TEXT,
                255,
                ['default' => null, 'nullable' => false],
                'type'
            )
            ->addColumn(
                'order_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'order_id'
            )
            ->addColumn(
                'customer_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'customer_id'
            )
            ->addColumn(
                'offer_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'offer_id'
            )
            ->addColumn(
                'is_read',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'is_read'
            )

            ->addColumn(
                'order_increment_id',
                Table::TYPE_TEXT,
                255,
                ['default' => null, 'nullable' => false],
                'order_increment_id'
            )
            ->addColumn(
                'order_status',
                Table::TYPE_TEXT,
                255,
                ['default' => null, 'nullable' => false],
                'order_status'
            )
            ->addColumn(
                'order_message',
                Table::TYPE_TEXT,
                255,
                ['default' => null, 'nullable' => false],
                'order_message'
            )
            ->addColumn(
                'order_grandtotal',
                Table::TYPE_TEXT,
                255,
                ['default' => null, 'nullable' => false],
                'order_grandtotal'
            )
            ->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                255,
                ['nullable' => false],
                'created_at'
            );
            $installer->getConnection()->createTable($table);
        }
        if (version_compare($context->getVersion(), '1.1.3', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('magemobcart'),
                'is_showing',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'length' => 10,
                    'nullable' => false,
                    'default' => 0,
                    'comment' => 'Category Title Show'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.1.4', '<')) {

        }
        if (version_compare($context->getVersion(), '1.1.5', '<')) {
            if ($installer->tableExists('magemob_bannerslider')) {
                $installer->getConnection()->dropTable($installer->getTable('magemob_bannerslider'));
            }
            if ($installer->tableExists('magemob_offerslider')) {
                $installer->getConnection()->dropTable($installer->getTable('magemob_offerslider'));
            }
            if ($installer->tableExists('magemobcart')) {
                if ($installer->getConnection()->tableColumnExists($installer->getTable('magemobcart'), 'is_showing') === true) {
                    $installer->getConnection()->dropColumn($installer->getTable('magemobcart'), 'is_showing');
                }
                if ($installer->getConnection()->tableColumnExists($installer->getTable('magemobcart'), 'store_id') === true) {
                    $installer->getConnection()->dropColumn($installer->getTable('magemobcart'), 'store_id');
                }
            }
            if (!$installer->tableExists('magemob_layout')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('magemob_layout')
                )
                ->addColumn(
                    'layout_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Layout Id'
                )
                ->addColumn(
                    'title',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => false],
                    'Layout Title'
                )
                ->addColumn(
                    'store_id',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => false],
                    'Store Id'
                )
                ->addColumn(
                    'is_default',
                    Table::TYPE_INTEGER,
                    0,
                    ['nullable' => false, 'default' => 0],
                    'Is Default'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    255,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    255,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                );
                $installer->getConnection()->createTable($table);
            }
            if (!$installer->tableExists('magemob_layout_components')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('magemob_layout_components')
                )
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Id'
                )
                ->addColumn(
                    'layout_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Layout Id'
                )
                ->addIndex(
                    $installer->getIdxName(
                        $installer->getTable('magemob_layout_components'),
                        ['layout_id'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                    ),
                    ['layout_id'],
                    ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
                )
                ->addForeignKey(
                    $installer->getFkName(
                        'magemob_layout_components',
                        'layout_id',
                        'magemob_layout',
                        'layout_id'
                    ),
                    'layout_id',
                    $installer->getTable('magemob_layout'), 
                    'layout_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                )
                ->addColumn(
                    'component_data',
                    Table::TYPE_TEXT,
                    null,
                    ['default' => null, 'nullable' => false],
                    'Component Data (JSON Data)'
                )
                ->addColumn(
                    'fcb_count',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Featured Category Component Count (Max Allow 1)'
                )
                ->addColumn(
                    'recent_product_count',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Recent Accessed Product Component Count (Max Allow 1)'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    255,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    255,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                );
                $installer->getConnection()->createTable($table);
            }
            if (!$installer->tableExists('magemob_productgrid')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('magemob_productgrid')
                )
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    255,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'ID'
                )
                ->addColumn(
                    'productgrid_id',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Productgrid ID'
                )
                ->addIndex(
                    $installer->getIdxName(
                        $installer->getTable('magemob_productgrid'),
                        ['productgrid_id'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                    ),
                    ['productgrid_id'],
                    ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
                )
                ->addColumn(
                    'layout_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Layout ID'
                )->addForeignKey(
                    $installer->getFkName(
                        'magemob_productgrid',
                        'layout_id',
                        'magemob_layout',
                        'layout_id'
                    ),
                    'layout_id',
                    $installer->getTable('magemob_layout'), 
                    'layout_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                )
                ->addColumn(
                    'component_title',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => false],
                    'component_title'
                )
                ->addColumn(
                    'product_type',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => false],
                    'Product Type'
                )
                ->addColumn(
                    'category_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => true],
                    'Category Id'
                )
                ->addColumn(
                    'category_products',
                    Table::TYPE_TEXT,
                    null,
                    ['default' => null, 'nullable' => true],
                    'Category Products'
                )
                ->addColumn(
                    'product_list',
                    Table::TYPE_TEXT,
                    null,
                    ['default' => null, 'nullable' => true],
                    'Product List'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    255,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    255,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                );
                $installer->getConnection()->createTable($table);
            }
            if (!$installer->tableExists('magemob_producthorizontalslider')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('magemob_producthorizontalslider')
                )
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    255,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'ID'
                )
                ->addColumn(
                    'phs_id',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Product Horizontal Slider ID'
                )
                ->addIndex(
                    $installer->getIdxName(
                        $installer->getTable('magemob_producthorizontalslider'),
                        ['phs_id'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                    ),
                    ['phs_id'],
                    ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
                )
                ->addColumn(
                    'layout_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Layout ID'
                )->addForeignKey(
                    $installer->getFkName(
                        'magemob_producthorizontalslider',
                        'layout_id',
                        'magemob_layout',
                        'layout_id'
                    ),
                    'layout_id',
                    $installer->getTable('magemob_layout'), 
                    'layout_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                )
                ->addColumn(
                    'component_title',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => false],
                    'component_title'
                )
                ->addColumn(
                    'phs_product_type',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => false],
                    'Product Type'
                )->addColumn(
                    'new_arrival_product_type',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Type of Show New Arrival Products'
                )
                ->addColumn(
                    'phs_display_category',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => true],
                    'Display Category'
                )
                ->addColumn(
                    'phs_category_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => true],
                    'Category ID'
                )
                ->addColumn(
                    'phs_category_products',
                    Table::TYPE_TEXT,
                    null,
                    ['default' => null, 'nullable' => true],
                    'Category Products'
                )
                ->addColumn(
                    'phs_product_list',
                    Table::TYPE_TEXT,
                    null,
                    ['default' => null, 'nullable' => true],
                    'Product List'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    255,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    255,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                );
                $installer->getConnection()->createTable($table);
            }
            if (!$installer->tableExists('magemob_bannerslider1')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('magemob_bannerslider1')
                )
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    255,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'ID'
                )
                ->addColumn(
                    'bannerslider_id',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Bannerslider ID'
                )
                ->addColumn(
                    'layout_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Layout ID'
                )->addForeignKey(
                    $installer->getFkName(
                        'magemob_bannerslider1',
                        'layout_id',
                        'magemob_layout',
                        'layout_id'
                    ),
                    'layout_id',
                    $installer->getTable('magemob_layout'), 
                    'layout_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                )
                ->addColumn(
                    'component_title',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => false],
                    'Component Title'
                )
                ->addColumn(
                    'image_type',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => false],
                    'Image Type'
                )
                ->addColumn(
                    'filename',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => true],
                    'Image File name'
                )
                ->addColumn(
                    'filepath',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => true],
                    'Image File path'
                )
                ->addColumn(
                    'image_url',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => true],
                    'Image URL'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                    'Enable / Disable'
                )
                ->addColumn(
                    'redirect_activity',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => true],
                    'Redirect Activity'
                )
                ->addColumn(
                    'offer_link',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => true],
                    'Offer Link'
                )
                ->addColumn(
                    'product_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Product Id'
                )
                ->addColumn(
                    'category_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Category Id'
                )
                ->addColumn(
                    'sort_order',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Banner Order'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    255,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    255,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                );
                $installer->getConnection()->createTable($table);
            }
        }
        if (version_compare($context->getVersion(), '1.1.6', '<')) {
            if ($installer->tableExists('magemobcart')) {
                $installer->getConnection()->dropTable($installer->getTable('magemobcart'));
            }
            if ($installer->tableExists('magemob_layout_components')) {
                if ($installer->getConnection()->tableColumnExists($installer->getTable('magemob_layout_components'), 'fcb_count') === true) {
                    $installer->getConnection()->dropColumn($installer->getTable('magemob_layout_components'), 'fcb_count');
                }
            }
            if (!$installer->tableExists('magemob_featuredcategoryblocks')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('magemob_featuredcategoryblocks')
                )
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    255,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'ID'
                )
                ->addColumn(
                    'fcb_id',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Featured Category Block ID'
                )
                ->addColumn(
                    'layout_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Layout ID'
                )->addForeignKey(
                    $installer->getFkName(
                        'magemob_featuredcategoryblocks',
                        'layout_id',
                        'magemob_layout',
                        'layout_id'
                    ),
                    'layout_id',
                    $installer->getTable('magemob_layout'), 
                    'layout_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                )
                ->addColumn(
                    'category_title',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => false],
                    'Category Title'
                )
                ->addColumn(
                    'image_type',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => false],
                    'Image Type'
                )
                ->addColumn(
                    'filename',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => true],
                    'Image File name'
                )
                ->addColumn(
                    'filepath',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => true],
                    'Image File path'
                )
                ->addColumn(
                    'image_url',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => true],
                    'Image URL'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                    'Enable / Disable'
                )
                ->addColumn(
                    'category_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Category Id'
                )
                ->addColumn(
                    'sort_order',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Categories Order'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    255,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    255,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                );
                $installer->getConnection()->createTable($table);
            }
            if (!$installer->tableExists('magemob_fcbdisplaytype')) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable('magemob_fcbdisplaytype')
                )
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    255,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'ID'
                )
                ->addColumn(
                    'fcb_id',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Featured Category Block ID'
                )->addIndex(
                    $installer->getIdxName(
                        $installer->getTable('magemob_fcbdisplaytype'),
                        ['fcb_id'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                    ),
                    ['fcb_id'],
                    ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
                )
                ->addColumn(
                    'layout_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Layout ID'
                )->addForeignKey(
                    $installer->getFkName(
                        'magemob_fcbdisplaytype',
                        'layout_id',
                        'magemob_layout',
                        'layout_id'
                    ),
                    'layout_id',
                    $installer->getTable('magemob_layout'), 
                    'layout_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
                )
                ->addColumn(
                    'display_type',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => false],
                    'Fcb Display Type'
                )
                ->addColumn(
                    'fcb_title',
                    Table::TYPE_TEXT,
                    255,
                    ['default' => null, 'nullable' => true],
                    'Fcb Title'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    255,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    255,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                );
                $installer->getConnection()->createTable($table);
            }
        }
        if (version_compare($context->getVersion(), '1.1.7', '<')) {
            if ($installer->tableExists('magemob_producthorizontalslider')) {
                if ($installer->getConnection()->tableColumnExists($installer->getTable('magemob_producthorizontalslider'), 'phs_display_category') !== true) {
                    $installer->getConnection()->addColumn(
                        $installer->getTable('magemob_producthorizontalslider'),
                        'phs_display_category',
                        [
                            'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            'length' => 10,
                            'nullable' => true,
                            'comment' => 'Display Category'
                        ]
                    );
                }
            }
        }

        $setup->endSetup();
    }
}
