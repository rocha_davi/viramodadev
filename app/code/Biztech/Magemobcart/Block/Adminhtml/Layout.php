<?php
namespace Biztech\Magemobcart\Block\Adminhtml;

class Layout extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_layout';
        $this->_blockGroup = 'Biztech_Magemobcart';
        $this->_headerText = __('Home Page Layout');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->cartHelper = $objectManager->get('Biztech\Magemobcart\Helper\Data');
        $this->messageManager = $objectManager->get('Magento\Framework\Message\ManagerInterface');
        if ($this->cartHelper->isEnable()) {
            $this->_addButtonLabel = __('Add Layout');
            parent::_construct();
        } else {
            $this->messageManager->addError("Please activate the extension");
            return false;
        }
    }
}
