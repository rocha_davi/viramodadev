<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Block\Adminhtml\Layout;

use Magento\Backend\Block\Widget\Form\Container;

class Edit extends Container
{

    /**
     * Get edit form container header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        return __('Create Layout');
    }

    /**
     * @return Void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Biztech_Magemobcart';
        $this->_controller = 'adminhtml_layout';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Layout'));
        $this->buttonList->update('delete', 'label', __('Delete Layout'));
        $this->buttonList->remove('reset');
        if (strpos($this->_storeManager->getStore()->getCurrentUrl(), 'magemobcart/layout/new') !== false) {
            $this->buttonList->remove('delete');
        }

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('block_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'hello_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'hello_content');
                }
            }
        ";
    }
}
