<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Block\Adminhtml\Layout;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\App\Request\Http;
use Biztech\Magemobcart\Model\Config\Category;
use Biztech\Magemobcart\Model\Product;
use Biztech\Magemobcart\Model\FcbdisplaytypeFactory;

class Featuredcategoryblockspopup extends \Magento\Framework\View\Element\Template
{

    protected $_categoryModel;
    protected $_product;
    protected $_fcbDisplayTypeModel;
    protected $request;

    public function __construct(
        Context $context,
        Category $categoryModel,
        Http $request,
        Product $product,
        FcbdisplaytypeFactory $fcbDisplayTypeModel,
        array $data = []
    ) {
        $this->request = $request;
        $this->_categoryModel = $categoryModel;
        $this->_product = $product;
        $this->_fcbDisplayTypeModel = $fcbDisplayTypeModel;
        parent::__construct($context, $data);
    }

    public function getCategoryArray(){
        return $this->_categoryModel->toOptionArray();
    }

    public function getProductArray(){
        return $this->_product->getOptionArray();
    }
    
    public function getLayoutId(){
        return $this->request->getParam('layout_id');
    }
    public function getComponentId(){
        return $this->request->getParam('id');
    }

    public function getDisplayType($componentId){
        try {
            $fcbId = str_replace("edit_", "", $componentId);
            $displayType = "";
            $fcbDisplayTypeCollection = $this->_fcbDisplayTypeModel->create()->getCollection()->addFieldToFilter("fcb_id", $fcbId);
            if ($fcbDisplayTypeCollection->count()) {
                $displayType = $this->_fcbDisplayTypeModel->create()->load($fcbDisplayTypeCollection->getLastItem()->getId())->getDisplayType();
            }
            return $displayType;
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    public function getFcbTitle($componentId)
    {
        try {
            $fcbId = str_replace("edit_", "", $componentId);
            $fcbTitle = "";
            $fcbDisplayTypeCollection = $this->_fcbDisplayTypeModel->create()->getCollection()->addFieldToFilter("fcb_id", $fcbId);
            if ($fcbDisplayTypeCollection->count()) {
                $fcbTitle = $this->_fcbDisplayTypeModel->create()->load($fcbDisplayTypeCollection->getLastItem()->getId())->getFcbTitle();
            }
            return $fcbTitle;
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }
}
