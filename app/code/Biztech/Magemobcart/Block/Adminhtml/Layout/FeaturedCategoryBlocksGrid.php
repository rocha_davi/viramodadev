<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Block\Adminhtml\Layout;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;
use Magento\Framework\App\Request\Http;
use Biztech\Magemobcart\Model\FeaturedcategoryblocksFactory;
use Biztech\Magemobcart\Model\FcbdisplaytypeFactory;
use Biztech\Magemobcart\Helper\Data as mageMobHelper;

class FeaturedCategoryBlocksGrid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_featuredcategoryblocksModel;
    protected $_fcbDisplayTypeModel;
    protected $_magemobcartHelper;
    protected $_messageManager;
    protected $_categoryFactory;
    protected $_request;
    protected $_helper;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        FeaturedcategoryblocksFactory $featuredcategoryblocksModel,
        FcbdisplaytypeFactory $fcbDisplayTypeModel,
        \Biztech\Magemobcart\Helper\Data $magemobcartHelper,
        \Magento\Framework\Message\ManagerInterface $messageinterface,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        mageMobHelper $helper,
        Http $request,
        array $data = []
    ) {
        $this->_featuredcategoryblocksModel = $featuredcategoryblocksModel;
        $this->_fcbDisplayTypeModel = $fcbDisplayTypeModel;
        $this->_magemobcartHelper = $magemobcartHelper;
        $this->_messageManager = $messageinterface;
        $this->_categoryFactory = $categoryFactory;
        $this->_request = $request;
        $this->_helper = $helper;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();

        $this->setId('featuredcategoryblocksystemGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
    * getCategoriesCollection gets the featured categories collection
    * @return _bannersliderCollection
    */
    public function getCategoriesCollection()
    {
        $fcb_id = str_replace("edit_", "",$this->_request->getParam('id'));
        return $this->_featuredcategoryblocksModel->create()
                    ->getCollection()
                    ->addFieldToFilter("fcb_id", $fcb_id)->setOrder('sort_order', "ASC");
    }

    public function getCategoryName($catId)
    {
        return $this->_categoryFactory->create()->load($catId)->getName();
    }

    public function getImageUrl($image)
    {
        return $this->_helper->getImageUrl($image);
    }

    public function getDisplayType()
    {
        try {
            $fcbId = str_replace("edit_", "",$this->_request->getParam('id'));
            $displayType = "";
            $fcbDisplayTypeCollection = $this->_fcbDisplayTypeModel->create()->getCollection()->addFieldToFilter("fcb_id", $fcbId);
            if ($fcbDisplayTypeCollection->count()) {
                $displayType = $this->_fcbDisplayTypeModel->create()->load($fcbDisplayTypeCollection->getLastItem()->getId())->getDisplayType();
            }
            return $displayType;
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }
}