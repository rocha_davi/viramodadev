<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Block\Adminhtml\Layout;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;
use Magento\Framework\App\Request\Http;
use Biztech\Magemobcart\Model\Bannerslider1Factory;
use Biztech\Magemobcart\Helper\Data as mageMobHelper;

class SliderGrid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $bannersliderCollection;
    protected $_magemobcartHelper;
    protected $_messageManager;
    protected $_request;
    protected $_helper;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        Bannerslider1Factory $bannersliderCollection,
        \Biztech\Magemobcart\Helper\Data $magemobcartHelper,
        \Magento\Framework\Message\ManagerInterface $messageinterface,
        mageMobHelper $helper,
        Http $request,
        array $data = []
    ) {
        $this->_bannersliderCollection = $bannersliderCollection;
        $this->_magemobcartHelper = $magemobcartHelper;
        $this->_messageManager = $messageinterface;
        $this->_request = $request;
        $this->_helper = $helper;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();

        $this->setId('bannerslidersystemGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
    * getSliderCollection gets the bannerslider collection
    * @return _bannersliderCollection
    */
    public function getSliderCollection()
    {
        $banner_id = str_replace("edit_", "",$this->_request->getParam('id'));
        return $this->_bannersliderCollection->create()
                    ->getCollection()
                    ->addFieldToFilter("bannerslider_id", $banner_id)->setOrder('sort_order', "ASC");
    }  

    public function getImageUrl($image)
    {
        return $this->_helper->getImageUrl($image);
    }
}