<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Block\Adminhtml\Layout;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;

class Productgridpopup extends Generic
{
    protected $_layoutModel;
    protected $_productGridModel;
    protected $_productGridTypeOptions;
    protected $_categoryProductOptions;
    protected $_productOptions;
    protected $_imgContentModeOptions;
    protected $_categoryModel;

    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        \Biztech\Magemobcart\Model\Layout $layoutModel,
        \Biztech\Magemobcart\Model\ProductgridFactory $productGridModel,
        \Biztech\Magemobcart\Model\Productgridtypeoptions $productGridTypeOptions,
        \Biztech\Magemobcart\Model\Categoryproducts $categoryProductOptions,
        \Biztech\Magemobcart\Model\Products $productOptions,
        \Biztech\Magemobcart\Model\Imagecontentoptions $imgContentModeOptions,
        \Biztech\Magemobcart\Model\Config\Category $categoryModel,
        array $data = []
    ) {
        $this->_registry = $registry;
        $this->_layoutModel = $layoutModel;
        $this->_productGridModel = $productGridModel;
        $this->_productGridTypeOptions = $productGridTypeOptions;
        $this->_categoryProductOptions = $categoryProductOptions;
        $this->_productOptions = $productOptions;
        $this->_imgContentModeOptions = $imgContentModeOptions;
        $this->_categoryModel = $categoryModel;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    public function canShowTab()
    {
        return false;
    }

    public function isHidden()
    {
        return false;
    }

    protected function _prepareForm()
    {
        $layout_id = 0;
        $productgrid_id = null;
        $params = $this->getRequest()->getParams();
        if (array_key_exists('layout_id', $params) && array_key_exists('id', $params)) {
            $layout_id = $params['layout_id'];
            $productgrid_id = $params['id'];
        }
        
        $productgrid_model = $this->_productGridModel->create()->getCollection()->addFieldToFilter('layout_id', ['eq' => $layout_id]);
        
        $isElementDisabled = false;

        $product_grid_id = str_replace("edit_", "", $productgrid_id);

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

            $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('')]);
        if ($data = $productgrid_model->getData()) {
            foreach ($data as $product_grid) {
                if ($product_grid['productgrid_id'] == $productgrid_id) {
                    $model = $this->_productGridModel->load($productgrid_id);
                    break;
                }
            }
        }

            $fieldset->addField('productgrid_id', 'hidden', ['name' => 'productgrid_id', 'value' =>  $product_grid_id]);
            $fieldset->addField('layout_id', 'hidden', ['name' => 'layout_id', 'value' => $layout_id]);
            
            $fieldset->addField(
                'component_title',
                'text',
                [
                    'name' => 'component_title',
                    'label' => __('Component Title'),
                    'title' => __('Component Title'),
                    'required' => true
                ]
            );

            $type = $fieldset->addField(
                'product_type',
                'select',
                [
                    'name' => 'product_type',
                    'label' => __('Select Product Type'),
                    'title' => __('Select Product Type'),
                    'options' => $this->_productGridTypeOptions->toOptionArray(),
                    'required' => true
                ]
            );

            $category = $fieldset->addField(
                "category_id",
                'select',
                [
                    'name' => "category_id",
                    'label' => __('Select Category'),
                    'title' => __('Select Category'),
                    'options' => $this->_categoryModel->toOptionArray(),
                    'required' => true
                ]
            );

            $product_list = $fieldset->addField(
                'product_list',
                'multiselect',
                [
                    'name'     => 'product_list[]',
                    'label'    => __('Select Products'),
                    'title'    => __('Select Products'),
                    'required' => true,
                    'values'   => $this->_productOptions->getOptionArray()
                ]
            );

        if (!is_null($product_grid_id)) {
            $modelData = $this->_productGridModel->create()->getCollection()->addFieldToFilter("productgrid_id", $product_grid_id);
            $model = $this->_productGridModel->create();
            if ($modelData->count()) {
                $model->load($modelData->getLastItem()->getId());

                $category_products = $fieldset->addField(
                    'category_products',
                    'multiselect',
                    [
                        'name'     => 'category_products[]',
                        'label'    => __('Select Products from Category'),
                        'title'    => __('Select Products from Category'),
                        'required' => true,
                        'values'   => $this->_categoryProductOptions->toOptionArray($model->getCategoryId())
                    ]
                );
                if($model->getCategoryProducts()){
                    $model->setCategoryProducts(implode(",", json_decode($model->getCategoryProducts())));
                }
                if ($model->getProductList()) {
                    $model->setProductList(implode(",", json_decode($model->getProductList())));
                }
                $form->setValues($model->getData());
                $this->setForm($form);
            } else {
                $category_products = $fieldset->addField(
                    'category_products',
                    'multiselect',
                    [
                        'name'     => 'category_products[]',
                        'label'    => __('Select Products from Category'),
                        'title'    => __('Select Products from Category'),
                        'required' => true,
                        'values'   => $this->_categoryProductOptions->toOptionArray()
                    ]
                );
                $this->setForm($form);
            }
        }
        

        return parent::_prepareForm();
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
