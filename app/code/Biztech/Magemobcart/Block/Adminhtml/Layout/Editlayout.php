<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Block\Adminhtml\Layout;

use Magento\Backend\Block\Widget\Form\Container;
use Magento\Backend\Block\Widget\Context;

class Editlayout extends Container
{
    protected $_layoutComponentModelFactory;
    protected $_fcbDisplayTypeFactory;
    protected $_categoryFactory;

    public function __construct(
        Context $context,
        \Biztech\Magemobcart\Model\Layout $layoutModel,
        \Biztech\Magemobcart\Model\LayoutcomponentFactory $layoutComponentModelFactory,
        \Biztech\Magemobcart\Model\FcbdisplaytypeFactory $fcbDisplayTypeFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        array $data = []
    ) {
        $this->_layoutModel = $layoutModel;
        $this->_layoutComponentModelFactory = $layoutComponentModelFactory;
        $this->_fcbDisplayTypeFactory = $fcbDisplayTypeFactory;
        $this->_categoryFactory = $categoryFactory;
        parent::__construct($context);
    }

    /**
     * Get edit form container header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        return __('Edit Layout');
    }

    /**
     * @return Void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Biztech_Magemobcart';
        $this->_controller = 'adminhtml_layout';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Layout'));
        $this->buttonList->update('delete', 'label', __('Delete Layout'));

        if (strpos($this->_storeManager->getStore()->getCurrentUrl(), 'magemobcart/layout/editlayout') !== false) {
            $this->buttonList->remove('save');
            $this->buttonList->remove('reset');
        }

        $this->_formScripts[] = "
        function toggleEditor() {
            if (tinyMCE.getInstanceById('block_content') == null) {
                tinyMCE.execCommand('mceAddControl', false, 'hello_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'hello_content');
                }
            }
            ";
    }

    public function getLayoutModel()
    {
        return $this->_layoutModel;
    }

    public function getLayoutComponentModel()
    {
        return $this->_layoutComponentModelFactory->create();
    }

    public function getFeaturedCategoryListData($fcbId)
    {
        $featured_categories_html = '<li class="list-group-item" id="' . $fcbId . '">';
        $featured_categories_html .= '<span class="slideTitle">Featured Categories</span>';
        $featured_categories_html .= '<span class="edit_component featured-category-block-modal-popup" id="edit_' . $fcbId . '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
        $featured_categories_html .= '<span class="trash" id="delete_' . $fcbId . '" onclick="Layout.prototype.trashFeaturedcategoryblocksComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
        $featured_categories_html .= '</li>';
        return $featured_categories_html;
    }

    public function getFeaturedCategoryMobileListData($fcbId)
    {
        $collection = $this->_fcbDisplayTypeFactory->create()->getCollection()->addFieldToFilter("fcb_id", $fcbId);
        if ($collection->count()) {
            $fcbDisplayType = $this->_fcbDisplayTypeFactory->create()->load($collection->getLastItem()->getId())->getDisplayType();
        } else {
            $fcbDisplayType = 'circle';
        }
        $featured_categories_html_mobile = '<li class="slide" id="' . $fcbId . '">';
        $featured_categories_html_mobile .= '<span class="slideTitle">Featured Categories</span>';
        $featured_categories_html_mobile .= '<span class="edit_component edit_mobile_component featured-category-block-modal-popup" id="edit_' . $fcbId . '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
        $featured_categories_html_mobile .= '<span class="trash" id="delete_' . $fcbId . '" onclick="Layout.prototype.trashFeaturedcategoryblocksComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
        if ($fcbDisplayType == 'circle') {
            $featured_categories_html_mobile .= '<div class="banner_preview layout_div"><img id="' . $fcbId . '_img" class="banner_preview_image" src="' . $this->getViewFileUrl('Biztech_Magemobcart::images/layout_components/featured_categories_circle.png') . '" /></div>';
        } else {
            $featured_categories_html_mobile .= '<div class="banner_preview layout_div"><img id="' . $fcbId . '_img" class="banner_preview_image" src="' . $this->getViewFileUrl('Biztech_Magemobcart::images/layout_components/featured_categories_rect.png') . '" /></div>';
        }
        return $featured_categories_html_mobile;
    }

    public function getProductGridListData($productGridId)
    {
        $product_grid_html = '<li class="list-group-item" id="' . $productGridId . '">';
        $product_grid_html .= '<span class="slideTitle">Product Grid</span>';
        $product_grid_html .= '<span class="edit_component product-grid-modal-popup" id="edit_' . $productGridId . '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
        $product_grid_html .= '<span class="trash" id="delete_' . $productGridId . '" onclick="Layout.prototype.trashProductgridComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
        $product_grid_html .= '</li>';
        return $product_grid_html;
    }

    public function getProductGridMobileListData($productGridId)
    {
        $product_grid_html_mobile = '<li class="slide" id="' . $productGridId . '">';
        $product_grid_html_mobile .= '<span class="slideTitle">Product Grid</span>';
        $product_grid_html_mobile .= '<span class="edit_component edit_mobile_component product-grid-modal-popup" id="edit_' . $productGridId . '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
        $product_grid_html_mobile .= '<span class="trash" id="delete_' . $productGridId . '" onclick="Layout.prototype.trashProductgridComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
        $product_grid_html_mobile .= '<div class="banner_preview layout_div"><img class="banner_preview_image" src="' . $this->getViewFileUrl('Biztech_Magemobcart::images/layout_components/product_grid.png') . '" /></div>';
        $product_grid_html_mobile .= '</li>';
        return $product_grid_html_mobile;
    }

    public function getPhsListData($phsId)
    {
        $product_sldier_html = '<li class="list-group-item" id="' . $phsId . '">';
        $product_sldier_html .= '<span class="slideTitle">Products Horizontal Slider</span>';
        $product_sldier_html .= '<span class="edit_component product-slider-modal-popup" id="edit_' . $phsId . '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
        $product_sldier_html .= '<span class="trash" id="delete_' . $phsId . '" onclick="Layout.prototype.trashProducthorizontalslidingComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
        $product_sldier_html .= '</li>';
        return $product_sldier_html;
    }

    public function getPhsMobileListData($phsId)
    {
        $product_sldier_html_mobile = '<li class="slide" id="' . $phsId . '">';
        $product_sldier_html_mobile .= '<span class="slideTitle">Products Horizontal Slider</span>';
        $product_sldier_html_mobile .= '<span class="edit_component edit_mobile_component product-slider-modal-popup" id="edit_' . $phsId . '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
        $product_sldier_html_mobile .= '<span class="trash" id="delete_' . $phsId . '" onclick="Layout.prototype.trashProducthorizontalslidingComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
        $product_sldier_html_mobile .= '<div class="banner_preview layout_div"><img class="banner_preview_image" src="' . $this->getViewFileUrl('Biztech_Magemobcart::images/layout_components/products_horizontal_slider.png') . '" /></div>';
        $product_sldier_html_mobile .= '</li>';
        return $product_sldier_html_mobile;
    }

    public function getBannerSliderListData($bannerSliderId)
    {
        $banner_slider_html = '<li class="list-group-item" id="' . $bannerSliderId . '">';
        $banner_slider_html .= '<span class="slideTitle">Banner/Offer Slider</span>';
        $banner_slider_html .= '<span class="edit_component banner-slider-modal-popup" id="edit_' . $bannerSliderId . '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
        $banner_slider_html .= '<span class="trash" id="delete_' . $bannerSliderId . '" onclick="Layout.prototype.trashBannersliderComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
        $banner_slider_html .= '</li>';
        return $banner_slider_html;
    }

    public function getBannerSliderMobileListData($bannerSliderId)
    {
        $banner_slider_html_mobile = '<li class="slide" id="' . $bannerSliderId . '">';
        $banner_slider_html_mobile .= '<span class="slideTitle">Banner/Offer Slider</span>';
        $banner_slider_html_mobile .= '<span class="edit_component edit_mobile_component banner-slider-modal-popup" id="edit_' . $bannerSliderId . '" title="Edit"><i class="glyphicon glyphicon-edit" style="padding-right:5px"></i></span>';
        $banner_slider_html_mobile .= '<span class="trash" id="delete_' . $bannerSliderId . '" onclick="Layout.prototype.trashBannersliderComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
        $banner_slider_html_mobile .= '<div class="banner_preview layout_div"><img class="banner_preview_image" src="' . $this->getViewFileUrl('Biztech_Magemobcart::images/layout_components/banners_offers.png') . '" /></div>';
        $banner_slider_html_mobile .= '</li>';
        return $banner_slider_html_mobile;
    }

    public function getRecentProductListData($recentProductId)
    {
        $recent_product_html = '<li class="list-group-item" id="' . $recentProductId . '">';
        $recent_product_html .= '<span class="slideTitle">Recently Accessed Products</span>';
        $recent_product_html .= '<span class="trash" id="delete_' . $recentProductId . '" onclick="Layout.prototype.trashProductrecentlyviewedComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
        $recent_product_html .= '</li>';
        return $recent_product_html;
    }

    public function getRecentProductMobileListData($recentProductId)
    {
        $recent_product_html_mobile = '<li class="slide" id="' . $recentProductId . '">';
        $recent_product_html_mobile .= '<span class="slideTitle">Recently Accessed Products</span>';
        $recent_product_html_mobile .= '<span class="trash" id="delete_' . $recentProductId . '" onclick="Layout.prototype.trashProductrecentlyviewedComponentFunction(this)" title="Delete"><i class="glyphicon glyphicon-remove-circle" style="padding-right:5px"></i></span>';
        $recent_product_html_mobile .= '<div class="banner_preview layout_div"><img class="banner_preview_image" src="' . $this->getViewFileUrl('Biztech_Magemobcart::images/layout_components/recently_accessed_products.png') . '" /></div>';
        $recent_product_html_mobile .= '</li>';
        return $recent_product_html_mobile;
    }

    /**
     * Get product ids list based on category id
     *
     * @return array
     */
    public function getCategoryProductIds($catId = '')
    {
        if ($catId != '' && $catId != 'undefined') {
            return $this->_categoryFactory->create()->load($catId)->getProductCollection()->getAllIds();
        } else {
            return [];
        }
    }
}