<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Block\Adminhtml\Layout\Edit\Tab;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;
use Biztech\Magemobcart\Model\Config\Source\GetStoreview;

class Layout extends Generic implements TabInterface
{
    protected $_systemStore;
    protected $status;
    protected $storeManagerInterface;
    protected $_layoutModel;

    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        GetStoreview $systemStore,
        \Biztech\Magemobcart\Model\Status $status,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Biztech\Magemobcart\Model\Layout $layoutModel,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_status = $status;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_registry = $registry;
        $this->_layoutModel = $layoutModel;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    public function getTabLabel()
    {
        return __('Layout Information');
    }

    public function getTabTitle()
    {
        return __('Layout Information');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    protected function _prepareForm()
    {
        $registry = $this->_registry;
        $model = $this->_coreRegistry->registry('magemobcart_layout_data');
        $isElementDisabled = false;
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        if ($model->getId()) {
            
        } else {

            $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Layout Information')]);

            if ($model->getId()) {
                $fieldset->addField('layout_id', 'hidden', ['name' => 'layout_id']);
            }
            
            $fieldset->addField(
                'title',
                'text',
                [
                    'name' => 'title',
                    'label' => __('Layout Title'),
                    'title' => __('Layout Title'),
                    'required' => true
                ]
            );
            $fieldset->addField(
                'store_id',
                'select',
                [
                    'name'     => 'store_id',
                    'label'    => __('Store Views'),
                    'title'    => __('Store Views'),
                    'required' => true,
                    'values'   => $this->_systemStore->toOptionArray()
                ]
            );
            $fieldset->addField(
                'is_default',
                'checkbox',
                [
                    'label' => __('Apply This Layout'),
                    'title' => __('Apply This Layout'),
                    'name' => 'is_default',
                    'data-form-part' => $this->getData('target_form'),
                    'onchange' => 'this.value = this.checked ? 1 : 0;'
                ]
            );
        }
        
        if (!$model->getId()) {
            $model->setData('status', $isElementDisabled ? '2' : '1');
        }

        if ($model) {
            $form->setValues($model->getData());
            $this->setForm($form);
        }

        return parent::_prepareForm();
    }

    /**
    * Check permission for passed action
    *
    * @param string $resourceId
    * @return bool
    */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
