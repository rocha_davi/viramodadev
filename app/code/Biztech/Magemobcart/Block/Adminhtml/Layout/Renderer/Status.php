<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Block\Adminhtml\Layout\Renderer;

use Magento\Backend\Block\Context;
use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

class Status extends AbstractRenderer
{
    protected $_storeManager;

    /**
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * @param \Magento\Framework\DataObject $row
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function render(DataObject $row)
    {   
        $html = '<div class="custom-row">';

        $layoutEditUrl = $this->getUrl('magemobcart/layout/editlayout', ['id' => $row->getId()]);
        $layoutDeleteUrl = $this->getUrl('magemobcart/layout/delete', ['id' => $row->getId()]);
        $html .= '<a href="'.$layoutEditUrl.'" class="btn btn-success custom-button">Edit</a>';
        $html .= '<a href="'.$layoutDeleteUrl.'" class="btn btn-danger custom-button" onclick="return confirm(\'Are you sure want to remove this layout?\')">Delete</a>';

        if($row->getIsDefault()){
            $html .= '<span class="grid-severity-notice custom-message"><span>Active Layout</span></span>';
        } else {
            $applyDefaultUrl = $this->getUrl('magemobcart/layout/applydefault', ['id' => $row->getId()]);
            $html .= '<a href="'.$applyDefaultUrl.'" class="btn btn-warning custom-button">Apply as default</a>';
        }
        $html .= '<div>';
        return $html;
    }
}
