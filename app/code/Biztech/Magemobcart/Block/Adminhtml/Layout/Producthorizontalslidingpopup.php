<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Block\Adminhtml\Layout;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;

class Producthorizontalslidingpopup extends Generic
{
    protected $_layoutModel;
    protected $_productHorizontalSlidingModel;
    protected $_productTypeOptions;
    protected $_newProductType;
    protected $_categoryProductOptions;
    protected $_imgContentModeOptions;
    protected $_categoryModel;
    protected $_categoryProductsModel;

    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        \Biztech\Magemobcart\Model\Layout $layoutModel,
        \Biztech\Magemobcart\Model\Producthorizontalsliding $productHorizontalSlidingModel,
        \Biztech\Magemobcart\Model\Producttypeoptions $productTypeOptions,
        \Biztech\Magemobcart\Model\Newproductstype $newProductType,
        \Biztech\Magemobcart\Model\Products $categoryProductOptions,
        \Biztech\Magemobcart\Model\Imagecontentoptions $imgContentModeOptions,
        \Biztech\Magemobcart\Model\Config\Category $categoryModel,
        \Biztech\Magemobcart\Model\Categoryproducts $categoryProductsModel,
        array $data = []
    ) {
        $this->_registry = $registry;
        $this->_layoutModel = $layoutModel;
        $this->_productHorizontalSlidingModel = $productHorizontalSlidingModel;
        $this->_productTypeOptions = $productTypeOptions;
        $this->_newProductType = $newProductType;
        $this->_categoryProductOptions = $categoryProductOptions;
        $this->_imgContentModeOptions = $imgContentModeOptions;
        $this->_categoryModel = $categoryModel;
        $this->_categoryProductsModel = $categoryProductsModel;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    public function canShowTab()
    {
        return false;
    }

    public function isHidden()
    {
        return false;
    }

    protected function _prepareForm()
    {
        $registry = $this->_registry;
        $model = $this->_coreRegistry->registry('magemobcart_layout_data');
        $model = $this->_productHorizontalSlidingModel;

        $layout_id = 0;
        $phs_id = "";
        $params = $this->getRequest()->getParams();
        if (array_key_exists('layout_id', $params) && array_key_exists('id', $params)) {
            $layout_id = $params['layout_id'];
            $phs_id = str_replace('edit_', '', $params['id']);
        }
        
        $phs_model = $this->_productHorizontalSlidingModel->getCollection()->addFieldToFilter('layout_id', ['eq' => $layout_id])->addFieldToFilter('phs_id', ['eq' => $phs_id]);
        
        $isElementDisabled = false;
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

            $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('')]);

            $fieldset->addField('phs_id', 'hidden', ['name' => 'phs_id', 'value' => $phs_id]);
            $fieldset->addField('layout_id', 'hidden', ['name' => 'layout_id', 'value' => $layout_id]);
            
            $fieldset->addField(
                'component_title',
                'text',
                [
                    'name' => 'component_title',
                    'label' => __('Component Title'),
                    'title' => __('Component Title'),
                    'required' => true
                ]
            );

            $type = $fieldset->addField(
                'phs_product_type',
                'select',
                [
                    'name' => 'phs_product_type',
                    'label' => __('Select Product Type'),
                    'title' => __('Select Product Type'),
                    'options' => $this->_productTypeOptions->toOptionArray(),
                    'required' => true
                ]
            );

            $type = $fieldset->addField(
                'new_arrival_product_type',
                'select',
                [
                    'name' => 'new_arrival_product_type',
                    'label' => __('Display New Products Based On'),
                    'title' => __('Display New Products Based On'),
                    'options' => $this->_newProductType->toOptionArray(),
                    'required' => true
                ]
            );

            $display_category = $fieldset->addField(
                "phs_display_category",
                'select',
                [
                    'name' => "phs_display_category",
                    'label' => __('Select Display Category'),
                    'title' => __('Select Display Category'),
                    'options' => $this->_categoryModel->toOptionArray(),
                    'required' => true,
                    'disabled' => $isElementDisabled
                ]
            );

            $category = $fieldset->addField(
                "phs_category_id",
                'select',
                [
                    'name' => "phs_category_id",
                    'label' => __('Select Category'),
                    'title' => __('Select Category'),
                    'options' => $this->_categoryModel->toOptionArray(),
                    'required' => true,
                    'disabled' => $isElementDisabled
                ]
            );

            $product_list = $fieldset->addField(
                'phs_product_list',
                'multiselect',
                [
                    'name'     => 'phs_product_list[]',
                    'label'    => __('Select Products'),
                    'title'    => __('Select Products'),
                    'required' => true,
                    'values'   => $this->_categoryProductOptions->getOptionArray(),
                    'disabled' => $isElementDisabled
                ]
            );

        if ($phs_model->count()) {
            $model->load($phs_model->getLastItem()->getId());

            $category_products = $fieldset->addField(
                'phs_category_products',
                'multiselect',
                [
                    'name'     => 'phs_category_products[]',
                    'label'    => __('Select Products from Category'),
                    'title'    => __('Select Products from Category'),
                    'required' => true,
                    'values'   => $this->_categoryProductsModel->toOptionArray($model->getPhsCategoryId()),
                    'disabled' => $isElementDisabled
                ]
            );
            if($model->getPhsCategoryProducts()){
                $model->setPhsCategoryProducts(implode(",", json_decode($model->getPhsCategoryProducts())));
            }
            if ($model->getPhsProductList()) {
                $model->setPhsProductList(implode(",", json_decode($model->getPhsProductList())));
            }
            $form->setValues($model->getData());
        } else {
            $category_products = $fieldset->addField(
                'phs_category_products',
                'multiselect',
                [
                    'name'     => 'phs_category_products[]',
                    'label'    => __('Select Products from Category'),
                    'title'    => __('Select Products from Category'),
                    'required' => true,
                    'values'   => $this->_categoryProductsModel->toOptionArray(),
                    'disabled' => $isElementDisabled
                ]
            );
        }

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
    * Check permission for passed action
    *
    * @param string $resourceId
    * @return bool
    */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
