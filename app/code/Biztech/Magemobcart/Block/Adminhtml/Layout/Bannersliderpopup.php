<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Block\Adminhtml\Layout;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\App\Request\Http;
use Biztech\Magemobcart\Model\Config\Category;
use Biztech\Magemobcart\Model\Product;

class Bannersliderpopup extends \Magento\Framework\View\Element\Template
{

    protected $_categoryModel;
    protected $_product;
    protected $request;

    public function __construct(
        Context $context,
        Category $categoryModel,
        Http $request,
        Product $product,
        array $data = []
    ) {
        $this->request = $request;
        $this->_categoryModel = $categoryModel;
        $this->_product = $product;
        parent::__construct($context, $data);
    }

    public function getCategoryArray(){
        return $this->_categoryModel->toOptionArray();
    }

    public function getProductArray(){
        return $this->_product->getOptionArray();
    }
    
    public function getLayoutId(){
        return $this->request->getParam('layout_id');
    }
    public function getComponentId(){
        return $this->request->getParam('id');
    }
    
}
