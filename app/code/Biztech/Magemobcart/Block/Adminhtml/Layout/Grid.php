<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Block\Adminhtml\Layout;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_layoutCollection;
    protected $_magemobcartHelper;
    protected $_messageManager;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Biztech\Magemobcart\Model\ResourceModel\Layout\Collection $layoutCollection,
        \Biztech\Magemobcart\Helper\Data $magemobcartHelper,
        \Magento\Framework\Message\ManagerInterface $messageinterface,
        array $data = []
    ) {
        $this->_layoutCollection = $layoutCollection;
        $this->_magemobcartHelper = $magemobcartHelper;
        $this->_messageManager = $messageinterface;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();

        $this->setId('layoutGrid');
        $this->setDefaultSort('layout_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * Prepare collection
     * @return $this
     */
    protected function _prepareCollection()
    {
        try {
            if (!empty($this->_magemobcartHelper->getAllWebsites())) {
                $collection = $this->_layoutCollection;
                $this->setCollection($collection);
                parent::_prepareCollection();
                return $this;
            } else {
                $this->_messageManager->addError(__('Extension - Magemob Appbuilder is not enabled. Please enable it from Store > Configuration > AppJetty > Magemob App Builder'));
                return $this;
            }
        } catch (\Exception $e) {
            $this->_messageManager->addError(__($e->getMessage()));
            return $this;
        }
    }

    /**
     * Prepare columns
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'layout_id',
            [
            'header' => __('Layout ID'),
            'index' => 'layout_id',
            'class' => 'entity_id',
            ]
        );
        $this->addColumn(
            'title',
            [
            'header' => __('Layout Name'),
            'index' => 'title',
            'class' => 'entity_id'
                ]
        );
        $this->addColumn(
            'store_id',
            [
            'header' => __('Store Name'),
            'class' => 'entity_id',
            'index'     => 'store_id',
            'type'      => 'options',
            'renderer' => 'Biztech\Magemobcart\Block\Adminhtml\Layout\Renderer\Stores',
            'filter' => false,
            'sortable' => false
            ]
        );
        
        $this->addColumn(
            'action',
            [
            'header' => __('Action'),
            'type' => 'action',
            'getter' => 'getId',
            'actions' => [
                [
                    'caption' => __('Apply as default'),
                    'url' => ['base' => '*/*/applydefault'],
                    'field' => 'id',
                    'confirm' => __('Are you sure want to apply this layout as default?')
                ]
            ],
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'renderer' => 'Biztech\Magemobcart\Block\Adminhtml\Layout\Renderer\Status',
            'header_css_class' => 'col-action',
            'column_css_class' => 'col-action',
            'align' => 'center'
            ]
        );

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('layout_id');
        $this->getMassactionBlock()->setFormFieldName('layout');

        /* on select of this massaction it will set the values from js */
        $this->getMassactionBlock()->addItem('remove', [
            'label' => __('Delete'),
            'url' => $this->getUrl('*/*/massdelete', ['_current' => true]),
            'confirm' => __('Are you sure want to remove selected slide/s?')
        ]);
        return $this;
    }
    public function getRowUrl($row)
    {
        return false;
    }
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', ['_current' => true]);
    }
}
