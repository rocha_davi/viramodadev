<?php
/**
 *
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Magemobcart\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Data extends AbstractHelper
{
    const XML_NOTIFCATION_KEY = 'magemobcart/pushnotification/notification_key';
    // const SECRET = "dfbDSkfdskf17hkfbgqwrsaFBfkdbf6bj";
    const SECRET = "YTgyMzZjZjgzODU4ZjllMzMyZWE2NDlkMjQ3YmJhN2Y=";
    const KEY = 'secretkey';
    protected $jsonFactory;
    protected $moduleManager;
    protected $storeManager;
    protected $scopeConfig;
    protected $encryptor;
    protected $customerSession;
    protected $customerModel;
    protected $_customerFactory;
    protected $_customerRepository;
    protected $wishlistModel;
    protected $productModel;
    protected $reviewSummaryModel;
    protected $catalogRule;
    protected $pricingHelper;
    protected $pricingInterface;
    protected $checkoutSession;
    protected $deviceData;
    protected $stockItemRepository;
    protected $stockApiRepository;
    protected $dirCurrencyFactory;
    protected $mathRandom;
    protected $imageHelper;
    protected $messageManager;
    protected $categoryRepository;
    protected $categoryFactory;
    protected $productFactory;
    protected $productCustomOptions;
    protected $onepageCheckout;
    protected $optionFactory;
    protected $attributeOptionCollection;
    protected $eavAttributeModel;
    protected $objectManager;
    protected $cartTotalRepository;
    protected $formKeyData;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Customer $customerModel,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Wishlist\Model\Wishlist $wishlistModel,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Catalog\Model\productFactory $productFactory,
        \Magento\Review\Model\Review\Summary $reviewSummaryModel,
        \Magento\CatalogRule\Model\Rule $catalogRule,
        \Magento\Framework\Pricing\Helper\Data $pricingHelper,
        \Magento\Framework\Pricing\PriceCurrencyInterface $pricingInterface,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Biztech\Magemobcart\Model\Devicedata $deviceData,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockApiRepository,
        \Magento\Directory\Model\CurrencyFactory $dirCurrencyFactory,
        \Magento\Framework\Math\Random $mathRandom,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\Product\Option $productCustomOptions,
        \Magento\Checkout\Model\Type\Onepage $onepageCheckout,
        \Magento\Framework\Data\Form\FormKey $formKeyData,
        \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $optionFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attributeOptionCollection,
        \Magento\Catalog\Model\ResourceModel\Eav\Attribute $eavAttributeModel,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Quote\Api\CartTotalRepositoryInterface $cartTotalRepository
    ) {
        $this->_jsonFactory = $jsonFactory;
        $this->_moduleManager = $moduleManager;
        $this->_storeManager = $storeManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_encryptor = $encryptor;
        $this->_customerSession = $customerSession;
        $this->_customerModel = $customerModel;
        $this->_customerFactory = $customerFactory;
        $this->_customerRepository = $customerRepository;
        $this->_wishlistModel = $wishlistModel;
        $this->_productModel = $productModel;
        $this->_reviewSummaryModel = $reviewSummaryModel;
        $this->_catalogRule = $catalogRule;
        $this->_pricingHelper = $pricingHelper;
        $this->_pricingInterface = $pricingInterface;
        $this->_checkoutSession = $checkoutSession;
        $this->_deviceData = $deviceData;
        $this->_stockItemRepository = $stockItemRepository;
        $this->_stockApiRepository = $stockApiRepository;
        $this->_dirCurrencyFactory = $dirCurrencyFactory;
        $this->_mathRandom = $mathRandom;
        $this->_imageHelper = $imageHelper;
        $this->_messageManager = $messageManager;
        $this->_categoryRepository = $categoryRepository;
        $this->_categoryFactory = $categoryFactory;
        $this->_productFactory = $productFactory;
        $this->_productCustomOptions = $productCustomOptions;
        $this->_onepageCheckout = $onepageCheckout;
        $this->_optionFactory = $optionFactory;
        $this->_attributeOptionCollection = $attributeOptionCollection;
        $this->_eavAttributeModel = $eavAttributeModel;
        $this->_objectManager = $objectManager;
        $this->_cartTotalRepository = $cartTotalRepository;
        $this->formKeyData = $formKeyData;
        parent::__construct($context);
    }

    /**
     * Returns if module exists or not
     *
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function isModuleEnabled($module)
    {
      return $this->_moduleManager->isEnabled($module);
    }

    public function getCustomerByEmail($email, $websiteId)
    {
        $customer = $this->_customerModel;
        $customer->setWebsiteId($websiteId);
        $customer->loadByEmail($email);
        return $customer;
    }

    /**
     * This function is used for get customer by apple id custom attribute.
     * @return customer
     */
    public function getCustomerByAppleId($appleId, $websiteId)
    {
        try {
            $customerData = $this->_customerFactory->create()->getCollection()
                        ->addAttributeToSelect("*")
                        ->addAttributeToFilter("apple_id", ["eq" => $appleId])
                        ->load()->getData();
            if (count($customerData) > 0) {
                $email = $customerData[0]['email'];
                $customer = $this->_customerModel;
                $customer->setWebsiteId($websiteId);
                $customer->loadByEmail($email);
                return $customer;
            }
        } catch (\Exception $e) {
        }
    }

    /**
     * This function is used for update customer email for apple login.
     * This email is valid only for apple login.
     */
    public function replaceEmail($email, $id, $websiteId)
    {
        try {
            $customer = $this->_customerRepository->getById($id);
            if ($customer->getId()) {
                $customer->setWebsiteId($websiteId);
                $customer->setEmail($email);
            }
            $this->_customerRepository->save($customer);
        } catch (\Exception $e) {
        }
    }

    public function createCustomerMultiWebsite($data, $appleId, $websiteId, $storeId)
    {
        $customer = $this->_customerModel->setId(null);
        $customer->setFirstname($data['firstname'])
                ->setLastname($data['lastname'])
                ->setEmail($data['email'])
                ->setWebsiteId($websiteId)
                ->setStoreId($storeId);
        
        // added for sign in with apple
        if (!empty($appleId)) {
            $customer->setData('apple_id', $appleId);
        }
        
        $customer->save();

        $length = 8;
        $chars = \Magento\Framework\Math\Random::CHARS_LOWERS
            . \Magento\Framework\Math\Random::CHARS_UPPERS
            . \Magento\Framework\Math\Random::CHARS_DIGITS;

        $newPassword = $this->_mathRandom->getRandomString($length, $chars);
        // $newPassword = $customer->generatePassword();
        $customer->setPassword($newPassword);
        try {
            $customer->save();
        } catch (\Exception $e) {
        }
        return $customer;
    }
    public function orderFormattedPrice($price, $currencyCode)
    {
        $orderCurrency = $this->_dirCurrencyFactory->create()->load($currencyCode);
        $orderCurrencySymbol = $orderCurrency->getCurrencySymbol();
        $formattedorderprice = $orderCurrency->formatTxt($price, array());
        return $formattedorderprice;
    }
    public function isEnable($defaultStoreId = '')
    {
        $websiteId = ($defaultStoreId == '') ? $this->_storeManager->getWebsite()->getId() : $this->_storeManager->getStore($defaultStoreId)->getWebsiteId();
        $isenabled = $this->getScopeConfigValue('magemobcart/magemobcart_general/enabled');
        if ($isenabled) {
            if ($websiteId) {
                $stores = $this->getAllWebsites();
                $websites = [];
                foreach ($stores as $storeId) {
                    $websites[] = $this->_storeManager->getStore($storeId)->getWebsiteId();
                }
                $websites = array_unique($websites);
                $key = $this->getScopeConfigValue('magemobcart/activation/key');
                if ($key == null || $key == '') {
                    return false;
                } else {
                    $en = $this->getScopeConfigValue('magemobcart/activation/en');
                    if ($isenabled && $en && in_array($websiteId, $websites)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                $en = $this->getScopeConfigValue('magemobcart/activation/en');
                if ($isenabled && $en) {
                    return true;
                }
            }
        }
    }
    public function getScopeConfigValue($configPath)
    {
        return $this->_scopeConfig->getValue($configPath);
    }

    public function getAllStoreDomains()
    {
        $domains = array();
        foreach ($this->_storeManager->getWebsites() as $website) {
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    $url = $store->getConfig('web/unsecure/base_url');
                    $domains[] = $this->getFormatUrl($url);
                    $url = $store->getConfig('web/secure/base_url');
                    $domains[] = $this->getFormatUrl($url);

                }
            }
        }
        return array_unique($domains);
    }
    public function getAllWebsites()
    {
        if (!$this->getScopeConfigValue('magemobcart/activation/installed')) {
            return array();
        }
        $data = $this->getScopeConfigValue('magemobcart/activation/data');
        $web = $this->getScopeConfigValue('magemobcart/activation/websites');
        $websites = explode(',', str_replace($data, '', $this->_encryptor->decrypt($web)));
        $websites = array_diff($websites, [""]);
        return $websites;
    }
    public function getDataInfo()
    {
        $data = $this->getScopeConfigValue('magemobcart/activation/data');
        return json_decode(base64_decode($this->_encryptor->decrypt($data)));
    }

    public function getFormatUrl($url)
    {
        $input = trim($url, '/');
        if (!preg_match('#^http(s)?://#', $input)) {
            $input = 'http://' . $input;
        }
        $urlParts = parse_url($input);
        if (isset($urlParts['path'])) {
            $domain = preg_replace('/^www\./', '', $urlParts['host'] . $urlParts['path']);
        } else {
            $domain = preg_replace('/^www\./', '', $urlParts['host']);
        }
        return $domain;
    }

    public function relogin($customerId)
    {
        $session = $this->_customerSession;
        $customerModel = $this->_customerModel->load($customerId);
        $session->setCustomerAsLoggedIn($customerModel);
    }
    public function getPriceByStoreWithCurrency($amount, $storeId, $currencyCode)
    {
        // $currency = $this->_scopeConfig->getValue('currency/options/default', \Magento\Store\Model\ScopeInterface::SCOPE_STORES, $storeId);
        $this->_storeManager->getStore($storeId)->setCurrentCurrencyCode($currencyCode);
        return $this->_pricingHelper->currencyByStore($amount, $storeId, true, false);
    }
    public function getPriceByStoreWithoutCurrency($amount, $storeId, $currencyCode)
    {
        // $currency = $this->_scopeConfig->getValue('currency/options/default', \Magento\Store\Model\ScopeInterface::SCOPE_STORES, $storeId);
        $this->_storeManager->getStore($storeId)->setCurrentCurrencyCode($currencyCode);
        $currentCurrency = $this->_storeManager->getStore($storeId)->getCurrentCurrency();
        return $this->_pricingInterface->convert($amount, $storeId, $currentCurrency);
    }
    public function getWishlistData($customerId, $storeId, $currencyCode, $byi_url_id = '')
    {
        $jsonResult = $this->_jsonFactory->create();
        try {
            $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrencyCode();
            $currentCurrencyCode = $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();
            $currencyCode = ($currencyCode != '') ? $currencyCode : $currentCurrencyCode;
            $wishlist = $this->_wishlistModel->loadByCustomerId($customerId, true);
            $wishlistCollection = array();
            $wishListItemCollection = $wishlist->getItemCollection();
            
            foreach ($wishListItemCollection->getData() as $_item) {
                $byiUrl = '';
                $isByiExists = '';
                $productData = $this->_productFactory->create()->load($_item['product_id']);
                $summaryData = $this->_reviewSummaryModel->setStoreId($storeId)->load($_item['product_id']);
                if ($this->_catalogRule->calcProductPriceRule($productData, $productData->getPrice())) {
                    $specialPrice = $this->_catalogRule->calcProductPriceRule($productData, $productData->getPrice());
                } else {
                    $specialPrice = $productData->getSpecialPrice();
                }
                if ($productData->getTypeId() == 'grouped') {
                    $aProductIds = $productData->getTypeInstance()->getChildrenIds($productData->getId());
                    $prices = array();
                    foreach ($aProductIds as $ids) {
                        foreach ($ids as $id) {
                            $aProduct = $this->_productFactory->create()->load($id);
                            $prices[] = $aProduct->getPriceModel()->getPrice($aProduct);
                        }
                    }
                    krsort($prices);
                    $price_array = array_keys($prices);
                    $price = $this->getPriceByStoreWithCurrency(min($prices), $storeId, $currencyCode);
                } elseif ($productData->getTypeId() == 'configurable') {
                    $price = $this->getPriceByStoreWithCurrency($productData->getFinalPrice(), $storeId, $currencyCode);
                } else {
                    $price = $this->getPriceByStoreWithCurrency($productData->getPrice(), $storeId, $currencyCode);
                }
                $productStockData = $this->_stockApiRepository->getStockItem($_item['product_id']);
                $product = array(
                    'id' => $productData->getId(),
                    'sku' => $productData->getSku(),
                    'name' => $productData->getName(),
                    'status' => $productData->getStatus(),
                    'type' => $productData->getTypeId(),
                    'in_stock' => (string)$productStockData->getIsInStock(),
                    'review_count' => $summaryData->getReviewsCount(),
                    'rating_summary' => $summaryData->getRatingSummary(),
                    'short_desc' => strip_tags($productData->getShortDescription()),
                    'price' => $price,
                    'image' => $this->_imageHelper->init($productData, 'product_page_image_medium')->resize(300, 330)->constrainOnly(true)->keepAspectRatio(true)->getUrl(),
                    'special_price' => $this->getPriceByStoreWithCurrency($specialPrice, $storeId, $currencyCode),
                    'byi_url_id' => $isByiExists,
                    'byi_url' => $byiUrl,
                    'has_custom_option' => $this->hasCustomOption($productData),
                    'save_discount' => $this->getDiscount($productData->getId())
                );
                $wishlistCollection[] = array(
                    'wishlist_item_id' => $_item['wishlist_item_id'],
                    'wishlist_id' => $_item['wishlist_id'],
                    'product_id' => $_item['product_id'],
                    'store_id' => $_item['store_id'],
                    'added_at' => $_item['added_at'],
                    'description' => $_item['description'],
                    'qty' => $_item['qty'],
                    'product_detail' => $product
                );
            }
            return $wishlistCollection;
        } catch (\Exception $e) {
            $errorResultArr = array(
                'status' => 'false',
                'message' => __($e->getMessage())
            );
            return $errorResultArr;
        }
    }
    public function getCartData($items, $totals, $action, $param = null, $storeId, $currencyCode)
    {
        $jsonResult = $this->_jsonFactory->create();
        $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrencyCode();
        $currentCurrencyCode = $this->_storeManager->getStore($storeId)->getCurrentCurrencyCode();
        $currencyCode = ($currencyCode != '') ? $currencyCode : $currentCurrencyCode;
        $this->_storeManager->getStore($storeId)->setCurrentCurrencyCode($currencyCode);
        $this->_checkoutSession->getQuote()->setStoreId($storeId);
        
        if (!empty($items)) {
            try {
                $productDetailResult = array();
                $temp_items = array();
                $wishlist_items = array();
                if ($this->_customerSession->isLoggedIn()) {
                    $customerData = $this->_customerSession->getCustomer();
                    $wishList = $this->_wishlistModel->loadByCustomerId($customerData->getEntityId());
                    $wishListItemCollection = $wishList->getItemCollection();
                    foreach ($wishListItemCollection as $itemValue) {
                        $wishlist_items[] = $itemValue->getProductId();
                    }
                }
                
                $discountTotal = '';
                foreach ($items as $item) {
                    $types[] = $item->getProductType();
                    $message = '';
                    $options = [];
                    $optionsData = $item->getProduct()->setStoreId($storeId)->getTypeInstance(true)->getOrderOptions($item->getProduct());
                    if (!empty($optionsData)) {
                        if ($item->getProductType() == 'configurable') {
                            if (array_key_exists('attributes_info', $optionsData) && !empty($optionsData['attributes_info'])) {
                                $attributes = $optionsData['attributes_info'];
                                foreach ($attributes as $attr) {
                                    $storeBasedOptionValue = $this->getStoreBasedOptionValue($attr, $storeId);
                                    $options[] = [
                                        'label' => $attr['label'],
                                        'option_id' => $attr['option_id'],
                                        'value' => $storeBasedOptionValue
                                    ];
                                }
                            }
                            if (array_key_exists('options', $optionsData) && !empty($optionsData['options'])) {
                                $customOptions = $optionsData['options'];
                                foreach ($customOptions as $option) {
                                    $options[] = [
                                        'label' => $option['label'],
                                        'option_id' => $option['option_id'],
                                        'value' => $option['value']
                                    ];
                                }
                            }
                        } else {
                            if (array_key_exists('options', $optionsData) && !empty($optionsData['options'])) {
                                $customOptions = $optionsData['options'];
                                foreach ($customOptions as $optionKey => $valuesVal) {
                                    $options[] = [
                                        'label' => $valuesVal['label'],
                                        'option_id' => $valuesVal['option_id'],
                                        'value' => $valuesVal['value']
                                    ];
                                }
                            }
                        }
                    }
                    $product_data = $this->_productFactory->create()->load($item->getProductId());
                    $links_name = array();
                    if ($item->getProductType() == 'downloadable') {
                        $linksOptions = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct());
                        $title = $item->getProduct()->getLinksTitle();
                        if (!isset($title)) {
                            $title = "";
                        }
                        if (isset($linksOptions['links']) && is_array($linksOptions['links'])) {
                            $_myprodlinks = $this->_objectManager->get('Magento\Downloadable\Model\Link');
                            $_myLinksCollection = $_myprodlinks->getCollection()->addProductToFilter($item->getProduct()->getId());
                            foreach ($_myLinksCollection->getData() as $key => $valuelinkCollection) {
                                foreach ($linksOptions['links'] as $key => $valueOptions) {
                                    if ($valuelinkCollection['link_id'] == $valueOptions) {
                                        $links_name['title'] = $title;
                                        // $links_name['link_details'][] = $valuelinkCollection['title'];
                                    }
                                }
                            }
                        }
                    }
                    if ($item->getHasError() == 1) {
                        $message = __($item->getMessage());
                    }
                    $design_id = '';
                    $params = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct());
                    if (in_array($item->getProduct()->getId(), $wishlist_items)) {
                        $is_wishlist = true;
                    } else {
                        $is_wishlist = false;
                    }
                    $productModelData = $this->_productFactory->create()->setStoreId($storeId)->load($item->getProduct()->getId());
                    $temp_items[$item->getId()] = array(
                        'product_id' => $item->getProduct()->getId(),
                        'name' => $productModelData->getName(),
                        'type' => $item->getProductType(),
                        'price' => $this->getPriceByStoreWithCurrency($item->getPrice(), $storeId, $currencyCode),
                        'row_total' => $this->getPriceByStoreWithCurrency($item->getRowTotal(), $storeId, $currencyCode),
                        'pimg' => $this->_imageHelper->init($productModelData, 'product_page_image_medium')->resize(300, 330)->constrainOnly(true)->keepAspectRatio(true)->getUrl(),
                        'qty' => $item->getQty(),
                        'disAmount' => $this->getPriceByStoreWithCurrency($item->getDiscountAmount(), $storeId, $currencyCode),
                        'options' => $options,
                        'links' => $links_name,
                        'is_wishlist' => $is_wishlist
                    );
                    $discountTotal = (float)$discountTotal + $item->getDiscountAmount();
                    if (!empty($message)) {
                        $temp_items[$item->getId()]['message'] = __($message);
                    }
                    unset($product_data);
                }
                $t = array_unique($types);
                $f = 1;

                $ty = array('simple', 'virtual');
                foreach ($t as $value) {
                    if ($value != 'downloadable' && $value != 'virtual') {
                        $f = 0;
                        break;
                    } elseif ($value == 'downloadable') {
                        $f = 1;
                    } elseif ($value == 'virtual') {
                        $f = 1;
                    } else {
                        $f = 0;
                        break;
                    }
                }
                $productDetailResult['is_contain_virtual_product'] = $f;
                $productDetailResult['quote_id'] = $this->_checkoutSession->getQuoteId();
                $productDetailResult['items'] = $temp_items;
                $productDetailResult['totals'] = array(
                    'subtotal' => $this->getPriceByStoreWithCurrency($totals['subtotal']->getData('value'), $storeId, $currencyCode),
                    'grand_total' => $this->getPriceByStoreWithCurrency($totals['grand_total']->getData('value'), $storeId, $currencyCode)
                );
                if ($totals['shipping']) {
                    $productDetailResult['totals']['shipping'] = $this->getPriceByStoreWithCurrency($totals['shipping']->getData('value'), $storeId, $currencyCode);
                }
                if ($totals['tax']) {
                    $productDetailResult['totals']['tax'] = $this->getPriceByStoreWithCurrency($totals['tax']->getData('value'), $storeId, $currencyCode);
                }
                if (isset($discountTotal)) {
                    $productDetailResult['totals']['discount'] = $this->getPriceByStoreWithCurrency($discountTotal, $storeId, $currencyCode);
                }
                if ($action == 'add') {
                    $productDetailResult['status'] = 'success';
                    $productDetailResult['message'] = __('Product Added Successfully');
                    $productDetailResult['cart_count'] = count($productDetailResult['items']);
                } elseif ($action == 'cart_remove') {
                    $productDetailResult['status'] = 'success';
                    $productDetailResult['message'] = __('Product Removed Successfully.');
                    $productDetailResult['cart_count'] = count($productDetailResult['items']);
                } elseif ($action == 'coupon') {
                    $productDetailResult['status'] = 'success';
                    $productDetailResult['message'] = __('Coupon code was applied.');
                    $productDetailResult['coupon_code'] = $this->_checkoutSession->getQuote()->getCouponCode();
                } elseif ($action == 'coupon_remove') {
                    $productDetailResult['status'] = 'success';
                    $productDetailResult['message'] = __('Coupon code was canceled.');
                    $productDetailResult['coupon_code'] = '';
                } elseif ($action == 'displaycart') {
                    if ($this->_checkoutSession->getQuote()->getCouponCode()) {
                        $productDetailResult['coupon_code'] = $this->_checkoutSession->getQuote()->getCouponCode();
                    } else {
                        $productDetailResult['coupon_code'] = '';
                    }
                } elseif ($action == 'get_total') {
                    if ($this->_checkoutSession->getQuote()->getShippingAddress()->getShippingMethod() != '') {
                        $productDetailResult['saved_shipping_method'] = explode('_', $this->_checkoutSession->getQuote()->getShippingAddress()->getShippingMethod())[0];
                    } else {
                        $productDetailResult['saved_shipping_method'] = '';
                    }
                }

                // below code to disable checkout flow based on magento default sales config
                if ($this->_scopeConfig->getValue('sales/minimum_order/active') == 1) {
                    $minOrderAmount = $this->_scopeConfig->getValue('sales/minimum_order/amount');
                    if ($minOrderAmount != NULL && $minOrderAmount != 0) {
                        if (isset($totals) && array_key_exists('subtotal', $totals) && $totals['subtotal']->getData('value') >= $minOrderAmount) {
                            $productDetailResult['is_checkout_enable'] = true;
                            $productDetailResult['checkout_message'] = "";
                        } else {
                            $productDetailResult['is_checkout_enable'] = false;
                            $productDetailResult['checkout_message'] = (!is_null($this->_scopeConfig->getValue('sales/minimum_order/description'))) ? $this->_scopeConfig->getValue('sales/minimum_order/description') : __('Minimum order amount is ' . $this->getPriceByStoreWithCurrency($minOrderAmount, $storeId, $currencyCode));
                        }
                    } else {
                        $productDetailResult['is_checkout_enable'] = true;
                        $productDetailResult['checkout_message'] = "";
                    }
                } else {
                    $productDetailResult['is_checkout_enable'] = true;
                    $productDetailResult['checkout_message'] = "";
                }
                // above code to disable checkout flow based on magento default sales config
                
                if ($this->_checkoutSession->getQuote()->getCouponCode()) {
                    $productDetailResult['coupon_code'] = $this->_checkoutSession->getQuote()->getCouponCode();
                }
                if ($param == 'save_payment') {
                    if ($this->_customerSession->isLoggedIn()) {
                        $customerData = $this->_customerSession->getCustomer();
                        $customerid = $customerData->getId();
                    }
                    $productDetailResult['payment_url'] = $this->_storeManager->getStore()->getBaseUrl(). 'magemobcart/checkout/processing/?quoteid=' . $this->_checkoutSession->getQuoteId() . '&customer_id=' . $customerid . '&storeid=' . $this->_checkoutSession->getQuote()->getStoreId();
                    $productDetailResult['payment_url_android'] = $this->_storeManager->getStore()->getBaseUrl(). 'magemobcart/checkout/customerlogin/?customer_id=' . $customerid;
                }
                if ($param == 'save_shipping') {
                    if ($this->_customerSession->isLoggedIn()) {
                        $customerData = $this->_customerSession->getCustomer();
                        $customerid = $customerData->getId();
                    }
                    $productDetailResult['payment_url'] = $this->_storeManager->getStore()->getBaseUrl(). 'magemobcart/checkout/getpaymenthtml/?quoteid=' . $this->_checkoutSession->getQuoteId() . '&customer_id=' . $customerid . '&storeid=' . $this->_checkoutSession->getQuote()->getStoreId();
                }
                $productDetailResult['quote_id'] = $this->_checkoutSession->getQuote()->getId();
                $productDetailResult['base_grand_total'] = (string)$this->_checkoutSession->getQuote()->getBaseGrandTotal();
                $productDetailResult['base_currency_code'] = $this->_checkoutSession->getQuote()->getBaseCurrencyCode();
                $productDetailResult['current_currency_grand_total'] = (string)$this->_checkoutSession->getQuote()->getGrandTotal();
                $productDetailResult['current_currency_code'] = $this->_checkoutSession->getQuote()->getCurrencyCode();

                $jsonResult->setData($productDetailResult);
                return $jsonResult;
            } catch (\Exception $e) {
                $errorResultArr = array(
                    'status' => 'false',
                    'message' => __($e->getMessage())
                );
                $jsonResult->setData($errorResultArr);
                return $jsonResult;
            }
        } else {
            $productDetailResult = array(
                'status' => 'success',
                'message' => __('Shopping cart is empty')
            );
            $jsonResult->setData($productDetailResult);
            return $jsonResult;
        }
    }
    public function getStoreBasedOptionValue($attr, $storeId) {
        $attributeCode = $this->_eavAttributeModel->load($attr['option_id'])->getAttributeCode();
        $optionData = $this->_attributeOptionCollection->create()
                        ->setPositionOrder('asc')
                        ->setAttributeFilter($attr['option_id'])
                        ->setStoreFilter($storeId)
                        ->load()
                        ->getData();
        $elementId = $this->searchForOptionValue($attr['value'], $optionData);
        return $optionData[$elementId]['value'];
    }
    public function searchForOptionValue($value, $array) {
        foreach ($array as $key => $val) {
            if ($val['default_value'] == $value) {
                return $key;
            }
        }
        return null;
    }
    public function getDiscount($productId)
    {
        $productCollection = $this->_productFactory->create()->load($productId);
        $price = $productCollection->getPrice();
        $specialPrice = $productCollection->getSpecialPrice();
        $productSpecialPrice = $productCollection->getSpecialPrice();
        if ($specialPrice && $price) {
            if ($specialPrice < $price) {
                $savePercent = $specialPrice * 100 / $price;
            } else {
                $savePercent = 0;
                return round($savePercent);
            }
        } else {
            $savePercent = 0;
            return round($savePercent);
        }
        return round(100 - $savePercent);
    }
    public function create($post_data)
    {
        $collectionDevice = $this->_deviceData->getCollection();
        $deviceModel = $this->_deviceData;

        foreach ($collectionDevice as $device) {
            $token = $device->getDeviceToken();
            $CustomerEmail = $device->getCustomerEmail();
            
            $fields = array();

            if (isset($token) && $token != null && ($post_data['devicetoken'] == $token)) {
                if ($post_data['is_logout'] != 1) {
                    if (isset($CustomerEmail) && $CustomerEmail != null) {
                        if ($post_data['username'] != $CustomerEmail) {
                            if ($device->getId()) {
                            }
                            $fields['customer_email'] = $post_data['username'];
                            $fields['password'] = $post_data['password'];
                            $fields['customer_id'] = $post_data['customer_id'];
                            $fields['is_logout'] = $post_data['is_logout'];

                            // $prefix = Mage::getConfig()->getTablePrefix();
                            // $connection->update($prefix . 'magemob_devicedata', $fields, $where);
                            // $connection->commit();
                        } else {
                            $fields['password'] = $post_data['password'];
                            $fields['customer_id'] = $post_data['customer_id'];
                            $fields['is_logout'] = $post_data['is_logout'];

                            // $prefix = Mage::getConfig()->getTablePrefix();
                            // $connection->update($prefix . 'magemob_devicedata', $fields, $where);
                            // $connection->commit();
                        }
                    } else {
                        if ($device->getDeviceToken() == $post_data['devicetoken']) {
                            $updateDeviceData = $this->_deviceData->load($device->getId());
                            $updateDeviceData->setCustomerEmail($post_data['username']);
                            $updateDeviceData->setPassword($post_data['password']);
                            $updateDeviceData->setIsLogout($post_data['is_logout']);
                            $updateDeviceData->setCustomerId($post_data['customer_id']);
                            $updateDeviceData->save();
                        }
                    }
                }
            }
        }
    }
    public function sendNotification($data)
    {
        $result = $this->send($data);
        if ($result) {
            $this->_messageManager->addSuccess(__("Message successfully delivered."));
        } else {
            $this->_messageManager->addError(__("Message not delivered."));
        }
        return $result;
    }
    public function send($data)
    {
        $website = $data['website_id'];
        $collectionDevice = $this->_deviceData->getCollection();
        if (isset($data['notification_id'])) {
            $collectionDevice = $collectionDevice->addFieldToFilter('cron_status', 'pending');
        }
        if ($data['os'] == 'all') {
            $resultAndroid = $this->sendAndroid($collectionDevice, $data);
            $resultIOS = $this->sendIOS($collectionDevice, $data);
            if ($resultIOS || $resultAndroid) {
                return true;
            } else {
                return false;
            }
        } elseif ($data['os'] == 'android') {
            return $this->sendAndroid($collectionDevice, $data);
        } elseif ($data['os'] == 'ios') {
            return $this->sendIOS($collectionDevice, $data);
        }
    }
    public function sendIOS($collectionDevice, $data)
    {
        $collectionDeviceios = $this->_deviceData->getCollection()
        ->addFieldToFilter('device_type', 'ios');

        // ->addFieldToFilter('device_token', 'd2I0RBeeMkQ:APA91bGhspvrNSzcfsXIwPGUaGGGKFglGAa--6ukcfPiLcQBzWjyWmTGPY5gQO_nujHLjnmEbuTubeyRrRbohaTgRQ3FgsCVdiNZ_S53Y4x4O2noJykXzyTfp7KBI2OrXE0tmdJK42ZC');
        // ->addFieldToFilter('device_id', array('neq'=> null));
        if (!array_key_exists("flag", $data)) {
            $collectionDeviceios->addFieldToFilter('cron_status', 'pending');
        }
        $collectionDeviceios->getSelect()->group('device_id');
        $deviceModel = $this->_deviceData;
        $iosKey = $this->_scopeConfig->getValue(self::XML_NOTIFCATION_KEY);
        $message = [];
        if (array_key_exists('message', $data)) {
            if (!empty($data['message']) && $data['message'] != null) {
                $message['notification']['body'] = $data['message'];
            }
        }
        if (array_key_exists('url', $data)) {
            if (!empty($data['url']) && $data['url'] != null) {
                $message['notification']['url'] = $data['url'];
            }
        }
        if (array_key_exists('title', $data)) {
            if (!empty($data['title']) && $data['title'] != null) {
                $message['notification']['title'] = $data['title'];
            }
        }
        if (array_key_exists('flag', $data)) {
            if (isset($data['flag']) && $data['flag'] == 1) {
            }
        }
        if (array_key_exists('category_id', $data)) {
            if (!empty($data['category_id']) && $data['category_id'] > 0) {
                $_category = $this->_categoryFactory->create()->load($data['category_id']);
                $message['notification']['category_name'] = $_category->getName();
                $message['notification']['category_id'] = $data['category_id'];
            }
        }
        if (array_key_exists('product_id', $data)) {
            if (!empty($data['product_id']) && $data['product_id'] != null) {
                $message['notification']['product_id'] = $data['product_id'];
            }
        }
        if (array_key_exists('entity_id', $data)) {
            if (!empty($data['entity_id']) && $data['entity_id'] != null) {
                $message['notification']['entity_id'] = $data['entity_id'];
            }
        }
        if (array_key_exists('imagefilename', $data)) {
            if (!empty($data['imagefilename']) && $data['imagefilename'] != null) {
                $message['data']['imagefilename'] = $data['imagefilename'];
                $message['notification']['content_available'] = "true";
                $message['notification']['mutable_content'] = "true";
            }
        }
        $url = "https://fcm.googleapis.com/fcm/send";
        $headers = array(
            'Authorization: key=' . $iosKey,
            'Content-Type: application/json');
        $registrationIDs = array();
        foreach ($collectionDeviceios as $item) {
            $customer_email = $item['customer_email'];
            if (isset($data['flag']) && $data['flag'] == 1) {
                if (isset($customer_email) && $customer_email == $data['customer_email']) {
                    $registrationIDs[] = $item['device_token'];
                }
                continue;
            } else {
                if (array_key_exists('notification_id', $data)) {
                    if ($item->getNotificationId() !== null) {
                        $notiId = array();
                        $notiId = explode(',', $item->getNotificationId());
                        if (in_array($data['notification_id'], $notiId)) {
                            $registrationIDs[] = $item->getDeviceToken();
                            if (($key = array_search($data['notification_id'], $notiId)) !== false) {
                                unset($notiId[$key]);
                            }
                            if (!empty($notiId)) {
                                $notiId = implode(',', $notiId);
                                $deviceModel->setCronStatus('pending')
                                        ->setNotificationID($notiId)
                                        ->setId($item->getId())
                                        ->save();
                            } else {
                                $notiId = null;
                                $deviceModel->setCronStatus('completed')
                                        ->setNotificationID($notiId)
                                        ->setId($item->getId())
                                        ->save();
                            }
                        }
                    }
                }
            }
        }
        if (count($registrationIDs) > 1000) {
            $idArray = array();
            $idArray = array_chunk($registrationIDs, 1000);
            foreach ($idArray as $tokenId) {
                $fields = array(
                    'registration_ids' => $tokenId,
                    'data' => array("message" => $message),
                );
                $result = '';
                $result = $this->iosCurlRequest($url, $headers, $fields);
            }
        } else {
            $result = '';
            $result = $this->iosCurlRequest($url, $headers, $registrationIDs, $message);
        }
        $re = json_decode($result);
        /*$this->_messageManager->addSuccess(__("Message successfully delivered (IOS)"));*/
        return true;
    }
    public function iosCurlRequest($url, $headers, $registrationIDs, $data)
    {
        $url = "https://fcm.googleapis.com/fcm/send";
        
        foreach ($registrationIDs as $key => $id) {
            $data['to'] = $id;
            $data['notification']['sound'] = "default";
            $json = json_encode($data);
            $iosKey = $this->_scopeConfig->getValue(self::XML_NOTIFCATION_KEY);
            $headers = array(
                    'Content-Type:application/json',
                    'Authorization:key='.$iosKey
                );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

            $result = curl_exec($ch);
            curl_close($ch);
        }
    }
    public function sendAndroid($collectionDevice, $data)
    {
        $collectionDeviceandroid = $this->_deviceData->getCollection()
        ->addFieldToFilter('device_type', 'android');
        // ->addFieldToFilter('device_token', 'cO0T5mblx5g:APA91bFYZXdX1g-3ZlkOf7zi9xYXKg_0vxA5gLxiSsZPsdqJyVgVmLjm3jeIWt1hP8LAVr803SM-uAhqHPqLsB4si8x-os0d0UG4IirQccJNEUIjDTgMX_HWGL0Ns0oPOxXCqDyE4M-r');
        if (!array_key_exists("flag", $data)) {
            $collectionDeviceandroid->addFieldToFilter('cron_status', 'pending');
        }
        $deviceModel = $this->_deviceData;
        $apiKey = $this->_scopeConfig->getValue(self::XML_NOTIFCATION_KEY);
        $message = [];
        if (array_key_exists('message', $data)) {
            if (!empty($data['message']) && $data['message'] != null) {
                $message['message'] = $data['message'];
            }
        }
        if (array_key_exists('url', $data)) {
            if (!empty($data['url']) && $data['url'] != null) {
                $message['url'] = $data['url'];
            }
        }
        if (array_key_exists('title', $data)) {
            if (!empty($data['title']) && $data['title'] != null) {
                $message['title'] = $data['title'];
            }
        }
        if (array_key_exists('flag', $data)) {
            if (isset($data['flag']) && $data['flag'] == 1) {
                $message['type'] = 'order';
            }
        }
        if (array_key_exists('type', $data)) {
            if (!empty($data['type']) && $data['type'] != null) {
                $message['type'] = $data['type'];
            }
        }
        if (array_key_exists('category_id', $data)) {
            if (!empty($data['category_id']) && $data['category_id'] != null) {
                $_category = $this->_categoryFactory->create()->load($data['category_id']);
                $message['category_name'] = $_category->getName();
                $message['category_id'] = $data['category_id'];
            }
        }
        if (array_key_exists('product_id', $data)) {
            if (!empty($data['product_id']) && $data['product_id'] != null) {
                $message['product_id'] = $data['product_id'];
            }
        }
        if (array_key_exists('entity_id', $data)) {
            if (!empty($data['entity_id']) && $data['entity_id'] != null) {
                $message['entity_id'] = $data['entity_id'];
            }
        }
        if (array_key_exists('imagefilename', $data)) {
            if (!empty($data['imagefilename']) && $data['imagefilename'] != null) {
                $message['imagefilename'] = $data['imagefilename'];
            }
        }
        $url = "https://fcm.googleapis.com/fcm/send";
        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json');
        
        $registrationIDs = array();
        foreach ($collectionDeviceandroid as $item) {
            $customer_email = $item['customer_email'];
            if (isset($data['flag']) && $data['flag'] == 1) {
                if (isset($customer_email) && $customer_email == $data['customer_email']) {
                    $registrationIDs[] = $item['device_token'];
                }
            } else {
                if (array_key_exists('notification_id', $data)) {
                    if ($item->getNotificationId() !== null) {
                        $notiId = array();
                        $notiId = explode(',', $item->getNotificationId());
                        if (in_array($data['notification_id'], $notiId)) {
                            $registrationIDs[] = $item->getDeviceToken();
                            if (($key = array_search($data['notification_id'], $notiId)) !== false) {
                                unset($notiId[$key]);
                            }
                            if (!empty($notiId)) {
                                $notiId = implode(',', $notiId);
                                $deviceModel->setCronStatus('pending')
                                        ->setNotificationID($notiId)
                                        ->setId($item->getId())
                                        ->save();
                            } else {
                                $notiId = null;
                                $deviceModel->setCronStatus('completed')
                                        ->setNotificationID($notiId)
                                        ->setId($item->getId())
                                        ->save();
                            }
                        }
                    }
                }
            }
        }
        if (count($registrationIDs) > 1000) {
            $idArray = array();
            $idArray = array_chunk($registrationIDs, 1000);
            foreach ($idArray as $tokenId) {
                $fields = array(
                    'registration_ids' => $tokenId,
                    'data' => array("message" => $message),
                );
                $result = '';
                $result = $this->curlRequest($url, $headers, $fields);
            }
        } else {
            $fields = array(
                'registration_ids' => $registrationIDs,
                'data' => array("message" => $message),
            );
            $result = '';
            $result = $this->curlRequest($url, $headers, $fields);
        }
        $re = json_decode($result);
        // if ($re->success == 0) {
        //     $this->_messageManager->addError(__("Message not delivered (Android)"));
        //     return false;
        // }
        // $this->_messageManager->addSuccess(__("Message successfully delivered (Android)"));
        return true;
    }
    public function curlRequest($url, $headers, $fields)
    {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
        } catch (\Exception $e) {
        }
        return $result;
    }
    public function hasCustomOption($product)
    {
        $customOptions = $this->_productCustomOptions->getProductOptionCollection($product);

        if (count($customOptions) > 0) {
            return true;
        }
        return false;
    }
    public function getFormKey()
    {
        $formKey = $this->formKeyData->getFormKey();
        return $formKey;
    }
    public function getTierPrice($productId, $customerId)
    {
        $groupId = $this->getCustomerGroupDetail($customerId);
        $groupId = 0;
        $_product = $this->_productFactory->create()->load($productId);
        $priceHelper = $this->_pricingHelper;
        $productAllTierPrices = $_product->getData('tier_price');
        $allTierPrices = array();

        foreach ($productAllTierPrices as $tierPrices) {
            if ($tierPrices['cust_group'] == $groupId) {
                $allTierPrices[] = "Buy ".$tierPrices['price_qty']." for ".$priceHelper->currency($tierPrices['price'], true, false)." each and save ".$this->getGroupDiscount($_product->getFinalPrice(), $tierPrices['price'])."%";
            }
        }
        return $allTierPrices;
    }
    public function getCustomerGroupDetail($customerId)
    {
        $customerModel = $this->_customerModel->load($customerId);
        $customerGroupId = $customerModel->getGroupId();
        return $customerGroupId;
    }
    public function getGroupDiscount($price, $specialPrice)
    {
        if ($specialPrice && $price) {
            if ($specialPrice < $price) {
                $savePercent = $specialPrice * 100 / $price;
            } else {
                $savePercent = 0;
                return round($savePercent);
            }
        } else {
            $savePercent = 0;
            return round($savePercent);
        }
        return round(100 - $savePercent);
    }
    /**
     * [getHeaderInfo use for the create Header if already headler function is not there]
     * @return [type] [reutrn headers value]
     */
    
    public function getHeaderInfo()
    {
        if (!function_exists('getallheaders')) {
            function getallheaders()
            {
                $headers = [];
                foreach ($_SERVER as $name => $value) {
                    if (substr($name, 0, 5) == 'HTTP_') {
                        $headers[str_replace(' ', '-', strtolower(str_replace('_', ' ', substr($name, 5))))] = $value;
                    } else {
                        $headers[$name] = $value;
                    }
                }
                return $headers;
            }
        }
    }

    /**
     * [getHeaders use for the get the all requested call headers]
     * @return [type] [retuen headers value]
     */
    
    public function getHeaders()
    {
        $allHeaders = [];
        $headersSecretKey = "";
        $this->getHeaderInfo();
        $allHeaders = getallheaders();
        if (!array_key_exists(self::KEY, $allHeaders)) {
            if (array_key_exists('Secretkey', $allHeaders)) {
                $headersSecretKey = $allHeaders['Secretkey'];
            }
            $allHeaders[self::KEY] = $headersSecretKey;
            unset($allHeaders['Secretkey']);
        }
        $orignalValue = base64_decode(self::SECRET);
        $key = strtolower(self::KEY);
        foreach ($allHeaders as $name => $value) {
            if (array_key_exists(self::KEY, $allHeaders)) {
                if ($name == self::KEY) {
                    if (isset($value)) {
                        if (strcasecmp($value, $orignalValue) == 0) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }
    }
    public function getHeaderMessage()
    {
        return 'Please make sure your app is proper authenticated';
    }
    
    /**
     * @return boolean - not used till - activation with website and store
     * 06-12-2019 | added 
     */
    public function enableWebsiteForCurrentSiteStore()
    {
        $websites = $this->getAllWebsites();
        if (!empty($websites)) {
            /* Get website id of current website*/
            $website_id = $this->getRequest()->getParam('website');
            if($website_id) {
                $website = $this->_website->load($website_id);
                if ($website && in_array($website->getWebsiteId(), $websites)) {
                    return true;
                } else return false;
            } else {
                /* Get website id of current store*/
                $website_id = $this->getRequest()->getParam('store');
                if($website_id)
                {
                    $website = $this->_website->load($website_id);
                    if ($website && in_array($website->getWebsiteId(), $websites)) {
                        return true;
                    } else return false;
                } else return true; /* return true for default config*/
            }
        } else {
            return false;
        }
    }

    /**
     * @return boolean
     * 06-12-2019 | added - specific storeview activation checking
     */
    public function  enableSiteForStoreview($store_id)
    {
        $websites = $this->getAllWebsites();
        if(!empty($websites))
        {
            $store = $this->_storeManager->getStore($store_id);
            $this->_storeManager->setCurrentStore($store->getCode());
            $currentWebsite = $this->_storeManager->getStore()->getWebsiteId();
            if(in_array($currentWebsite, $websites))
            {
                if($this->scopeConfig->getValue(self::XML_PATH_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$store_id))
                {
                    return true;
                } else {
                    return false;
                }
            } else {
                if($currentWebsite == 0)
                {
                    if($this->scopeConfig->getValue(self::XML_PATH_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$store_id))
                    {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
        else {
            return false;
        }
    }

    /**
     * getimage link
     *
     * @param string $image
     * @return string
     */
    public function getImageUrl($image)
    {
        $sliderImage = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $sliderImage.$image;
    }

    public function getProductStockData($storeId, $productId)
    {
        $product = $this->_productFactory->create()->setStoreId($storeId)->load($productId);
        $productStockData = $this->_stockApiRepository->getStockItem($productId);
        $pro_qty = 0;
        $isInStock = $productStockData->getIsInStock();
        $salebleModelData = $this->getSalebleQty($product->getSku());
        if (!empty($salebleModelData)) {
            foreach ($salebleModelData as $key => $value) {
                $pro_qty = $value['qty'];
                if ($pro_qty < 0) {
                    $isInStock = 0;
                } else {
                    $isInStock = 1;
                }
            }
        } else {
            $pro_qty = $productStockData->getQty();
        }
        $stock_item = $productStockData;
        if ($stock_item->getBackorders() != 0) {
            if ($isInStock == 0) {
                $isInStock = '0';
            } else {
                $isInStock = '1';
            }
        } else {
            if (($pro_qty < 0 || $isInStock == 0) && $productTypeId != 'configurable') {
                $pro_qty = 'Out of Stock';
                $isInStock = '0';
            } elseif ($productTypeId == 'configurable') {
                $children = $product_data->getTypeInstance()->getUsedProducts($product_data);
                foreach ($children as $child) {
                    if ($this->_stockApiRepository->getStockItem($child->getId())->getQty() > 0) {
                        $isInStock = '1';
                        $pro_qty = $productStockData->getQty();
                        break;
                    } else {
                        $pro_qty = 'Out of Stock';
                        $isInStock = '0';
                    }
                }
            } else {
                $isInStock = '1';
            }
        }
    }

    public function getSalebleQty($productSku)
    {
        if (!$this->checkMsiEnabled()) {
            $salebleModelData =array();
            return $salebleModelData;
        }
        try {
            $this->_salebleModel = $this->_objectManager->get('Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku');
            $salebleModelData = $this->_salebleModel->execute((string)$productSku);
            return $salebleModelData;
        } catch (\Exception $e) {
            $salebleModelData = array();
            return $salebleModelData;
        }
        return $salebleModelData;
    }
    
    private function checkMsiEnabled()
    {
        $allowNormal = false;
        if ($this->isModuleEnabled('Magento_Inventory')) {
            $allowNormal = true;
        }
        return $allowNormal;
    }
}
