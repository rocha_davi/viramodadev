<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MLM
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */


namespace Webkul\MLM\Api;

/**
 * SponserConfigurationsRepository Interface
 */
interface SponserConfigurationsRepositoryInterface
{

    /**
     * get by id
     *
     * @param int $id
     * @return \Webkul\MLM\Model\SponserConfigurations
     */
    public function getById($id);
    /**
     * get by id
     *
     * @param int $id
     * @return \Webkul\MLM\Model\SponserConfigurations
     */
    public function save(\Webkul\MLM\Model\SponserConfigurations $subject);
    /**
     * get list
     *
     * @param Magento\Framework\Api\SearchCriteriaInterface $creteria
     * @return Magento\Framework\Api\SearchResults
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $creteria);
    /**
     * delete
     *
     * @param \Webkul\MLM\Model\SponserConfigurations $subject
     * @return boolean
     */
    public function delete(\Webkul\MLM\Model\SponserConfigurations $subject);
    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     */
    public function deleteById($id);
}
