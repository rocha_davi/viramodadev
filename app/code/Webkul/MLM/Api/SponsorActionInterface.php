<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Api;
 
/**
 * @api
 */
interface SponsorActionInterface
{
    /**
     * @api
     * @param int $sponsorId
     * @param int $memberLevelId
     * @return string
     */
    public function changeMemberLevel($sponsorId, $memberLevelId);

    /**
     * @api
     * @param int $sponsorId
     * @param float $amount
     * @param string $description
     * @return string
     */
    public function payToSponsor($sponsorId, $amount, $description);

    /**
     * @api
     * @param int $sponsorId
     * @param string $title
     * @param string $comment
     * @return string
     */
    public function addSponsorBusinessComment($sponsorId, $title, $comment);
}
