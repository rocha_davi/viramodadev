<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MLM
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */


namespace Webkul\MLM\Api;

/**
 * SponsorBannerRepository Interface
 */
interface SponsorBannerRepositoryInterface
{

    /**
     * get by id
     *
     * @param int $id
     * @return \Webkul\MLM\Model\SponsorBanner
     */
    public function getById($id);
    /**
     * get by id
     *
     * @param int $id
     * @return \Webkul\MLM\Model\SponsorBanner
     */
    public function save(\Webkul\MLM\Model\SponsorBanner $subject);
    /**
     * get list
     *
     * @param Magento\Framework\Api\SearchCriteriaInterface $creteria
     * @return Magento\Framework\Api\SearchResults
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $creteria);
    /**
     * delete
     *
     * @param \Webkul\MLM\Model\SponsorBanner $subject
     * @return boolean
     */
    public function delete(\Webkul\MLM\Model\SponsorBanner $subject);
    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     */
    public function deleteById($id);
}
