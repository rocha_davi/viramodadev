<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul <support@webkul.com>
 * @copyright Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html ASL Licence
 * @link      https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Api;

use Webkul\MLM\Api\Data\EarningTypeInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface EarningTypeRepositoryInterface
{
    /**
     * @param int $id
     * @return EarningTypeInterface
     */
    public function get($id);

    /**
     * @param int $id
     * @return EarningTypeInterface
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($id);

    /**
     * @param EarningTypeInterface $MLM
     * @return EarningTypeInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(EarningTypeInterface $EarningType);

    /**
     * @param EarningTypeInterface $MLM
     * @return bool
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(EarningTypeInterface $EarningType);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Webkul\MLM\Model\ResourceModel\EarningType\Collection
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
