<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul <support@webkul.com>
 * @copyright Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html ASL Licence
 * @link      https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Api;

use Webkul\MLM\Api\Data\MemberLevelInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface MemberLevelRepositoryInterface
{
    /**
     * @param int $id
     * @return MemberLevelInterface
     */
    public function get($id);

    /**
     * @param int $id
     * @return MemberLevelInterface
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($id);

    /**
     * @param MemberLevelInterface $MLM
     * @return MemberLevelInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(MemberLevelInterface $memberLevel);

    /**
     * @param MemberLevelInterface $MLM
     * @return bool
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(MemberLevelInterface $memberLevel);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Webkul\MLM\Model\ResourceModel\MemberLevel\Collection
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
