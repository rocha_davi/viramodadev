<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Api\Data;

/**
 * SponsorsInterface Interface
 */
interface SponsorsInterface
{
    /**#@+
     * Constants for keys of data array
     */
    const ENTITY_ID = 'entity_id';
    const SPONSOR_CODE = 'sponsor_code';
    const CUSTOMER_ID = 'customer_id';
    const FILENAME = 'filename';
    const PARENT_SPONSOR_ID = 'parent_sponsor_id';
    const MAX_DOWNLINE_MEMBER_LIMIT = 'max_downline_member_limit';
    const STATUS = 'status';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    /**#@-*/

    /**
     * Get entity id
     *
     * @return string
     */
    public function getId();

    /**
     * Set entity id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get sponsor code
     *
     * @return string
     */
    public function getSponsorCode();

    /**
     * Set sponsor code
     *
     * @param string $sponsorCode
     * @return $this
     */
    public function setSponsorCode($sponsorCode);

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getCustomerId();

    /**
     * Set Customer Id
     *
     * @param string $customerId
     * @return $this
     */
    public function setCustomerId($customerId);

    /**
     * Get filename
     *
     * @return string|null
     */
    public function getFilename();

    /**
     * Set filename
     *
     * @param string $filename
     * @return $this
     */
    public function setFilename($filename);

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus();

    /**
     * Set status
     *
     * @param boolean $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * Get ParentSponsorId
     *
     * @return boolean
     */
    public function getParentSponsorId();

    /**
     * Set ParentSponsorId
     *
     * @param boolean $ParentSponsorId
     * @return $this
     */
    public function setParentSponsorId($parentSponsorId);

    /**
     * Get MaxDownlineMemberLimit
     *
     * @return boolean
     */
    public function getMaxDownlineMemberLimit();

    /**
     * Set MaxDownlineMemberLimit
     *
     * @param boolean $MaxDownlineMemberLimit
     * @return $this
     */
    public function setMaxDownlineMemberLimit($maxDownlineMemberLimit);

    /**
     * Get createdAt
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Set createdAt
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updatedAt
     *
     * @return string
     */
    public function getUpdatedAt();

    /**
     * Set updatedAt
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt);
}
