<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MLM
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */


namespace Webkul\MLM\Api\Data;

/**
 * SponserConfigurations Interface
 */
interface SponserConfigurationsInterface
{

    const ENTITY_ID = 'entity_id';

    const SPONSOR_ID = 'sponsor_id';

    const LOGO_IMAGE = 'logo_image';

    const LOGO_WIDTH = 'logo_width';

    const LOGO_HEIGHT = 'logo_height';

    /**
     * Set EntityId
     *
     * @param int $entityId
     * @return Webkul\MLM\Api\Data\SponserConfigurationsInterface
     */
    public function setEntityId($entityId);
    /**
     * Get EntityId
     *
     * @return int
     */
    public function getEntityId();
    /**
     * Set SponsorId
     *
     * @param int $sponsorId
     * @return Webkul\MLM\Api\Data\SponserConfigurationsInterface
     */
    public function setSponsorId($sponsorId);
    /**
     * Get SponsorId
     *
     * @return int
     */
    public function getSponsorId();
    /**
     * Set LogoImage
     *
     * @param string $logoImage
     * @return Webkul\MLM\Api\Data\SponserConfigurationsInterface
     */
    public function setLogoImage($logoImage);
    /**
     * Get LogoImage
     *
     * @return string
     */
    public function getLogoImage();
    /**
     * Set LogoWidth
     *
     * @param int $logoWidth
     * @return Webkul\MLM\Api\Data\SponserConfigurationsInterface
     */
    public function setLogoWidth($logoWidth);
    /**
     * Get LogoWidth
     *
     * @return int
     */
    public function getLogoWidth();
    /**
     * Set LogoHeight
     *
     * @param int $logoHeight
     * @return Webkul\MLM\Api\Data\SponserConfigurationsInterface
     */
    public function setLogoHeight($logoHeight);
    /**
     * Get LogoHeight
     *
     * @return int
     */
    public function getLogoHeight();
}
