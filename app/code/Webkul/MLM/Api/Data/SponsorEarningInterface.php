<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Api\Data;

/**
 * SponsorsInterface Interface
 */
interface SponsorEarningInterface
{
    /**#@+
     * Constants for keys of data array
     */
    const ENTITY_ID = 'entity_id';
    const EARNING_TYPE_ID = 'earning_type_id';
    const SPONSOR_ID = 'sponsor_id';
    const COMMISSION_AMOUNT = 'commission_amount';
    const COMMISSION_PERCENT = 'commission_percent';
    const DESCRIPTION = 'description';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    /**#@-*/

    /**
     * Get entity id
     *
     * @return string
     */
    public function getId();

    /**
     * Set entity id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get sponsor code
     *
     * @return string
     */
    public function getEarningTypeId();

    /**
     * Set sponsor code
     *
     * @param string $EarningTypeId
     * @return $this
     */
    public function setEarningTypeId($earningTypeId);

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getSponsorId();

    /**
     * Set Customer Id
     *
     * @param string $SponsorId
     * @return $this
     */
    public function setSponsorId($sponsorId);

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getCommissionAmount();

    /**
     * Set Customer Id
     *
     * @param string $CommissionAmount
     * @return $this
     */
    public function setCommissionAmount($commissionAmount);

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getCommissionPercent();

    /**
     * Set Customer Id
     *
     * @param string $CommissionAmount
     * @return $this
     */
    public function setCommissionPercent($commissionPercent);

    /**
     * Get createdAt
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Set createdAt
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updatedAt
     *
     * @return string
     */
    public function getUpdatedAt();

    /**
     * Set updatedAt
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt);
}
