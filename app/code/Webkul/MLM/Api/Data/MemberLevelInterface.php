<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Api\Data;

/**
 * SponsorsInterface Interface
 */
interface MemberLevelInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    /**#@+
     * Constants for keys of data array
     */
    const ENTITY_ID = 'entity_id';
    const LEVEL_NAME = 'level_name';
    const MAXIMUM_BUSINESS_AMOUNT = 'maximum_business_amount';
    const BONUS_AMOUNT = 'bonus_amount';
    const PRIORITY = 'priority';
    const BADGE_IMAGE = 'badge_image';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    /**#@-*/

    /**
     * Get entity id
     *
     * @return string
     */
    public function getId();

    /**
     * Set entity id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get sponsor code
     *
     * @return string
     */
    public function getLevelName();

    /**
     * Set sponsor code
     *
     * @param string $LevelName
     * @return $this
     */
    public function setLevelName($LevelName);

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getMaximumBusinessAmount();

    /**
     * Set Customer Id
     *
     * @param string $MaximumBusinessAmount
     * @return $this
     */
    public function setMaximumBusinessAmount($MaximumBusinessAmount);

    /**
     * Get BonusAmount
     *
     * @return string|null
     */
    public function getBonusAmount();

    /**
     * Set BonusAmount
     *
     * @param string $BonusAmount
     * @return $this
     */
    public function setBonusAmount($BonusAmount);

    /**
     * Get Priority
     *
     * @return boolean
     */
    public function getPriority();

    /**
     * Set Priority
     *
     * @param boolean $Priority
     * @return $this
     */
    public function setPriority($Priority);

    /**
     * Get Priority
     *
     * @return boolean
     */
    public function getBadgeImage();

    /**
     * Set Priority
     *
     * @param boolean $Priority
     * @return $this
     */
    public function setBadgeImage($Priority);

    /**
     * Get createdAt
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Set createdAt
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updatedAt
     *
     * @return string
     */
    public function getUpdatedAt();

    /**
     * Set updatedAt
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Retrieve existing extension attributes object or create a new one.
     *
     * @return \Webkul\MLM\Api\Data\MemberLevelExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     *
     * @param \Webkul\MLM\Api\Data\MemberLevelExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Webkul\MLM\Api\Data\MemberLevelExtensionInterface $extensionAttributes
    );
}
