<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul <support@webkul.com>
 * @copyright Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html ASL Licence
 * @link      https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Api\Data;

interface EarningTypeSearchResultInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get items.
     *
     * @return \Webkul\MLM\Api\Data\EarningTypeInterface[] Array of collection items.
     */
    public function getItems();

    /**
     * Set items.
     *
     * @param \Webkul\MLM\Api\Data\EarningTypeInterface[] $items
     * @return $this
     */
    public function setItems(array $items = null);
}
