<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Api\Data\MemberLevel;

/**
 * SponsorsInterface Interface
 */
interface MemberCountInterface
{
    /**#@+
     * Constants for keys of data array
     */
    const MEMBER_COUNT = 'member_count';
    /**#@-*/

    /**
     * Get entity id
     *
     * @return int
     */
    public function getMemberCount();

    /**
     * Get entity id
     *
     * @return int
     */
    public function setMemberCount($memberCount);
}
