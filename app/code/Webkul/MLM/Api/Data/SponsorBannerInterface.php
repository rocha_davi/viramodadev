<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MLM
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */


namespace Webkul\MLM\Api\Data;

/**
 * SponsorBanner Interface
 */
interface SponsorBannerInterface
{

    const ENTITY_ID = 'entity_id';

    const SPONSOR_ID = 'sponsor_id';

    const BANNER_IMAGE = 'banner_image';

    const BANNER_LINK = 'banner_link';

    const SORTORDER = 'sortorder';

    /**
     * Set EntityId
     *
     * @param int $entityId
     * @return Webkul\MLM\Api\Data\SponsorBannerInterface
     */
    public function setEntityId($entityId);
    /**
     * Get EntityId
     *
     * @return int
     */
    public function getEntityId();
    /**
     * Set SponsorId
     *
     * @param int $sponsorId
     * @return Webkul\MLM\Api\Data\SponsorBannerInterface
     */
    public function setSponsorId($sponsorId);
    /**
     * Get SponsorId
     *
     * @return int
     */
    public function getSponsorId();
    /**
     * Set BannerImage
     *
     * @param string $bannerImage
     * @return Webkul\MLM\Api\Data\SponsorBannerInterface
     */
    public function setBannerImage($bannerImage);
    /**
     * Get BannerImage
     *
     * @return string
     */
    public function getBannerImage();
    /**
     * Set BannerLink
     *
     * @param string $bannerLink
     * @return Webkul\MLM\Api\Data\SponsorBannerInterface
     */
    public function setBannerLink($bannerLink);
    /**
     * Get BannerLink
     *
     * @return string
     */
    public function getBannerLink();
    /**
     * Set Sortorder
     *
     * @param int $sortorder
     * @return Webkul\MLM\Api\Data\SponsorBannerInterface
     */
    public function setSortorder($sortorder);
    /**
     * Get Sortorder
     *
     * @return int
     */
    public function getSortorder();
}
