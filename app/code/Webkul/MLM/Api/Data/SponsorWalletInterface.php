<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Api\Data;

/**
 * SponsorsInterface Interface
 */
interface SponsorWalletInterface
{
    /**#@+
     * Constants for keys of data array
     */
    const ENTITY_ID = 'entity_id';
    const SPONSOR_ID = 'sponsor_id';
    const TOTAL_EARNING = 'total_earning';
    const CURRENT_BALANCE = 'current_balance';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    /**#@-*/

    /**
     * Get entity id
     *
     * @return string
     */
    public function getId();

    /**
     * Set entity id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getSponsorId();

    /**
     * Set Customer Id
     *
     * @param string $SponsorId
     * @return $this
     */
    public function setSponsorId($sponsorId);

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getTotalEarning();

    /**
     * Set Customer Id
     *
     * @param string $TotalEarning
     * @return $this
     */
    public function setTotalEarning($totalEarning);

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getCurrentBalance();

    /**
     * Set Customer Id
     *
     * @param string $CurrentBalance
     * @return $this
     */
    public function setCurrentBalance($currentBalance);

    /**
     * Get createdAt
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Set createdAt
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updatedAt
     *
     * @return string
     */
    public function getUpdatedAt();

    /**
     * Set updatedAt
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt);
}
