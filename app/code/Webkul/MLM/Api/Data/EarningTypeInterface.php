<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Api\Data;

/**
 * SponsorsInterface Interface
 */
interface EarningTypeInterface
{
    /**#@+
     * Constants for keys of data array
     */
    const ENTITY_ID = 'entity_id';
    const LABEL = 'label';
    const VALUE = 'value';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    /**#@-*/

    /**
     * Get entity id
     *
     * @return string
     */
    public function getId();

    /**
     * Set entity id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get sponsor code
     *
     * @return string
     */
    public function getLabel();

    /**
     * Set sponsor code
     *
     * @param string $Label
     * @return $this
     */
    public function setLabel($label);

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getValue();

    /**
     * Set Customer Id
     *
     * @param string $Value
     * @return $this
     */
    public function setValue($value);

    /**
     * Get createdAt
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Set createdAt
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updatedAt
     *
     * @return string
     */
    public function getUpdatedAt();

    /**
     * Set updatedAt
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt);
}
