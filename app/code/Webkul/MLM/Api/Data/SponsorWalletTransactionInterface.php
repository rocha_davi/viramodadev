<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Api\Data;

/**
 * Sponsors WalletTransaction Interface
 */
interface SponsorWalletTransactionInterface
{
    /**#@+
     * Constants for keys of data array
     */
    const ENTITY_ID = 'entity_id';
    const SPONSOR_ID = 'sponsor_id';
    const WALLET_ID = 'wallet_id';
    const EARNING_TYPE = 'earning_type';
    const WALLET_BALANCE = 'wallet_balance';
    const DESCRIPTION = 'description';
    const AMOUNT = 'amount';
    const STATUS = 'status';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    /**#@-*/

    /**
     * Get entity id
     *
     * @return string
     */
    public function getId();

    /**
     * Set entity id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get entity SponsorId
     *
     * @return string
     */
    public function getSponsorId();

    /**
     * Set entity SponsorId
     *
     * @param integer $SponsorId
     * @return $this
     */
    public function setSponsorId($sponsorId);

    /**
     * Get entity walletId
     *
     * @return string
     */
    public function getWalletId();

    /**
     * Set entity walletId
     *
     * @param integer $walletId
     * @return $this
     */

    public function setWalletId($walletId);

    /**
     * Get entity walletId
     *
     * @return string
     */
    public function getWalletBalance();

    /**
     * Set entity walletId
     *
     * @param integer $walletId
     * @return $this
     */

    public function setWalletBalance($walletBalance);

    /**
     * Get entity EarningType
     *
     * @return string
     */
    public function getEarningType();

    /**
     * Set entity EarningType
     *
     * @param integer $EarningType
     * @return $this
     */
    public function setEarningType($earningType);

    /**
     * Get entity Description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set entity Description
     *
     * @param integer $Description
     * @return $this
     */
    public function setDescription($description);

    /**
     * Get entity Amount
     *
     * @return string
     */
    public function getAmount();

    /**
     * Set entity Amount
     *
     * @param integer $Amount
     * @return $this
     */
    public function setAmount($amount);

    /**
     * Get entity Status
     *
     * @return string
     */
    public function getStatus();

    /**
     * Set entity Status
     *
     * @param integer $Status
     * @return $this
     */
    public function setStatus($status);

    /**
     * Get createdAt
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Set createdAt
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updatedAt
     *
     * @return string
     */
    public function getUpdatedAt();

    /**
     * Set updatedAt
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt);
}
