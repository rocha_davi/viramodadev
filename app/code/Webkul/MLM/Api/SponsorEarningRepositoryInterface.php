<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul <support@webkul.com>
 * @copyright Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html ASL Licence
 * @link      https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Api;

use Webkul\MLM\Api\Data\SponsorEarningInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface SponsorEarningRepositoryInterface
{
    /**
     * @param int $id
     * @return SponsorEarningInterface
     */
    public function get($id);

    /**
     * @param int $id
     * @return SponsorEarningInterface
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($id);

    /**
     * @param SponsorEarningInterface $MLM
     * @return SponsorEarningInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(SponsorEarningInterface $SponsorEarning);

    /**
     * @param SponsorEarningInterface $MLM
     * @return bool
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(SponsorEarningInterface $SponsorEarning);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Webkul\MLM\Model\ResourceModel\SponsorEarning\Collection
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
