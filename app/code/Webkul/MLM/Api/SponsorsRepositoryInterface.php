<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Api;

/**
 * Sponsors repository interface.
 *
 * An sponsor is a customer who becomes a sponsor to make some assets and earn
 *
 * @api
 * @since 3.0.0
 */
interface SponsorsRepositoryInterface
{
    /**
     * Lists Sponsor that match specified search criteria.
     *
     * This call returns an array of objects.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria The search criteria.
     * @return \Webkul\MLM\Api\Data\SponsorsSearchResultInterface Sponsors search result interface.
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Loads a specified Sponsors.
     *
     * @param int $id The Sponsors ID.
     * @return \Webkul\MLM\Api\Data\SponsorsInterface Sponsors interface.
     */
    public function get($id);

    /**
     * Deletes a specified Sponsors.
     *
     * @param \Webkul\MLM\Api\Data\SponsorsInterface $entity The Sponsors ID.
     * @return bool
     */
    public function delete(\Webkul\MLM\Api\Data\SponsorsInterface $entity);

    /**
     * Performs persist operations for a specified Sponsor.
     *
     * @param \Webkul\MLM\Api\Data\SponsorsInterface $entity The Sponsors ID.
     * @return \Webkul\MLM\Api\Data\SponsorsInterface Sponsors interface.
     */
    public function save(\Webkul\MLM\Api\Data\SponsorsInterface $entity);
}
