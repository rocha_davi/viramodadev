<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Api;
 
interface SendMailInterface
{
    /**
     * Returns string
     *
     * @api
     * @param string $referenceId
     * @return string
     */
    public function sendReferralMail($referenceId);
}
