<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UninstallInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Entity\Attribute\SetFactory;

class Uninstall implements UninstallInterface
{
    /**
     * @param \Magento\Config\Model\Config\Factory $configFactory
     * @param CustomerSetupFactory $customerSetupFactory
     * @param SetFactory $attributeSetFactory
     */
    public function __construct(
        \Magento\Config\Model\Config\Factory $configFactory,
        CustomerSetupFactory $customerSetupFactory,
        SetFactory $attributeSetFactory
    ) {
    
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->configFactory = $configFactory;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function uninstall(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        $customerSetup = $this->customerSetupFactory->create(
            [
                "setup" => $this->_moduleDataSetup
            ]
        );

        $custometSetup->removeAttribute(
            Customer::ENTITY,
            "sponsor_reference_id"
        );

        $connection = $setup->getConnection();
        $connection->dropTable($connection->getTableName('mlm_sponsor'));
        $connection->dropTable($connection->getTableName('sponsor_wallet'));
        $connection->dropTable($connection->getTableName('mlm_earning_type'));
        $connection->dropTable($connection->getTableName('mlm_sponsor_earning'));
        $connection->dropTable($connection->getTableName('mlm_commission'));
        $connection->dropTable($connection->getTableName('mlm_sponsor_business'));
        $connection->dropTable($connection->getTableName('sponsor_payment_request'));
        $connection->dropTable($connection->getTableName('sponsor_business_comment'));
        $connection->dropTable($connection->getTableName('sponsor_wallet_transaction'));
        $connection->dropTable($connection->getTableName('mlm_memberlevel'));
        $connection->dropTable($connection->getTableName('mlm_memberlevel_sponsor'));
        $connection->dropTable($connection->getTableName('mlm_sponsor_sponsor'));
        $connection->dropTable($connection->getTableName('mlm_sponsor_sales_request'));

        $setup->endSetup();
    }
}
