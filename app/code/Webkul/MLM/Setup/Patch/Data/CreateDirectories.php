<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class CreateDirectories implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface $moduleDataSetup
     */
    private $moduleDataSetup;

    /**
     * @param \Magento\Framework\Filesystem\Io\File $file
     */
    public function __construct(
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Filesystem\Io\File $file,
        \Magento\Framework\Module\Dir\Reader $reader
    ) {
        $this->filesystem = $filesystem;
        $this->file = $file;
        $this->reader = $reader;
    }

    /**
     * Do Upgrade
     *
     * @return void
     */
    public function apply()
    {
        $this->createDirectories();
        $this->processDefaultImages();
    }

    /**
     * Create default directories
     */
    private function createDirectories()
    {
        $mediaDirectories = [
            'webkul_mlm',
            'webkul_mlm/memberlevel',
            'webkul_mlm/memberlevel/badge/',
            'webkul_mlm/memberlevel/badge/images',
        ];
        foreach ($mediaDirectories as $mediaDirectory) {
            $directory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
            $path = $directory->getAbsolutePath($mediaDirectory);
            if (!$this->file->fileExists($path)) {
                $this->file->mkdir($path, 0777, true);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Copy Banner and Icon Images to Media
     */
    private function processDefaultImages()
    {
        $directory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
        $ds = "/";
        $baseModulePath = $this->reader->getModuleDir('', 'Webkul_MLM');
        $mediaDetails = [
            "webkul_mlm/memberlevel/badge/images" => [
                "view/base/web/images/memberlevel/badge" => [
                    "badge-image.png",
                ]
            ],
        ];

        foreach ($mediaDetails as $mediaDirectory => $imageDetails) {
            foreach ($imageDetails as $modulePath => $images) {
                foreach ($images as $image) {
                    $path = $directory->getAbsolutePath($mediaDirectory);
                    $mediaFilePath = $path.$ds.$image;
                    $moduleFilePath = $baseModulePath.$ds.$modulePath.$ds.$image;

                    if ($this->file->fileExists($mediaFilePath)) {
                        continue;
                    }

                    if (!$this->file->fileExists($moduleFilePath)) {
                        continue;
                    }

                    $this->file->cp($moduleFilePath, $mediaFilePath);
                }
            }
        }
    }
}
