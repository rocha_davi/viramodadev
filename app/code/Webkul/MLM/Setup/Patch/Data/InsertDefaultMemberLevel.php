<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Webkul\MLM\Model\ResourceModel\MemberLevel\Collection as MemberLevelCollection;

class InsertDefaultMemberLevel implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $_moduleDataSetup;

    /**
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Config\Model\Config\Factory $configFactory
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        MemberLevelCollection $memberLevelCollection
    ) {
        $this->_moduleDataSetup = $moduleDataSetup;
        $this->memberLevelCollection = $memberLevelCollection;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        if ($this->memberLevelCollection->getSize() < 1) {
            $this->_moduleDataSetup->startSetup();
            $memberLevels  =
            [
                ['Starter Level', 100.00, 0.00, 9999, 'badge-image.png:badge-image.png'],
            ];
    
            $this->_moduleDataSetup->getConnection()->insertArray(
                $this->_moduleDataSetup->getTable('mlm_memberlevel'),
                ['level_name', 'maximum_business_amount', 'bonus_amount', 'priority', 'badge_image'],
                $memberLevels
            );
    
            $this->_moduleDataSetup->endSetup();
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        /**
         * This is dependency to another patch. Dependency should be applied first
         * One patch can have few dependencies
         * Patches do not have versions, so if in old approach with Install/Ugrade data scripts you used
         * versions, right now you need to point from patch with higher version to patch with lower version
         * But please, note, that some of your patches can be independent and can be installed in any sequence
         * So use dependencies only if this important for you
         */
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function revert()
    {
        $this->_moduleDataSetup->getConnection()->startSetup();
        //Here should go code that will revert all operations from `apply` method
        //Please note, that some operations, like removing data from column, that is in role of foreign key reference
        //is dangerous, because it can trigger ON DELETE statement
        $this->_moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        /**
         * This internal Magento method, that means that some patches with time can change their names,
         * but changing name should not affect installation process, that's why if we will change name of the patch
         * we will add alias here
         */
        return [];
    }
}
