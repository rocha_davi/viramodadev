<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Entity\Attribute\SetFactory;

class AdminSponsorData implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    const CONFIGURATION_SECTION = "mlm";
    const CONFIGURATION_GROUP = "configuration";
    const CONFIGURATION_FIELD = "admin_sponsor_id";
    const DS = "/";
    const DEFAULT_ADM_SPNSR_ID = "w00000000k";

    /**
     * CustomerSetupFactory
     *
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * SetFactory
     *
     * @var SetFactory
     */
    protected $attributeSetFactory;

    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $_moduleDataSetup;

    /**
     * @var \Magento\Config\Model\Config\Factory
     */
    private $_configFactory;

    /**
     * @var \Webkul\MLM\Helper\StoreConfiguration
     */
    private $_storeConfigurationHelper;

    /**
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Config\Model\Config\Factory $configFactory
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Config\Model\Config\Factory $configFactory,
        CustomerSetupFactory $customerSetupFactory,
        SetFactory $attributeSetFactory,
        \Webkul\MLM\Helper\StoreConfiguration $storeConfigurationHelper
    ) {
        $this->_moduleDataSetup = $moduleDataSetup;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->_storeConfigurationHelper = $storeConfigurationHelper;
        $this->_configFactory = $configFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $setup = $this->_moduleDataSetup->getConnection();
        $setup->startSetup();

        $customerSetup = $this->customerSetupFactory->create(
            [
                "setup" => $this->_moduleDataSetup
            ]
        );
        $customerEntity = $customerSetup->getEavConfig()->getEntityType("customer");
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
        $customerSetup->addAttribute(
            Customer::ENTITY,
            "sponsor_reference_id",
            [
                "type" => "varchar",
                "label" => "Sponsor Reference Id",
                "input" => "text",
                "system" => 0,
                "visible" => true,
                "position" => 1000,
                "required" => false,
                "user_defined" => true,
                'backend' => '',
                'frontend_class' => 'required-entry',
                'source' => ''
            ]
        );
        $attribute = $customerSetup->getEavConfig()
            ->getAttribute(Customer::ENTITY, "sponsor_reference_id")
            ->addData(
                [
                    "attribute_set_id" => $attributeSetId,
                    "attribute_group_id" => $attributeGroupId,
                    "used_in_forms" => [
                        "adminhtml_customer",
                        "customer_account_edit",
                        "customer_account_create"
                    ]
                ]
            );

        $attribute->save();

        $this->_storeConfigurationHelper->generateSponsorIdForAdmin();
        $setup->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        /**
         * This is dependency to another patch. Dependency should be applied first
         * One patch can have few dependencies
         * Patches do not have versions, so if in old approach with Install/Ugrade data scripts you used
         * versions, right now you need to point from patch with higher version to patch with lower version
         * But please, note, that some of your patches can be independent and can be installed in any sequence
         * So use dependencies only if this important for you
         */
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function revert()
    {
        $this->_moduleDataSetup->getConnection()->startSetup();
        //Here should go code that will revert all operations from `apply` method
        //Please note, that some operations, like removing data from column, that is in role of foreign key reference
        //is dangerous, because it can trigger ON DELETE statement
        $this->_moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        /**
         * This internal Magento method, that means that some patches with time can change their names,
         * but changing name should not affect installation process, that's why if we will change name of the patch
         * we will add alias here
         */
        return [];
    }
}
