/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'uiComponent',
    'ko',
], function ($, Component, ko) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Webkul_MLM/view/grid-js'
        },
        selectedChoice: ko.observable(),
        items : ko.observableArray([]),
        methods : ko.observableArray([]),
        selectedItems : ko.observableArray([]),
        initialize: function () {
            this._super();
            var self = this;
            window.gridJsSelf = self;
            let items = JSON.parse(window.allPaymentMethods);
            var result = Object.keys(items).map((key) =>  { 
                if (!items[key].label) {
                    items[key].label = items[key].value;
                }
                return items[key]; 
            }); 
            self.methods(result);
            
            $("body").on("click", ".admin__control-checkbox.main-checkbox", (event) => {
                if ($(event.currentTarget).prop("checked")) {
                    self.selectAll($(event.currentTarget));
                } else {
                    self.unSelectAll($(event.currentTarget));
                }
            });
            $("body").on("click", ".admin__control-checkbox.row-checkbox", (event) => {
                if ($(event.currentTarget).prop("checked")) {
                    let oldValue = $("#payment_options_values").val();
                    $("#payment_options_values").val(oldValue + "," + event.currentTarget.value);
                } else {
                    let oldValue = $("#payment_options_values").val();
                    let removeItem = event.currentTarget.value;
                    let oldArray = oldValue.split(",");
                    oldArray = $.grep(oldArray, function(value) {
                        return value != removeItem;
                    });
                    $("#payment_options_values").val(oldArray.toString());
                }
            });
        },
        selectedPaymentMethod : (data, event) => {
            let parentSelf = window.gridJsSelf;
            let selectedItemArray = parentSelf.selectedItems();
            if ($.inArray(data.value, selectedItemArray) > -1) {
                event.currentTarget.checked = true;
            } else {
                parentSelf.selectedItems.push(data.value);
            }
        },
        selectAll : (currentElement) => {
            let tHead =  currentElement.parent().parent().parent();
            let trs = tHead.next().find(".data-row.data-grid");
            trs.each((index, row) => {
                $(row).find("input").prop("checked", true);
                let oldValue = $("#payment_options_values").val();
                $("#payment_options_values").val(oldValue + "," + $(row).find("input").val());
            });
        },
        unSelectAll : (currentElement) => {
            let tHead =  currentElement.parent().parent().parent();
            let trs = tHead.next().find(".data-row.data-grid");
            trs.each((index, row) => {
                $(row).find("input").prop("checked", false);
            });
            $("#payment_options_values").val("");
        }
    });
});