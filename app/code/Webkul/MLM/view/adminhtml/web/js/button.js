/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
define([
    "jquery",
    "mageUtils",
    "Magento_Ui/js/modal/alert",
    'Magento_Ui/js/modal/confirm',
    "jquery/ui",
], function ($, utils, alert, confirmation) {
    "use strict";

    $.widget("mage.generateButton", {
        /** @inheritdoc */
        _create: function () {
            var options = this.options;

            function showConfirmation()
            {
                confirmation({
                    title: $.mage.__("Generate Sponsor Id"),
                    content: $.mage.__('This operation will update the existing sponors\'s parent sponsor id with the new one. Are you sure want to proceed?'),
                    actions: {
                        confirm: function () {
                            generateNewSponsorId();
                        },
                    },
                    buttons: [
                        {
                            text: $.mage.__("No"),
                            class: "action-secondary action-dismiss",
                            click: function (event) {
                                this.closeModal(event);
                            },
                        },
                        {
                            text: $.mage.__("Proceed"),
                            class: "action-primary action-accept",
                            click: function (event) {
                                this.closeModal(event, true);
                            },
                        },
                    ],
                });
            }

            function generateNewSponsorId()
            {
                $.post(
                    options.url,
                    {
                        form_key: options.formKey,
                    },
                    function ($data) {
                        var data =
                            typeof $data === "object"
                                ? $data
                                : JSON.parse($data);
                        if (data.success) {
                            alert({
                                title: $.mage.__("Generat Sponsor Id"),
                                content: $.mage.__(
                                    "Sponsor Id successfully generated."
                                ),
                                buttons: [
                                    {
                                        text: $.mage.__("OK"),
                                        class: "action-primary action-accept",
                                        click: function () {
                                            this.closeModal(true);
                                            window.setTimeout(() => {
                                                window.location.reload();
                                            }, 100);
                                        },
                                    },
                                ],
                            });
                        }

                        if (!data.success) {
                            alert({
                                title: $.mage.__("Generat Sponsor Id"),
                                content: data.message,
                                actions: {
                                    always: function () {},
                                },
                            });
                        }
                    }
                );
            }

            $("body").on("click", "#generateId", function () {
                showConfirmation();
            });
        },
    });
    return $.mage.generateButton;
});
