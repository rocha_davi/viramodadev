/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
var config = {
    map: {
        '*': {
            genealogyTree: 'Webkul_MLM/js/sponsor/genealogyTree',
            verifySponsorCode: 'Webkul_MLM/js/sponsor/verify-sponsor-code',
            bannerlist: 'Webkul_MLM/js/bannerlist',
            owlcarousel: "Webkul_MLM/js/owl.carousel"
        }
    },
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        }
    }
};
