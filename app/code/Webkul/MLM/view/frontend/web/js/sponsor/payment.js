/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'uiComponent',
    "ko",
    "jquery",
    "mage/translate",
    'mage/storage',
    "Magento_Ui/js/modal/modal",
    "Magento_Ui/js/modal/alert"
  ], function(
      Component,
      ko,
      $,
      $t,
      storage,
      modal,
      alert
    ) {
    return Component.extend({
        defaults: {
            // template : 'Webkul_MLM/sponsor/payment'
        },
        paymentMethod : ko.observable([]),
        initialize: function ()  {
            this._super();
            var self = this;
            self.paymentMethod((window.paymentMethod));
        }
    });
  });
  