/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

define(
    [
        'uiComponent',
        'jquery',
        'ko',
        'uiLayout',
        'mage/loader',
        'mage/mage'       
    ], function(Component, $, ko, layout) {
        'use strict';
        return Component.extend({
            defaults: {
                template: '',
                formData: window.formData,
                formSubmitUrl: window.formSubmitUrl,
                submitButtonTitle: window.submitButtonTitle,
                inputName: ''
            },

            /**
             * execution starts here
             */
            initialize: function () {
                var self = this;
                this._super();
                
                $('body').on("click", '#emailSaveButton', function(e) {                    
                    e.preventDefault();
                    $("#submit-form").mage('validation', {});
                    var form = $("#submit-form");
                    var validate =  form.validation();
                    if (validate && form.validation('isValid')) {
                        $('#submit-form').submit();
                    }
                });
                
            },

            /**
             * initialize observers
             */
            initObservable: function () {
                this._super().observe(
                    'formData formSubmitUrl submitButtonTitle inputName'
                );
                return this;
            },
            getEditor : function (data, type) {
                if (data.input == type) {

                }
                return false
            },
            checkType: function(data, inputType) {
                if (data.input == inputType) {
                    return true;
                } else {
                    return false;
                }
            },
            getDeleteUrl: function(d, e) {
                return window.deleteUrl;
            },

            getBackUrl: function(d, e) {
                return window.backUrl;
            },

            isEditPage: function() {
                return window.isEditPage;
            }
        });
    }
);
