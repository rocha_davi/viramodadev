/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'uiComponent',
    "ko",
    "jquery",
    "mage/translate",
    'mage/storage',
    "Magento_Ui/js/modal/modal",
    "Magento_Ui/js/modal/alert"
  ], function(
      Component,
      ko,
      $,
      $t,
      storage,
      modal,
      alert
    ) {
  
    return Component.extend({
        defaults: {
            template : 'Webkul_MLM/sponsor/referral'
        },
        pageTitle : ko.observable(),
        referralId : ko.observable(),
        addCustomerLabel : ko.observable(),
        shareableLink : ko.observable(),
        document : document,
        copiedText : $t("Copied "),
        emailPlaceHolder : ko.observable(),
        initialize: function ()  {
            this._super();
            var self = this;
            self.pageTitle = window.referralData.pageTitle;
            self.referralId = window.referralData.referralId;
            self.addCustomerLabel = window.referralData.addCustomerLabel;
            self.shareableLink = window.referralData.shareableLink;
            self.emailPlaceHolder = $.mage.__('Digite o e-mail para compartilhar');

            $('body').on("click", '#linkSaveButton', function (e) {
                e.preventDefault();
                $("#submit-form").mage('validation', {});
                var dataForm = $('#submit-form');
                dataForm.mage('validation', {});
                if (dataForm.validation()) {}
            });
        },
        copyCode : function (data, event) {
            let copyText = data.referralId;
            let textElement =  $('<input>').val(copyText).appendTo('body').select();
            document.execCommand('copy');
            textElement.fadeOut();
            let tooltip = $("#wkTooltip");
            let toolTipText = $(".tooltiptext");
            toolTipText.css("visibility", "visible");
            tooltip.text("");
            tooltip.append(data.copiedText + ':' + copyText);
        },
        copyLink : function(data) {
            let copyText = data.shareableLink;
            let textElement =  $('<input>').val(copyText).appendTo('body').select();
            document.execCommand('copy');
            textElement.fadeOut();
            let tooltip = $("#wkPopupTooltip");
            let toolTipText = $(".popupTooltiptext");
            toolTipText.css("visibility", "visible");
            $(".link-label").css("background-color", "yellow");
            tooltip.text("");
            $("#copy-link").text(data.copiedText);
            tooltip.append(data.copiedText + ':' + copyText);
        },
        sendMail : function() {
            window.location.href = window.referralData.emailPageUrl;
        },
        showShareableLink: function (data) {
            $("#submit-form").mage('validation', {});
            var options = {
                type: 'popup',
                responsive: true,
                title: $.mage.__('Opções de Compartilhamento'),
                buttons: [{ 
                    text: $.mage.__('OK'),
                    class: '',
                    click: function () {
                        if (!data.referralId)
                        if ($("#receiver-mail").val()) { 
                            if (data.shareableLink) {
                                let self = this;
                                let sponsorId = {
                                'sponsorId' : data.shareableLink,
                                'email' : $("#receiver-mail").val()
                                };
                                
                                var serviceUrl,payload;
                                /**
                                 * Set values .
                                 */
                                serviceUrl        = 'rest/V1/send/referra-mail/';
                                payload           = {
                                    referenceId: JSON.stringify(sponsorId),
                                };
                                $("body").trigger('processStart');
                                storage.post(
                                    serviceUrl,
                                    JSON.stringify(payload)
                                ).done(function (response) {
                                    $("body").trigger('processStop');
                                    response = JSON.parse(response);
                                    if (response.success == false) {
                                        alert({
                                            content: $t(response.message)
                                        });
                                    } else {
                                        alert({
                                            content: $t(response.message)
                                        });
                                    }
                                }).fail(function (response) {
                                    $("body").trigger('processStop');
                                    alert({
                                        content: $t(response.responseJSON.message)
                                    });
                                });
                            }
                            this.closeModal();
                        } else {
                            alert({
                                content: $t("Por favor, preencha o email.")
                            });
                        }
                        this.closeModal();
                    }
                }]            
            };
            var popup = modal(options, $('#modal-content'));
            $('#modal-content').modal('openModal');
        }
    });
  });
  