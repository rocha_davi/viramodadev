/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'uiComponent',
    "ko",
    "jquery",
    "mage/translate",
    'mage/storage',
    "Magento_Ui/js/modal/modal",
    "Magento_Ui/js/modal/alert",
    'mage/mage'
  ], function(
      Component,
      ko,
      $,
      $t,
      storage,
      modal,
      alert
    ) {
    return Component.extend({
        defaults: {
            template : 'Webkul_MLM/sponsor/paymentrequest'
        },
        currentBalance : ko.observable(),
        sponsorId : ko.observable(),
        walletId : ko.observable(),
        formUrl : ko.observable(),
        initialize: function ()  {
            this._super();
            var self = this;
            self.currentBalance(window.currentBalance);
            self.formUrl(window.formUrl);
            self.sponsorId(window.sponsorId);
            self.walletId(window.walletId);
            var dataForm = $('.form-payment-request');
            dataForm.mage('validation', {});
        }
    });
  });
  