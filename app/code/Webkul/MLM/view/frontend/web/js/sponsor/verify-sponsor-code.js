/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint jquery:true*/
define([
    "jquery",
    'mage/translate',
    'mage/template',
    'Magento_Ui/js/modal/alert',
    "jquery/ui",
    "mage/url"
], function ($, $t, mageTemplate, alert, url) {
    'use strict';
    $.widget('mage.verifySponsorCode', {
        options: {
            backUrl: '',
            sponsorCode: '[data-role="sponsor-code"]',
            becomeSponsorBoxWrapper: '[data-role="wk-mp-become-sponsor-box-wrapper"]',
            available: '.available',
            unavailable: '.unavailable',
            emailAddress: '#email_address',
            wantSponsorDiv: '#wantptr',
            wantSponsorTemplate: '#wantptr-template',
            sponsorcodeClass: '.sponsorcode',
            wantSponsorClass: '.wantsponsor',
            pageLoader: '#wk-load',
            codeLabel: $t('Código do Consultor(a)'),
            codeTitle: $t('Código do Consultor(a)'),
            codeText: $t(' (Isso será usado como código de referência) ')
        },
        _create: function () {
            var self = this;
            $(self.options.emailAddress).parents('div.field').after($(self.options.wantSponsorDiv));
            $(self.options.wantSponsorDiv).show();
            $(self.options.wantpartnerClass).on('change', function () {
                self.callAppendShopBlockFunction(this.element, $(this).val());
            });
            // $(this.element).delegate(self.options.sponsorCode, 'keyup', function () {
            //     var sponsorCodeVal = $(this).val();
            //     $(self.options.sponsorCode).val(sponsorCodeVal.replace(/[^a-z^A-Z^0-9\.\-]/g,''));
            // });
            $(this.element).delegate(self.options.sponsorCode, 'change', function () {
                self.callAjaxFunction();
            });
            $(this.element).delegate("#sponsor_name", 'keyup', function () {
                var sponsorNameVal = $(this).val();
                $("#sponsor_name").val(sponsorNameVal.replace(/[^a-z^A-Z^0-9\_\-]/g,''));
            });
            $(this.element).delegate("#sponsor_name", 'change', function () {
                self.callSponsorNameFunction();
            });
        },
        callAppendShopBlockFunction: function (parentelem, elem) {
            var self = this;
            if (elem==1) {
                $(self.options.pageLoader).parents(parentelem)
                .find('button.submit').addClass('disabled');
                var progressTmpl = mageTemplate(self.options.wantSponsorTemplate),
                          tmpl;
                tmpl = progressTmpl({
                    data: {
                        label: self.options.codeLabel,
                        src: self.options.loaderImage,
                        title: self.options.codeTitle,
                        text: self.options.codeText
                    }
                });
                $(self.options.wantSponsorDiv).after(tmpl);
            } else {
                $(self.options.pageLoader).parents(parentelem)
                .find('button.submit').removeClass('disabled');
                $(self.options.sponsorcodeClass).remove();
            }
        },
        callAjaxFunction: function () {
            var self = this;
            $(self.options.button).addClass('disabled');
            var sponsorCodeVal = $(self.options.sponsorCode).val();
            $(self.options.available).remove();
            $(self.options.unavailable).remove();
            if (sponsorCodeVal) {
                $(self.options.pageLoader).removeClass('no-display');
                $.ajax({
                    type: "POST",
                    url: self.options.ajaxSaveUrl,
                    data: {
                        sponsorcode: sponsorCodeVal
                    },
                    success: function (response) {
                        $(self.options.pageLoader).addClass('no-display');
                        if (response===0) {
                            $(self.options.button).removeClass('disabled');
                            $(self.options.becomeSponsorBoxWrapper).append(
                                $('<div/>').addClass('available message success')
                                .text(self.options.successMessage)
                            );
                        } else {
                            $(self.options.button).removeClass('disabled');
                            $(self.options.sponsorCode).val('');
                            $(self.options.becomeSponsorBoxWrapper).append(
                                $('<div/>').addClass('available message error')
                                .text(self.options.errorMessage)
                            );
                        }
                    },
                    error: function (response) {
                        alert({
                            content: $t('Ocorreu um erro durante a verificação dos dados do código do patrocinador')
                        });
                    }
                });
            }
        },
        callSponsorNameFunction: function () {
            var self = this;
            // $(self.options.button).addClass('disabled');
            var shopUrlVal = $("#sponsor_name").val();
            // $(self.options.available).remove();
            // $(self.options.unavailable).remove();
            if (shopUrlVal) {
                // $(self.options.pageLoader).removeClass('no-display');
                $.ajax({
                    type: "POST",
                    url: self.options.verifySponsorUrl,
                    data: {
                        profileurl: shopUrlVal
                    },
                    success: function (response) {

                        console.log(response);
                        if (response === 0) {
                            $("#sponsor_name").parent().append($('<div/>').addClass('available message success')
                            .text('Parabéns! O nome escolhido está perfeito!'))
                            
                        } else {
                            $("#sponsor_name").parent().append($('<div/>').addClass('available message error')
                            .text('Ops! Algo deu errado com o seu nome. Tente novamente!'))
                            
                        }
                    },
                    error: function (response) {
                        alert({
                            content: $t('Ocorreu um erro durante a verificação dos dados da loja do vendedor')
                        });
                    }
                });
            }
        }
    });
    return $.mage.verifySponsorCode;
});
