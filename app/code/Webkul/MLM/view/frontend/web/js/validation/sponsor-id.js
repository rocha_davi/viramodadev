/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'uiComponent',
    'jquery',
    'Magento_Ui/js/modal/alert',
    'mage/translate',
    'mage/storage',
    'ko',
    "loader",
    'mage/url'
], function (
        Component,
        $,
        alert,
        $t,
        storage,
        ko,
        loader,
        url
    ) {
    'use strict';

    return Component.extend({
        initialize: function () {
            this._super();
            var self = this;
            $('#make_sponsor').attr('checked', true);
            $(".field-name-sponsors-reference-id").show();
            $(".field-name-sponsors-name").show();
            $(".field-name-sponsors-reference-id").val("1");

            $("#make_sponsor").on('change', () => {
                if ($("#make_sponsor").prop('checked')) {
                    $(".field-name-sponsors-reference-id").show();
                    $(".field-name-sponsors-name").show();
                    $(".field-name-sponsors-reference-id").val("1");
                } else {
                    $(".field-name-sponsors-reference-id").hide();
                    $(".field-name-sponsors-name").hide();
                    $(".field-name-sponsors-reference-id").val("0");
                }
            });

            $("#sponsor_reference_id").trigger("change");

            $('body').on('change', "#sponsor_reference_id", function() {
                if ($(this).val()) {
                    let self = this;
                    let sponsorId = {
                      'sponsorId' : $(this).val()
                    };

                    var serviceUrl,payload;
                     /**
                      * Set values .
                      */
                     serviceUrl        = 'rest/V1/verify/sponsors-id';
                     payload           = {
                        referenceId: JSON.stringify(sponsorId),
                     };
                     $("body").trigger('processStart');
                     storage.post(
                         serviceUrl,
                         JSON.stringify(payload)
                     ).done(function (response) {
                        $("body").trigger('processStop');
                        response = JSON.parse(response);
                        if (response.success == false) {
                            $(self).val("");
                            $(self)
                                .parent()
                                .after(
                                    "<span id='sponsor-info-message' style='color:red'>" +
                                        response.message +
                                        "<span>"
                                );
                            $(self).parent().next().fadeOut(5000);
                        } else {
                            $(self).parent().after("<span id='sponsor-info-message' style='color:green'>" + response.message + "<span>");
                            $(self).parent().next().fadeOut(5000);
                        }
                     }).fail(function (response) {
                        $("body").trigger('processStop');
                        $(self)
                            .parent()
                            .after(
                                "<span id='sponsor-info-message' style='color:red'>" +
                                    response.responseJSON.message +
                                    "<span>"
                            );
                        $(self).parent().next().fadeOut(5000);
                    });
                  }
            });
            $('body').on('keyup', "#sponsor_name", function () {
                var sponsorUrlVal = $(this).val();
                console.log(sponsorUrlVal)
                    $("#sponsor_name").val(sponsorUrlVal.normalize('NFD').replace(/([\u0300-\u036f]|[^a-z])/g, ''));
                });
            $('body').on('change', "#sponsor_name",  function () {
                self.callAjaxFunction();
            });

        },
        callAjaxFunction: function () {
            var self = this;
            // $(self.options.button).addClass('disabled');
            var shopUrlVal = $("#sponsor_name").val();
            // $(self.options.available).remove();
            // $(self.options.unavailable).remove();
            if (shopUrlVal) {
                // $(self.options.pageLoader).removeClass('no-display');
                $.ajax({
                    type: "POST",
                    url: url.build('mlm/customer/verifysponsor'),
                    data: {
                        profileurl: shopUrlVal
                    },
                    success: function (response) {

                        console.log(response);
                        if (response === 0) {
                            $("#sponsor_name").parent().after("<span id='sponsor-info-message' style='color:green'>" + 'Parabéns! O nome escolhido está perfeito!'+ "<span>");
                            $("#sponsor_name").parent().next().fadeOut(10000);

                        } else {
                            $("#sponsor_name")
                                .parent()
                                .after(
                                    "<span id='sponsor-info-message' style='color:red'>" +
                                        'Ops! Você não pode usar esse nome. Tente novamente!' +
                                        "<span>"
                                );
                            $("#sponsor_name").parent().next().fadeOut(10000);
                        }
                    },
                    error: function (response) {
                        alert({
                            content: $t('Ocorreu um erro durante a verificação dos dados da loja do vendedor')
                        });
                    }
                });
            }
        }
    });
});
