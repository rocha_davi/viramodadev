/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint jquery:true*/
define([
    "jquery",
    'mage/translate',
    "mage/template",
    "mage/mage"
], function ($, $t,mageTemplate, alert) {
    'use strict';
    $.widget('mage.genealogyTree', {
        options: {},
        _create: function () {
            var self = this;
            if (self.options.tree.length) {
                $(self.options.tree).each(function(index, value){
                    for (let key in value) {
                        $(value[key]).each(function(key, data){
                            self.insertNode(data);
                        });
                    }
                });
            }
        },
        insertNode: function(data) {
            var addRowNow;
            this.addRow = mageTemplate('#treeNodeTemplate');
                addRowNow = this.addRow({
                    data: data
                });
                var parent = $("#containerFor" + data.sponsor_reference_code).length ? $("#containerFor" + data.sponsor_reference_code) : $(".tree-container");
                parent.append(addRowNow);
            },

    });
    return $.mage.genealogyTree;
});
