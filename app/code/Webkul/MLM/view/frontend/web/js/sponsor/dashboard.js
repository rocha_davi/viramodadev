/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define([
    "uiComponent",
    "ko",
    "jquery",
    "mage/translate",
    "mage/storage",
    "Magento_Ui/js/modal/modal",
    "Magento_Ui/js/modal/alert",
], function (Component, ko, $, $t, storage, modal, alert) {
    return Component.extend({
        defaults: {
            template: "Webkul_MLM/sponsor/dashboard",
        },
        salesEarning: ko.observable(),
        newJoiningSalesEarning: ko.observable(),
        levelSalesEarning: ko.observable(),
        totalBusiness: ko.observable(),
        currentMonthSales: ko.observable(),
        downlineMembersCount: ko.observable(),
        getGenealogyTreeUrl: ko.observable(),
        becomeDiamondMemberThreshold: ko.observable(),
        mySponsorId: ko.observable(),
        referralSponsorId: ko.observable(),
        referralSponsorName: ko.observable(),
        referralSponsorEmail: ko.observable(),
        getSponsorStatisticsGraphUrl: ko.observable(),
        getBadgeImageUrl: ko.observable(),
        getMemberLevelName: ko.observable(),
        getDailyEarningChartData: ko.observable(),
        getWeeklyEarningChartData: ko.observable(),
        getMonthlyEarningChartData: ko.observable(),
        getYearlyEarningChartData: ko.observable(),
        getLast4MemberActivities: ko.observable(),

        initialize: function () {
            this._super();
            var self = this;
            self.salesEarning(window.salesEarning);
            self.newJoiningSalesEarning(window.newJoiningSalesEarning);
            self.levelSalesEarning(window.levelSalesEarning);
            self.totalBusiness(window.totalBusiness);
            self.currentMonthSales(window.currentMonthSales);
            self.downlineMembersCount(window.downlineMembersCount);
            self.getGenealogyTreeUrl(window.getGenealogyTreeUrl);
            self.becomeDiamondMemberThreshold(
                window.becomeDiamondMemberThreshold
            );
            self.mySponsorId(window.mySponsorId);
            self.referralSponsorId(window.referralSponsorId);
            self.referralSponsorName(window.referralSponsorName);
            self.referralSponsorEmail(window.referralSponsorEmail);
            self.getSponsorStatisticsGraphUrl(
                window.getSponsorStatisticsGraphUrl
            );
            self.getBadgeImageUrl(window.getBadgeImageUrl);
            self.getMemberLevelName(window.memberLevelName);
            self.getLast4MemberActivities(JSON.parse(window.last4MemberActivities));

            this.getDailyEarningChartData(JSON.parse(window.dailyEarningChartData));
            this.getWeeklyEarningChartData(JSON.parse(window.weeklyEarningChartData));
            this.getMonthlyEarningChartData(JSON.parse(window.monthlyEarningChartData));
            this.getYearlyEarningChartData(JSON.parse(window.yearlyEarningChartData));

            self.drawMonthlyGraph();
        },

        drawGraph: function (data) {
            google.charts.load("current", { packages: ["line"] });
            google.charts.setOnLoadCallback(drawChart);

            function drawChart() {
                var chart = new google.charts.Line(
                    document.getElementById("mlm-dashboard-chart-content")
                );
                var options = {
                    curveType: "function",
                    height: 306,
                };
                chart.draw(data, google.charts.Line.convertOptions(options));
            }
        },

        drawYealyGraph: function () {
            google.charts.load("current", { packages: ["line"] });
            
            const drawChart = () => {
                var data = new google.visualization.DataTable();
                data.addColumn("string", "Mês");
                data.addColumn("number", "Bônus de Nível");
                data.addColumn("number", "Unificado");
                data.addColumn("number", "Venda");

                data.addRows(this.getYearlyEarningChartData());
                this.drawGraph(data);
            }
            google.charts.setOnLoadCallback(drawChart);
        },

        drawMonthlyGraph: function () {
            google.charts.load("current", { packages: ["line"] });
            const drawChart = () => {
                var data = new google.visualization.DataTable();
                data.addColumn("string", "Dia");
                data.addColumn("number", "Bônus de Nível");
                data.addColumn("number", "Unificado");
                data.addColumn("number", "Venda");
    
                data.addRows(this.getMonthlyEarningChartData());
                this.drawGraph(data);
            };
            google.charts.setOnLoadCallback(drawChart);
        },

        drawWeeklyGraph: function () {
            google.charts.load("current", { packages: ["line"] });
            const drawChart = () => {

                var data = new google.visualization.DataTable();
                data.addColumn("string", "Semana");
                data.addColumn("number", "Bônus de Nível");
                data.addColumn("number", "Unificado");
                data.addColumn("number", "Venda");

                data.addRows(this.getWeeklyEarningChartData());
                this.drawGraph(data);
            }
            google.charts.setOnLoadCallback(drawChart);
        },

        drawDailyGraph: function () {
            google.charts.load("current", { packages: ["line"] });
            const drawChart = () => {

                var data = new google.visualization.DataTable();
                data.addColumn("string", "Hora");
                data.addColumn("number", "Bônus de Nível");
                data.addColumn("number", "Unificado");
                data.addColumn("number", "Venda");

                data.addRows(this.getDailyEarningChartData());
                this.drawGraph(data);
            }
            google.charts.setOnLoadCallback(drawChart);
        },


    });
});