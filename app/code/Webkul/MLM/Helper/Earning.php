<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

use Magento\Framework\DB\Sql\Expression as SqlExpression;
use Webkul\MLM\Model\Sponsors\Source\Status as SponsorStatus;

class Earning extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param Data $dataHelper
     * @param \Webkul\MLM\Model\ResourceModel\EarningType\CollectionFactory $earningTypeCollF
     * @param \Webkul\MLM\Model\ResourceModel\SponsorEarning\CollectionFactory $sponsorEarningCollF
     * @param \Webkul\MLM\Model\ResourceModel\Sponsors\CollectionFactory $sponsorsCollF
     * @param \Webkul\MLM\Model\ResourceModel\SponsorSponsor\CollectionFactory $sponsorSponsorCollF
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Customer\Model\CustomerFactory $customerF
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        Data $dataHelper,
        \Webkul\MLM\Model\ResourceModel\EarningType\CollectionFactory $earningTypeCollF,
        \Webkul\MLM\Model\ResourceModel\SponsorEarning\CollectionFactory $sponsorEarningCollF,
        \Webkul\MLM\Model\ResourceModel\Sponsors\CollectionFactory $sponsorsCollF,
        \Webkul\MLM\Model\ResourceModel\SponsorSponsor\CollectionFactory $sponsorSponsorCollF,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Customer\Model\CustomerFactory $customerF
    ) {
        parent::__construct($context);
        $this->dataHelper = $dataHelper;
        $this->earningTypeCollF = $earningTypeCollF;
        $this->sponsorEarningCollF = $sponsorEarningCollF;
        $this->dateTime = $dateTime;
        $this->sponsorsCollF = $sponsorsCollF;
        $this->sponsorSponsorCollF = $sponsorSponsorCollF;
        $this->customerF = $customerF;
    }

    /**
     * @param int $sponsorId
     * @return float
     */
    public function getTotalSalesEarningAmt($sponsorId)
    {
        $salesEarningTypeId = $this->getSalesEarningTypeId();
        $salesEarningAmount = $this->sponsorEarningCollF->create()
            ->addFieldToFilter("sponsor_id", $sponsorId)
            ->addFieldToFilter("earning_type_id", $salesEarningTypeId);
        $salesEarningAmount->getSelect()
            ->columns([
                "total_sales_earning" => new SqlExpression("SUM(amount)")
            ]);
        $salesEarningAmount = $salesEarningAmount->getFirstItem()
            ->getTotalSalesEarning();
        return $salesEarningAmount;
    }

    /**
     * @param int $sponsorId
     * @return float
     */
    public function getTotalJoiningEarningAmt($sponsorId)
    {
        $salesEarningTypeId = $this->getJoiningEarningTypeId();
        $salesEarningAmount = $this->sponsorEarningCollF->create()
            ->addFieldToFilter("sponsor_id", $sponsorId)
            ->addFieldToFilter("earning_type_id", $salesEarningTypeId);
        $salesEarningAmount->getSelect()
            ->columns([
                "total_sales_earning" => new SqlExpression("SUM(amount)")
            ]);
        $salesEarningAmount = $salesEarningAmount->getFirstItem()
            ->getTotalSalesEarning();
        return $salesEarningAmount;
    }

    /**
     * @param int $sponsorId
     * @return float
     */
    public function getTotalLevelEarningAmt($sponsorId)
    {
        $salesEarningTypeId = $this->getLevelBonusEarningTypeId();
        $salesEarningAmount = $this->sponsorEarningCollF->create()
            ->addFieldToFilter("sponsor_id", $sponsorId)
            ->addFieldToFilter("earning_type_id", $salesEarningTypeId);
        $salesEarningAmount->getSelect()
            ->columns([
                "total_sales_earning" => new SqlExpression("SUM(amount)")
            ]);
        $salesEarningAmount = $salesEarningAmount->getFirstItem()
            ->getTotalSalesEarning();
        return $salesEarningAmount;
    }

    /**
     * @param int $sponsorId
     * @return float
     */
    public function getTotalEarningAmt($sponsorId)
    {
        $salesEarningAmount = $this->sponsorEarningCollF->create()
            ->addFieldToFilter("sponsor_id", $sponsorId)
            ->addFieldToFilter("amount", ['gt' => "0"]);
        $salesEarningAmount->getSelect()
            ->columns([
                "total_sales_earning" => new SqlExpression("SUM(amount)")
            ]);
        $salesEarningAmount = $salesEarningAmount->getFirstItem()
            ->getTotalSalesEarning();
        return $salesEarningAmount;
    }

    /**
     * @return DataObject
     */
    public function getMonthlyDateDomain()
    {
        $from = date("Y-m-01 00:00:00");
        $fromdate = date_create($from);
        $from = date_format($fromdate, 'Y-m-d H:i:s');
        
        $to = date('Y-m-d 23:59:59');
        $todate = date_create($to);
        $to = date_format($todate, 'Y-m-d 23:59:59');
        $monthlyDate = new \Magento\Framework\DataObject();
        $monthlyDate->setFromDate($from)
            ->setToDate($to);
        return $monthlyDate;
    }

    /**
     * @param int $sponsorId
     * @return float
     */
    public function getMonthlySalesEarning($sponsorId)
    {
        return $this->getMonthlyEarningAmt($sponsorId, $this->getSalesEarningTypeId());
    }
    
    /**
     * @param int $sponsorId
     * @return float
     */
    public function getMonthlyJoiningEarning($sponsorId)
    {
        return $this->getMonthlyEarningAmt($sponsorId, $this->getJoiningEarningTypeId());
    }
    
    /**
     * @param int $sponsorId
     * @return float
     */
    public function getMonthlyBonusEarning($sponsorId)
    {
        return $this->getMonthlyEarningAmt($sponsorId, $this->getLevelBonusEarningTypeId());
    }

    /**
     * @param int $sponsorId
     * @param int|null $earningTypeId
     * @return float
     */
    public function getMonthlyEarningAmt($sponsorId, $earningTypeId = false)
    {
        $monthlyDate = $this->getMonthlyDateDomain();
        $salesEarningAmount = $this->sponsorEarningCollF->create()
            ->addFieldToFilter("sponsor_id", $sponsorId)
            ->addFieldToFilter("amount", ['gt' => 0])
            ->addFieldToFilter(
                'main_table.created_at',
                ['datetime' => true, 'from' => $monthlyDate->getFromDate(), 'to' => $monthlyDate->getToDate()]
            );
        if ($earningTypeId) {
            $salesEarningAmount->addFieldToFilter(
                "earning_type_id",
                $earningTypeId
            );
        }
        $salesEarningAmount->getSelect()
            ->columns([
                "total_sales_earning" => new SqlExpression("SUM(amount)")
            ]);
        $salesEarningAmount = $salesEarningAmount->getFirstItem()
            ->getTotalSalesEarning();
        return $salesEarningAmount;
    }

    /**
     * @param float $salesEarningAmount
     * @param int $earningTypeId
     * @return Earning\Collection
     */
    public function getEarningTotalAmountColl($salesEarningAmount, $earningTypeId)
    {
        $salesEarningAmount->addFieldToFilter(
            "earning_type_id",
            $earningTypeId
        );
        $salesEarningAmount->getSelect()
            ->columns([
                "total_sales_earning" => new SqlExpression("IFNULL(SUM(amount),0)")
            ]);
        $salesEarningAmount = $salesEarningAmount->getFirstItem()
            ->getTotalSalesEarning();

        return (float)$salesEarningAmount;
    }

    /**
     * @param int $sponsorId
     * @param int $earningTypeId
     * @return array
     */
    public function getDailyEarningChartData($sponsorId, $earningTypeId = false)
    {
        $hourlyEarnings = [];
        for ($h = 0; $h < 24; $h++) {
            $hours = (string)$h;
            if (strlen($hours) == 1) {
                $hours = "0".$hours;
            }
            $from = date("Y-m-d $hours:00:00");
            $fromdate = date_create($from);
            $from = date_format($fromdate, 'Y-m-d H:i:s');
            
            $to = date("Y-m-d $hours:59:59");
            $todate = date_create($to);
            $to = date_format($todate, 'Y-m-d H:i:s');
            
            $salesEarningAmount = $this->sponsorEarningCollF->create()
                ->addFieldToFilter("sponsor_id", $sponsorId)
                ->addFieldToFilter("amount", ['gt' => 0])
                ->addFieldToFilter(
                    'main_table.created_at',
                    ['datetime' => true, 'from' => $from, 'to' => $to]
                );
            $joining = $this->getEarningTotalAmountColl(
                clone $salesEarningAmount,
                $this->getJoiningEarningTypeId()
            );
            $sale = $this->getEarningTotalAmountColl(
                clone $salesEarningAmount,
                $this->getSalesEarningTypeId()
            );
            $level = $this->getEarningTotalAmountColl(
                clone $salesEarningAmount,
                $this->getLevelBonusEarningTypeId()
            );
            $row = [];
            $row[] = $hours;
            $row[] = $level;
            $row[] = $joining;
            $row[] = $sale;
            $hourlyEarnings[] = $row;
        }
        return $hourlyEarnings;
    }

    /**
     * @param int $sponsorId
     * @param int $earningTypeId
     * @return array
     */
    public function getWeeklyEarningChartData($sponsorId, $earningTypeId = false)
    {
        $weeklyEarnings = [];
        $weekStartDay = date('d') - date('w') - 1;
        $startDateTimeStamp = strtotime(date("Y-m-$weekStartDay 00:00:00"));
        $v = function ($val) {
            return $val;
        };
        for ($day = 1; $day <= 7; $day++) {
            $currentDayTimeStamp = strtotime("+$day day{$v($day > 1 ? 's' : '')}", $startDateTimeStamp);
            $weekName = date('D', $currentDayTimeStamp);
            $from = date("Y-m-d H:i:s", $currentDayTimeStamp);
            $fromdate = date_create($from);
            $from = date_format($fromdate, 'Y-m-d H:i:s');

            $to = date("Y-m-d 23:59:59", $currentDayTimeStamp);
            $todate = date_create($to);
            $to = date_format($todate, 'Y-m-d H:i:s');

            $salesEarningAmount = $this->sponsorEarningCollF->create()
                ->addFieldToFilter("sponsor_id", $sponsorId)
                ->addFieldToFilter("amount", ['gt' => 0])
                ->addFieldToFilter(
                    'main_table.created_at',
                    ['datetime' => true, 'from' => $from, 'to' => $to]
                );
            $joining = $this->getEarningTotalAmountColl(
                clone $salesEarningAmount,
                $this->getJoiningEarningTypeId()
            );
            $sale = $this->getEarningTotalAmountColl(
                clone $salesEarningAmount,
                $this->getSalesEarningTypeId()
            );
            $level = $this->getEarningTotalAmountColl(
                $salesEarningAmount,
                $this->getLevelBonusEarningTypeId()
            );
            $row = [];
            $row[] = $weekName;
            $row[] = $level;
            $row[] = $joining;
            $row[] = $sale;
            $weeklyEarnings[] = $row;
        }
        return $weeklyEarnings;
    }

    /**
     * @param int $sponsorId
     * @param int $earningTypeId
     * @return array
     */
    public function getMonthlyEarningChartData($sponsorId, $earningTypeId = false)
    {
        $monthlyEarnings = [];
        $m = date('m');
        $startDateTimeStamp = strtotime(date("Y-$m-1 00:00:00"));
        $lastDay = date('t');
        $v = function ($val) {
            return $val;
        };
        for ($day = 0; $day < $lastDay; $day++) {
            $currentDayTimeStamp = strtotime("+$day day{$v($day > 1 ? 's' : '')}", $startDateTimeStamp);
            $from = date("Y-m-d H:i:s", $currentDayTimeStamp);
            $fromdate = date_create($from);
            $from = date_format($fromdate, 'Y-m-d H:i:s');

            $to = date("Y-m-d 23:59:59", $currentDayTimeStamp);
            $todate = date_create($to);
            $to = date_format($todate, 'Y-m-d H:i:s');

            $salesEarningAmount = $this->sponsorEarningCollF->create()
                ->addFieldToFilter("sponsor_id", $sponsorId)
                ->addFieldToFilter("amount", ['gt' => 0])
                ->addFieldToFilter(
                    'main_table.created_at',
                    ['datetime' => true, 'from' => $from, 'to' => $to]
                );
            $joining = $this->getEarningTotalAmountColl(
                clone $salesEarningAmount,
                $this->getJoiningEarningTypeId()
            );
            $sale = $this->getEarningTotalAmountColl(
                clone $salesEarningAmount,
                $this->getSalesEarningTypeId()
            );
            $level = $this->getEarningTotalAmountColl(
                $salesEarningAmount,
                $this->getLevelBonusEarningTypeId()
            );
            $row = [];
            $row[] = (string)($day +1);
            $row[] = $level;
            $row[] = $joining;
            $row[] = $sale;
            $monthlyEarnings[] = $row;
        }
        return $monthlyEarnings;
    }

    /**
     * @param int $sponsorId
     * @param int $earningTypeId
     * @return array
     */
    public function getYearlyEarningChartData($sponsorId, $earningTypeId = false)
    {
        $yearlyEarnings = [];
        $startDateTimeStamp = strtotime(date("Y-1-1 00:00:00"));
        $v = function ($val) {
            return $val;
        };
        for ($month = 0; $month <= 11; $month++) {
            $currentMonthTimeStamp = strtotime("+$month month{$v($month > 1 ? 's' : '')}", $startDateTimeStamp);
            $from = date("Y-m-d H:i:s", $currentMonthTimeStamp);
            $fromdate = date_create($from);
            $from = date_format($fromdate, 'Y-m-d H:i:s');
            $to = date("Y-m-t 23:59:59", $currentMonthTimeStamp);
            $todate = date_create($to);
            $to = date_format($todate, 'Y-m-d H:i:s');

            $salesEarningAmount = $this->sponsorEarningCollF->create()
                ->addFieldToFilter("sponsor_id", $sponsorId)
                ->addFieldToFilter("amount", ['gt' => 0])
                ->addFieldToFilter(
                    'main_table.created_at',
                    ['datetime' => true, 'from' => $from, 'to' => $to]
                );
            $joining = $this->getEarningTotalAmountColl(
                clone $salesEarningAmount,
                $this->getJoiningEarningTypeId()
            );
            $sale = $this->getEarningTotalAmountColl(
                clone $salesEarningAmount,
                $this->getSalesEarningTypeId()
            );
            $level = $this->getEarningTotalAmountColl(
                $salesEarningAmount,
                $this->getLevelBonusEarningTypeId()
            );
            $monthName = date('M', $currentMonthTimeStamp);
            $row = [];
            $row[] = $monthName;
            $row[] = $level;
            $row[] = $joining;
            $row[] = $sale;
            $yearlyEarnings[] = $row;
        }
        return $yearlyEarnings;
    }

    /**
     * @param int $sponsorId
     * @return float
     */
    public function getCurrentBalance($sponsorId)
    {
        $salesEarningAmount = $this->sponsorEarningCollF->create()
            ->addFieldToFilter("sponsor_id", $sponsorId);
        $salesEarningAmount->getSelect()
        ->columns([
                "total_sales_earning" => new SqlExpression("SUM(amount)")
            ]);
        $salesEarningAmount = $salesEarningAmount->getFirstItem()
            ->getTotalSalesEarning();
        return $salesEarningAmount;
    }

    /**
     * @param int $sponsorId
     * @return array
     */
    public function getLast4MemberActivities($sponsorId)
    {
        $downlineMembersColl = $this->sponsorsCollF->create()->addFieldToFilter(
            'parent_sponsor_id',
            $sponsorId
        )->addFieldToFilter(
            'status',
            SponsorStatus::STATUS_ENABLED
        );
        $downlineMemberEarnings = [];
        $items = $downlineMembersColl->getItems();
        foreach ($items as $item) {
            $earningColl = $this->sponsorEarningCollF->create()
            ->addFieldToFilter(
                'sponsor_id',
                $item->getId()
            )->setOrder('created_at');
            $limit = $earningColl->getSize() < 4 ? $earningColl->getSize() : 4;
            foreach ($earningColl as $earning) {
                $downlineMemberEarnings[] = $earning;
                $limit --;
                if ($limit < 1) {
                    break;
                }
            }
        }
        shuffle($downlineMemberEarnings);
        $last4Earnings = array_chunk($downlineMemberEarnings, 4)[0] ?? [];

        $formattedEarnings = [];
        foreach ($last4Earnings as $earning) {
            $sponsorName = $this->getSponsorName($earning->getSponsorId());
            $earningTypeName = $this->getEarningTypeLabel($earning->getEarningTypeId());
            $earningDate = $this->dataHelper->formatDateMedium($earning->getCreatedAt());
            $formattedEarnings[] = [
                $sponsorName,
                $earningTypeName,
                $earningDate,
            ];
        }

        return $formattedEarnings;
    }

    /**
     * @param int $sponsorId
     * @return string
     */
    public function getSponsorName($sponsorId)
    {
        $customerId = $this->sponsorsCollF->create()
            ->addFieldToFilter('main_table.entity_id', $sponsorId)
            ->getFirstItem()
            ->getCustomerId();
        return $this->customerF->create()->load($customerId)->getName();
    }

    /**
     * @param int $sponsorId
     * @return int
     */
    public function getSponsorId($customerId)
    {
        $sponsor = $this->sponsorsCollF->create()
            ->addFieldToFilter('customer_id', $customerId)->getFirstItem();
        return $sponsor->getId();
    }

    /**
     * @return int
     */
    public function getSalesEarningTypeId()
    {
        $earningType = $this->earningTypeCollF->create()
            ->addFieldToFilter('value', "sale")->getFirstItem();
        return $earningType->getId();
    }

    /**
     * @return int
     */
    public function getJoiningEarningTypeId()
    {
        $earningType = $this->earningTypeCollF->create()
            ->addFieldToFilter('value', "joining")->getFirstItem();
        return $earningType->getId();
    }

    /**
     * @return int
     */
    public function getLevelBonusEarningTypeId()
    {
        $earningType = $this->earningTypeCollF->create()
            ->addFieldToFilter('value', "level_bonus")->getFirstItem();
        return $earningType->getId();
    }

    /**
     * @return int
     */
    public function getPaymentRequestEarningTypeId()
    {
        $earningType = $this->earningTypeCollF->create()
            ->addFieldToFilter('value', "transfer_bank")->getFirstItem();
        return $earningType->getId();
    }

    /**
     * @param int $earningTypeId
     * @return string
     */
    public function getEarningTypeLabel($earningTypeId)
    {
        $earningType = $this->earningTypeCollF->create()
            ->addFieldToFilter('entity_id', $earningTypeId)->getFirstItem();
        return $earningType->getLabel();
    }
}
