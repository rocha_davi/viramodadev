<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\DB\Sql\Expression as SqlExpression;
use Webkul\MLM\Model\Sponsors\Source\Status as SponsorStatus;

/**
 * Class Data
 * $this->_moduleManager
 * $this->_logger
 * $this->_request
 * $this->_urlBuilder
 * $this->_httpHeader
 * $this->_eventManager
 * $this->_remoteAddress
 * $this->_cacheConfig
 * $this->urlEncoder
 * $this->urlDecoder
 * $this->scopeConfig
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const EMPTY_STRING = "";

    /**
     * Store scope
     * @var string
     */
    private $_storeScope= \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

    /**
     * @var \Webkul\MLM\Model\SponsorsFactory $sponsorsFactory
     */
    protected $sponsorsFactory;

    /**
     * @var HttpContext
     */
    private $httpContext;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Pricing\Helper\Data $priceHelper
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Directory\Helper\Data $helperDirectory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Webkul\MLM\Model\SponsorsFactory $sponsorsFactory
     * @param \Webkul\MLM\Model\MemberLevelFactory $memberLevelF
     * @param \Magento\Framework\Math\Random $mathRandom
     * @param HttpContext $httpContext
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Magento\Directory\Model\Currency $currency,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Directory\Helper\Data $helperDirectory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Webkul\MLM\Model\SponsorsFactory $sponsorsFactory,
        \Webkul\MLM\Model\MemberLevelFactory $memberLevelF,
        \Magento\Framework\Math\Random $mathRandom,
        \Webkul\MLM\Model\ResourceModel\Sponsors\CollectionFactory $sponsorsCollF,
        \Webkul\MLM\Model\SponserConfigurationsFactory $sponsorConfigurationFactory,
        HttpContext $httpContext
    ) {
        parent::__construct($context);
        $this->jsonHelper = $jsonHelper;
        $this->sponsorsFactory = $sponsorsFactory;
        $this->storeManager = $storeManager;
        $this->priceHelper = $priceHelper;
        $this->currency = $currency;
        $this->httpContext = $httpContext;
        $this->magentoDirectoryHelper = $helperDirectory;
        $this->localeDate = $localeDate;
        $this->memberLevelF = $memberLevelF;
        $this->mathRandom = $mathRandom;
        $this->urlInterface = $context->getUrlBuilder();
        $this->sponsorsCollF = $sponsorsCollF;
        $this->sponsorConfiguration = $sponsorConfigurationFactory;
    }

    /**
     * @return Object
     */
    public function getJsonHelper()
    {
        return $this->jsonHelper;
    }

    /**
     * @return bool
     */
    public function isSponsorExist()
    {
        $sponsorStatus = 0;
        $sponsorId = $this->getCustomerId();
        $collection = $this->sponsorsFactory->create()->getCollection();
        $collection->addFieldToFilter("customer_id", $sponsorId);
        foreach ($collection as $model) {
            $sponsorStatus = $model->getStatus() ? $model->getStatus() : 2;
        }
        return $sponsorStatus;
    }

    /**
     * Return Customer id.
     *
     * @return bool|0|1
     */
    public function getCustomerId()
    {
        return $this->httpContext->getValue('customer_id');
    }

    /**
     * Configuration Values
     *
     * @param string $path
     * @return string
     */
    public function getConfigData($path) : string
    {
        /**
         * @var object \Magento\Framework\App\Config\ScopeConfigInterface
         */
        $value = $this->scopeConfig->getValue(
            $path,
            $this->_storeScope
        );
        return $value ?? self::EMPTY_STRING;
    }

    /**
     * @return bool
     */
    public function isModuleEnabled()
    {
        return (bool)$this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::CONFIGURATION_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::ENABLE
        );
    }

    /**
     * @return sting
     */
    public function getSponsorDetailsCMS()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::CONFIGURATION_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SPONSOR_DETAILS_CMS
        );
    }

    /**
     * @return float
     */
    public function getMinSponsorRequestAmt()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::CONFIGURATION_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::MIN_SPONSOR_REQUEST_AMOUNT
        );
    }

    /**
     * @return float
     */
    public function getDiamondMemberThreshold()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::CONFIGURATION_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::DIAMOND_MEMBER_THRESHOLD
        );
    }

    /**
     * @return string
     */
    public function getAdminSponsorId()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::CONFIGURATION_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::ADMIN_SPONSOR_ID
        );
    }

    /**
     * @return float
     */
    public function getMLMSalesCommission()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SALES_COMMISSION_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SALES_COMMISSION
        );
    }

    /**
     * @return float
     */
    public function getMLMSalesCommissionRemaining()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SALES_COMMISSION_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SALES_COMMISSION_REMAINING
        );
    }

    /**
     * @return string
     */
    public function getMLMSalesCommissionLevelRate()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SALES_COMMISSION_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::LEVEL_COMMISSION_RATE
        );
    }

    /**
     * @return bool
     */
    public function isSponsorJoiningEnabled()
    {
        return (bool)$this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SPONSOR_JOINING_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::ENABLE_SPONSOR_JOINING
        );
    }

    /**
     * @return float
     */
    public function getTotalAmountOnJoiningAlot()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SPONSOR_JOINING_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::TOTAL_AMOUNT_ON_JOINING_ALOT
        );
    }

    /**
     * @return flaot
     */
    public function getJoiningCommission()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SPONSOR_JOINING_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::JOINING_COMMISSION_PERCENT
        );
    }

    /**
     * @return float
     */
    public function getJoiningCommissionRemaining()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SPONSOR_JOINING_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::JOINING_COMMISSION_REAMINING_PERCENT
        );
    }

    /**
     * @return string
     */
    public function getJoiningLevelCommissionRate()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SPONSOR_JOINING_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::LEVEL_COMMISSION_RATE
        );
    }

    /**
     * @return bool
     */
    public function isLevelCommissionEnabled()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SPONSOR_LEVEL_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::ENABLE_LEVEL_COMMISSION
        );
    }

    /**
     * @return float
     */
    public function getLevelCommissionPercent()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SPONSOR_LEVEL_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::LEVEL_COMMISSION_PERCENT
        );
    }

    /**
     * @return float
     */
    public function getLevelCommissionRemainingPercent()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SPONSOR_LEVEL_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::LEVEL_COMMISSION_REAMINING_PERCENT
        );
    }

    /**
     * @return string
     */
    public function getLevelCommissionLevelRate()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SPONSOR_LEVEL_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::LEVEL_COMMISSION_RATE
        );
    }

    /**
     * @return string
     */
    public function getPaymentOptions()
    {
        $paymentMethods = $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::PAYMENT_OPTIONS_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::PAYMENT_OPTIONS
        );
        $paymentMethods = trim($paymentMethods, "\s,");
        return $paymentMethods;
    }

    /**
     * @return string
     */
    public function getReferalEmailTemplate()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SPONSOR_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::REFERAL_EMAIL_TEMPLATE
        );
    }

    /**
     * @return string
     */
    public function getAdminEmailAddress()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SPONSOR_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::EMAIL_IDENTITY
        );
    }

    /**
     * @return string
     */
    public function getEmailContent()
    {
        return $this->getConfigData(
            ConfigConstants::SECTION . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::SPONSOR_GROUP . ConfigConstants::DIR_SEPARATOR .
            ConfigConstants::EMAIL_CONTENT
        );
    }

    /**
     * @param float $price
     * @return string
     */
    public function getFormattedPrice($price)
    {
        $fromCurrency = $this->getCurrentCurrencyCode();
        $toCurrency = $this->getBaseCurrencyCode();
        $amount = $this->getCurrencyConvertedAmt($fromCurrency, $toCurrency, $price);
        return $this->priceHelper
            ->currency($amount, true, false);
    }

    /**
     * @return string
     */
    public function getCurrentCurrencyCode()
    {
        return $this->storeManager->getStore()->getCurrentCurrencyCode();
    }

    /**
     * @return string
     */
    public function getBaseCurrencyCode()
    {
        return $this->storeManager->getStore()->getBaseCurrencyCode();
    }

    /**
     * @return array
     */
    public function getConfigAllowCurrencies()
    {
        return $this->currency->getConfigAllowCurrencies();
    }

    /**
     * @return string
     */
    public function getCurrentCurrency()
    {
        return $this->storeManager->getStore()->getCurrentCurrency()->getCurrencyCode();
    }

    /**
     * @param string $fromCurrency
     * @param string $toCurrency
     * @param float $amount
     * @return float
     */
    public function getCurrencyConvertedAmt($fromCurrency, $toCurrency, $amount)
    {
        $currentCurrencyCode = $this->getCurrentCurrencyCode();
        $baseCurrencyCode = $this->getBaseCurrencyCode();
        $allowedCurrencies = $this->getConfigAllowCurrencies();
        $rates = $this->getCurrencyRates(
            $baseCurrencyCode,
            array_values($allowedCurrencies)
        );
        if (empty($rates[$fromCurrency])) {
            $rates[$fromCurrency] = 1;
        }

        if ($baseCurrencyCode == $toCurrency) {
            $currencyAmount = $amount/$rates[$fromCurrency];
        } else {
            $amount = $amount/$rates[$fromCurrency];
            $currencyAmount = $this->convertCurrency($amount, $baseCurrencyCode, $toCurrency);
        }
        return $currencyAmount;
    }

    /**
     * @param string $currency
     * @param string $toCurrencies
     * @return array
     */
    public function getCurrencyRates($currency, $toCurrencies = null)
    {
        return $this->currency->getCurrencyRates($currency, $toCurrencies);
    }

    /**
     * @param float $amount
     * @param string $from
     * @param string $to
     * @return float
     */
    public function convertCurrency($amount, $from, $to)
    {
        $finalAmount = $this->magentoDirectoryHelper
            ->currencyConvert($amount, $from, $to);

        return $finalAmount;
    }

    /**
     * Retrieve formatting date
     *
     * @param null|string|\DateTimeInterface $date
     * @param int $format
     * @param bool $showTime
     * @param null|string $timezone
     * @return string
     */
    public function formatDate(
        $date = null,
        $format = \IntlDateFormatter::SHORT,
        $showTime = false,
        $timezone = null
    ) {
        $date = $date instanceof \DateTimeInterface ? $date : new \DateTime($date);
        return $this->localeDate->formatDateTime(
            $date,
            $format,
            $showTime ? $format : \IntlDateFormatter::NONE,
            null,
            $timezone
        );
    }

    /**
     * @param string $date
     * @return string
     */
    public function formatDateMedium($date)
    {
        return $this->formatDate(
            $date,
            \IntlDateFormatter::MEDIUM,
            true
        );
    }

    /**
     * Get object created at date
     *
     * @param string $createdAt
     * @return \DateTime
     */
    public function getDateTime($createdAt)
    {
        return $this->localeDate->date(new \DateTime($createdAt));
    }

    /**
     * @return string
     */
    public function generateTxnId()
    {
        $prefix = 'TXN';
        $transactionId = $this->mathRandom->getRandomString(20);
        $transactionId = $prefix . $transactionId;
        $transactionId = strtoupper($transactionId);
        return $transactionId;
    }

    /**
     * @return int
     */
    public function getDefaultMemberLevelId()
    {
        $id = $this->memberLevelF->create()->getCollection()
            ->setOrder(
                'maximum_business_amount',
                \Magento\Framework\Data\Collection::SORT_ORDER_ASC
            )->getFirstItem()
            ->getEntityId();
        return $id;
    }

    /**
     * @param float $number
     * @return string
     */
    public function formatNumber($number)
    {
        $number = (float)$number;
        return number_format($number, 4);
    }
    /**
     * getBaseUrl
     *
     * @return string
     */
    public function getBaseUrl($url, $customerId)
    {
        $finalUrl = $url;
        $storeCode = $this->getStoreCode();
       
        try {
            $currentUrl = $this->getCurrentUrl();
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/url.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info($this->getBaseUrlFromCurrentUrl());
            if ($this->isModuleEnabled() && ($url == $this->getBaseUrlFromCurrentUrl() || strpos($url, $storeCode)===true)) {
                $logger->info('597-if');
                    $currentStoreId = $this->storeManager->getStore()->getId();
                
                    $baseUrlData = explode('/', $this->getAdminBaseUrl());
                    $baseUrl = $baseUrlData[0].'//'.$baseUrlData[2];
                    
                    $logger->info('603'.$baseUrl);
                if (strpos($currentUrl, 'stores/store/redirect') !== false) {
                        
                    $logger->info('if');
                } else {
                    $logger->info('else');
                    if ($customerId && $this->isCustomerApprovedSponsor($customerId)) {
                        $sponsorData = $this->sponsorsCollF->create()
                        ->addFieldToFilter('customer_id', $customerId)->getFirstItem();
                        $logger->info(json_encode($sponsorData));
                        if ($sponsorData->getSponserName() != null) {
                            $logger->info(json_encode($sponsorData));
                            $finalUrl  = $baseUrlData[0].'//'.$sponsorData->getSponserName().'.'.$baseUrlData[2].'/';
                          
                            return $finalUrl;
                        }
                    } elseif ($this->checkSponsorExistsByCurrentUrl()) {
                        $sponsor = $this->checkSponsorExistsByCurrentUrl();
                        $sponsorData = $this->sponsorsCollF->create()
                        ->addFieldToFilter('customer_id', $sponsor->getCustomerId())->getFirstItem();
                        $logger->info(json_encode($sponsorData));
                        if ($sponsorData->getSponserName() != null) {
                            $logger->info(json_encode($sponsorData));
                            $finalUrl  = $baseUrlData[0].'//'.$sponsorData->getSponserName().'.'.$baseUrlData[2].'/';
                          
                            return $finalUrl;
                        }
                    }
                        
                }
                             
            }
        } catch (\Exception $e) {
            $e->getMessage();
        }
        $logger->info('624'.$finalUrl);
        return $finalUrl;
    }
    /**
     * Get Store code
     *
     * @return string
     */
    public function getStoreCode()
    {
        return $this->storeManager->getStore()->getCode();
    }
    /**
     * get getCurrentUrl
     *
     * @return string
     */
    public function getCurrentUrl()
    {
        return $this->urlInterface->getCurrentUrl();
    }
    /**
     * getBaseUrlFromCurrentUrl
     *
     * @return string
     */
    public function getBaseUrlFromCurrentUrl()
    {
        $sslurldata = explode('//', $this->getCurrentUrl());
        $urlString = $this->getBaseUrlWithoutSSLFromCurrentUrl();
        return $sslurldata[0].'//'.$urlString.'/';
    }
    public function getBaseUrlWithoutSSLFromCurrentUrl()
    {
        $urldata = explode('/', $this->getAdminBaseUrl());
      
        return $urldata[2];
    }
    /**
     * get main base url
     *
     * @return string
     */
    public function getAdminBaseUrl()
    {
        $secureEnabled = $this->scopeConfig->getValue(
            'web/secure/use_in_frontend',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if ($secureEnabled) {
            return $this->scopeConfig->getValue(
                'web/secure/base_url',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        }
        return $this->scopeConfig->getValue(
            'web/unsecure/base_url',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    public function isCustomerApprovedSponsor($customerId)
    {
        $sponsor = $this->sponsorsCollF->create()
        ->addFieldToFilter('customer_id', $customerId)->getFirstItem();
        // print_r($customerId);exit;
        return $sponsor->getStatus() == SponsorStatus::STATUS_ENABLED;
    }
    public function getCurrentSellerUrlFromPrevUrl($url)
    {
        
        return 'http://192.168.15.171/mage235/?sponsor=test';
    }
    /**
     * checkShopExistsByCurrentUrl
     *
     * @return boolean||integer
     */
    public function checkSponsorExistsByCurrentUrl()
    {
        $result = false;
        $currentUrl = $this->getCurrentUrl();
       
        if ($this->isModuleEnabled()) {
            $baseUrlData = explode('/', $currentUrl);
            $sponsorName = rtrim(strstr($baseUrlData[2], $this->getBaseUrlWithoutSSLFromCurrentUrl(), true), '.');
            if ($sponsorName != null) {
                $result = $this->getSponsorIdFromUrl($sponsorName);
               
            }
        }
       
        return $result;
    }

    public function getSponsorIdFromUrl($sponsorName)
    {
        $sponsorData = $this->sponsorsCollF
                        ->create()
                        ->addFieldToFilter('sponser_name', $sponsorName)
                        ->getFirstItem();
        return $sponsorData;
    }
    /**
     * get main domain url if url is not allowed
     *
     * @return string
     */
    public function getAllowedUrl()
    {
        $newUrl = "";
        
        $parsedData = parse_url($this->getCurrentUrl());
        // print_r($this->getBaseUrlWithoutSSLFromCurrentUrl());exit;
        $newUrl = $parsedData['scheme']
                . '://radhikagarg.'
                . $this->getBaseUrlWithoutSSLFromCurrentUrl()
                . (isset($parsedData['port']) ? ':' . $parsedData['port'] : '')
                . $parsedData['path']
                . (isset($parsedData['query']) ? ':' . $parsedData['query'] : '');
        
        return $newUrl;
    }
    public function getBaseUrlWithoutSsl()
    {
        $urldata = explode('/', $this->getAdminBaseUrl());
        $urlString = $urldata[2];
        return $urlString;
    }
}
