<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

class ConfigConstants extends \Magento\Framework\App\Helper\AbstractHelper
{
    const SECTION = "mlm";
    const DIR_SEPARATOR = "/";
    
    const CONFIGURATION_GROUP = "configuration";
    const ENABLE = "enable";
    const SPONSOR_DETAILS_CMS = "sponsor_details_cms";
    const MIN_SPONSOR_REQUEST_AMOUNT = "min_sponsor_request_amount";
    const DIAMOND_MEMBER_THRESHOLD = "diamond_member_threshold";
    const ADMIN_SPONSOR_ID = "admin_sponsor_id";
    
    const SALES_COMMISSION_GROUP = "sales_commission";
    const SALES_COMMISSION = "commisssion_mlm_sales";
    const SALES_COMMISSION_REMAINING = "commisssion_remaining";
    const LEVEL_COMMISSION_RATE = "level_commission_rate";
    
    const SPONSOR_JOINING_GROUP = "sponsor_joining_amount_settings";
    const ENABLE_SPONSOR_JOINING = "enable";
    const TOTAL_AMOUNT_ON_JOINING_ALOT = "total_amount_on_joining_alot";
    const JOINING_COMMISSION_PERCENT = "joining_commission_percent";
    const JOINING_COMMISSION_REAMINING_PERCENT = "joining_commission_remaining_percent";

    const SPONSOR_LEVEL_GROUP = "sponsor_level_commission";
    const ENABLE_LEVEL_COMMISSION = "enable_level_commission";
    const LEVEL_COMMISSION_PERCENT = "level_commission_percent";
    const LEVEL_COMMISSION_REAMINING_PERCENT = "level_commission_remaining_percent";

    const PAYMENT_OPTIONS_GROUP = 'payment_options';
    const PAYMENT_OPTIONS = 'payment_options';

    const SPONSOR_GROUP = 'sponsor';
    const REFERAL_EMAIL_TEMPLATE = 'referral_email_template';
    const EMAIL_IDENTITY = 'email_identity';
    const EMAIL_CONTENT = 'bodycontent';
}
