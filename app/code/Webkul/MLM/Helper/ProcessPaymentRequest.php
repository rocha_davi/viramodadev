<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

use Webkul\MLM\Model\SponsorPaymentRequest\Source\Status as PaymentRequestStatus;

class ProcessPaymentRequest extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\DB\TransactionFactory $dbTransactionF
     * @param Data $dataHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\DB\TransactionFactory $dbTransactionF,
        Data $dataHelper
    ) {
        parent::__construct($context);
        $this->dataHelper = $dataHelper;
        $this->eventManager = $eventManager;
        $this->dbTransactionF = $dbTransactionF;
    }

    /**
     * @param Webkul\MLM\Model\SponsorPaymentRequest $paymentRequest
     * @return void
     */
    public function execute($paymentRequest)
    {
        $dbTransaction = $this->dbTransactionF->create();
        $transactionId = $this->dataHelper->generateTxnId();
        $paymentRequest->setStatus(PaymentRequestStatus::STATUS_APPROVED)
            ->setTransactionId($transactionId);
        $dbTransaction->addObject($paymentRequest);
        $dbTransaction->save();
    }
}
