<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

use Webkul\MLM\Model\SponsorPaymentRequest\Source\Status as PaymentRequestStatus;
use Magento\Framework\DB\Sql\Expression as SqlExpression;

class GetLevelCountCommissionNotAllocated extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Webkul\MLM\Model\ResourceModel\MemberLevel\CollectionFactory $memberlevelCollF
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Webkul\MLM\Model\ResourceModel\MemberLevel\CollectionFactory $memberlevelCollF
    ) {
        parent::__construct($context);
        $this->memberlevelCollF = $memberlevelCollF;
    }

    /**
     * @param int $levelIds
     * @return int
     */
    public function execute($levelIds)
    {
        $coll = $this->memberlevelCollF->create();
        if (count($levelIds) > 0) {
            $coll
            ->addFieldToFilter('entity_id', ['nin' => $levelIds])
            ;
        }
        $remainingLevelCount = $coll->getSize();
        return $remainingLevelCount;
    }
}
