<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

/**
 * Class Commission
 */
class MemberLevel extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Webkul\MLM\Model\ResourceModel\SponsorMemberLevel\CollectionFactory $sponsorMemLevCollF
     * @param \Webkul\MLM\Model\ResourceModel\MemberLevel\CollectionFactory $memberLevelCollF
     * @param Data $helperData
     * @param Filesystem $filesystemHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Webkul\MLM\Model\ResourceModel\SponsorMemberLevel\CollectionFactory $sponsorMemLevCollF,
        \Webkul\MLM\Model\ResourceModel\MemberLevel\CollectionFactory $memberLevelCollF,
        Data $helperData,
        Filesystem $filesystemHelper
    ) {
        parent::__construct($context);
        $this->helperData = $helperData;
        $this->filesystemHelper = $filesystemHelper;
        $this->sponsorMemLevCollF = $sponsorMemLevCollF;
        $this->memberLevelCollF = $memberLevelCollF;
    }

    /**
     * Configuration Values
     *
     * @param string $path
     * @return string
     */
    public function getConfigData($path) : string
    {
        /**
         * @var object \Magento\Framework\App\Config\ScopeConfigInterface
         */
        $value = $this->scopeConfig->getValue(
            $path,
            $this->_storeScope
        );
        return $value ?? self::EMPTY_STRING;
    }

    /**
     * @param int $sponsorId
     * @param int $memberLevelId
     * @param Transaction $dbTransaction
     * @return void
     */
    public function changeSponsorMemberLevel(
        $sponsorId,
        $memberLevelId,
        $dbTransaction = null
    ) {
        $sponsorMemLev = $this->sponsorMemLevCollF->create()
            ->addFieldToFilter("sponsor_id", $sponsorId)
            ->getFirstItem();
        if (empty($sponsorMemLev->getId())) {
            $sponsorMemLev->setSponsorId($sponsorId);
        }
        $sponsorMemLev->setMemberlevelId($memberLevelId);
        if (empty($dbTransaction)) {
            $sponsorMemLev->save();
        } else {
            $dbTransaction->addObject($sponsorMemLev);
        }
    }

    /**
     * @param int $sponsorId
     * @return string
     */
    public function getBadgeImageUrl($sponsorId)
    {
        $sponsorMemLevColl = $this->sponsorMemLevCollF->create()
            ->addFieldToFilter("sponsor_id", $sponsorId);
        $memberlevelTable = $sponsorMemLevColl->getTable('mlm_memberlevel');
        $sponsorMemLevColl->getSelect()->join(
            ["mlm_memberlevel" => $memberlevelTable],
            "mlm_memberlevel.entity_id=main_table.memberlevel_id",
            ['mlm_memberlevel.badge_image']
        );
        $badgeImageMap = $sponsorMemLevColl->getFirstItem()->getBadgeImage();
        $imageUploaderData = $this->filesystemHelper->getBadgeImageUploaderData($badgeImageMap);
        return $imageUploaderData[0]['url'] ?? null;
    }

    /**
     * @param int $levelId
     * @return float
     */
    public function getMemberLevelBonusAmount($levelId)
    {
        $memLevColl = $this->memberLevelCollF->create()
            ->addFieldToFilter("entity_id", $levelId);
        $levelBonus = $memLevColl->getFirstItem()->getBonusAmount();
        return $levelBonus;
    }

    /**
     * @param float $totalBusiness
     * @return int
     */
    public function getMLIdForTotalBusiness($totalBusiness)
    {
        $coll = $this->memberLevelCollF->create()
            ->addFieldToFilter(
                "maximum_business_amount",
                ['gteq' => $totalBusiness]
            )->setOrder(
                'maximum_business_amount',
                \Magento\Framework\Data\Collection::SORT_ORDER_ASC
            );
        return $coll->getFirstItem()->getId();
    }

    /**
     * @param int $levelId
     * @return int
     */
    public function getMemberLevelCount($levelId)
    {
        $coll = $this->memberLevelCollF->create();
        $memberLevelSponsorTableName = $coll->getTable("mlm_memberlevel_sponsor");
        $coll->getSelect()
            ->columns([
                "member_count" =>
                "(SELECT COUNT(*) FROM {$memberLevelSponsorTableName} WHERE memberlevel_id = main_table.entity_id)",
            ]);
        $collectionSql = $coll->getSelect()->assemble();
        $coll->getSelect()->reset();
        $coll->getSelect()->from(
            ["main_table" => new \Zend_Db_Expr("($collectionSql)")],
            new \Zend_Db_Expr("*")
        );
        $coll->addFieldToFilter('entity_id', $levelId);
        return $coll->getFirstItem()->getMemberCount();
    }
}
