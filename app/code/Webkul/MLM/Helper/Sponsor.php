<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

use Magento\Framework\DB\Sql\Expression as SqlExpression;
use Webkul\MLM\Model\SponsorPaymentRequest\Source\Status as PaymentRequestStatus;
use Webkul\MLM\Model\Sponsors\Source\Status as SponsorStatus;

class Sponsor extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param Data $dataHelper
     * @param \Webkul\MLM\Model\ResourceModel\Sponsors\CollectionFactory $sponsorsCollF
     * @param \Webkul\MLM\Model\ResourceModel\MemberLevel\CollectionFactory $memberLevelCollF
     * @param \Webkul\MLM\Model\ResourceModel\SponsorWalletTransaction\CollectionFactory $sponsorWalletTxnCollF
     * @param \Webkul\MLM\Model\ResourceModel\SponsorPaymentRequest\CollectionFactory $pmtRqstCollF
     * @param \Webkul\MLM\Model\ResourceModel\SponsorWallet\CollectionFactory $walletCollF
     * @param \Magento\Customer\Model\CustomerFactory $customerF
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Magento\Payment\Helper\Data $paymentHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        Data $dataHelper,
        \Webkul\MLM\Model\ResourceModel\Sponsors\CollectionFactory $sponsorsCollF,
        \Webkul\MLM\Model\ResourceModel\MemberLevel\CollectionFactory $memberLevelCollF,
        \Webkul\MLM\Model\ResourceModel\SponsorWalletTransaction\CollectionFactory $sponsorWalletTxnCollF,
        \Webkul\MLM\Model\ResourceModel\SponsorPaymentRequest\CollectionFactory $pmtRqstCollF,
        \Webkul\MLM\Model\ResourceModel\SponsorWallet\CollectionFactory $walletCollF,
        \Magento\Customer\Model\CustomerFactory $customerF,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Payment\Helper\Data $paymentHelper
    ) {
        parent::__construct($context);
        $this->dataHelper = $dataHelper;
        $this->sponsorsCollF = $sponsorsCollF;
        $this->sponsorWalletTxnCollF = $sponsorWalletTxnCollF;
        $this->memberLevelCollF = $memberLevelCollF;
        $this->pmtRqstCollF = $pmtRqstCollF;
        $this->customerF = $customerF;
        $this->walletCollF = $walletCollF;
        $this->dateTime = $dateTime;
        $this->paymentHelper = $paymentHelper;
    }

    /**
     * @param int $customerId
     * @return int
     */
    public function getSponsorId($customerId)
    {
        $sponsor = $this->sponsorsCollF->create()
            ->addFieldToFilter('customer_id', $customerId)->getFirstItem();
        return $sponsor->getId();
    }

    /**
     * @param int $customerId
     * @return bool
     */
    public function isCustomerApprovedSponsor($customerId)
    {
        $sponsor = $this->sponsorsCollF->create()
            ->addFieldToFilter('customer_id', $customerId)->getFirstItem();
        return $sponsor->getStatus() == SponsorStatus::STATUS_ENABLED;
    }

    /**
     * @param int $sponsorId
     * @return int
     */
    public function getCustomerId($sponsorId)
    {
        $sponsor = $this->sponsorsCollF->create()
            ->addFieldToFilter('entity_id', $sponsorId)->getFirstItem();
        return $sponsor->getCustomerId();
    }

    /**
     * @param int $customerId
     * @return string
     */
    public function getSponsorCode($customerId)
    {
        $sponsor = $this->sponsorsCollF->create()
            ->addFieldToFilter('customer_id', $customerId)->getFirstItem();
        return $sponsor->getSponsorCode();
    }

    /**
     * @param int $sponsorId
     * @return string
     */
    public function getJoiningDate($sponsorId)
    {
        $sponsor = $this->sponsorsCollF->create()
            ->addFieldToFilter('main_table.entity_id', $sponsorId)->getFirstItem();
        $createdAt = $sponsor->getCreatedAt();
        $createdAt = $this->dataHelper->formatDate(
            $this->dataHelper->getDateTime($createdAt),
            \IntlDateFormatter::MEDIUM,
            true
        );
        return $createdAt;
    }

    /**
     * @param int $sponsorId
     * @return string
     */
    public function getParentSponsorName($sponsorId)
    {
        $sponsor = $this->sponsorsCollF->create()
            ->addFieldToFilter('main_table.entity_id', $sponsorId)->getFirstItem();
        $parentSponsorId = $sponsor->getParentSponsorId();
        if ($parentSponsorId == 0) {
            return __("Admin");
        }
        return $this->getSponsorName($parentSponsorId);
    }

    /**
     * @param int $sponsorCode
     * @return int
     */
    public function getParentSponsorId($sponsorCode)
    {
        $sponsor = $this->sponsorsCollF->create()
            ->addFieldToFilter('main_table.sponsor_code', $sponsorCode)->getFirstItem();
        $parentSponsorId = $sponsor->getId();
        return $parentSponsorId;
    }

    /**
     * @param int $sponsorId
     * @return int
     */
    public function getParentSponsorIdSponsorId($sponsorId)
    {
        $sponsor = $this->sponsorsCollF->create()
            ->addFieldToFilter('main_table.entity_id', $sponsorId)->getFirstItem();
        $parentSponsorId = $sponsor->getParentSponsorId();
        return $parentSponsorId;
    }

    /**
     * @param int $sponsorId
     * @return int
     */
    public function getStatus($sponsorId)
    {
        $sponsor = $this->sponsorsCollF->create()
            ->addFieldToFilter('main_table.entity_id', $sponsorId)->getFirstItem();
        $parentSponsorId = $sponsor->getStatus();
        return $parentSponsorId;
    }

    /**
     * @param int $sponsorId
     * @return string
     */
    public function getSponsorName($sponsorId)
    {
        $customerId = $this->sponsorsCollF->create()
            ->addFieldToFilter('main_table.entity_id', $sponsorId)
            ->getFirstItem()
            ->getCustomerId();
        return $this->customerF->create()->load($customerId)->getName();
    }

    /**
     * @param int $sponsorId
     * @return string
     */
    public function getSponsorEmail($sponsorId)
    {
        $customerId = $this->sponsorsCollF->create()
            ->addFieldToFilter('main_table.entity_id', $sponsorId)
            ->getFirstItem()
            ->getCustomerId();
        return $this->customerF->create()->load($customerId)->getEmail();
    }

    /**
     * @param int $sponsorId
     * @return string
     */
    public function getSponsorGroupName($sponsorId)
    {
        $coll = $this->sponsorsCollF->create()
            ->addFieldToFilter('main_table.entity_id', $sponsorId);
        $customerTable = $coll->getTable("customer_entity");
        $customerGroupTable = $coll->getTable("customer_group");
        $coll->getSelect()->join(
            ["customer_entity" => $customerTable],
            "main_table.customer_id=customer_entity.entity_id"
        )->join(
            ["customer_group" => $customerGroupTable],
            "customer_entity.group_id=customer_group.customer_group_id",
            ["customer_group_code" => "customer_group.customer_group_code"]
        );
        return $coll->getFirstItem()->getCustomerGroupCode();
    }

    /**
     * @param int $sponsorId
     * @return string
     */
    public function getMemberLevelName($sponsorId)
    {
        $coll = $this->sponsorsCollF->create()
            ->addFieldToFilter('main_table.entity_id', $sponsorId);
        $memberLevelSponsorTable = $coll->getTable("mlm_memberlevel_sponsor");
        $memberLevelTable = $coll->getTable("mlm_memberlevel");
        $coll->getSelect()->join(
            ["mlm_memberlevel_sponsor" => $memberLevelSponsorTable],
            "mlm_memberlevel_sponsor.sponsor_id=main_table.entity_id",
            ["*"]
        )
        ->join(
            ["mlm_memberlevel" => $memberLevelTable],
            "mlm_memberlevel_sponsor.memberlevel_id=mlm_memberlevel.entity_id",
            ["level_name" => "mlm_memberlevel.level_name"]
        );
        return $coll->getFirstItem()->getLevelName() ?: __("Not Assigned");
    }

    /**
     * @param int $sponsorId
     * @return int
     */
    public function getMemberLevelId($sponsorId)
    {
        $coll = $this->sponsorsCollF->create()
            ->addFieldToFilter('main_table.entity_id', $sponsorId);
        $memberLevelSponsorTable = $coll->getTable("mlm_memberlevel_sponsor");
        $coll->getSelect()->join(
            ["mlm_memberlevel_sponsor" => $memberLevelSponsorTable],
            "mlm_memberlevel_sponsor.sponsor_id=main_table.entity_id",
            ["member_level_id" => "mlm_memberlevel_sponsor.memberlevel_id"]
        );
        $memberLevelId = $coll->getFirstItem()->getMemberLevelId();
        return $memberLevelId;
    }

    /**
     * @param int $sponsorId
     * @return int
     */
    public function getDownlineMemberCount($sponsorId)
    {
        $coll = $this->sponsorsCollF->create()
            ->addFieldToFilter('main_table.entity_id', $sponsorId);
        $sponsorSponsorTable = $coll->getTable("mlm_sponsor_sponsor");
        $coll->getSelect()->joinLeft(
            ["mlm_sponsor_sponsor" => $sponsorSponsorTable],
            "mlm_sponsor_sponsor.parent_sponsor_id=main_table.entity_id",
            ["downline_member" => "IFNULL(COUNT(mlm_sponsor_sponsor.entity_id), 0)"]
        )->group("main_table.entity_id");
        return $coll->getFirstItem()->getDownlineMember();
    }

    /**
     * @param int $sponsorId
     * @return float
     */
    public function getWalletBalance($sponsorId)
    {
        return $this->walletCollF->create()->addFieldToFilter(
            "sponsor_id",
            $sponsorId
        )->getFirstItem()
        ->getCurrentBalance();
    }

    /**
     * @param int $sponsorId
     * @return SponsorWallet\ResourceModel\Collection
     */
    public function getWalletTxnCollection($sponsorId)
    {
        $collection = $this->sponsorWalletTxnCollF->create();
            $collection->addFieldToFilter(
                "sponsor_id",
                $sponsorId
            );
        return $collection;
    }

    /**
     * @param int $sponsorId
     * @return float
     */
    public function getTotalPendingAmt($sponsorId)
    {
        $collection = $this->pmtRqstCollF->create();
        $collection
            ->addFieldToFilter(
                "sponsor_id",
                $sponsorId
            )
            ->addFieldToFilter(
                "status",
                PaymentRequestStatus::STATUS_NOT_APPROVED
            )
            ;
        $collection->getSelect()->columns(
            ["total_pending_amount" => new SqlExpression("SUM(amount)")]
        );
        return $collection->getFirstItem()->getTotalPendingAmount();
    }

    /**
     * @param int $sponsorId
     * @return SponsorPaymentTransaction
     */
    public function getLastPayment($sponsorId)
    {
        $collection = $this->pmtRqstCollF->create();
        $item = $collection
            ->addFieldToFilter(
                "sponsor_id",
                $sponsorId
            )
            ->setOrder("main_table.updated_at")->getFirstItem();
        return $item;
    }

    /**
     * @param int $sponsorId
     * @return string
     */
    public function getLastPaymentTxnId($sponsorId)
    {
        $lastPayment = $this->getLastPayment($sponsorId);
        if (!empty($lastPayment)) {
            return $lastPayment->getTransactionId();
        }
        return null;
    }

    /**
     * @param int $sponsorId
     * @return float
     */
    public function getLastPaymentAmt($sponsorId)
    {
        $lastPayment = $this->getLastPayment($sponsorId);
        if (!empty($lastPayment)) {
            return $lastPayment->getAmount();
        }
        return 0;
    }

    /**
     * @param int $sponsorId
     * @return string|null
     */
    public function getLastPaymentDate($sponsorId)
    {
        $lastPayment = $this->getLastPayment($sponsorId);
        if (!empty($lastPayment)) {
            return ($lastPayment->getUpdatedAt());
        }
        return null;
    }

    /**
     * @param int $sponsorId
     * @return string
     */
    public function getLastPaymentDescription($sponsorId)
    {
        $lastPayment = $this->getLastPayment($sponsorId);
        if (!empty($lastPayment)) {
            return $lastPayment->getDescription();
        }
        return null;
    }

    /**
     * @param int $sponsorId
     * @return string
     */
    public function getJoiningOtherDesc($sponsorId)
    {
        $sponsorName = $this->getSponsorName($sponsorId);
        $description = __("New Member Joining: %1", $sponsorName);
        return $description;
    }

    /**
     * @param int $sponsorId
     * @return string
     */
    public function getJoiningSurplusDesc($sponsorId)
    {
        $sponsorName = $this->getSponsorName($sponsorId);
        $description = __("Surplus Amount Joining: %1", $sponsorName);
        return $description;
    }

    /**
     * @param int $sponsorId
     * @return string
     */
    public function getJoiningSelfDesc($sponsorId)
    {
        return __("Joining Bonus: %1", "You");
    }

    /**
     * @param int $sponsorId
     * @return int
     */
    public function getLevelNameLevelId($levelId)
    {
        return $this->memberLevelCollF->create()
        ->addFieldToFilter('entity_id', $levelId)
        ->getFirstItem()->getLevelName();
    }

    /**
     * @param int $sponsorId
     * @param int $toLevelId
     * @return string
     */
    public function getLevelOtherDesc($sponsorId, $toLevelId)
    {
        $sponsorName = $this->getSponsorName($sponsorId);
        $fromLevelName = $this->getMemberLevelName($sponsorId);
        $toLevelName = $this->getLevelNameLevelId($toLevelId);
        $description = __(
            "%1 Level up commission from %2 to %3",
            $sponsorName,
            $fromLevelName,
            $toLevelName
        );
        return $description;
    }

    /**
     * @param int $sponsorId
     * @param int $toLevelId
     * @return string
     */
    public function getLevelSurplusDesc($sponsorId, $toLevelId)
    {
        $sponsorName = $this->getSponsorName($sponsorId);
        $fromLevelName = $this->getMemberLevelName($sponsorId);
        $toLevelName = $this->getLevelNameLevelId($toLevelId);
        $description = __(
            "%1 Level up from %2 to %3, surplus amount",
            $sponsorName,
            $fromLevelName,
            $toLevelName
        );
        return $description;
    }

    /**
     * @param int $sponsorId
     * @param int $toLevelId
     * @return string
     */
    public function getLevelUpSelfDesc($sponsorId, $toLevelId)
    {
        $fromLevelName = $this->getMemberLevelName($sponsorId);
        $toLevelName = $this->getLevelNameLevelId($toLevelId);
        return __(
            "Level up bonus from %1 to %2",
            $fromLevelName,
            $toLevelName
        );
    }

    /**
     * @param int $sponsorId
     * @return void
     */
    public function createWalletIfNotExists($sponsorId)
    {
        $sponsorWalletColl = $this->walletCollF->create()
            ->addFieldToFilter('sponsor_id', $sponsorId);
        if ($sponsorWalletColl->getSize() == 0) {
            $sponsorWallet = $sponsorWalletColl->getNewEmptyItem();
            $sponsorWallet->setSponsorId($sponsorId)
                ->setTotalEarning(0)
                ->setCurrentBalance(0);
            $sponsorWallet->save();
        }
    }

    /**
     * @param int $sponsorId
     * @param int $status
     * @return void
     */
    public function changeStatus($sponsorId, $status = SponsorStatus::STATUS_DISABLED)
    {
        $sponsor = $this->sponsorsCollF
            ->create()
            ->addFieldToFilter('entity_id', $sponsorId)
            ->getFirstItem();
        $sponsor->setStatus($status);
        $sponsor->save();
    }

    /**
     * @param int $sponsorId
     * @return string
     */
    public function getPaymentMethodTitle($sponsorId)
    {
        $sponsor = $this->sponsorsCollF
            ->create()
            ->addFieldToFilter('entity_id', $sponsorId)
            ->getFirstItem();
        $paymentMethodCode = $sponsor->getPaymentMethod();
        if (!empty($paymentMethodCode)) {
            $methodInstance = $this->paymentHelper->getMethodInstance($paymentMethodCode);
            return $methodInstance->getTitle();
        }
        return null;
    }
}
