<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

use Webkul\MLM\Model\SponsorPaymentRequest\Source\Status as PaymentRequestStatus;

class CreateWalletIfNotExist extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\DB\TransactionFactory $dbTransactionF
     * @param \Webkul\MLM\Helper\Sponsor $sponsorHelper
     * @param \Webkul\MLM\Model\SponsorWalletFactory $sponsorWalletF
     * @param \Webkul\MLM\Model\SponsorSponsorFactory $sponsorSponsorF
     * @param Data $dataHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\DB\TransactionFactory $dbTransactionF,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Model\SponsorWalletFactory $sponsorWalletF,
        \Webkul\MLM\Model\SponsorSponsorFactory $sponsorSponsorF,
        Data $dataHelper
    ) {
        parent::__construct($context);
        $this->dataHelper = $dataHelper;
        $this->eventManager = $eventManager;
        $this->sponsorHelper = $sponsorHelper;
        $this->dbTransactionF = $dbTransactionF;
        $this->sponsorWalletF = $sponsorWalletF;
    }

    /**
     * @param int $sponsorId
     * @return void
     */
    public function execute($sponsorId)
    {
         $sponsorWalletColl = $this->sponsorWalletF->create()->getCollection()
            ->addFieldToFilter('sponsor_id', $sponsorId);
        if ($sponsorWalletColl->getSize() == 0) {
            $sponsorWallet = $sponsorWalletColl->getNewEmptyItem();
            $sponsorWallet->setSponsorId($sponsorId)
                ->setTotalEarning(0)
                ->setCurrentBalance(0);
            $sponsorWallet->save();
        }
    }
}
