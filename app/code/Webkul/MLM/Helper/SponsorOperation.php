<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

use Webkul\MLM\Model\SponsorPaymentRequest\Source\Status as PaymentRequestStatus;

class SponsorOperation extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Webkul\MLM\Model\SponsorPaymentRequestFactory $sponsorPmtRqstF
     * @param \Webkul\MLM\Model\SponsorBusinessCommentFactory $sponsorBusinessCommF
     * @param Data $helperData
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Webkul\MLM\Model\SponsorPaymentRequestFactory $sponsorPmtRqstF,
        \Webkul\MLM\Model\SponsorBusinessCommentFactory $sponsorBusinessCommF,
        Data $helperData
    ) {
        parent::__construct($context);
        $this->helperData = $helperData;
        $this->sponsorPmtRqstF = $sponsorPmtRqstF;
        $this->sponsorBusinessCommF = $sponsorBusinessCommF;
    }

    /**
     * @param string $path
     * @return string
     */
    public function getConfigData($path) : string
    {
        $value = $this->scopeConfig->getValue(
            $path,
            $this->_storeScope
        );
        return $value ?? self::EMPTY_STRING;
    }

    /**
     * @param array $data
     * @return void
     */
    public function createPaymentRequest($data)
    {
        $data['status'] = PaymentRequestStatus::STATUS_NOT_APPROVED;
        $data['commission_percent'] = 0;
        $data['commission_amount'] = 0;
        $sponsorPayment = $this->sponsorPmtRqstF->create();
        $sponsorPayment->setData($data);
        $sponsorPayment->save();
    }

    /**
     * @param array $data
     * @return void
     */
    public function createBusinessComment($data)
    {
        $sponsorPayment = $this->sponsorBusinessCommF->create();
        $sponsorPayment->setData($data);
        $sponsorPayment->save();
    }
}
