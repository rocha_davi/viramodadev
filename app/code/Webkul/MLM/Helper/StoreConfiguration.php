<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

use Magento\Framework\Stdlib\StringUtils;
use Webkul\MLM\Setup\Patch\Data\AdminSponsorData;
use Magento\Framework\Math\Random as MathRandom;

class StoreConfiguration extends \Magento\Framework\App\Helper\AbstractHelper
{
    const SECTION               = "mlm";
    const GROUP                 = "referral_sponsor_code_format";
    const CODE_LENGTH           = "code_length";
    const CODE_FORMAT           = "code_format";
    const CODE_PREFIX           = "code_prefix";
    const CODE_SUFFIX           = "code_suffix";
    const DASH_EVERY_CHARACTER  = "dash_every_character";
    const DEFAULT_LENGTH        = 10;
    const MAX_LENGTH            = 30;
    const ALPHABET_LENGTH       = 10;
    const EMAIL_SECTION         = "sponsor";
    const EMAIL_BODY            = "bodycontent";

    /**
     * @var object StringUtils
     */
    private $_string;

    /**
     * @var object \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $_dataTime;

    /**
     * @var \Magento\Config\Model\Config\Factory
     */
    private $_configFactory;

    /**
     * @var \Webkul\MLM\Model\Sponsors
     */
    protected $sponsors;

    /**
     * @param StringUtils $stringUtils
     * @param \Webkul\MLM\Model\Sponsors $sponsors
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dataTime
     * @param \Magento\Config\Model\Config\Factory $configFactory
     * @param \Magento\Framework\Math\Random $mathRandom
     * @param Data $helperData
     */
    public function __construct(
        StringUtils $stringUtils,
        \Webkul\MLM\Model\Sponsors $sponsors,
        \Webkul\MLM\Model\SponsorsFactory $sponsorsF,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $dataTime,
        \Magento\Config\Model\Config\Factory $configFactory,
        \Magento\Framework\Math\Random $mathRandom,
        Data $helperData
    ) {
        $this->_dataTime = $dataTime;
        $this->_string  = $stringUtils;
        $this->_configFactory = $configFactory;
        $this->sponsors = $sponsors;
        $this->helperData = $helperData;
        $this->mathRandom = $mathRandom;
        $this->sponsorsF = $sponsorsF;
        parent::__construct(
            $context
        );
    }
    
    /**
     * All Code Format Configuration values
     *
     * @return array
     */
    public function getCodeFormatConfigurationData() : array
    {
        return [
            $this->getKey(
                self::CODE_LENGTH
            ) => $this->helperData->getConfigData(
                self::SECTION.AdminSponsorData::DS.self::GROUP.AdminSponsorData::DS.self::CODE_LENGTH
            ),
            $this->getKey(
                self::CODE_FORMAT
            ) => $this->helperData->getConfigData(
                self::SECTION.AdminSponsorData::DS.self::GROUP.AdminSponsorData::DS.self::CODE_FORMAT
            ),
            $this->getKey(
                self::CODE_PREFIX
            ) => $this->helperData->getConfigData(
                self::SECTION.AdminSponsorData::DS.self::GROUP.AdminSponsorData::DS.self::CODE_PREFIX
            ),
            $this->getKey(
                self::CODE_SUFFIX
            ) => $this->helperData->getConfigData(
                self::SECTION.AdminSponsorData::DS.self::GROUP.AdminSponsorData::DS.self::CODE_SUFFIX
            ),
            $this->getKey(
                self::DASH_EVERY_CHARACTER
            ) => $this->helperData->getConfigData(
                self::SECTION.AdminSponsorData::DS.self::GROUP.AdminSponsorData::DS.self::DASH_EVERY_CHARACTER
            )
        ];
    }

    /**
     * To convert into upper case
     *
     * @param string $key
     * @return string
     */
    private function getKey($key) : string
    {
        return lcfirst(str_replace('_', '', ucwords($key, '_')));
    }

    /**
     * Generate the Sponsors unigue id
     *
     * @param boolean $repeat
     * @return string
     */
    public function generateSponsorsId($repeat = false) : string
    {
        $codeLength = $this->_getCodeLength();
        $sponsorIdConstraints = $this->getCodeFormatConfigurationData();
        $randomChars = $this->randomStrings(
            $sponsorIdConstraints[
                    $this->getKey(
                        self::CODE_FORMAT
                    )
            ],
            $codeLength
        );
        $prefix = (string)$sponsorIdConstraints[
            $this->getKey(
                self::CODE_PREFIX
            )
        ];
        $suffix = (string)$sponsorIdConstraints[
            $this->getKey(
                self::CODE_SUFFIX
            )
        ];
        $dashEveryCharacter = (string)$sponsorIdConstraints[
            $this->getKey(
                self::DASH_EVERY_CHARACTER
            )
        ];
        if (!empty($dashEveryCharacter)) {
            $splittedCharaters = str_split($randomChars, 1);
            $randomChars = implode($dashEveryCharacter, $splittedCharaters);
        }
        return $prefix.$randomChars.$suffix;
    }

    /**
     * Random Strings
     *
     * @param boolean $type
     * @return string
     */
    private function randomStrings($type = "alphanumeric", $lengthOfCode = 10)
    {
        if ($type == \Webkul\MLM\Model\Config\Source\CodeFormat::ALPHA_NUMBERIC) {
            return $this->mathRandom->getRandomString(
                $lengthOfCode,
                MathRandom::CHARS_DIGITS.MathRandom::CHARS_LOWERS.MathRandom::CHARS_UPPERS
            );
        } elseif ($type == \Webkul\MLM\Model\Config\Source\CodeFormat::NUMBERIC) {
            return $this->mathRandom->getRandomString($lengthOfCode, MathRandom::CHARS_DIGITS);
        }
        return $this->mathRandom->getRandomString(
            $lengthOfCode,
            MathRandom::CHARS_LOWERS.MathRandom::CHARS_UPPERS
        );
    }

    /**
     * Get sponsors id's length
     *
     * @return integer
     */
    private function _getCodeLength() : int
    {
        $length = (int) $this->helperData->getConfigData(
            self::SECTION.AdminSponsorData::DS.self::GROUP.AdminSponsorData::DS.self::CODE_LENGTH
        );
        if (self::MAX_LENGTH < $length) {
            return self::MAX_LENGTH;
        }
        return $length = ($length >= self::DEFAULT_LENGTH ) ? $length : self::DEFAULT_LENGTH;
    }

    /**
     * Convert To Numbers
     *
     * @param string $string
     * @return string
     */
    private function convertToCharactors($string) : string
    {
        $alphabets = ['A','B','C','D','E','F','G','H','H','J'];
        for ($index = 0; $index < self::ALPHABET_LENGTH; $index++) {
            $string = str_replace($index, $alphabets[$index], $string);
        }
        return $string;
    }

    /**
     * Saved admin sponsor id
     *
     * @return string
     */
    public function getSavedAdminSponsorId()
    {
        $configObject = $this->_configFactory->create();
        $path = AdminSponsorData::CONFIGURATION_SECTION;
        $path .= AdminSponsorData::DS.AdminSponsorData::CONFIGURATION_GROUP;
        $path .= AdminSponsorData::DS.AdminSponsorData::CONFIGURATION_FIELD;
        $savedAdminSponsorId =  $configObject->getConfigDataValue(
            $path
        );
        return $savedAdminSponsorId;
    }

    /**
     * Generate unique SponsorId For Admin
     *
     * @return boolean
     */
    public function generateSponsorIdForAdmin()
    {
        $savedAdminSponsorId =  $this->getSavedAdminSponsorId();

        $adminSponsorId = $this->generateSponsorsId();
        $configData = [
            'section' => AdminSponsorData::CONFIGURATION_SECTION,
            'website' => null,
            'store'   => null,
            'groups'  => [
                AdminSponsorData::CONFIGURATION_GROUP => [
                    'fields' => [
                        AdminSponsorData::CONFIGURATION_FIELD => [
                            'value' => $adminSponsorId,
                        ],
                    ],
                ],
            ],
        ];
        
        /** @var \Magento\Config\Model\Config $configModel */
        $configModel = $this->_configFactory->create(['data' => $configData]);
        $configModel->save();

        $this->updateSponsorsAdminSponsorId($savedAdminSponsorId, $adminSponsorId);
        return true;
    }

    /**
     * @param string $oldId
     * @param string $newId
     * @return void
     */
    public function updateSponsorsAdminSponsorId($oldId, $newId)
    {
        $sponsorsColl = $this->sponsorsF->create()->getCollection()
            ->addFieldToFilter('sponsor_reference_code', $oldId);
        foreach ($sponsorsColl as $sponsor) {
            $sponsor->setSponsorReferenceCode($newId)
                ->save();
        }
    }
}
