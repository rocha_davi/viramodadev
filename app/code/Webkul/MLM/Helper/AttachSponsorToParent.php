<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

use Webkul\MLM\Model\SponsorPaymentRequest\Source\Status as PaymentRequestStatus;

class AttachSponsorToParent extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\DB\TransactionFactory $dbTransactionF
     * @param \Webkul\MLM\Helper\Sponsor $sponsorHelper
     * @param \Webkul\MLM\Model\SponsorSponsorFactory $sponsorSponsorF
     * @param Data $dataHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\DB\TransactionFactory $dbTransactionF,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Model\SponsorSponsorFactory $sponsorSponsorF,
        Data $dataHelper
    ) {
        parent::__construct($context);
        $this->dataHelper = $dataHelper;
        $this->eventManager = $eventManager;
        $this->sponsorHelper = $sponsorHelper;
        $this->dbTransactionF = $dbTransactionF;
        $this->sponsorSponsorF = $sponsorSponsorF;
    }

    /**
     * @param int $sponsorId
     * @return void
     */
    public function execute($sponsorId)
    {
        $parentSponsorId = $this->sponsorHelper->getParentSponsorIdSponsorId($sponsorId);
        $leafSponsorId = $sponsorId;
        while ($parentSponsorId != 0) {
            $this->createMemberLinkIfNotExists($parentSponsorId, $leafSponsorId);
            $parentSponsorId = $this->sponsorHelper->getParentSponsorIdSponsorId($parentSponsorId);
        }
    }

    /**
     * @param int $parentSponsorId
     * @param int $childSponsorId
     * @return void
     */
    public function createMemberLinkIfNotExists($parentSponsorId, $childSponsorId)
    {
        $sponsorSponsorColl = $this->sponsorSponsorF->create()->getCollection();
        $sponsorSponsorColl
            ->addFieldToFilter('parent_sponsor_id', $parentSponsorId)
            ->addFieldToFilter('child_sponsor_id', $childSponsorId)
        ;
        if ($sponsorSponsorColl->getSize() == 1) {
            return;
        }
        $sponsorSponsor = $sponsorSponsorColl->getNewEmptyItem()
            ->setParentSponsorId($parentSponsorId)
            ->setChildSponsorId($childSponsorId)
        ;
        $sponsorSponsor->save();
    }
}
