<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

class Commission extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Initialized Dependencies
     *
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        Data $helperData
    ) {
        parent::__construct($context);
        $this->helperData = $helperData;
    }

    /**
     * Configuration Values
     *
     * @param string $path
     * @return string
     */
    public function getConfigData($path) : string
    {
        /**
         * @var object \Magento\Framework\App\Config\ScopeConfigInterface
         */
        $value = $this->scopeConfig->getValue(
            $path,
            $this->_storeScope
        );
        return $value ?? self::EMPTY_STRING;
    }
}
