<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

use Webkul\MLM\Model\Sponsors\Source\Status as SponsorStatus;

class SponsorApprove extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Webkul\MLM\Model\SponsorWalletFactory $sponsorWalletF
     * @param \Webkul\MLM\Model\SponsorEarningFactory $sponsorEarningF
     * @param \Webkul\MLM\Model\SponsorWalletTransactionFactory $sponsorWalletTransactionF
     * @param \Webkul\MLM\Model\SponsorBusinessFactory $sponsorBusinessF
     * @param \Webkul\MLM\Model\SponsorsFactory $sponsorsF
     * @param \Webkul\MLM\Model\SponsorSponsorFactory $sponsorSponsorF
     * @param \Webkul\MLM\Model\CommissionFactory $commissionF
     * @param \Webkul\MLM\Helper\Earning $earningHelper
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\DB\TransactionFactory $dbTransactionF
     * @param Data $dataHelper
     * @param Sponsor $sponsorHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Webkul\MLM\Model\SponsorWalletFactory $sponsorWalletF,
        \Webkul\MLM\Model\SponsorEarningFactory $sponsorEarningF,
        \Webkul\MLM\Model\SponsorWalletTransactionFactory $sponsorWalletTransactionF,
        \Webkul\MLM\Model\SponsorBusinessFactory $sponsorBusinessF,
        \Webkul\MLM\Model\SponsorsFactory $sponsorsF,
        \Webkul\MLM\Model\SponsorSponsorFactory $sponsorSponsorF,
        \Webkul\MLM\Model\CommissionFactory $commissionF,
        \Webkul\MLM\Helper\Earning $earningHelper,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\DB\TransactionFactory $dbTransactionF,
        Data $dataHelper,
        Sponsor $sponsorHelper
    ) {
        parent::__construct($context);
        $this->dataHelper = $dataHelper;
        $this->sponsorWalletF = $sponsorWalletF;
        $this->sponsorEarningF = $sponsorEarningF;
        $this->sponsorWalletTransactionF = $sponsorWalletTransactionF;
        $this->sponsorBusinessF = $sponsorBusinessF;
        $this->commissionF = $commissionF;
        $this->earningHelper = $earningHelper;
        $this->sponsorsF = $sponsorsF;
        $this->eventManager = $eventManager;
        $this->sponsorSponsorF = $sponsorSponsorF;
        $this->sponsorHelper = $sponsorHelper;
        $this->dbTransactionF = $dbTransactionF;
    }

    /**
     * @param int $sponsorId
     * @return void
     */
    public function execute($sponsorId)
    {
        $isSponsorAmountJoiningEnabled = $this->dataHelper->isSponsorJoiningEnabled();
        if (!$isSponsorAmountJoiningEnabled) {
            $dbTransaction = $this->dbTransactionF->create();
            $this->changeStatus($sponsorId, $dbTransaction, SponsorStatus::STATUS_ENABLED);
            $dbTransaction->save();
            return;
        }
        $sponsorStatus = $this->sponsorHelper->getStatus($sponsorId);
        if ($sponsorStatus == SponsorStatus::STATUS_ENABLED) {
            return;
        }
        $dbTransaction = $this->dbTransactionF->create();
        $minSponsorRqstAmt = $this->dataHelper->getMinSponsorRequestAmt();
        $sponsorJoiningBonus = $this->dataHelper->getTotalAmountOnJoiningAlot();
        $joiningLevelCommissionRate = $this->dataHelper->getJoiningLevelCommissionRate();
        $joiningCommissionPercent = $this->dataHelper->getJoiningCommission();
        $earningTypeId = $this->earningHelper->getJoiningEarningTypeId();
        $description = $this->sponsorHelper->getJoiningOtherDesc($sponsorId);
        $this->changeStatus($sponsorId, $dbTransaction, SponsorStatus::STATUS_ENABLED);

        $netCommissionAmount = $sponsorJoiningBonus;
        $adminCommissionAmount = ($netCommissionAmount * $joiningCommissionPercent) / 100;
        $sponsorsCommissionAmount = $netCommissionAmount - $adminCommissionAmount;

        if ($adminCommissionAmount > 0) {
            $this->allocateCommToAdmin(
                $sponsorId,
                $netCommissionAmount,
                $adminCommissionAmount,
                $joiningCommissionPercent,
                $dbTransaction,
                $description
            );
        }

        $currentSponsorId = $sponsorId;
        $leafSponsorId = $sponsorId;
        $remainingCommissionAmount = $sponsorsCommissionAmount;

        if ($remainingCommissionAmount > 0) {
            $allocatedCommissionAmount =
                $this->allocateCommToSponsor(
                    $currentSponsorId,
                    $leafSponsorId,
                    $remainingCommissionAmount,
                    $dbTransaction,
                    true
                );
            $remainingCommissionAmount -= $allocatedCommissionAmount;
            $childSponsorId = $sponsorId;
            while ($remainingCommissionAmount > 0) {
                $currentSponsorId = $this->sponsorHelper->getParentSponsorIdSponsorId($childSponsorId);
                if ($currentSponsorId == 0) {
                    break;
                }
                $allocatedCommissionAmount =
                    $this->allocateCommToSponsor(
                        $currentSponsorId,
                        $leafSponsorId,
                        $remainingCommissionAmount,
                        $dbTransaction,
                        false
                    );
                $remainingCommissionAmount -= $allocatedCommissionAmount;
                $childSponsorId = $currentSponsorId;
            }
        }

        if ($remainingCommissionAmount > 0) {
            $reaminingCommissionPercent = ($remainingCommissionAmount * 100) / $netCommissionAmount;
            $surplusDescription = $this->sponsorHelper->getJoiningSurplusDesc($sponsorId);
            $this->allocateCommToAdmin(
                $sponsorId,
                $netCommissionAmount,
                $remainingCommissionAmount,
                $reaminingCommissionPercent,
                $dbTransaction,
                $surplusDescription
            );
        }
        $dbTransaction->save();
    }

    /**
     * @param int $levelId
     * @return float
     */
    public function getSponsorJoiningCommPercent($levelId)
    {
        $joiningLevelCommissionRate = $this->dataHelper->getJoiningLevelCommissionRate();
        $joiningLevelCommissionRate = \Zend_Json::decode($joiningLevelCommissionRate);
        foreach ($joiningLevelCommissionRate as $item) {
            if ($item['commission_level'] == $levelId) {
                return $item['commission_rate'];
            }
        }
        return 0;
    }

    /**
     * @param int $sponsorId
     * @param int $leafSponsorId
     * @param float $sponsorsCommissionAmount
     * @param Transaction $dbTransaction
     * @param bool $isLeafSponsor
     * @return float
     */
    private function allocateCommToSponsor(
        $sponsorId,
        $leafSponsorId,
        $sponsorsCommissionAmount,
        $dbTransaction,
        $isLeafSponsor = false
    ) {
        $earningTypeId = $this->earningHelper->getJoiningEarningTypeId();
        $description = $this->sponsorHelper->getJoiningOtherDesc($leafSponsorId);
        $ownDescription = $isLeafSponsor
            ? $this->sponsorHelper->getJoiningSelfDesc($leafSponsorId)
            : $description;

        $sponsorEarningData['sponsor_id'] = $sponsorId;
        $sponsorEarningData['earning_type_id'] = $earningTypeId;
        $sponsorEarningData['description'] = $ownDescription;
        $sponsorMemberLevelId = $this->sponsorHelper->getMemberLevelId($sponsorId);
        $sponsorCommPercent = $this->getSponsorJoiningCommPercent($sponsorMemberLevelId);
        $sponsorEarningAmount = ($sponsorCommPercent * $sponsorsCommissionAmount) / 100;
        $sponsorEarningData['amount'] = $sponsorEarningAmount;
        $sponsorEarningData['actual_amount'] = $sponsorsCommissionAmount;
        $sponsorEarningData['commission_amount'] = $sponsorEarningAmount;
        $sponsorEarningData['commission_percent'] = $sponsorCommPercent;
        if ($sponsorEarningAmount > 0) {
            $sponsorEarning = $this->sponsorEarningF->create()->setData($sponsorEarningData);
            $this->eventManager->dispatch('sponsor_earning_before_save', ['sponsor_earning' => $sponsorEarning]);
            $dbTransaction->addObject($sponsorEarning);
            $this->eventManager->dispatch('sponsor_earning_after_save', ['sponsor_earning' => $sponsorEarning]);

            $sponsorWallet = $this->sponsorWalletF->create()
                ->getCollection()->addFieldToFilter('sponsor_id', $sponsorId)
                ->getFirstItem();
            $currentBalance = $sponsorWallet->getCurrentBalance() + $sponsorEarningAmount;
            $sponsorWallet->setCurrentBalance($currentBalance);
            $totalEarning = $sponsorWallet->getTotalEarning() + $sponsorEarningAmount;
            $sponsorWallet->setTotalEarning($totalEarning);

            $this->eventManager->dispatch('sponsor_joining_wallet_before_save', ['sponsor_wallet' => $sponsorWallet]);
            $dbTransaction->addObject($sponsorWallet);
            $this->eventManager->dispatch('sponsor_joining_wallet_after_save', ['sponsor_wallet' => $sponsorWallet]);

            $walletTransactionData['wallet_id'] = $sponsorWallet->getId();
            $walletTransactionData['sponsor_id'] = $sponsorId;
            $walletTransactionData['earning_type'] = $earningTypeId;
            $walletTransactionData['description'] = $ownDescription;
            $walletTransactionData['amount'] = $sponsorEarningAmount;
            $walletTransactionData['wallet_balance'] = $sponsorWallet->getCurrentBalance();
            $walletTransaction = $this->sponsorWalletTransactionF->create();
            $walletTransaction->setData($walletTransactionData);
            $this->eventManager->dispatch(
                'sponsor_joining_wallet_transaction_before_save',
                ['sponsor_wallet_transaction' => $walletTransaction]
            );
            $dbTransaction->addObject($walletTransaction);
            $this->eventManager->dispatch(
                'sponsor_joining_wallet_transaction_after_save',
                ['sponsor_wallet_wallet_transaction' => $walletTransaction]
            );

            $sponsorBusinessData['earning_type_id'] = $earningTypeId;
            $sponsorBusinessData['sponsor_id'] = $sponsorId;
            $sponsorBusinessData['credit'] = $sponsorEarningAmount;
            $sponsorBusinessData['debit'] = 0;
            $sponsorBusinessData['balance'] = $sponsorWallet->getCurrentBalance();
            $sponsorBusinessData['description'] = $description;
            $sponsorBusiness = $this->sponsorBusinessF->create();
            $sponsorBusiness->setData($sponsorBusinessData);
            $this->eventManager->dispatch(
                'sponsor_joining_sponsor_business_before_save',
                ['sponsor_business' => $sponsorBusiness]
            );
            $dbTransaction->addObject($sponsorBusiness);
            $this->eventManager->dispatch(
                'sponsor_joining_sponsor_business_after_save',
                ['sponsor_business' => $sponsorBusiness]
            );
            return $sponsorEarningAmount;
        } else {
            return 0;
        }
    }

    /**
     * @param int $sponsorId
     * @param float $actualAmount
     * @param float $commissionAmount
     * @param float $commissionPercent
     * @param Transaction $dbTransaction
     * @param string $description
     * @return void
     */
    private function allocateCommToAdmin(
        $sponsorId,
        $actualAmount,
        $commissionAmount,
        $commissionPercent,
        $dbTransaction,
        $description = null
    ) {
        $earningTypeId = $this->earningHelper->getJoiningEarningTypeId();
        $description = $description ?: $this->sponsorHelper->getJoiningOtherDesc($sponsorId);
        $adminCommissionData = [];
        $adminCommissionData['sponsor_id'] = $sponsorId;
        $adminCommissionData['earning_type_id'] = $earningTypeId;
        $adminCommissionData['description'] = $description;
        $adminCommissionData['actual_amount'] = $actualAmount;
        $adminCommissionData['commission_amount'] = $commissionAmount;
        $adminCommissionData['commission_percent'] = $commissionPercent;
        $adminCommission = $this->commissionF->create()->setData($adminCommissionData);
        $this->eventManager->dispatch('admin_commission_before_save', ['commission' => $adminCommission]);
        $dbTransaction->addObject($adminCommission);
        $this->eventManager->dispatch('admin_commission_after_save', ['commission' => $adminCommission]);
    }

    /**
     * @param int $sponsorId
     * @param Transaction $dbTransaction
     * @param int $status
     * @return void
     */
    public function changeStatus($sponsorId, $dbTransaction, $status = SponsorStatus::STATUS_DISABLED)
    {
        $sponsor = $this->sponsorsF
            ->create()
            ->getCollection()
            ->addFieldToFilter('entity_id', $sponsorId)
            ->getFirstItem();
        $sponsor->setStatus($status);
        $dbTransaction->addObject($sponsor);
    }
}
