<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

use Webkul\MLM\Model\Sponsors\Source\Status as SponsorStatus;

class DistributeSalesCommissionToSponsors extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Webkul\MLM\Model\SponsorWalletFactory $sponsorWalletF
     * @param \Webkul\MLM\Model\SponsorEarningFactory $sponsorEarningF
     * @param \Webkul\MLM\Model\SponsorWalletTransactionFactory $sponsorWalletTransactionF
     * @param \Webkul\MLM\Model\SponsorBusinessFactory $sponsorBusinessF
     * @param \Webkul\MLM\Model\SponsorsFactory $sponsorsF
     * @param \Webkul\MLM\Model\SponsorSponsorFactory $sponsorSponsorF
     * @param \Webkul\MLM\Model\CommissionFactory $commissionF
     * @param \Webkul\MLM\Helper\Earning $earningHelper
     * @param \Webkul\MLM\Helper\MemberLevel $memberLevelHelper
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\DB\TransactionFactory $dbTransactionF
     * @param \Psr\Log\LoggerInterface $logger
     * @param Data $dataHelper
     * @param Sponsor $sponsorHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Webkul\MLM\Model\SponsorWalletFactory $sponsorWalletF,
        \Webkul\MLM\Model\SponsorEarningFactory $sponsorEarningF,
        \Webkul\MLM\Model\SponsorWalletTransactionFactory $sponsorWalletTransactionF,
        \Webkul\MLM\Model\SponsorBusinessFactory $sponsorBusinessF,
        \Webkul\MLM\Model\SponsorsFactory $sponsorsF,
        \Webkul\MLM\Model\SponsorSponsorFactory $sponsorSponsorF,
        \Webkul\MLM\Model\CommissionFactory $commissionF,
        \Webkul\MLM\Helper\Earning $earningHelper,
        \Webkul\MLM\Helper\MemberLevel $memberLevelHelper,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\DB\TransactionFactory $dbTransactionF,
        \Psr\Log\LoggerInterface $logger,
        Data $dataHelper,
        Sponsor $sponsorHelper
    ) {
        parent::__construct($context);
        $this->dataHelper = $dataHelper;
        $this->sponsorWalletF = $sponsorWalletF;
        $this->sponsorEarningF = $sponsorEarningF;
        $this->sponsorWalletTransactionF = $sponsorWalletTransactionF;
        $this->sponsorBusinessF = $sponsorBusinessF;
        $this->commissionF = $commissionF;
        $this->earningHelper = $earningHelper;
        $this->sponsorsF = $sponsorsF;
        $this->eventManager = $eventManager;
        $this->memberLevelHelper = $memberLevelHelper;
        $this->sponsorSponsorF = $sponsorSponsorF;
        $this->sponsorHelper = $sponsorHelper;
        $this->dbTransactionF = $dbTransactionF;
        $this->logger = $logger;
    }

    /**
     * @param int $sponsorId
     * @param float $amount
     * @return void
     */
    public function execute($sponsorId, $amount)
    {
        $sponsorStatus = $this->sponsorHelper->getStatus($sponsorId);
        if ($sponsorStatus == SponsorStatus::STATUS_DISABLED) {
            return ;
        }
        try {
            $dbTransaction = $this->dbTransactionF->create();
            $salesCommissionLevelRate = $this->dataHelper->getMLMSalesCommissionLevelRate();
            $salesCommissionPercent = $this->dataHelper->getMLMSalesCommission();
            $earningTypeId = $this->earningHelper->getSalesEarningTypeId();
            $description = $this->getSalesOtherDesc($sponsorId, $amount);

            $netCommissionAmount = $amount;
            $adminCommissionAmount = ($netCommissionAmount * $salesCommissionPercent) / 100;
            $sponsorsCommissionAmount = $netCommissionAmount - $adminCommissionAmount;
            
            if ($adminCommissionAmount) {
                $this->allocateCommToAdmin(
                    $sponsorId,
                    $netCommissionAmount,
                    $adminCommissionAmount,
                    $salesCommissionPercent,
                    $dbTransaction,
                    $description
                );
            }
    
            $currentSponsorId = $sponsorId;
            $leafSponsorId = $sponsorId;
            $remainingCommissionAmount = $sponsorsCommissionAmount;
            if ($remainingCommissionAmount > 0) {
                $allocatedCommissionAmount =
                    $this->allocateCommToSponsor(
                        $currentSponsorId,
                        $leafSponsorId,
                        $remainingCommissionAmount,
                        $amount,
                        $dbTransaction,
                        true
                    );
                $remainingCommissionAmount -= $allocatedCommissionAmount;
                $childSponsorId = $sponsorId;
                while ($remainingCommissionAmount > 0) {
                    $currentSponsorId = $this->sponsorHelper->getParentSponsorIdSponsorId($childSponsorId);
                    if ($currentSponsorId == 0) {
                        break;
                    }
                    $allocatedCommissionAmount =
                        $this->allocateCommToSponsor(
                            $currentSponsorId,
                            $leafSponsorId,
                            $remainingCommissionAmount,
                            $amount,
                            $dbTransaction,
                            false
                        );
                    $remainingCommissionAmount -= $allocatedCommissionAmount;
                    $childSponsorId = $currentSponsorId;
                }
            }
    
            if ($remainingCommissionAmount > 0) {
                $reaminingCommissionPercent = ($remainingCommissionAmount * 100) / $netCommissionAmount;
                $surplusDescription = $this->getSalesSurplusDesc($sponsorId, $amount);
                $this->allocateCommToAdmin(
                    $sponsorId,
                    $netCommissionAmount,
                    $remainingCommissionAmount,
                    $reaminingCommissionPercent,
                    $dbTransaction,
                    $surplusDescription
                );
            }
            $dbTransaction->save();
        } catch (\Throwable $t) {
            $this->logger->info(json_encode($t->getMessage(), JSON_PRETTY_PRINT));
            $this->logger->info(json_encode($t->getTraceAsString(), JSON_PRETTY_PRINT));
        }
    }

    /**
     * @param int $levelId
     * @return float
     */
    public function getSponsorSalesCommPercent($levelId)
    {
        $salesCommissionLevelRate = $this->dataHelper->getMLMSalesCommissionLevelRate();
        $salesCommissionLevelRate = \Zend_Json::decode($salesCommissionLevelRate);
        foreach ($salesCommissionLevelRate as $item) {
            if ($item['commission_level'] == $levelId) {
                return $item['commission_rate'];
            }
        }
        return 0;
    }

    /**
     * @param int $sponsorId
     * @param int $leafSponsorId
     * @param float $sponsorsCommissionAmount
     * @param float $saleAmount
     * @param Transaction $dbTransaction
     * @param bool $isLeafSponsor
     * @return float
     */
    private function allocateCommToSponsor(
        $sponsorId,
        $leafSponsorId,
        $sponsorsCommissionAmount,
        $saleAmount,
        $dbTransaction,
        $isLeafSponsor = false
    ) {
        $earningTypeId = $this->earningHelper->getSalesEarningTypeId();
        $description = $this->getSalesOtherDesc($leafSponsorId, $saleAmount);
        $ownDescription = $isLeafSponsor
            ? $this->getSalesOtherDesc($leafSponsorId, $saleAmount, __("You"))
            : $description;

        $sponsorEarningData['sponsor_id'] = $sponsorId;
        $sponsorEarningData['earning_type_id'] = $earningTypeId;
        $sponsorEarningData['description'] = $ownDescription;
        $sponsorMemberLevelId = $this->sponsorHelper->getMemberLevelId($sponsorId);
        $sponsorCommPercent = $this->getSponsorSalesCommPercent($sponsorMemberLevelId);
        $sponsorEarningAmount = ($sponsorCommPercent * $sponsorsCommissionAmount) / 100;
        $sponsorEarningData['amount'] = $sponsorEarningAmount;
        $sponsorEarningData['actual_amount'] = $sponsorsCommissionAmount;
        $sponsorEarningData['commission_amount'] = $sponsorEarningAmount;
        $sponsorEarningData['commission_percent'] = $sponsorCommPercent;
        if ($sponsorEarningAmount > 0) {
            $sponsorEarning = $this->sponsorEarningF->create()->setData($sponsorEarningData);
            $this->eventManager->dispatch('sponsor_earning_before_save', ['sponsor_earning' => $sponsorEarning]);
            $dbTransaction->addObject($sponsorEarning);
            $this->eventManager->dispatch('sponsor_earning_after_save', ['sponsor_earning' => $sponsorEarning]);

            $sponsorWallet = $this->sponsorWalletF->create()
                ->getCollection()->addFieldToFilter('sponsor_id', $sponsorId)
                ->getFirstItem();
            $currentBalance = $sponsorWallet->getCurrentBalance() + $sponsorEarningAmount;
            $sponsorWallet->setCurrentBalance($currentBalance);
            $totalEarning = $sponsorWallet->getTotalEarning() + $sponsorEarningAmount;
            $sponsorWallet->setTotalEarning($totalEarning);

            $this->eventManager->dispatch('sponsor_sale_wallet_before_save', ['sponsor_wallet' => $sponsorWallet]);
            $dbTransaction->addObject($sponsorWallet);
            $this->eventManager->dispatch('sponsor_sale_wallet_after_save', ['sponsor_wallet' => $sponsorWallet]);

            $walletTransactionData['wallet_id'] = $sponsorWallet->getId();
            $walletTransactionData['sponsor_id'] = $sponsorId;
            $walletTransactionData['earning_type'] = $earningTypeId;
            $walletTransactionData['description'] = $ownDescription;
            $walletTransactionData['amount'] = $sponsorEarningAmount;
            $walletTransactionData['wallet_balance'] = $sponsorWallet->getCurrentBalance();
            $walletTransaction = $this->sponsorWalletTransactionF->create();
            $walletTransaction->setData($walletTransactionData);
            $this->eventManager->dispatch(
                'sponsor_sale_wallet_transaction_before_save',
                ['sponsor_wallet_transaction' => $walletTransaction]
            );
            $dbTransaction->addObject($walletTransaction);
            $this->eventManager->dispatch(
                'sponsor_sale_wallet_transaction_after_save',
                ['sponsor_wallet_wallet_transaction' => $walletTransaction]
            );

            $sponsorBusinessData['earning_type_id'] = $earningTypeId;
            $sponsorBusinessData['sponsor_id'] = $sponsorId;
            $sponsorBusinessData['credit'] = $sponsorEarningAmount;
            $sponsorBusinessData['debit'] = 0;
            $sponsorBusinessData['balance'] = $sponsorWallet->getCurrentBalance();
            $sponsorBusinessData['description'] = $description;
            $sponsorBusiness = $this->sponsorBusinessF->create();
            $sponsorBusiness->setData($sponsorBusinessData);
            $this->eventManager->dispatch(
                'sponsor_sale_sponsor_business_before_save',
                ['sponsor_business' => $sponsorBusiness]
            );
            $dbTransaction->addObject($sponsorBusiness);
            $this->eventManager->dispatch(
                'sponsor_sale_sponsor_business_after_save',
                ['sponsor_business' => $sponsorBusiness]
            );
            return $sponsorEarningAmount;
        } else {
            return 0;
        }
    }

    /**
     * @param int $sponsorId
     * @param float $actualAmount
     * @param float $commissionAmount
     * @param float $commissionPercent
     * @param Transaction $dbTransaction
     * @param string $description
     * @return void
     */
    private function allocateCommToAdmin(
        $sponsorId,
        $actualAmount,
        $commissionAmount,
        $commissionPercent,
        $dbTransaction,
        $description = null
    ) {
        $earningTypeId = $this->earningHelper->getSalesEarningTypeId();
        $description = $description ?: $this->getSalesOtherDesc($sponsorId, $actualAmount);
        $adminCommissionData = [];
        $adminCommissionData['sponsor_id'] = $sponsorId;
        $adminCommissionData['earning_type_id'] = $earningTypeId;
        $adminCommissionData['description'] = $description;
        $adminCommissionData['actual_amount'] = $actualAmount;
        $adminCommissionData['commission_amount'] = $commissionAmount;
        $adminCommissionData['commission_percent'] = $commissionPercent;
        $adminCommission = $this->commissionF->create()->setData($adminCommissionData);
        $this->eventManager->dispatch('admin_commission_before_save', ['commission' => $adminCommission]);
        $dbTransaction->addObject($adminCommission);
        $this->eventManager->dispatch('admin_commission_after_save', ['commission' => $adminCommission]);
    }

    /**
     * @param int $sponsorId
     * @param float $amount
     * @param string $sponsorName
     * @return string
     */
    public function getSalesOtherDesc($sponsorId, $amount, $sponsorName = null)
    {
        $formattedAmount = $this->dataHelper->getFormattedPrice($amount);
        $sponsorName = $sponsorName ?: $this->sponsorHelper->getSponsorName($sponsorId);
        $description = __(
            "Venda de %1 consultor por %2",
            $formattedAmount,
            $sponsorName
        );
        return $description;
    }

    /**
     * @param int $sponsorId
     * @param float $amount
     * @return string
     */
    public function getSalesSurplusDesc($sponsorId, $amount)
    {
        $formattedAmount = $this->dataHelper->getFormattedPrice($amount);
        $sponsorName = $this->sponsorHelper->getSponsorName($sponsorId);
        $description = __(
            "Valor excedente da venda de %1 consultor por %2",
            $formattedAmount,
            $sponsorName
        );
        return $description;
    }
}
