<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

use Webkul\MLM\Model\SponsorPaymentRequest\Source\Status as PaymentRequestStatus;

class SubmitPaymentRequest extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Webkul\MLM\Model\SponsorWalletFactory $sponsorWalletF
     * @param \Webkul\MLM\Model\SponsorWalletTransactionFactory $sponsorWalletTransactionF
     * @param \Webkul\MLM\Model\SponsorBusinessFactory $sponsorBusinessF
     * @param \Webkul\MLM\Helper\Earning $earningHelper
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\DB\TransactionFactory $dbTransactionF
     * @param \Webkul\MLM\Model\SponsorPaymentRequestFactory $sponsorPmtRqstF
     * @param Data $dataHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Webkul\MLM\Model\SponsorWalletFactory $sponsorWalletF,
        \Webkul\MLM\Model\SponsorWalletTransactionFactory $sponsorWalletTransactionF,
        \Webkul\MLM\Model\SponsorBusinessFactory $sponsorBusinessF,
        \Webkul\MLM\Helper\Earning $earningHelper,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\DB\TransactionFactory $dbTransactionF,
        \Webkul\MLM\Model\SponsorPaymentRequestFactory $sponsorPmtRqstF,
        Data $dataHelper
    ) {
        parent::__construct($context);
        $this->dataHelper = $dataHelper;
        $this->sponsorWalletF = $sponsorWalletF;
        $this->sponsorWalletTransactionF = $sponsorWalletTransactionF;
        $this->sponsorBusinessF = $sponsorBusinessF;
        $this->earningHelper = $earningHelper;
        $this->eventManager = $eventManager;
        $this->dbTransactionF = $dbTransactionF;
        $this->sponsorPmtRqstF = $sponsorPmtRqstF;
    }

    /**
     * @param array $paymentRequestData
     * @return void
     */
    public function execute($paymentRequestData)
    {
        $dbTransaction = $this->dbTransactionF->create();
        $this->createPaymentRequest($paymentRequestData, $dbTransaction);
        $earningTypeId = $this->earningHelper->getPaymentRequestEarningTypeId();
        $description = $paymentRequestData['description'];
        $amount = $paymentRequestData['amount'];
        $sponsorId = $paymentRequestData['sponsor_id'];

        $sponsorWallet = $this->sponsorWalletF->create()
            ->getCollection()->addFieldToFilter('sponsor_id', $sponsorId)
            ->getFirstItem();
        $currentBalance = $sponsorWallet->getCurrentBalance() - $amount;
        $sponsorWallet->setCurrentBalance($currentBalance);

        $this->eventManager->dispatch(
            'sponsor_payment_request_wallet_before_save',
            ['sponsor_wallet' => $sponsorWallet]
        );
        $dbTransaction->addObject($sponsorWallet);
        $this->eventManager->dispatch(
            'sponsor_payment_request_wallet_after_save',
            ['sponsor_wallet' => $sponsorWallet]
        );

        $walletTransactionData['wallet_id'] = $sponsorWallet->getId();
        $walletTransactionData['sponsor_id'] = $sponsorId;
        $walletTransactionData['earning_type'] = $earningTypeId;
        $walletTransactionData['description'] = $description;
        $walletTransactionData['amount'] = -$amount;
        $walletTransactionData['wallet_balance'] = $sponsorWallet->getCurrentBalance();
        $walletTransaction = $this->sponsorWalletTransactionF->create();
        $walletTransaction->setData($walletTransactionData);
        $this->eventManager->dispatch(
            'sponsor_payment_reqeust_wallet_transaction_before_save',
            ['sponsor_wallet_transaction' => $walletTransaction]
        );
        $dbTransaction->addObject($walletTransaction);
        $this->eventManager->dispatch(
            'sponsor_payment_request_wallet_transaction_after_save',
            ['sponsor_wallet_wallet_transaction' => $walletTransaction]
        );

        $sponsorBusinessData['earning_type_id'] = $earningTypeId;
        $sponsorBusinessData['sponsor_id'] = $sponsorId;
        $sponsorBusinessData['credit'] = 0;
        $sponsorBusinessData['debit'] = $amount;
        $sponsorBusinessData['balance'] = $sponsorWallet->getCurrentBalance();
        $sponsorBusinessData['description'] = $description;
        $sponsorBusiness = $this->sponsorBusinessF->create();
        $sponsorBusiness->setData($sponsorBusinessData);
        $this->eventManager->dispatch(
            'sponsor_payment_request_sponsor_business_before_save',
            ['sponsor_business' => $sponsorBusiness]
        );
        $dbTransaction->addObject($sponsorBusiness);
        $this->eventManager->dispatch(
            'sponsor_payment_request_sponsor_business_after_save',
            ['sponsor_business' => $sponsorBusiness]
        );
        $dbTransaction->save();
    }

    /**
     * @param array $data
     * @param Transaction $dbTransaction
     * @return void
     */
    public function createPaymentRequest($data, $dbTransaction)
    {
        $data['status'] = PaymentRequestStatus::STATUS_NOT_APPROVED;
        $data['commission_percent'] = 0;
        $data['commission_amount'] = 0;
        $sponsorPayment = $this->sponsorPmtRqstF->create();
        $sponsorPayment->setData($data);
        $dbTransaction->addObject($sponsorPayment);
    }
}
