<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Helper;

use Webkul\MLM\Model\ModuleConstants;

/**
 * Class Data
 * $this->_moduleManager
 * $this->_logger
 * $this->_request
 * $this->_urlBuilder
 * $this->_httpHeader
 * $this->_eventManager
 * $this->_remoteAddress
 * $this->_cacheConfig
 * $this->urlEncoder
 * $this->urlDecoder
 * $this->scopeConfig
 */
class Filesystem extends \Magento\Framework\App\Helper\AbstractHelper
{
    const EMPTY_STRING = "";

    /**
     * Store scope
     * @var string
     */
    private $_storeScope= \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Filesystem\Driver\File $fileDriver
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Filesystem\Driver\File $fileDriver,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->fileDriver = $fileDriver;
        $this->filesystem = $filesystem;
        $this->fileUploaderFactory = $fileUploaderFactory;
        $this->storeManager = $storeManager;
        try {
            $this->mediaDirectory = $filesystem->getDirectoryWrite(
                \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
            );
        } catch (\Magento\Framework\Exception\FileSystemException $e) {
            $this->mediaDirectory = null;
        }
        parent::__construct($context);
    }

    /**
     * Configuration Values
     *
     * @param string $path
     * @return string
     */
    public function getConfigData($path) : string
    {
        /**
         * @var object \Magento\Framework\App\Config\ScopeConfigInterface
         */
        $value = $this->scopeConfig->getValue(
            $path,
            $this->_storeScope
        );
        return $value ?? self::EMPTY_STRING;
    }

    /**
     * @return sting
     */
    public function getMLBadgeImgDirPath()
    {
        $baseTmpPath  = ModuleConstants::MEMBER_LEVEL_BADGE_IMAGES_DIR;
        $target = $this->mediaDirectory->getAbsolutePath($baseTmpPath);
        return $target;
    }

    /**
     * @param string $filename
     * @return string
     */
    public function getMLBadgeImgPath($filename)
    {
        $directory  = $this->getMLBadgeImgDirPath();
        $directory = rtrim($directory, "/\\");
        $result = $directory . DIRECTORY_SEPARATOR . $filename;
        return $result;
    }

    /**
     * @param string $filename
     * @param string $imageName
     * @return string
     */
    public function getBadgeImageMap($filename, $imageName)
    {
        return $filename . ':' . $imageName;
    }

    /**
     * @param string $imageMapStr
     * @return array
     */
    public function getBadgeImageUploaderData($imageMapStr)
    {
        if (empty($imageMapStr)) {
            return null;
        }
        $magentoDir  = ModuleConstants::MEMBER_LEVEL_BADGE_IMAGES_DIR;
        return $this->getImageUploderData($imageMapStr, $magentoDir);
    }

    /**
     * @param string $imageMapStr
     * @param string $magentoImgDir
     * @return array
     */
    public function getImageUploderData($imageMapStr, $magentoImgDir)
    {
        $imageData = [];
        list($filename, $imageName) = explode(":", $imageMapStr);
        $imageData[0] = [];
        $imageData[0]['url'] = $this->getFileMediaUrl($magentoImgDir, $filename);
        $imageData[0]['name'] = $imageName;
        $imageData[0]['file'] = $filename;
        $filePath = $this->getFileAbsPath($magentoImgDir, $filename);
        $filesize = $this->getFileSize($filePath);
        $imageData[0]["size"] = $filesize;
        return $imageData;
    }

    /**
     * @param string $magentoDir
     * @param string $filename
     * @return string
     */
    public function getFileMediaUrl($magentoDir, $filename)
    {
        $mediaUrl = $this->getMediaUrl();
        $url = $mediaUrl . $magentoDir . '/' . $filename;
        return $url;
    }

    /**
     * @param string $magentoDir
     * @param string $filename
     * @return string
     */
    public function getFileAbsPath($magentoDir, $filename)
    {
        $filePath = $this->mediaDirectory->getAbsolutePath($magentoDir);
        $filePath = rtrim($filePath, "/\\") . DIRECTORY_SEPARATOR;
        $filePath .= $filename;
        return $filePath;
    }

    /**
     * @param string $filePath
     * @return int
     */
    public function getFileSize($filePath)
    {
        $size = 0;
        try {
            if ($this->fileDriver->isFile($filePath)) {
                $fileStats = $this->fileDriver->stat($filePath);
                $size = empty($fileStats['size']) ? 0 : $fileStats['size'];
            }
        } catch (\Throwable $t) {
            $this->logger->debug($t->getMessage());
        }
        return $size;
    }

    /**
     * @return string
     */
    public function getMediaUrl()
    {
        $mediaUrl = $this->storeManager->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return rtrim($mediaUrl, '/') . '/';
    }
}
