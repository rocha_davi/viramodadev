<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Plugin\Customer\Block\Widget;

/**
 * Plugin for showing the sponsors reference id.
 *
 */
class Name
{
    /**
     * Customer Name view model
     *
     * @var \Webkul\MLM\ViewModel\Customer
     */
    private $_customer;

    /**
     * @param \Webkul\MLM\ViewModel\Customer $customer
     * @param \Webkul\MLM\Helper\Data $dataHelper
     */
    public function __construct(
        \Webkul\MLM\ViewModel\Customer $customer,
        \Webkul\MLM\Helper\Data $dataHelper
    ) {
        $this->_customer = $customer;
        $this->dataHelper = $dataHelper;
    }

    /**
     * @param \Magento\Customer\Block\Widget\Name $subject
     * @param callable $proceed
     * @param string $template
     * @return void
     */
    public function aroundSetTemplate(
        \Magento\Customer\Block\Widget\Name $subject,
        callable $proceed,
        string $template
    ) {
        $proceed('Webkul_MLM::widget/name.phtml');
        $subject->setData('viewModel', $this->_customer);
    }
}
