<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_SellerSubDomain
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Plugin;

class Store
{
    /**
     * @var UrlInterface
     */
    protected $urlInterface;

    /**
     * @var \Webkul\SellerSubDomain\Helper\Data
     */
    protected $_helper;

    /**
     * @param \Magento\Framework\UrlInterface     $urlInterface
     * @param \Webkul\SellerSubDomain\Helper\Data $helepr
     */
    public function __construct(
        \Webkul\MLM\Helper\Data $helper,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Customer\Model\Session $customer,
        \Webkul\MLM\Model\SponserConfigurationsFactory $sponsorConfiguration
    ) {
        $this->_helper = $helper;
        $this->httpContext = $httpContext;
        $this->customer = $customer;
        $this->sponsorConfiguration = $sponsorConfiguration;
    }

    /**
     * @param \Magento\Store\Model\Store $subject
     * @param $result
     * @return string
     */
    public function afterGetBaseUrl(
        \Magento\Store\Model\Store $subject,
        $result
    ) {
    
        $customerContextId = $this->httpContext->getValue('customer_id');
        $customer = $this->customer;
        $customerId = $customer->getId() ? $customer->getId() :$customerContextId;
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/plugin.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(json_encode($customerId));
        $logger->info(json_encode($result));
        if ($this->_helper->isCustomerApprovedSponsor($customerId) || $this->_helper->checkSponsorExistsByCurrentUrl()) {
           
            return $this->_helper->getBaseUrl($result, $customerId);
        }
        
        
        return $result;
    }

    /**
     * @param \Magento\Store\Model\Store $subject
     * @param $result
     * @return string
     */
    // public function afterGetCurrentUrl(
    //     \Magento\Store\Model\Store $subject,
    //     $url
    // ) {
    //     return $this->_helper->getCurrentSellerUrlFromPrevUrl($url);
    // }
}
