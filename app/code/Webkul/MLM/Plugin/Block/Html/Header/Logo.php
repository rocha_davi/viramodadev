<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_SellerSubDomain
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Plugin\Block\Html\Header;

use \Magento\Framework\App\Helper\Context;

class Logo
{

    /**
     * @param Context                             $context
     * @param \Webkul\SellerSubDomain\Helper\Data $data
     */
    public function __construct(
        Context $context,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\MLM\Model\SponserConfigurationsFactory $sponsorConfiguration,
        \Magento\Framework\UrlInterface    $urlBuilder,
        \Magento\Framework\App\Http\Context $httpContext,
        \Webkul\MLM\Helper\Data $helper
    ) {
        $this->sponsorHelper = $sponsorHelper;
        $this->customerSession = $customerSession;
        $this->sponsorConfiguration = $sponsorConfiguration;
        $this->urlBuilder = $urlBuilder;
        $this->httpContext = $httpContext;
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Theme\Block\Html\Header\Logo $subject
     * @param $result
     * @return string
     */
    public function afterGetLogoSrc(\Magento\Theme\Block\Html\Header\Logo $subject, $result)
    {
        $customerId =  $this->httpContext->getValue('customer_id');
        $sponsorExist = $this->helper->checkSponsorExistsByCurrentUrl();
        if ($this->sponsorHelper->isCustomerApprovedSponsor($customerId) || $sponsorExist) {
            if ($customerId) {
                $logoDetails = $this->sponsorConfiguration->create()->getCollection()
                ->addFieldToFilter('sponsor_id', $customerId)->getFirstItem();
                if ($logoDetails->getLogoImage() != null) {
                    $logoUrl = $this->urlBuilder
                    ->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]);
                    $logoUrl = $logoUrl.'mlm/'.$customerId.'/'.$logoDetails->getLogoImage();
                    return $logoUrl;
                }
            } else {
                $logoDetails = $this->sponsorConfiguration->create()->getCollection()
                ->addFieldToFilter('sponsor_id', $sponsorExist->getCustomerId())->getFirstItem();
                if ($logoDetails->getLogoImage() != null) {
                    $logoUrl = $this->urlBuilder
                    ->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]);
                    $logoUrl = $logoUrl.'mlm/'.$sponsorExist->getCustomerId().'/'.$logoDetails->getLogoImage();
                    return $logoUrl;
                }
            }
           
            
            return $result;
        }
        return $result;
    }
    /**
     * Retrieve logo height
     *
     * @return int
     */
    public function afterGetLogoHeight(\Magento\Theme\Block\Html\Header\Logo $subject, $result)
    {
        $customerId =  $this->httpContext->getValue('customer_id');
        $sponsorExist = $this->helper->checkSponsorExistsByCurrentUrl();
        if ($this->sponsorHelper->isCustomerApprovedSponsor($customerId) || $sponsorExist) {
            if ($customerId) {
                $logoDetails = $this->sponsorConfiguration->create()->getCollection()
                ->addFieldToFilter('sponsor_id', $customerId)->getFirstItem();
            } else {
                $logoDetails = $this->sponsorConfiguration->create()->getCollection()
                ->addFieldToFilter('sponsor_id', $sponsorExist->getCustomerId())->getFirstItem();
            }
           
            if ($logoDetails->getLogoHeight()) {
                $height = $logoDetails->getLogoHeight();
                return $height;
            }
            return $result;
        }
        return $result;
    }
    /**
     * Retrieve logo width
     *
     * @return int
     */
    public function afterGetLogoWidth(\Magento\Theme\Block\Html\Header\Logo $subject, $result)
    {
        $customerId =  $this->httpContext->getValue('customer_id');
        $sponsorExist = $this->helper->checkSponsorExistsByCurrentUrl();
        if ($this->sponsorHelper->isCustomerApprovedSponsor($customerId) || $sponsorExist) {
            if ($customerId) {
                $logoDetails = $this->sponsorConfiguration->create()->getCollection()
                ->addFieldToFilter('sponsor_id', $customerId)->getFirstItem();
            } else {
                $logoDetails = $this->sponsorConfiguration->create()->getCollection()
                ->addFieldToFilter('sponsor_id', $sponsorExist->getCustomerId())->getFirstItem();
            }
           
            if ($logoDetails->getLogoWidth() != 0) {
                $height = $logoDetails->getLogoWidth();
                return $height;
            }
            return $result;
        }
        return $result;
    }
}
