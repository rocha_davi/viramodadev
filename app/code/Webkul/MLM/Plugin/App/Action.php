<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_SellerSubDomain
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Plugin\App;

use Magento\Framework\App\Action\Context;

class Action
{
    /**
     * @var \Webkul\SellerSubDomain\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;

    /**
     * @param Context                             $context
     * @param \Webkul\SellerSubDomain\Helper\Data $data
     */
    public function __construct(
        Context $context,
        \Webkul\MLM\Helper\Data $data,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Customer\Model\Session $customer
    ) {
        $this->_helper = $data;
        $this->_response = $context->getResponse();
        $this->httpContext = $httpContext;
        $this->customer = $customer;
    }

    /**
     * Redirect to main domain
     *
     * @param  $request
     * @return void
     */
    public function beforeDispatch(\Magento\Framework\App\Action\Action $dispatch, $request)
    {
       
        $customerContextId = $this->httpContext->getValue('customer_id');
        $customer = $this->customer;
        $customerId = $customer->getId() ? $customer->getId() :$customerContextId;
        
        if ($this->_helper->isModuleEnabled() &&
         ($this->_helper->checkSponsorExistsByCurrentUrl()
         || $this->_helper->isCustomerApprovedSponsor($customerId))
         && $this->_helper->getCurrentUrl() != $this->_helper->getAllowedUrl() ) {
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/urlredirect.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info($this->_helper->getCurrentUrl());
            $logger->info($this->_helper->getAllowedUrl());
            $this->_response->setRedirect($this->_helper->getAllowedUrl());
            $this->_response->sendResponse();
        }
    }
}
