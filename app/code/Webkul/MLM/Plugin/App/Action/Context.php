<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_MLM
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Plugin\App\Action;

use \Magento\Customer\Model\Session;

class Context
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;
 
    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $httpContext;
 
    /**
     * @param Session $customerSession
     * @param \Magento\Framework\App\Http\Context $httpContext
     */
    public function __construct(
        Session $customerSession,
        \Magento\Framework\App\Http\Context $httpContext
    ) {
        $this->customerSession = $customerSession;
        $this->httpContext = $httpContext;
    }
 
    /**
     * @param \Magento\Framework\App\ActionInterface $subject
     * @param callable $proceed
     * @param \Magento\Framework\App\RequestInterface $request
     * @return mixed
     */
    public function aroundDispatch(
        \Magento\Framework\App\ActionInterface $subject,
        \Closure $proceed,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->httpContext->setValue(
            'customer_id',
            $this->customerSession->getCustomerId(),
            false
        );
 
        return $proceed($request);
    }
}
