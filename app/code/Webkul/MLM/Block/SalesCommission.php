<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block;

use Magento\Catalog\Model\Config;
use Magento\Backend\Block\Template\Context;
use Webkul\MLM\Helper\Commission as CommissionHelper;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class SalesCommission extends Field
{
    /**
     * @var CommissionHelper
     */
    private $helper;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param CommissionHelper $helper
     * @param Config $config
     * @param Context $context
     */
    public function __construct(
        CommissionHelper $helper,
        Config $config,
        Context $context
    ) {
        parent::__construct($context);
        $this->helper = $helper;
        $this->config = $config;
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->setTemplate('Webkul_MLM::system/commission.phtml');
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $this->setElement($element);

        return $this->_toHtml();
    }

    /**
     * @return array
     */
    public function getPositions()
    {
        $positions =  [];
        if ($positions === []) {
            $positions = $this->getOptionalArray();
        } else {
            $availableOptions = $this->getOptionalArray();
            // delete disabled options
            $positions = array_intersect($availableOptions, $positions);
            $newOptions = array_diff($availableOptions, $positions);
            $positions = array_merge($positions, $newOptions);
        }

        return $positions;
    }

    /**
     * @param $index
     * @return string
     */
    public function getNamePrefix($index)
    {
        return $this->getElement()->getName() . '[' . $index . ']';
    }

    /**
     * @return array
     */
    private function getOptionalArray()
    {
        $positions = [];
        $methods = $this->config->getAttributeUsedForSortByArray();
        foreach ($methods as $key => $methodObject) {
            if (is_object($methodObject)) {
                $positions[$key] = $methodObject->getText();
            } else {
                $positions[$key] = $methodObject;
            }
        }
        return $positions;
    }
}
