<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Sponsor;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Http\Context as HttpContext;

class Paymentrequest extends \Magento\Framework\View\Element\Template
{
    /**
     * Path to template file in theme.
     *
     * @var string
     */
    protected $_template = "sponsor/paymentrequest.phtml";

    /**
     * Sponsors
     *
     * @var \Webkul\MLM\Model\Sponsors $sponsor
     */
    private $_sponsorFactory;

    /**
     * @var HttpContext
     */
    private $_httpContext;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var SponsorWallet
     */
    protected $sponsorWallet;

    /**
     * @var FormKey
     */
    protected $formKey;

    /**
     * @var ScopeConfig
     */
    public $scopeConfig;

    /**
     * Initialized dependencies
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Webkul\MLM\Model\SponsorWallet $sponsorWallet,
        \Webkul\MLM\Model\SponsorsFactory $sponsorFactory,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Helper\Data $dataHelper,
        HttpContext $httpContext,
        Context $context,
        array $data = []
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->scopeConfig = $context->getScopeConfig();
        $this->_httpContext = $httpContext;
        $this->session = $session;
        $this->sponsorHelper = $sponsorHelper;
        $this->sponsorWallet = $sponsorWallet;
        $this->priceHelper = $priceHelper;
        $this->dataHelper = $dataHelper;
        $this->formKey = $formKey;
        $this->_sponsorFactory = $sponsorFactory;
        parent::__construct($context, $data);
    }

    /**
     * Return Customer id.
     *
     * @return bool|0|1
     */
    public function getCustomerId()
    {
        $customerId = $this->_httpContext->getValue('customer_id');
        return $customerId;
    }

    /**
     * Get Currency Price
     *
     * @return string
     */
    private function getCurrencyPrice($price)
    {
        return $this->priceHelper->currency(
            number_format($price, 2),
            true,
            false
        );
    }
    
    /**
     * Current Monthly Balance
     *
     * @return string
     */
    public function currentMonthlyBalance()
    {
        $wallet = $this->getWallet();
        $currentBalance = $wallet->getCurrentBalance();
        return $this->dataHelper->getFormattedPrice($currentBalance);
    }

    /**
     * Get Genealogy Tree
     *
     * @return float
     */
    public function formUrl()
    {
        return $this->urlBuilder->getUrl("mlm/sponsor/submitrequest");
    }

    /**
     * Current Monthly Balance
     *
     * @return string
     */
    public function walletId()
    {
        $wallet = $this->getWallet();
        return $wallet->getId();
    }

    /**
     * Current Monthly Balance
     *
     * @return string
     */
    public function getWallet()
    {
        $sponsorId = $this->getSponsorId();
        $wallet = $this->sponsorWallet->getWallet($sponsorId);
        return $wallet;
    }

    /**
     * @return int
     */
    public function getSponsorId()
    {
        $customerId = $this->getCustomerId();
        $sponsorId = $this->sponsorHelper->getSponsorId($customerId);
        return $sponsorId;
    }
}
