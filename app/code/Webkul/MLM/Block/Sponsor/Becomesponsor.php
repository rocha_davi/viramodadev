<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Sponsor;

use Magento\Framework\View\Element\Template\Context;

class Becomesponsor extends \Magento\Framework\View\Element\Template
{
    /**
     * Initialized dependencies
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Webkul\MLM\Helper\Data $helper,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
    }

    /**
     * @return \Webkul\MLM\Helper\Data
     */
    public function getHelper()
    {
        return $this->helper;
    }
}
