<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Sponsor;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Http\Context as HttpContext;

class Payment extends \Magento\Framework\View\Element\Template
{
    /**
     * Path to template file in theme.
     *
     * @var string
     */
    protected $_template = "sponsor/payment.phtml";

    /**
     * Sponsors
     *
     * @var \Webkul\MLM\Model\Sponsors $sponsor
     */
    private $_sponsorFactory;

    /**
     * @var HttpContext
     */
    private $_httpContext;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var SponsorWallet
     */
    protected $sponsorWallet;

    /**
     * @var FormKey
     */
    protected $formKey;

    /**
     * @var ScopeConfig
     */
    public $scopeConfig;

    /**
     * Initialized dependencies
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Webkul\MLM\Model\SponsorWallet $sponsorWallet,
        \Webkul\MLM\Helper\Data $dataHelper,
        \Webkul\MLM\Model\SponsorsFactory $sponsorFactory,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Magento\Payment\Helper\Data $paymentHelper,
        HttpContext $httpContext,
        Context $context,
        array $data = []
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->scopeConfig = $context->getScopeConfig();
        $this->_httpContext = $httpContext;
        $this->session = $session;
        $this->sponsorWallet = $sponsorWallet;
        $this->priceHelper = $priceHelper;
        $this->formKey = $formKey;
        $this->dataHelper = $dataHelper;
        $this->paymentHelper = $paymentHelper;
        $this->_sponsorFactory = $sponsorFactory;
        parent::__construct($context, $data);
    }

    /**
     * Return Customer id.
     *
     * @return bool|0|1
     */
    public function getCustomerId()
    {
        return $customerId = $this->_httpContext->getValue('customer_id');
    }
    
    /**
     * Current Monthly Balance
     *
     * @return string
     */
    public function getPaymentMethods()
    {
        $paymentMethod = $this->dataHelper->getPaymentOptions();
        $paymentMethods = explode(",", $paymentMethod);
        $result = [];
        foreach ($paymentMethods as $method) {
            if ($method) {
                $methodInstance = $this->paymentHelper->getMethodInstance($method);
                $title = $methodInstance->getTitle();
                $result[] = [
                    "label" => $title,
                    "value" => $method
                ];
            }
        }
        return $result;
    }

    /**
     * Get Form Url
     *
     * @return float
     */
    public function formUrl()
    {
        return $this->urlBuilder->getUrl("mlm/sponsor/submitpayment");
    }

    /**
     * @return string
     */
    public function getSelectedPaymentMethod()
    {
        $model = $this->_sponsorFactory->create()->getCollection()->addFieldToFilter(
            "customer_id",
            $this->getCustomerId()
        )->getFirstItem();
        return $model->getPaymentMethod();
    }

    /**
     * @return bool
     */
    public function arePaymentMethodsEmpty()
    {
        return count($this->getPaymentMethods()) == 0;
    }
}
