<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Sponsor;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class SponsorProducts extends \Magento\Catalog\Block\Product\AbstractProduct
{

    /**
     * Sponsors
     *
     * @var \Webkul\MLM\Model\Sponsors $sponsor
     */
    private $_sponsorFactory;

    /**
     * @var HttpContext
     */
    private $_httpContext;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var FormKey
     */
    protected $formKey;

    /**
     * @var ScopeConfig
     */
    public $scopeConfig;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productlists;

    /**
     * Initialized dependencies
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Webkul\MLM\Model\SponsorsFactory $sponsorFactory,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        HttpContext $httpContext,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Model\SponserConfigurationsFactory $sponsorConfiguration,
        \Magento\Framework\App\RequestInterface $request,
        \Webkul\MLM\Model\SponsorBannerFactory $sponsorBannerFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        CollectionFactory $collectionFactory,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Webkul\MLM\Helper\Data $helper,
        array $data = []
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->scopeConfig = $context->getScopeConfig();
        $this->_httpContext = $httpContext;
        $this->session = $session;
        $this->priceHelper = $priceHelper;
        $this->formKey = $formKey;
        $this->_sponsorFactory = $sponsorFactory;
        $this->sponsorHelper = $sponsorHelper;
        $this->sponsorConfiguration = $sponsorConfiguration;
        $this->request = $request;
        $this->sponsorBannerFactory = $sponsorBannerFactory;
        $this->_storeManager = $storeManager;
        $this->_productCollectionFactory = $collectionFactory;
        $this->imageHelper = $imageHelper;
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * Return Customer id.
     *
     * @return bool|0|1
     */
    public function getCustomerId()
    {
        return $customerId = $this->_httpContext->getValue('customer_id');
    }

    /**
     * getProImgUrl
     * @param Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getProImgUrl($products)
    {
        return $this->imageHelper->init($products, 'product_page_image_small')
                                    ->setImageFile($products->getFile())->getUrl();
    }
    
    public function getLoadedProductCollection()
    {
        try {
            $customerId = $this->getCustomerId();
            $sponsorExist = $this->helper->checkSponsorExistsByCurrentUrl();
            if ($this->sponsorHelper->isCustomerApprovedSponsor($customerId) || $sponsorExist) {
                $sponsorData  = [];
                if ($customerId) {
                    $sponsorData = $this->sponsorConfiguration->create()->getCollection()
                    ->addFieldToFilter('sponsor_id', $customerId)->getFirstItem();
                } else {
                    $sponsorData = $this->sponsorConfiguration->create()->getCollection()
                    ->addFieldToFilter('sponsor_id', $sponsorExist->getCustomerId())->getFirstItem();
                }
               
                if ($sponsorData->getProductIds() != null) {
                    $productIds = json_decode($sponsorData->getProductIds());
                    $collection = $this->_productCollectionFactory->create()->addIdFilter($productIds);
                    $collection->addMinimalPrice()
                        ->addFinalPrice()
                        ->addTaxPercents()
                        ->addAttributeToFilter('status', \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
                        ->addAttributeToSelect('*');
                
                    return $collection;
                }
            }
        } catch (\Exception $e) {

        }
    }
}
