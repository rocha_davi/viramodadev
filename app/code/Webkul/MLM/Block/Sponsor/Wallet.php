<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Sponsor;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Http\Context as HttpContext;

class Wallet extends \Magento\Framework\View\Element\Template
{
    /**
     * Path to template file in theme.
     *
     * @var string
     */
    protected $_template = "sponsor/wallet.phtml";

    /**
     * Sponsors
     *
     * @var \Webkul\MLM\Model\Sponsors $sponsor
     */
    private $_sponsorFactory;

    /**
     * @var HttpContext
     */
    private $_httpContext;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var FormKey
     */
    protected $formKey;

    /**
     * @var ScopeConfig
     */
    public $scopeConfig;

    /**
     * Initialized dependencies
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Webkul\MLM\Model\SponsorsFactory $sponsorFactory,
        \Webkul\MLM\Model\SponsorWalletTransaction $sponsorWalletTransaction,
        \Webkul\MLM\Model\SponsorWallet $sponsorWallet,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Helper\Data $dataHelper,
        \Webkul\MLM\Helper\Earning $earningHelper,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        HttpContext $httpContext,
        Context $context,
        array $data = []
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->scopeConfig = $context->getScopeConfig();
        $this->_httpContext = $httpContext;
        $this->session = $session;
        $this->priceHelper = $priceHelper;
        $this->formKey = $formKey;
        $this->_sponsorFactory = $sponsorFactory;
        $this->sponsorWalletTransaction = $sponsorWalletTransaction;
        $this->sponsorWallet = $sponsorWallet;
        $this->sponsorHelper = $sponsorHelper;
        $this->earningHelper = $earningHelper;
        $this->dataHelper = $dataHelper;
        parent::__construct($context, $data);
        $this->setCollection($this->getGridCollection());
    }
    
    /**
     * Get Grid Collection
     *
     * @return void
     */
    public function getGridCollection()
    {
        $collection = $this->sponsorWalletTransaction->getCollection();
        $collection->addFieldToFilter("sponsor_id", ['eq' => $this->getSponsorId()]);
        return $collection;
    }

    /**
     * Return Customer id.
     *
     * @return bool|0|1
     */
    public function getCustomerId()
    {
        return $customerId = $this->_httpContext->getValue('customer_id');
    }

    /**
     * _prepareLayout prepare pager for rate list
     * @return void
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getCollection()) {
            $pager = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager::class,
                'wallet.sponsor.list.pager'
            )->setCollection(
                $this->getCollection()
            );
            $this->setChild('pager', $pager);
            $this->getCollection()->load();
        }
        return $this;
    }

    public function walletId()
    {
        $wallet = $this->getWallet();
        return $wallet->getId();
    }

    /**
     * Current Monthly Balance
     *
     * @return string
     */
    public function getWallet()
    {
        $sponsorId = $this->getSponsorId();
        $wallet = $this->sponsorWallet
            ->getCollection()->addFieldToFilter('sponsor_id', $sponsorId)->getFirstItem();
        return $wallet;
    }

    /**
     * @return int
     */
    public function getSponsorId()
    {
        $customerId = $this->getCustomerId();
        $sponsorId = $this->sponsorHelper->getSponsorId($customerId);
        return $sponsorId;
    }

    /**
     * Get Currency Price
     *
     * @return string
     */
    private function getCurrencyPrice($price)
    {
        return $this->priceHelper->currency(
            number_format($price, 2),
            true,
            false
        );
    }

    /**
     * Current Monthly Balance
     *
     * @return string
     */
    public function currentMonthlyBalance()
    {
        $wallet = $this->getWallet();
        $currentBalance = $wallet->getCurrentBalance();
        return $this->dataHelper->getFormattedPrice($currentBalance);
    }
    
    /**
     * Current Monthly Balance
     *
     * @return string
     */
    public function totalEarning()
    {
        $sponsorId = $this->getSponsorId();
        $totalEarning = $this->earningHelper->getTotalEarningAmt($sponsorId);
        return $this->dataHelper->getFormattedPrice($totalEarning);
    }

    /**
     * Current Monthly Balance
     *
     * @return string
     */
    public function requestedAmount()
    {
        $sponsorId = $this->getSponsorId();
        $totalEarning = $this->sponsorHelper->getTotalPendingAmt($sponsorId);
        return $this->dataHelper->getFormattedPrice($totalEarning);
    }

    /**
     * Get Genealogy Tree
     *
     * @return float
     */
    public function requestedAmountUrl()
    {
        return $this->urlBuilder->getUrl("mlm/sponsor/paymentrequest");
    }
}
