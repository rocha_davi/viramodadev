<?php
/**
 * Webkul MLM Banner.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Block\Sponsor;

use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Model\Session;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Webkul\MLM\Helper\Data as DataHelper;
use Webkul\MLM\Helper\Sponsor as SponsorHelper;
use Magento\Catalog\Helper\Image as ImageHelper;

class Ads extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productlists;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    private $imageHelper;

    /**
     * @param Context           $context
     * @param Session           $customerSession,
     * @param AffDataHelper     $affDataHelper,
     * @param CollectionFactory $collectionFactory,
     * @param array             $data
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CollectionFactory $collectionFactory,
        DataHelper $dataHelper,
        SponsorHelper $sponsorHelper,
        ImageHelper $imageHelper,
        array $data = []
    ) {
        $this->dataHelper = $dataHelper;
        $this->collectionFactory = $collectionFactory;
        $this->imageHelper = $imageHelper;
        $this->sponsorHelper = $sponsorHelper;
        $this->customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    /**
     * @return Session
     */
    public function getCustomerSession()
    {
        return $this->customerSession;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        $customerId = $this->getCustomerSession()->getCustomerId();
        return $customerId;
    }

    /**
     * @return string
     */
    public function getSponsorCode()
    {
        $customerId = $this->getCustomerId();
        return $this->sponsorHelper->getSponsorCode($customerId);
    }

    /**
     * @return bool|\Magento\Ctalog\Model\ResourceModel\Product\Collection
     */
    public function getAllProducts()
    {

        $customerId = $this->getCustomerId();
        if (!$customerId) {
            return false;
        }

        if (!$this->productlists) {
            $this->productlists = $this->collectionFactory->create()
                                                        ->addFieldToFilter("status", 1)
                                                        ->addAttributeToSelect('*')
                                                        ->addFieldToFilter('visibility', ['neq'=>1]);
            ;
        }
        $param = $this->getRequest()->getParams();
        if (isset($param['proName']) && $param['proName']) {
            $proName = $param['proName'];
            $this->productlists->addFieldToFilter('name', ['like' => '%' . $proName. '%']);
        }
        $page = $this->getRequest()->getParam('p', 1);
        $pageSize = $this->getRequest()->getParam('limit', 5);
        $this->productlists->setPageSize($pageSize);
        $this->productlists->setCurPage($page);
        return $this->productlists;
    }

    /**
     * getProImgUrl
     * @param Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getProImgUrl($products)
    {
        return $this->imageHelper->init($products, 'product_page_image_small')
                                    ->setImageFile($products->getFile())->getUrl();
    }

    /**
     * getHtmlCodeForAds
     * @param Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getHtmlCodeForAds($baseUrl, $product, $sponsorCode)
    {
        return "<div style='padding: 10px; border: 1px solid #CCC;width:12%; '>
        <a href='".$baseUrl."catalog/product/view/id/".$product->getId()
                ."?&ref_id=".$sponsorCode."' style='text-decoration: none;'><div style='text-align:center;'><strong>"
                .$product->getName()."</strong><div><img src='".$this->getProImgUrl($product)."' alt='Ad' /></a></div>";
    }

    /**
     * @return $this
     */
    public function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getAllProducts()) {
            $pager = $this->getLayout()
                    ->createBlock(\Magento\Theme\Block\Html\Pager::class, 'mlm.product.list.pager')
                    ->setAvailableLimit([5 => 5, 10 => 10, 15 => 15, 20 => 20])
                    ->setShowPerPage(true)
                    ->setCollection($this->getAllProducts());
            $this->setChild('pager', $pager);
            $this->getAllProducts()->load();
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
