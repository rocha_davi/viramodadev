<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Sponsor;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class Configurations extends \Magento\Framework\View\Element\Template
{

    /**
     * Sponsors
     *
     * @var \Webkul\MLM\Model\Sponsors $sponsor
     */
    private $_sponsorFactory;

    /**
     * @var HttpContext
     */
    private $_httpContext;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var FormKey
     */
    protected $formKey;

    /**
     * @var ScopeConfig
     */
    public $scopeConfig;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productlists;

    /**
     * Initialized dependencies
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Webkul\MLM\Model\SponsorsFactory $sponsorFactory,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        HttpContext $httpContext,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Model\SponserConfigurationsFactory $sponsorConfiguration,
        \Magento\Framework\App\RequestInterface $request,
        \Webkul\MLM\Model\SponsorBannerFactory $sponsorBannerFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Context $context,
        CollectionFactory $collectionFactory,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Webkul\MLM\Helper\Data $helper,
        array $data = []
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->scopeConfig = $context->getScopeConfig();
        $this->_httpContext = $httpContext;
        $this->session = $session;
        $this->priceHelper = $priceHelper;
        $this->formKey = $formKey;
        $this->_sponsorFactory = $sponsorFactory;
        $this->sponsorHelper = $sponsorHelper;
        $this->sponsorConfiguration = $sponsorConfiguration;
        $this->request = $request;
        $this->sponsorBannerFactory = $sponsorBannerFactory;
        $this->_storeManager = $storeManager;
        $this->collectionFactory = $collectionFactory;
        $this->imageHelper = $imageHelper;
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * Return Customer id.
     *
     * @return bool|0|1
     */
    public function getCustomerId()
    {
        return $customerId = $this->_httpContext->getValue('customer_id');
    }

    /**
     * Get Genealogy Tree
     *
     * @return float
     */
    public function formUrl()
    {
        return $this->urlBuilder->getUrl("mlm/sponsor/submitsponsordetails");
    }

    /**
     * get sponsor logo data
     *
     * @return void
     */
    public function getLogoData()
    {
        $customerId =  $this->getCustomerId();
        if ($this->sponsorHelper->isCustomerApprovedSponsor($customerId)) {
            $logoDetails = $this->sponsorConfiguration->create()->getCollection()
            ->addFieldToFilter('sponsor_id', $customerId)->getFirstItem();
            
            return $logoDetails;
        }
    }
    /**
     * get add banner url
     *
     * @return string
     */
    public function getBannerUrl()
    {
        return $this->urlBuilder->getUrl("mlm/sponsor_banner/add", ['_secure' => $this->request->isSecure()]);
    }
    /**
     * get save banner url
     *
     * @return string
     */
    public function addBannerFormUrl()
    {
        return $this->urlBuilder->getUrl('mlm/sponsor_banner/saveBanner', ['_secure' => $this->request->isSecure()]);
    }
    public function getSponsorBannerCollection()
    {
        try {
            $customerId =  $this->getCustomerId();
            $sponsorExist = $this->helper->checkSponsorExistsByCurrentUrl();
            if ($this->sponsorHelper->isCustomerApprovedSponsor($customerId) || $sponsorExist) {
                if ($customerId) {
                    $bannerDetails = $this->sponsorBannerFactory->create()->getCollection()
                    ->addFieldToFilter('sponsor_id', $customerId)
                    ->setOrder('sortorder', 'ASC');
                    return $bannerDetails;
                } else {
                    $bannerDetails = $this->sponsorBannerFactory->create()->getCollection()
                    ->addFieldToFilter('sponsor_id', $sponsorExist->getCustomerId())
                    ->setOrder('sortorder', 'ASC');
                    return $bannerDetails;
                }
                
               
            }
        } catch (\Exception $e) {

        }
    }
    public function getMediaUrl()
    {
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        return $mediaUrl;
    }
    public function getEditBannerUrl($banner)
    {
        return $this->urlBuilder->getUrl('mlm/sponsor_banner/add', ['_current' => true,'_use_rewrite' => true, 'id' => $banner->getEntityId()]);
    }

    public function getBannerData()
    {
       
        try {
            $customerId =  $this->getCustomerId();
            
            if ($this->sponsorHelper->isCustomerApprovedSponsor($customerId)) {
                $id = $this->request->getParam('id');
                $bannerDetails = $this->sponsorBannerFactory->create()->getCollection()
                ->addFieldToFilter('sponsor_id', $customerId)
                ->addFieldToFilter('entity_id', $id)->getFirstItem();
                
                return $bannerDetails;
            }
        } catch (\Exception $e) {

        }
    }
    public function getProductList()
    {
        $customerId = $this->getCustomerId();
        if (!$customerId) {
            return false;
        }

        if (!$this->productlists) {
            $this->productlists = $this->collectionFactory->create()
                                        ->addFieldToFilter("status", 1)
                                        ->addAttributeToSelect('*')
                                        ->addFieldToFilter('visibility', ['neq'=>1]);
            ;
        }
        $param = $this->getRequest()->getParams();
        
        $page = $this->getRequest()->getParam('p', 1);
        $pageSize = $this->getRequest()->getParam('limit', 5);
        $this->productlists->setPageSize($pageSize);
        $this->productlists->setCurPage($page);
        return $this->productlists;
    }
    /**
     * getProImgUrl
     * @param Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getProImgUrl($products)
    {
        return $this->imageHelper->init($products, 'product_page_image_small')
                                    ->setImageFile($products->getFile())->getUrl();
    }
        /**
         * @return $this
         */
    public function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getProductList()) {
            $pager = $this->getLayout()
                    ->createBlock(\Magento\Theme\Block\Html\Pager::class, 'mlm.configuration.product.list.pager')
                    ->setAvailableLimit([5 => 5, 10 => 10, 15 => 15, 20 => 20])
                    ->setShowPerPage(true)
                    ->setCollection($this->getProductList());
            $this->setChild('pager', $pager);
            $this->getProductList()->load();
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
