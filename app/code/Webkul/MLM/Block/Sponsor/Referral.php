<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Sponsor;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Http\Context as HttpContext;

class Referral extends \Magento\Framework\View\Element\Template
{
    const CREATE_ACCOUNT_URL = "customer/account/create";
    /**
     * Path to template file in theme.
     *
     * @var string
     */
    protected $_template = "sponsor/referral.phtml";

    /**
     * Sponsors
     *
     * @var \Webkul\MLM\Model\Sponsors $sponsor
     */
    private $_sponsorFactory;

    /**
     * @var HttpContext
     */
    private $_httpContext;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var FormKey
     */
    protected $formKey;

    /**
     * @var ScopeConfig
     */
    public $scopeConfig;

    /**
     * Initialized dependencies
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Webkul\MLM\Model\SponsorsFactory $sponsorFactory,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Customer\Model\Session $session,
        HttpContext $httpContext,
        Context $context,
        array $data = []
    ) {
        $this->scopeConfig = $context->getScopeConfig();
        $this->_httpContext = $httpContext;
        $this->session = $session;
        $this->formKey = $formKey;
        $this->_sponsorFactory = $sponsorFactory;
        parent::__construct($context, $data);
    }

    /**
     * Return Customer id.
     *
     * @return bool|0|1
     */
    public function getCustomerId()
    {
        return $customerId = $this->_httpContext->getValue('customer_id');
    }

    /**
     * Get Referral Data
     *
     * @return array
     */
    public function getReferralData()
    {
        $returnData = [];
        $customerId = $this->getCustomerId();
        $collection = $this->_sponsorFactory->create()->getCollection();
        $collection->addFieldToFilter("customer_id", $customerId);
        $returnData["pageTitle"] = __("Código de Referência");
        $returnData["referralId"] = "";
        $returnData["emailPageUrl"] = $this->getUrl("mlm/sponsor/email");
        $returnData["addCustomerLabel"] = __("adicione até 2 clientes");
        
        foreach ($collection as $model) {
            $returnData["referralId"] = $model->getSponsorCode();
        }
        $returnData["shareableLink"] = $this->getUrl(
            self::CREATE_ACCOUNT_URL,
            [
                "referralCode" => $returnData["referralId"]
            ]
        );
        return $returnData;
    }
}
