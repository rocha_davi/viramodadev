<?php
/**
 * Block for Quote list at customer end.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Block\Sponsor\Wallet;

use Webkul\MLM\Model\ResourceModel\SponsorWalletTransaction\CollectionFactory;
use Magento\Framework\Pricing\Helper\Data;
use Webkul\MLM\Model\SponsorWalletTransaction;
use Webkul\MLM\Helper\Data as Helper;

class Transaction extends \Magento\Framework\View\Element\Template
{
    /**
     * @var $nameClass
     */
    public $nameClass = "fa fa-sort";

    /**
     * @var $quantityClass
     */
    public $quantityClass = "fa fa-sort";

    /**
     * @var $priceClass
     */
    public $priceClass = "fa fa-sort";

    /**
     * [__construct description]
     *
     * @param MagentoCustomerModelSession         $customerSession
     * @param MagentoCatalogBlockProductContext   $context
     * @param CollectionFactory                   $quotesCollectionFactory
     * @param Data                                $pricingHelper
     * @param MagentoFrameworkUrlEncoderInterface $urlEncoder
     * @param Quotes                              $quotesModel
     * @param [type]                              $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        CollectionFactory $walletTransactionCollF,
        Data $pricingHelper,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        SponsorWalletTransaction $walletTransModel,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Helper\Earning $earningHelper,
        Helper $helper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        array $data = []
    ) {
        $this->customerSession = $customerSession;
        $this->walletTransactionCollF = $walletTransactionCollF;
        $this->pricingHelper = $pricingHelper;
        $this->urlEncoder = $urlEncoder;
        $this->sponsorHelper = $sponsorHelper;
        $this->walletTransModel = $walletTransModel;
        $this->earningHelper = $earningHelper;
        $this->storeManager =  $context->getStoreManager();
        $this->helper = $helper;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context, $data);
    }
    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $collection = $this->getWalletTransactionCollection();
        if ($collection) {
            $pager = $this->getLayout()->createBlock(
                \Magento\Theme\Block\Html\Pager ::class,
                'sponsor.wallet.transaction.pager'
            )->setAvailableLimit([5 => 5, 10 => 10, 15 => 15, 20 => 20])
            ->setShowPerPage(true)
            ->setCollection(
                $collection
            );
            $this->setChild('pager', $pager);
            $collection->load();
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * customer Id by customer session
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerSession->getCustomerId();
    }

    /**
     * get Collection of quotes
     *
     * @return collection
     */
    public function getWalletTransactionCollection()
    {
        $filter = '';
        $filterStatus = 0;
        $filterSorting = '';
        $customerId = $this->getCustomerId();
        $sponsorId = $this->sponsorHelper->getSponsorId($customerId);
        $collection = $this->sponsorHelper->getWalletTxnCollection($sponsorId);
        $paramData = $this->getRequest()->getParams();
        $page = $this->getRequest()->getParam('p', 1);
        $pageSize = $this->getRequest()->getParam('limit', 5);
        $filterDataTo = '';
        $filterDataFrom = '';
        $from = null;
        $to = null;

        if (!empty($paramData['from_date'])) {
            $filterDataFrom = $this->formatFilterDate($paramData['from_date']) ;
        }
        if (!empty($paramData['from_to'])) {
            $filterDataTo = $this->formatFilterDate($paramData['from_to']) ;
        }
        if ($filterDataTo) {
            $todate = date_create($filterDataTo);
            $to = date_format($todate, 'Y-m-d 23:59:59');
        }
        if ($filterDataFrom) {
            $fromdate = date_create($filterDataFrom);
            $from = date_format($fromdate, 'Y-m-d H:i:s');
        }
        if ($from && (!$to)) {
            $collection->addFieldToFilter(
                'created_at',
                ['datetime' => true, 'from' => $from]
            );
        }
        if ((!$from) && ($to)) {
            $collection->addFieldToFilter(
                'created_at',
                ['datetime' => true, 'to' => $to]
            );
        }
        if (($from) && ($to)) {
            $collection->addFieldToFilter(
                'created_at',
                ['datetime' => true, 'to' => $to, 'from'=>$from]
            );
        }

        $collection->setOrder(
            'created_at',
            'desc'
        );
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);

        return $collection;
    }

    /**
     * use to get current url.
     */
    public function getCurrentUrl()
    {
        return $this->_urlBuilder->getCurrentUrl();
    }

    /**
     * @param string $date
     * @return string
     */
    public function formatFilterDate($date)
    {
        $time = strtotime($date);
        return date('Y-m-d', $time);
    }

    /**
     * getIsSecure
     */
    public function getIsSecure()
    {
        return $this->getRequest()->isSecure();
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->getBaseUrl();
    }

    /**
     * Get Helper
     *
     * @return Webkul\MLM\Helper\Data
     */
    public function getHelper()
    {
        return $this->helper;
    }

    /**
     * Get Json Helper
     *
     * @return Magento\Framework\Json\Helper\Data
     */
    public function getJsonHelper()
    {
        return $this->jsonHelper;
    }

    /**
     * @return \Webkul\MLM\Helper\Sponsor
     */
    public function getSponsorHelper()
    {
        return $this->sponsorHelper;
    }

    /**
     * @return \Webkul\MLM\Helper\Earning
     */
    public function getEarningHelper()
    {
        return $this->earningHelper;
    }
}
