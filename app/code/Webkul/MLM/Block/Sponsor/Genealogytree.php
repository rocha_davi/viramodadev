<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Sponsor;

use Webkul\MLM\Helper\StoreConfiguration;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Http\Context as HttpContext;
use Webkul\MLM\Model\Sponsors\Source\Status as SponsorStatus;

class Genealogytree extends \Magento\Framework\View\Element\Template
{
    private $_nodes = [];

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $json;
    
    /**
     * Path to template file in theme.
     *
     * @var string
     */
    protected $_template = "sponsor/genealogytree.phtml";

    /**
     * Sponsors
     *
     * @var \Webkul\MLM\Model\Sponsors $sponsor
     */
    private $_sponsorFactory;

    /**
     * @var HttpContext
     */
    private $_httpContext;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var ScopeConfig
     */
    public $scopeConfig;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepositoryInterface;

    /**
     * Initialized dependencies
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Webkul\MLM\Model\SponsorsFactory $sponsorFactory,
        \Magento\Customer\Model\Session $session,
        HttpContext $httpContext,
        Context $context,
        \Magento\Framework\Json\Helper\Data $json,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        array $data = []
    ) {
        $this->scopeConfig = $context->getScopeConfig();
        $this->_httpContext = $httpContext;
        $this->json = $json;
        $this->session = $session;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_sponsorFactory = $sponsorFactory;
        parent::__construct($context, $data);
    }

    /**
     * Return Customer id.
     *
     * @return bool|0|1
     */
    public function getCustomerId()
    {
        return $customerId = $this->_httpContext->getValue('customer_id');
    }

    /**
     * Get Tree
     *
     * @return array
     */
    public function getTreeCollection()
    {
        $customerId =  $this->getCustomerId();
        $sponsorCode = $this->getCustomerSponsorId($customerId);
        if ($sponsorCode) {
            $this->getNode($sponsorCode);
        }
        return $this->_nodes;
    }

    /**
     * Json Encode
     *
     * @param array $data
     * @return string
     */
    public function jsonEncode($data)
    {
        return $this->json->jsonEncode($data);
    }

    /**
     * Get Node
     *
     * @param string $sponsorCode
     * @return void
     */
    private function getNode($sponsorCode)
    {
        $collection = $this->_sponsorFactory->create()->getCollection();
        $collection->addFieldToFilter("sponsor_reference_code", $sponsorCode)
        ->addFieldToFilter("status", SponsorStatus::STATUS_ENABLED);
        foreach ($collection as $model) {
            $customer = $this->getCustomer(
                $model->getCustomerId()
            );
            $this->_nodes[$sponsorCode][] = [
                "sponsor_code" => $model->getSponsorCode(),
                "sponsor_reference_code" => $model->getSponsorReferenceCode(),
                "customer_id" => $model->getCustomerId(),
                "customer_name" => $customer->getFirstname()." ".$customer->getLastname()
            ];
            $this->getNode(
                $model->getSponsorCode()
            );
        }
    }

    /**
     * Get Customer Sponsor Id
     *
     * @param int $customerId
     * @return Sponsors
     */
    public function getSponsor($customerId)
    {
        return $this->_sponsorFactory->create()->load(
            $customerId,
            "customer_id"
        );
    }

    /**
     * Get Customer Sponsor Id
     *
     * @param int $customerId
     * @return Sponsors
     */
    public function getReferralSponsor($code)
    {
        return $this->_sponsorFactory->create()->getCollection()->addFieldToFilter(
            "sponsor_code",
            $code
        )->getFirstItem();
    }

    /**
     * Get Customer Sponsor Id
     *
     * @param int $customerId
     * @return string
     */
    public function getCustomerSponsorId($customerId)
    {
        return $this->getSponsor($customerId)->getSponsorCode();
    }

    /**
     * Get Customer
     *
     * @param int $customerId
     * @return Customer
     */
    public function getCustomer($customerId)
    {
        return $this->_customerRepositoryInterface->getById(
            $customerId
        );
    }
}
