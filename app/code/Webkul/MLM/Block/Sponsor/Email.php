<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Sponsor;

use Webkul\MLM\Helper\StoreConfiguration;

class Email extends Referral
{
    /**
     * Path to template file in theme.
     *
     * @var string
     */
    protected $_template = "sponsor/email.phtml";

    /**
     * get form key
     *
     * @return string
     */
    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }

    /**
     * Get Body Hint
     *
     * @return void
     */
    public function getBodyContentHint()
    {
        $path = StoreConfiguration::SECTION."/";
        $path .= StoreConfiguration::EMAIL_SECTION."/";
        $path .= StoreConfiguration::EMAIL_BODY;
        return $this->scopeConfig->getValue($path);
    }
    /**
     * Get form data
     *
     * @return string
     */
    public function getFormData()
    {
        $referralData = $this->getReferralData();
        
        $rows = [
            [
                "value" => "",
                "input" => "hidden",
                "options" => [],
                "name" => "sponsorId",
                "index" => "sponsorId",
                "class" => " sponsorId  input-text",
                'label' => __('sponsorId'),
                "isRequired" => '',
                "value" => $referralData["referralId"]
            ],
            [
                "value" => "",
                "input" => "hidden",
                "options" => [],
                "name" => "form_key",
                "index" => "form_key",
                "class" => " form_key  input-text",
                'label' => __('Form Key'),
                "isRequired" => '',
                "value" => $this->getFormKey()
            ],
            [
                "value" => "",
                "input" => "text",
                "options" => [],
                "name" => "email",
                "index" => "email",
                "class" => " name required-entry input-text",
                'label' => __('Email'),
                "isRequired" => 'required',
                "placeHolder" => __("Friend Email Id")
            ],
            [
                "value" => "",
                "input" => "text",
                "options" => [],
                "name" => "subject",
                "index" => "subject",
                "class" => "subject required-entry input-text",
                'label' => __('Subject'),
                "isRequired" => 'required',
                "placeHolder" => __("Subject")
            ],
            [
                "value" => "",
                "input" => "editor",
                "options" => [],
                "name" => "body",
                "index" => "body",
                "class" => "body required-entry input-text",
                'label' => __('Body'),
                "isRequired" => 'required',
                "placeHolder" => __("Body")
            ]
        ];
        $data = [];
        
        if ($this->isEditPage() && $data) {
            array_push($rows, [
                "value" => "",
                "input" => "hidden",
                "options" => [],
                "name" => "entity_id",
                "index" => 'entity_id',
                "class" => "",
                'label' => '',
                "isRequired" => ''
            ]);

            foreach ($rows as &$row) {
                if (isset($data[$row['index']])) {
                    $row["value"] = $data[$row['index']];
                }
            }
        }
        return \Zend_Json::encode($rows, true);
    }

    /**
     * IsEditPage
     *
     * @return boolean
     */
    public function isEditPage()
    {
        return false;
    }
}
