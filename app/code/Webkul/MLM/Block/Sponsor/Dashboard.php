<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Sponsor;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\Config\ConfigOptionsListConstants;

class Dashboard extends \Magento\Framework\View\Element\Template
{
    /**
     * @var string
     */
    protected $_deploymentConfigDate;

    /**
     * Seller statistics graph width.
     *
     * @var string
     */
    protected $_width = '800';

    /**
     * Seller statistics graph height.
     *
     * @var string
     */
    protected $_height = '375';
    /**
     * Path to template file in theme.
     *
     * @var string
     */
    protected $_template = "sponsor/dashboard.phtml";

    /**
     * Sponsors
     *
     * @var \Webkul\MLM\Model\Sponsors $sponsor
     */
    private $_sponsorFactory;

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var HttpContext
     */
    private $_httpContext;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var FormKey
     */
    protected $formKey;

    /**
     * @var ScopeConfig
     */
    public $scopeConfig;

    /**
     * Initialized dependencies
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Webkul\MLM\Model\SponsorsFactory $sponsorFactory,
        \Webkul\MLM\Helper\Earning $earningHelper,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        HttpContext $httpContext,
        Genealogytree $genealogyTree,
        Context $context,
        EncryptorInterface $encryptor,
        DeploymentConfig $deploymentConfig,
        \Webkul\MLM\Helper\MemberLevel $memberLevelHelper,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Helper\Data $dataHelper,
        array $data = []
    ) {
        $this->encryptor = $encryptor;
        $this->_deploymentConfigDate = $deploymentConfig->get(
            ConfigOptionsListConstants::CONFIG_PATH_INSTALL_DATE
        );
        $this->urlBuilder = $context->getUrlBuilder();
        $this->scopeConfig = $context->getScopeConfig();
        $this->_httpContext = $httpContext;
        $this->session = $session;
        $this->priceHelper = $priceHelper;
        $this->formKey = $formKey;
        $this->genealogyTree = $genealogyTree;
        $this->_sponsorFactory = $sponsorFactory;
        $this->earningHelper = $earningHelper;
        $this->memberLevelHelper = $memberLevelHelper;
        $this->sponsorHelper = $sponsorHelper;
        $this->dataHelper = $dataHelper;
        parent::__construct($context, $data);
    }

    /**
     * Return Customer id.
     *
     * @return bool|0|1
     */
    public function getCustomerId()
    {
        return $customerId = $this->_httpContext->getValue('customer_id');
    }

    /**
     * @return string
     */
    public function getMemberLevelName()
    {
        $sponsorId = $this->earningHelper->getSponsorId($this->getCustomerId());
        $memberLevelName = $this->sponsorHelper->getMemberLevelName($sponsorId);
        return $memberLevelName;
    }

    /**
     * Get Sales Earning
     *
     * @return float
     */
    public function getSalesEarning()
    {
        $sponsorId = $this->earningHelper->getSponsorId($this->getCustomerId());
        $salesEarningAmt = $this->earningHelper->getTotalSalesEarningAmt($sponsorId);
        return $this->dataHelper->getFormattedPrice($salesEarningAmt);
    }

    /**
     * Get New Joining Sales Earning
     *
     * @return float
     */
    public function getNewJoiningSalesEarning()
    {
        $sponsorId = $this->earningHelper->getSponsorId($this->getCustomerId());
        $salesEarningAmt = $this->earningHelper->getTotalJoiningEarningAmt($sponsorId);
        return $this->dataHelper->getFormattedPrice($salesEarningAmt);
    }

    /**
     * Get Level Sales Earing
     *
     * @return float
     */
    public function getLevelSalesEarning()
    {
        $sponsorId = $this->earningHelper->getSponsorId($this->getCustomerId());
        $salesEarningAmt = $this->earningHelper->getTotalLevelEarningAmt($sponsorId);
        return $this->dataHelper->getFormattedPrice($salesEarningAmt);
    }

    /**
     * Get Level Sales Earing
     *
     * @return float
     */
    public function getTotalEarning()
    {
        $sponsorId = $this->earningHelper->getSponsorId($this->getCustomerId());
        $salesEarningAmt = $this->earningHelper->getTotalEarningAmt($sponsorId);
        return $this->dataHelper->getFormattedPrice($salesEarningAmt);
    }

    /**
     * Get Current Sales Earning
     *
     * @return float
     */
    public function getCurrentMonthSalesEarning()
    {
        $sponsorId = $this->earningHelper->getSponsorId($this->getCustomerId());
        $salesEarningAmt = $this->earningHelper->getMonthlyEarningAmt($sponsorId);
        return $this->dataHelper->getFormattedPrice($salesEarningAmt);
    }

    /**
     * Get Current Sales Earning
     *
     * @return float
     */
    public function getDownlineMembersCount()
    {
        $sponsorId = $this->sponsorHelper->getSponsorId($this->getCustomerId());
        return $this->sponsorHelper->getDownlineMemberCount($sponsorId);
    }
    
    /**
     * Get Genealogy Tree
     *
     * @return float
     */
    public function getGenealogyTreeUrl()
    {
        return $this->urlBuilder->getUrl("mlm/sponsor/genealogytree");
    }
    
    /**
     * Get Current Sales Earning
     *
     * @return float
     */
    public function becomeDiamondMemberThreshold()
    {
        $amount = $this->scopeConfig->getValue(
            "mlm/configuration/diamond_member_threshold"
        ) ?? 800;
        return $this->dataHelper->getFormattedPrice($amount);
    }
    
    /**
     * Get Current Sales Earning
     *
     * @return float
     */
    public function mySponsorId()
    {
        $customerId =  $this->getCustomerId();
        $sponsorCode = $this->genealogyTree->getCustomerSponsorId($customerId);
        return $sponsorCode;
    }
    
    /**
     * Get Current Sales Earning
     *
     * @return float
     */
    public function referralSponsorId()
    {
        $customerId =  $this->getCustomerId();
        $model = $this->genealogyTree->getSponsor($customerId);
        $referralSponsorCode = $model->getData("sponsor_reference_code");
        return $referralSponsorCode;
    }
    
    /**
     * Get Current Sales Earning
     *
     * @return float
     */
    public function getCustomer()
    {
        $customerId =  $this->getCustomerId();
        $model = $this->genealogyTree->getCustomer($customerId);
        return $model;
    }
    
    /**
     * Get Referral Customer
     *
     * @return float
     */
    public function getReferralCustomer()
    {
        $referralId =  $this->referralSponsorId();
        $adminReferralId = $this->scopeConfig->getValue(
            "mlm/configuration/admin_sponsor_id"
        );
        if ($adminReferralId == $referralId) {
            return 'admin';
        }
        $model = $this->genealogyTree->getReferralSponsor($referralId);
        $model = $this->genealogyTree->getCustomer($this->getCustomerId());
        return $model;
    }

    /**
     * Get Admin Email
     *
     * @return string
     */
    public function getAdminEmail()
    {
        return $this->scopeConfig->getValue(
            "trans_email/ident_general/email"
        );
    }

    /**
     * @return string
     */
    public function getDailyEarningChartData()
    {
        $earningData = $this->earningHelper->getDailyEarningChartData($this->getSponsorId());
        return \Zend_Json::encode($earningData);
    }

    /**
     * @return string
     */
    public function getWeeklyEarningChartData()
    {
        $earningData = $this->earningHelper->getWeeklyEarningChartData($this->getSponsorId());
        return \Zend_Json::encode($earningData);
    }

    /**
     * @return string
     */
    public function getMonthlyEarningChartData()
    {
        $earningData = $this->earningHelper->getMonthlyEarningChartData($this->getSponsorId());
        return \Zend_Json::encode($earningData);
    }

    /**
     * @return string
     */
    public function getYearlyEarningChartData()
    {
        $earningData = $this->earningHelper->getYearlyEarningChartData($this->getSponsorId());
        return \Zend_Json::encode($earningData);
    }

    /**
     * @return int
     */
    public function getSponsorId()
    {
        $sponsorId = $this->earningHelper->getSponsorId($this->getCustomerId());
        return $sponsorId;
    }

    /**
     * @return string
     */
    public function getLast4MemberActivities()
    {
        $last4Earnings = $this->earningHelper
            ->getLast4MemberActivities($this->getSponsorId());
        return \Zend_Json::encode($last4Earnings);
    }

    /**
     * @return string
     */
    public function getBadgeImageUrl()
    {
        $sponsorId = $this->earningHelper->getSponsorId($this->getCustomerId());
        return $this->memberLevelHelper->getBadgeImageUrl($sponsorId);
    }
}
