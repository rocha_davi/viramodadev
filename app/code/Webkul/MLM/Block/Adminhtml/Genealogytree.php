<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Block\Adminhtml;

use Webkul\MLM\Helper\ConfigConstants as CC;
use Webkul\MLM\Model\Sponsors\Source\Status as SponsorStatus;

class Genealogytree extends \Magento\Framework\View\Element\Template
{
    /**
     * Admin Sponsor Id
     *
     * @var string
     */
    private $_adminSponsorId;

    /**
     * Nodes
     *
     * @var array
     */
    private $_nodes = [];

    /**
     * Path to template file in theme.
     *
     * @var string
     */
    protected $_template = "genealogytree.phtml";

    /**
     * Scope Config
     *
     * @var ScopeConfgInterface
     */
    protected $config;

    /**
     * Json
     *
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $json;

    /**
     * Initialised dependencies
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Json\Helper\Data $json
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\Helper\Data $json,
        \Webkul\MLM\Model\SponsorsFactory $sponsorFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        array $data = []
    ) {
        $this->config = $context->getScopeConfig();
        $this->json = $json;
        $this->_sponsorFactory = $sponsorFactory;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        parent::__construct($context, $data);
    }

    /**
     * Json Encode
     *
     * @param array $data
     * @return void
     */
    public function jsonEncode($data)
    {
        return $this->json->jsonEncode($data);
    }

    /**
     * Get Tree
     *
     * @return void
     */
    public function getTreeCollection()
    {
        $sponsorCode = $this->getAdminSponsorId();
        if ($sponsorCode) {
            $this->getNode($sponsorCode);
        }
        return $this->_nodes;
    }

    /**
     * Get Node
     *
     * @param string $sponsorCode
     * @return void
     */
    private function getNode($sponsorCode)
    {
        $collection = $this->_sponsorFactory->create()->getCollection();
        $collection->addFieldToFilter("sponsor_reference_code", $sponsorCode)
        ->addFieldToFilter("status", SponsorStatus::STATUS_ENABLED);
        foreach ($collection as $model) {
            $customer = $this->getCustomer(
                $model->getCustomerId()
            );
            $this->_nodes[$sponsorCode][] = [
                "sponsor_code" => $model->getSponsorCode(),
                "sponsor_reference_code" => $model->getSponsorReferenceCode(),
                "customer_id" => $model->getCustomerId(),
                "customer_name" => $customer->getFirstname()." ".$customer->getLastname()
            ];
            $this->getNode(
                $model->getSponsorCode()
            );
        }
    }
    
    /**
     * Get Customer
     *
     * @param int $customerId
     * @return object
     */
    public function getCustomer($customerId)
    {
        return $this->_customerRepositoryInterface->getById(
            $customerId
        );
    }

    /**
     * Admin Sponsor function
     *
     * @return string
     */
    public function getAdminSponsorId()
    {
        if (!$this->_adminSponsorId) {
            $this->_adminSponsorId = $this->config->getValue(
                CC::SECTION.
                CC::DIR_SEPARATOR.
                CC::CONFIGURATION_GROUP.
                CC::DIR_SEPARATOR.
                CC::ADMIN_SPONSOR_ID
            );
        }
        return $this->_adminSponsorId;
    }
}
