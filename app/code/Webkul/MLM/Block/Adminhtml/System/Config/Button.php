<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Webkul\MLM\Helper\Data;
use Webkul\MLM\Setup\Patch\Data\AdminSponsorData;

/**
 * Button in configuration for sponsor id
 */
class Button extends Field
{
    /**
     * Helper data reference
     *
     * @var Data
     */
    private $_helper;

    /**
     * Sets the template for block
     *
     * @var string
     */
    protected $_template = 'Webkul_MLM::adminhtml/system/config/button.phtml';

    /**
     * Initialized Dependencies
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $helper,
        \Webkul\MLM\Model\SponsorsFactory $sponsorF,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_helper = $helper;
        $this->sponsorF = $sponsorF;
    }

    /**
     * Remove scope label
     *
     * @param  AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * Return element html
     *
     * @param  AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * Return ajax url for Admin Id generate button
     *
     * @return string
     */
    public function getAjaxUrl()
    {
        return $this->getUrl('mlm/system_config/generate');
    }

    /**
     * Generate collect button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            \Magento\Backend\Block\Widget\Button::class
        )->setData(
            [
                'id' => 'generateId',
                'label' => __('Generate Unique Id'),
            ]
        );
        return $button->toHtml();
    }

    /**
     * GetCurrent Admin sponsor Id
     *
     * @return string
     */
    public function getCurrentValue() :string
    {
        return $this->_helper->getConfigData(
            AdminSponsorData::CONFIGURATION_SECTION.AdminSponsorData::DS.
            AdminSponsorData::CONFIGURATION_GROUP.AdminSponsorData::DS.
            AdminSponsorData::CONFIGURATION_FIELD
        );
    }

    /**
     * Should show button or not in admin configuration
     *
     * @return boolean
     */
    public function canShow() : bool
    {
        return true;
    }
}
