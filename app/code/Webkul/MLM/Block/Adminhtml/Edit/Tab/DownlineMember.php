<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_MLM
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Adminhtml\Edit\Tab;

use Magento\Customer\Controller\RegistryConstants;
use Magento\Ui\Component\Layout\Tabs\TabInterface;
use Magento\Backend\Block\Widget\Form\Generic;

class DownlineMember extends Generic implements TabInterface
{
    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory
     * @param AccountManagementInterface $customerAccountManagement
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Webkul\MLM\Helper\Data $dataHelper,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->dataHelper = $dataHelper;
        $this->sponsorHelper = $sponsorHelper;
    }

    /**
     * @inheritdoc
     */
    public function getTabUrl()
    {
        return $this->getUrl('mlm/sponsors/downlinemember', ['_current' => true]);
    }

    /**
     * @return string
     */
    public function getTabLabel()
    {
        return __('Downline Member');
    }

    /**
     * @return string
     */
    public function getTabTitle()
    {
        return __('Downline Member');
    }

    /**
     * Tab should be loaded trough Ajax call.
     *
     * @return bool
     */
    public function isAjaxLoaded()
    {
        return true;
    }

    /**
     * Tab class getter.
     *
     * @return string
     */
    public function getTabClass()
    {
        return '';
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return (bool)$this->getSponsorId();
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return !$this->canShowTab();
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
    }

    /**
     * @return int
     */
    public function getSponsorId()
    {
        return $this->sponsorHelper->getSponsorId($this->getCustomerId());
    }

    /**
     * @return string
     */
    public function getSponsorCode()
    {
        return $this->sponsorHelper->getSponsorCode($this->getCustomerId());
    }

    /**
     * @return string
     */
    public function getFormHtml()
    {
        $html = parent::getFormHtml();
        $html .= $this->getLayout()->createBlock(
            \Webkul\MLM\Block\Adminhtml\Edit\Form\DownlineMember::class
        )->setData(
            [
                'sponsor_code' => $this->getSponsorCode(),
            ]
        )->toHtml();

        return $html;
    }
}
