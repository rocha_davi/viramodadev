<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_MLM
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Adminhtml\Edit\Tab;

use Magento\Customer\Controller\RegistryConstants;
use Magento\Ui\Component\Layout\Tabs\TabInterface;

class BusinessComment extends \Magento\Backend\Block\Widget\Grid\Extended implements TabInterface
{
    /**
     * Sales reorder
     *
     * @var \Magento\Sales\Helper\Reorder
     */
    protected $_salesReorder = null;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var  \Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory $collectionFactory
     * @param \Magento\Sales\Helper\Reorder $salesReorder
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory $collectionFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Webkul\MLM\Model\EarningType\Source\Options $earningTypeOptions,
        \Webkul\MLM\Helper\Data $dataHelper,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_collectionFactory = $collectionFactory;
        $this->earningTypeOptions = $earningTypeOptions;
        $this->dataHelper = $dataHelper;
        $this->sponsorHelper = $sponsorHelper;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('mlm_sponsor_business_comment_grid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('desc');
        $this->setUseAjax(true);
    }

    /**
     * Apply various selection filters to prepare the sales order grid collection.
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_collectionFactory->getReport('sponsor_business_comment_data_source')->addFieldToSelect(
            'entity_id'
        )->addFieldToSelect(
            'title'
        )->addFieldToSelect(
            'comment'
        )->addFieldToSelect(
            'created_at'
        )->addFieldToFIlter(
            'sponsor_id',
            $this->getSponsorId()
        );
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @inheritdoc
     */
    protected function _prepareColumns()
    {

        $this->addColumn('title', ['header' => __('Title'), 'index' => 'title']);
        $this->addColumn('comment', ['header' => __('Comment'), 'index' => 'comment']);

        $this->addColumn(
            'created_at',
            ['header' => __('Date'), 'index' => 'created_at', 'type' => 'datetime']
        );
        return parent::_prepareColumns();
    }

    /**
     * @inheritdoc
     */
    public function getTabUrl()
    {
        return $this->getGridUrl();
    }

    /**
     * @inheritdoc
     */
    public function getGridUrl()
    {
        return $this->getUrl('mlm/sponsors/businesscomment', ['_current' => true]);
    }

    /**
     * @return string
     */
    public function getTabLabel()
    {
        return __('Comment');
    }

    /**
     * @return string
     */
    public function getTabTitle()
    {
        return __('Comment');
    }

    /**
     * Tab should be loaded trough Ajax call.
     *
     * @return bool
     */
    public function isAjaxLoaded()
    {
        return true;
    }

    /**
     * Tab class getter.
     *
     * @return string
     */
    public function getTabClass()
    {
        return '';
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return (bool)$this->getSponsorId();
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return !$this->canShowTab();
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
    }

    /**
     * @return int
     */
    public function getSponsorId()
    {
        return $this->sponsorHelper->getSponsorId($this->getCustomerId());
    }
}
