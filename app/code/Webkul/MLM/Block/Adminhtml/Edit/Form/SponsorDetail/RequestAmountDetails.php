<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Adminhtml\Edit\Form\SponsorDetail;

class RequestAmountDetails extends \Magento\Backend\Block\Widget
{
    protected $_template = 'Webkul_MLM::sponsor/tab/detail/request_amount_details.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * Admin helper
     *
     * @var \Magento\Sales\Helper\Admin
     */
    protected $_adminHelper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Helper\Data $dataHelper,
        array $data = []
    ) {
        $this->sponsorHelper = $sponsorHelper;
        $this->dataHelper = $dataHelper;
        $this->_adminHelper = $adminHelper;
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getRequestedAmount()
    {
        $amount = $this->sponsorHelper->getLastPaymentAmt($this->getSponsorId());
        return $amount ? $this->dataHelper->getFormattedPrice($amount) : __("Not Available");
    }

    /**
     * @return string
     */
    public function getRequestedAmountDate()
    {
        $date = $this->sponsorHelper->getLastPaymentDate($this->getSponsorId());
        return $date ? $this->dataHelper->formatDateMedium($date)
            : __("Not Available");
    }

    /**
     * @return string
     */
    public function getRequestedAmountTxnId()
    {
        return $this->sponsorHelper->getLastPaymentTxnId($this->getSponsorId()) ?: __("Not Available");
    }

    /**
     * @return string
     */
    public function getRequestedAmountDescription()
    {
        return $this->sponsorHelper->getLastPaymentDescription($this->getSponsorId()) ?: __("Not Available");
    }

    /**
     * Preparing global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $button = $this->getLayout()->createBlock(
            \Magento\Backend\Block\Widget\Button::class
        )->setData(
            [
                'label' => __('Pay to Sponsor'),
                'class' => 'action-save action-secondary',
                'id' => 'pay-to-sponsor-button',
            ]
        );
        $this->setChild('pay_to_sponsor_button', $button);
        return parent::_prepareLayout();
    }
}
