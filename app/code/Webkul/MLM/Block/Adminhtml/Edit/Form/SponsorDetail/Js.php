<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Adminhtml\Edit\Form\SponsorDetail;

class Js extends \Magento\Backend\Block\Widget
{
    protected $_template = 'Webkul_MLM::sponsor/tab/detail/js.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * Admin helper
     *
     * @var \Magento\Sales\Helper\Admin
     */
    protected $_adminHelper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \Webkul\MLM\Helper\Earning $earningHelper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Webkul\MLM\Helper\Data $dataHelper,
        array $data = []
    ) {
        $this->_adminHelper = $adminHelper;
        $this->_coreRegistry = $registry;
        $this->earningHelper = $earningHelper;
        $this->dataHelper = $dataHelper;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context, $data);
    }

    /**
     * Submit URL getter
     *
     * @return string
     */
    public function getChangeMemberLevelUrl()
    {
        return $this->getUrl('mlm/sponsors/changememberlevel');
    }

    /**
     * Submit URL getter
     *
     * @return string
     */
    public function getPayToSponsorUrl()
    {
        return $this->getUrl('mlm/sponsors/submitpaymentrequest');
    }

    /**
     * Submit URL getter
     *
     * @return string
     */
    public function getSubmitCommentUrl()
    {
        return $this->getUrl('mlm/sponsors/addbusinesscomment');
    }

    public function getSetMemberLevelConfig()
    {
        $config = [
            "url" => $this->getChangeMemberLevelUrl()
        ];
        $config = $this->jsonHelper->jsonEncode($config);
        return $config;
    }
}
