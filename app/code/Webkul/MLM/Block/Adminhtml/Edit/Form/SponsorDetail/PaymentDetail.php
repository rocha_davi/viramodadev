<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Adminhtml\Edit\Form\SponsorDetail;

class PaymentDetail extends \Magento\Backend\Block\Widget
{
    protected $_template = 'Webkul_MLM::sponsor/tab/detail/payment_detail.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * Admin helper
     *
     * @var \Magento\Sales\Helper\Admin
     */
    protected $_adminHelper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        array $data = []
    ) {
        $this->_adminHelper = $adminHelper;
        $this->_coreRegistry = $registry;
        $this->sponsorHelper = $sponsorHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getPaymentMethodName()
    {
        return $this->sponsorHelper->getPaymentMethodTitle($this->getSponsorId()) ?: __("Not Available");
    }
}
