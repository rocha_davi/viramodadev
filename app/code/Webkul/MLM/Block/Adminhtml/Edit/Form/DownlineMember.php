<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Block\Adminhtml\Edit\Form;

use Webkul\MLM\Model\Sponsors\Source\Status as SponsorStatus;

class DownlineMember extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $_nodes = [];

    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    protected $_template = "Webkul_MLM::sponsor/tab/detail/genealogytree.phtml";

    /**
     * @param \Magento\Framework\Registry               $registry
     * @param \Magento\Backend\Block\Widget\Context     $context
     * @param \Webkul\Marketplace\Block\Adminhtml\Customer\Edit $customerEdit
     * @param array                                     $data
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepo,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Model\SponsorsFactory $sponsorF,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->sponsorF = $sponsorF;
        $this->sponsorHelper = $sponsorHelper;
        $this->customerRepo = $customerRepo;
        parent::__construct($context, $data);
    }

    /**
     * Get Tree
     *
     * @return void
     */
    public function getTreeCollection()
    {
        $sponsorCode = $this->getSponsorCode();
        if ($sponsorCode) {
            $this->getSponsorTree($sponsorCode);
        }
        return $this->_nodes;
    }

    /**
     * Json Encode
     *
     * @param array $data
     * @return string
     */
    public function jsonEncode($data)
    {
        return \Zend_Json::encode($data);
    }

    /**
     * @return string
     */
    public function getAdminSponsorId()
    {
        return $this->getSponsorCode();
    }

    /**
     * Get Node
     *
     * @param string $sponsorCode
     * @return void
     */

    public function getSponsorTree($sponsorCode)
    {
        $collection = $this->sponsorF->create()->getCollection();
        $collection->addFieldToFilter("sponsor_reference_code", $sponsorCode)
        ->addFieldToFilter("status", SponsorStatus::STATUS_ENABLED);
        foreach ($collection as $model) {
            $customer = $this->getCustomer(
                $model->getCustomerId()
            );
            $this->_nodes[$sponsorCode][] = [
                "sponsor_code" => $model->getSponsorCode(),
                "sponsor_reference_code" => $model->getSponsorReferenceCode(),
                "customer_id" => $model->getCustomerId(),
                "customer_name" => $customer->getFirstname()." ".$customer->getLastname()
            ];
            $this->getSponsorTree(
                $model->getSponsorCode()
            );
        }
    }

    /**
     * Get Customer
     *
     * @param int $customerId
     * @return object
     */
    public function getCustomer($customerId)
    {
        return $this->customerRepo->getById(
            $customerId
        );
    }
}
