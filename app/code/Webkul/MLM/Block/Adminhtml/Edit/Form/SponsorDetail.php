<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Block\Adminhtml\Edit\Form;

use Magento\Customer\Controller\RegistryConstants;

class SponsorDetail extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    const COMM_TEMPLATE = 'sponsor/tab/detail.phtml';

    /**
     * @param \Magento\Framework\Registry               $registry
     * @param \Magento\Backend\Block\Widget\Context     $context
     * @param \Webkul\Marketplace\Block\Adminhtml\Customer\Edit $customerEdit
     * @param array                                     $data
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Block\Widget\Context $context,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->sponsorHelper = $sponsorHelper;
        parent::__construct($context, $data);
    }

    /**
     * Set template to itself.
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $sponsorInfo = $this->getLayout()->createBlock(
            \Webkul\MLM\Block\Adminhtml\Edit\Form\SponsorDetail\SponsorInfo::class
        )->setData(
            [
                'sponsor_id' => $this->getSponsorId(),
                'customer_id' => $this->getCustomerId(),
            ]
        );
        $businessStat = $this->getLayout()->createBlock(
            \Webkul\MLM\Block\Adminhtml\Edit\Form\SponsorDetail\BusinessStat::class
        )->setData(
            [
                'sponsor_id' => $this->getSponsorId(),
                'customer_id' => $this->getCustomerId(),
            ]
        );
        $commentSetMemberLevel = $this->getLayout()->createBlock(
            \Webkul\MLM\Block\Adminhtml\Edit\Form\SponsorDetail\CommentSetMemberLevel::class
        )->setData(
            [
                'sponsor_id' => $this->getSponsorId(),
                'customer_id' => $this->getCustomerId(),
            ]
        );
        $memberLevelCurrentBusiness = $this->getLayout()->createBlock(
            \Webkul\MLM\Block\Adminhtml\Edit\Form\SponsorDetail\MemberLevelCurrentBusiness::class
        )->setData(
            [
                'sponsor_id' => $this->getSponsorId(),
                'customer_id' => $this->getCustomerId(),
            ]
        );
        $js = $this->getLayout()->createBlock(
            \Webkul\MLM\Block\Adminhtml\Edit\Form\SponsorDetail\Js::class
        )->setData(
            [
                'sponsor_id' => $this->getSponsorId(),
                'customer_id' => $this->getCustomerId(),
            ]
        );
        $paymentDetail = $this->getLayout()->createBlock(
            \Webkul\MLM\Block\Adminhtml\Edit\Form\SponsorDetail\PaymentDetail::class
        )->setData(
            [
                'sponsor_id' => $this->getSponsorId(),
                'customer_id' => $this->getCustomerId(),
            ]
        );
        $requestAmountDetails = $this->getLayout()->createBlock(
            \Webkul\MLM\Block\Adminhtml\Edit\Form\SponsorDetail\RequestAmountDetails::class
        )->setData(
            [
                'sponsor_id' => $this->getSponsorId(),
                'customer_id' => $this->getCustomerId(),
            ]
        );
        $this->setChild('sponsor_info', $sponsorInfo);
        $this->setChild('business_stat', $businessStat);
        $this->setChild('comment_set_member_level', $commentSetMemberLevel);
        $this->setChild('member_level_current_business', $memberLevelCurrentBusiness);
        $this->setChild('payment_detail', $paymentDetail);
        $this->setChild('request_amount_details', $requestAmountDetails);
        $this->setChild('js_block', $js);
        $this->setTemplate(static::COMM_TEMPLATE);
        return $this;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
    }

    /**
     * @return int
     */
    public function getSponsorId()
    {
        return $this->sponsorHelper->getSponsorId($this->getCustomerId());
    }
}
