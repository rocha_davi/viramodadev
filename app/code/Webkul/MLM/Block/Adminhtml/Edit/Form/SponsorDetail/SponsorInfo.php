<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Adminhtml\Edit\Form\SponsorDetail;

use Magento\Eav\Model\AttributeDataFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order\Address;

class SponsorInfo extends \Magento\Backend\Block\Widget
{
    protected $_template = 'Webkul_MLM::sponsor/tab/detail/sponsor_info.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * Admin helper
     *
     * @var \Magento\Sales\Helper\Admin
     */
    protected $_adminHelper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        array $data = []
    ) {
        $this->_adminHelper = $adminHelper;
        $this->_coreRegistry = $registry;
        $this->sponsorHelper = $sponsorHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getSponsorCode()
    {
        return $this->sponsorHelper->getSponsorCode($this->getCustomerId());
    }

    /**
     * @return string
     */
    public function getJoiningDate()
    {
        return $this->sponsorHelper->getJoiningDate($this->getSponsorId());
    }

    /**
     * @return string
     */
    public function getParentSponsorName()
    {
        return $this->sponsorHelper->getParentSponsorName($this->getSponsorId());
    }

    /**
     * @return string
     */
    public function getMemberLevelName()
    {
        return $this->sponsorHelper->getMemberLevelName($this->getSponsorId());
    }

    /**
     * @return int
     */
    public function getDownlineMemberCount()
    {
        return $this->sponsorHelper->getDownlineMemberCount($this->getSponsorId());
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        return $this->sponsorHelper->getSponsorName($this->getSponsorId());
    }

    /**
     * @return string
     */
    public function getCustomerGroup()
    {
        return $this->sponsorHelper->getSponsorGroupName($this->getSponsorId());
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->sponsorHelper->getSponsorEmail($this->getSponsorId());
    }
}
