<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Adminhtml\Edit\Form\SponsorDetail;

class MemberLevelCurrentBusiness extends \Magento\Backend\Block\Widget
{
    protected $_template = 'Webkul_MLM::sponsor/tab/detail/member_level_current_business.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * Admin helper
     *
     * @var \Magento\Sales\Helper\Admin
     */
    protected $_adminHelper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Helper\Data $dataHelper,
        \Webkul\MLM\Model\MemberLevel\Source\Options $memberLevelOptions,
        array $data = []
    ) {
        $this->_adminHelper = $adminHelper;
        $this->_coreRegistry = $registry;
        $this->sponsorHelper = $sponsorHelper;
        $this->dataHelper = $dataHelper;
        $this->memberLevelOptions = $memberLevelOptions;
        parent::__construct($context, $data);
    }

    /**
     * @return int
     */
    public function getCurrentMemberLevelId()
    {
        $currentId = $this->sponsorHelper->getMemberLevelId($this->getSponsorId());
        return $currentId;
    }

    /**
     * @return array
     */
    public function getMemberLevels()
    {
        return $this->memberLevelOptions->getOptionsWithLabel();
    }

    public function getWalletBalance()
    {
        return $this->dataHelper->getFormattedPrice(
            $this->sponsorHelper->getWalletBalance($this->getSponsorId())
        );
    }

    /**
     * @return string
     */
    public function getPendingBalance()
    {
        return $this->dataHelper->getFormattedPrice(
            $this->sponsorHelper->getTotalPendingAmt($this->getSponsorId())
        );
    }

    /**
     * Preparing global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $button = $this->getLayout()->createBlock(
            \Magento\Backend\Block\Widget\Button::class
        )->setData(
            [
                'label' => __('Change Member Level'),
                'class' => 'action-save action-secondary change-member-level-button',
                'id' => 'change-member-level-button',
            ]
        );
        $this->setChild('change_member_level_button', $button);
        return parent::_prepareLayout();
    }

    /**
     * Submit URL getter
     *
     * @return string
     */
    public function getSubmitButtonUrl()
    {
        return $this->getUrl('mlm/sponsors/changememberlevel');
    }
}
