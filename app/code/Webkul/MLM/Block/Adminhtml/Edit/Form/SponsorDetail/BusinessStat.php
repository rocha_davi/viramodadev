<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Adminhtml\Edit\Form\SponsorDetail;

class BusinessStat extends \Magento\Backend\Block\Widget
{
    protected $_template = 'Webkul_MLM::sponsor/tab/detail/business_stat.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * Admin helper
     *
     * @var \Magento\Sales\Helper\Admin
     */
    protected $_adminHelper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \Webkul\MLM\Helper\Earning $earningHelper,
        \Webkul\MLM\Helper\Data $dataHelper,
        array $data = []
    ) {
        $this->_adminHelper = $adminHelper;
        $this->_coreRegistry = $registry;
        $this->earningHelper = $earningHelper;
        $this->dataHelper = $dataHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getLifetimeSalesEarning()
    {
        return $this->dataHelper->getFormattedPrice(
            $this->earningHelper->getTotalSalesEarningAmt($this->getSponsorId())
        );
    }
    
    /**
     * @return string
     */
    public function getLifetimeJoiningEarning()
    {
        return $this->dataHelper->getFormattedPrice(
            $this->earningHelper->getTotalJoiningEarningAmt($this->getSponsorId())
        );
    }

    /**
     * @return string
     */
    public function getLifetimeMemberBonus()
    {
        return $this->dataHelper->getFormattedPrice(
            $this->earningHelper->getTotalLevelEarningAmt($this->getSponsorId())
        );
    }

    /**
     * @return string
     */
    public function getCurrentMonthSalesEarning()
    {
        return $this->dataHelper->getFormattedPrice(
            $this->earningHelper->getMonthlySalesEarning($this->getSponsorId())
        );
    }

    /**
     * @return string
     */
    public function getCurrentMonthJoiningEarning()
    {
        return $this->dataHelper->getFormattedPrice(
            $this->earningHelper->getMonthlyJoiningEarning($this->getSponsorId())
        );
    }

    /**
     * @return string
     */
    public function getCurrentMonthMemberBonus()
    {
        return $this->dataHelper->getFormattedPrice(
            $this->earningHelper->getMonthlyBonusEarning($this->getSponsorId())
        );
    }
}
