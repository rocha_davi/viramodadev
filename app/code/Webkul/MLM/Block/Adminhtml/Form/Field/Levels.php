<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

class Levels extends AbstractFieldArray
{
    /**
     * @var string
     */
    protected $_template = 'Webkul_MLM::system/config/form/field/array.phtml';

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Webkul\MLM\Helper\GetLevelCountCommissionNotAllocated $getLevelCountCommNotAllocated,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->getLevelCountCommNotAllocated = $getLevelCountCommNotAllocated;
    }

    /**
     * Prepare to render fields
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $this->addColumn(
            'commission_level',
            [
                'label'     => __('Level'),
                'class' => 'required-entry validate-number',
            ]
        );
        $this->addColumn(
            'commission_rate',
            [
                'label' => __('Commision (%)'),
                'class' => 'required-entry validate-range range-0-100',
            ]
        );
        $this->_addAfter = true;
        $this->_addButtonLabel = __('Add Values');
    }

    /**
     * GetRemainingLevelLabel function
     *
     * @return string
     */
    public function getRemainingLevelLabel()
    {
        return __('Level Remaining');
    }

    /**
     * GetRemainingLevels function
     *
     * @return int
     */
    public function getRemainingLevels()
    {
        $levelIds = [];
        foreach ($this->getArrayRows() as $row) {
            $levelIds[] = $row->getCommissionLevel();
        }
        $levelCount = $this->getLevelCountCommNotAllocated->execute($levelIds);
        return $levelCount;
    }
}
