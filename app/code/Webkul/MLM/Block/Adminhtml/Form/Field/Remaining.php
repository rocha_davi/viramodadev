<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Webkul\MLM\Helper\ConfigConstants as CC;

class Remaining extends Field
{
    /**
     * Scope Config
     *
     * @var ScopeConfgInterface
     */
    private $config;

    /**
     * Initialised dependencies
     *
     * @param \Magento\Backend\Block\Template\Context $context
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context
    ) {
        $this->config = $context->getScopeConfig();
        parent::__construct($context, []);
    }

    /**
     * Retrieve element HTML markup
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $salesPercent = $this->config->getValue(
            CC::SECTION."/".CC::SALES_COMMISSION_GROUP."/".CC::SALES_COMMISSION
        );
        return (100 - (($salesPercent) ? $salesPercent : 0))."%";
    }
}
