<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Webkul\MLM\Helper\ConfigConstants as CC;

class PaymentOptions extends Field
{
    const PAYMENT_METHODS = "mlm/payment_options/payment_options";

    /**
     * @var string
     */
    protected $_template = 'Webkul_MLM::system/config/form/field/grid.phtml';

    /**
     * Scope Config
     *
     * @var ScopeConfgInterface
     */
    protected $config;

    /**
     * All payment Methods
     *
     * @var \Magento\Payment\Model\Config\Source\Allmethods $allPaymentMethods
     */
    protected $allPaymentMethods;

    /**
     * Initialized Dependencies
     *
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Magento\Payment\Model\Config\Source\Allmethods $allPaymentMethods,
        \Magento\Payment\Helper\Data $paymentHelper,
        array $data = []
    ) {
        $this->allPaymentMethods = $allPaymentMethods;
        $this->paymentHelper = $paymentHelper;
        parent::__construct($context, $data);
        $this->scopeConfig = $context->getScopeConfig();
    }

    /**
     * Get Selected Methods
     *
     * @return string
     */
    public function getSelectedMethods()
    {
        $methods = $this->scopeConfig->getValue(self::PAYMENT_METHODS);
        return explode(",", $methods);
    }

    /**
     * All Payment Methods
     *
     * @return array
     */
    public function getAllPaymentMethods()
    {
        $paymentMethods = $this->paymentHelper->getPaymentMethodList(true, false, true);
        $selectPaymentMethods = $this->getSelectedMethods();
        $allPayments = $this->allPaymentMethods->toOptionArray();
        $allPayments = $allPayments["offline"]["value"];
        foreach ($allPayments as $k => &$payment) {
            if (!$payment["label"]) {
                $payment["label"] = ucfirst($payment["value"]);
            }
            if (in_array($payment["value"], $selectPaymentMethods)) {
                $payment["ischecked"] = true;
            } else {
                $payment["ischecked"] = false;
            }
            if (is_array($payment["value"])) {
                unset($allPayments[$k]);
                $allPayments = $this->arrayMerge($allPayments, $payment["value"]);
            }
        }
        return $allPayments;
    }

    /**
     * @return string
     */
    public function getJsLayout()
    {
        $data['jsLayout']["components"] = [
            "config-js-field" => [
                "component" => "uiComponent",
                "config" => [
                    "template" => "Webkul_MLM/js-config"
                ],
                "children" => [
                    "grid-js" => [
                        "sortOrder" => 1,
                        "component" => "Webkul_MLM/js/view/grid-js",
                        "displayArea" => "grid-js"
                    ]
                ]
            ]
        ];
        return \Zend_Json::encode($data['jsLayout']);
    }

    /**
     * Return element html
     *
     * @param  AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * @param array $array
     * @param array $array2
     * @return array
     */
    public function arrayMerge($array, $array2)
    {
        return array_merge($array, $array2);
    }
}
