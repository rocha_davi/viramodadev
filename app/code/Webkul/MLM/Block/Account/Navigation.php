<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Block\Account;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Http\Context as HttpContext;
use Webkul\MLM\Model\Sponsors\Source\Status as SponsorStatus;

class Navigation extends \Magento\Framework\View\Element\Html\Link
{
    /**
     * @param \Webkul\MLM\Model\SponsorsFactory $sponsorFactory
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param HttpContext $httpContext
     * @param \Magento\Framework\App\RequestInterface $request
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        \Webkul\MLM\Model\SponsorsFactory $sponsorFactory,
        \Magento\Framework\Data\Form\FormKey $formKey,
        HttpContext $httpContext,
        \Magento\Framework\App\RequestInterface $request,
        Context $context,
        array $data = []
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->_httpContext = $httpContext;
        $this->_sponsorFactory = $sponsorFactory;
        $this->request = $request;
        parent::__construct($context, $data);
    }

    /**
     * GetCurrentUrl Give the current url of recently viewed page
     *
     * @return string
     */
    public function getCurrentUrl()
    {
        return $this->_urlBuilder->getCurrentUrl();
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        $model = $this->_sponsorFactory->create()->getCollection()->addFieldToFilter(
            "customer_id",
            $this->getCustomerId()
        )->getFirstItem();
        return $model->getStatus();
    }

    /**
     * @return bool
     */
    public function isSponsor()
    {
        $model = $this->_sponsorFactory->create()->getCollection()->addFieldToFilter(
            "customer_id",
            $this->getCustomerId()
        )->getFirstItem();
        return $model->getId() != null;
    }

    /**
     * @return bool
     */
    public function isApproved()
    {
        $status = $this->getStatus();
        return SponsorStatus::STATUS_ENABLED == $status;
    }

    /**
     * Return Customer Id
     *
     * @return bool|0|1
     */
    public function getCustomerId()
    {
        return $customerId = $this->_httpContext->getValue('customer_id');
    }

    /**
     * @return string
     */
    public function getDashboardUrl()
    {
        return $this->urlBuilder->getUrl(
            'mlm/sponsor/dashboard',
            ['_secure' => $this->request->isSecure()]
        );
    }

    /**
     * @return string
     */
    public function getAdsUrl()
    {
        return $this->urlBuilder->getUrl('mlm/sponsor/Ads', ['_secure' => $this->request->isSecure()]);
    }

    /**
     * @return string
     */
    public function getWalletUrl()
    {
        return $this->urlBuilder->getUrl('mlm/sponsor/wallet', ['_secure' => $this->request->isSecure()]);
    }

    /**
     * @return string
     */
    public function getGenealogyTreeUrl()
    {
        return $this->urlBuilder->getUrl('mlm/sponsor/genealogytree', ['_secure' => $this->request->isSecure()]);
    }

    /**
     * @return string
     */
    public function getReferralUrl()
    {
        return $this->urlBuilder->getUrl('mlm/sponsor/referral', ['_secure' => $this->request->isSecure()]);
    }

    /**
     * @return string
     */
    public function getPaymentMethodUrl()
    {
        return $this->urlBuilder->getUrl('mlm/sponsor/payment', ['_secure' => $this->request->isSecure()]);
    }

    /**
     * @return string
     */
    public function getBecomeSponsorUrl()
    {
        return $this->urlBuilder->getUrl('mlm/sponsor/becomesponsor', ['_secure' => $this->request->isSecure()]);
    }
    /**
     * @return string
     */
    public function getSponsorConfigurationUrl()
    {
        return $this->urlBuilder->getUrl('mlm/sponsor/sponsorDetails', ['_secure' => $this->request->isSecure()]);
    }
     /**
      * @return string
      */
    public function getSponsorBannerConfigurationUrl()
    {
        return $this->urlBuilder->getUrl('mlm/sponsor/banner', ['_secure' => $this->request->isSecure()]);
    }
}
