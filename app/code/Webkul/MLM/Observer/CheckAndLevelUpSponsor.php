<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MLM\Model\Sponsors\Source\Status as SponsorsStatus;

class CheckAndLevelUpSponsor implements ObserverInterface
{
    /**
     * @param \Webkul\MLM\Helper\Sponsor $sponsorHelper
     * @param \Webkul\MLM\Helper\MemberLevel $memberLevelHelper
     * @param \Webkul\MLM\Helper\Earning $earningHelper
     * @param \Webkul\MLM\Helper\Data $dataHelper
     * @param \Webkul\MLM\Helper\AssignMemberLevelToSponsor $assignMemberLevelToSponsor
     * @param \Webkul\MLM\Model\ResourceModel\MemberLevel\CollectionFactory $memberLevelCollF
     */
    public function __construct(
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Helper\MemberLevel $memberLevelHelper,
        \Webkul\MLM\Helper\Earning $earningHelper,
        \Webkul\MLM\Helper\Data $dataHelper,
        \Webkul\MLM\Helper\AssignMemberLevelToSponsor $assignMemberLevelToSponsor,
        \Webkul\MLM\Model\ResourceModel\MemberLevel\CollectionFactory $memberLevelCollF
    ) {
        $this->sponsorHelper = $sponsorHelper;
        $this->memberLevelHelper = $memberLevelHelper;
        $this->earningHelper = $earningHelper;
        $this->assignMemberLevelToSponsor = $assignMemberLevelToSponsor;
        $this->dataHelper = $dataHelper;
        $this->memberLevelCollF = $memberLevelCollF;
    }

    /**
     * Address after save event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->dataHelper->isModuleEnabled()) {
            $sponsorEarning = $observer->getSponsorEarning();
            $sponsorId = $sponsorEarning->getSponsorId();
            $totalEarning = $this->earningHelper->getTotalEarningAmt($sponsorId);
            $memberLevelId = $this->sponsorHelper->getMemberLevelId($sponsorId);
            $currentMemberLevelMaxBusinessAmount = $this->memberLevelCollF->create()
                ->addFieldToFilter("entity_id", $memberLevelId)
                ->getFirstItem()
                ->getMaximumBusinessAmount();
            $levelIdAccordingToMaxBusiness = $this->memberLevelHelper
                ->getMLIdForTotalBusiness($totalEarning);
            $targetMemberLevelMaxBusinessAmount = $this->memberLevelCollF->create()
                    ->addFieldToFilter("entity_id", $levelIdAccordingToMaxBusiness)
                    ->getFirstItem()
                    ->getMaximumBusinessAmount();
            if ($targetMemberLevelMaxBusinessAmount > $currentMemberLevelMaxBusinessAmount) {
                $this->assignMemberLevelToSponsor->execute($sponsorId, $levelIdAccordingToMaxBusiness);
            }
        }
    }
}
