<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MLM\Model\Sponsors\Source\Status as SponsorsStatus;

class CustomerRegisterAfterObserver implements ObserverInterface
{
    /**
     * DateTime
     *
     * @var \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     */
    protected $dateTime;

    /**
     * StoreConfiguration
     *
     * @var \Webkul\MLM\Helper\StoreConfiguration $storeConfiguration
     */
    protected $storeConfiguration;

    /**
     * @var \Webkul\MLM\Model\SponsorsFactory $sponsorsFactory
     */
    protected $sponsorsFactory;

    /**
     * @param \Webkul\MLM\Model\SponsorsFactory $sponsorsFactory
     * @param \Webkul\MLM\Helper\StoreConfiguration $storeConfiguration
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
     * @param \Webkul\MLM\Helper\Sponsor $sponsorHelper
     * @param \Webkul\MLM\Helper\Data $dataHelper
     * @param \Webkul\MLM\Helper\AttachSponsorToParent $attachSponsorToParent
     * @param \Webkul\MLM\Helper\AssignMemberLevelToSponsor $assignMemberLevelToSponsor
     * @param \Webkul\MLM\Helper\CreateWalletIfNotExist $createWalletIfNotExist
     * @param \Webkul\MLM\Api\VerifySponsorInterface $verifySponsor
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        \Webkul\MLM\Model\SponsorsFactory $sponsorsFactory,
        \Webkul\MLM\Helper\StoreConfiguration $storeConfiguration,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Helper\Data $dataHelper,
        \Webkul\MLM\Helper\AttachSponsorToParent $attachSponsorToParent,
        \Webkul\MLM\Helper\AssignMemberLevelToSponsor $assignMemberLevelToSponsor,
        \Webkul\MLM\Helper\CreateWalletIfNotExist $createWalletIfNotExist,
        \Webkul\MLM\Api\VerifySponsorInterface $verifySponsor,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->sponsorsFactory = $sponsorsFactory;
        $this->storeConfiguration = $storeConfiguration;
        $this->dateTime = $dateTime;
        $this->sponsorHelper = $sponsorHelper;
        $this->attachSponsorToParent = $attachSponsorToParent;
        $this->assignMemberLevelToSponsor = $assignMemberLevelToSponsor;
        $this->createWalletIfNotExist = $createWalletIfNotExist;
        $this->dataHelper = $dataHelper;
        $this->verifySponsor = $verifySponsor;
        $this->jsonHelper = $jsonHelper;
        $this->messageManager = $messageManager;
    }

    /**
     * Address after save event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $accountController = $observer->getAccountController();
        $postData = $accountController->getRequest()->getParams();
        $customer = $observer->getCustomer();
        $sponsorId = $postData["sponsor_reference_id"] ?? "";
        $sponsorName = $postData["sponsor_name"] ?? "";
        if (!empty($postData['make_sponsor'])) {
            $sponsorVerifyData = $this->jsonHelper->jsonEncode([
                'sponsorId' => $sponsorId
            ]);
            $response = $this->verifySponsor->verifySponsorId($sponsorVerifyData);
            $response = $this->jsonHelper->jsonDecode($response);
            $sponsorNameResponse = $this->verifySponsorName($sponsorName);
            $sponsorNameResponse = $this->jsonHelper->jsonDecode($sponsorNameResponse);
            if (!$response['success']) {
                $message = __(
                    'O Código de referência \'%1\' está correto.'.
                    ' Clique em \'Seja um consultor(a) Viramoda\' para se tornar um consultor(a).',
                    $sponsorId
                );
                $this->messageManager->addWarning($message);
            } elseif (!$sponsorNameResponse['success']) {
                $message = __(
                    ' O Nome para sua loja \'%1\' está correto.'.
                    ' Clique em \'Seja um consultor(a) Viramoda\' para se tornar um consultor(a).',
                    $sponsorName
                );
                $this->messageManager->addWarning($message);
            } else {
                if ($sponsorId && $sponsorName) {
                    $collection = $this->sponsorsFactory->create()->getCollection();
                    $collection->addFieldToFilter("sponsor_reference_code", $sponsorId);
                    $collection->addFieldToFilter("customer_id", $customer->getId());
                    $customerId = $customer->getId();
                    if (!$collection->getSize()) {
                        $sponsorCode = $this->storeConfiguration->generateSponsorsId();
                        $model = $this->sponsorsFactory->create();
                        $parentSponsorId = $this->sponsorHelper->getParentSponsorId($sponsorId);
                        $model->setSponsorCode($sponsorCode);
                        $model->setSponsorReferenceCode($sponsorId);
                        $model->setCustomerId($customerId);
                        $model->setParentSponsorId($parentSponsorId);
                        $model->setSponserName($sponsorName);
                        $model->setStatus(SponsorsStatus::STATUS_DISABLED);
                        $model->save();
                        $sponsorId = $model->getId();
                        $this->attachSponsorToParent->execute($sponsorId);
                        $this->createWalletIfNotExist->execute($sponsorId);
                        $defaultMemberLevelId = $this->dataHelper->getDefaultMemberLevelId();
                        $this->assignMemberLevelToSponsor->execute($sponsorId, $defaultMemberLevelId);
                    }
                }
            }
        }
    }
    /**
     * Check for Existing Sponsor Name
     *
     * @return string
     */
    private function verifySponsorName($sponsorName)
    {
        $response = [
            "success" => false,
            "message" => __("O nome da sua loja é válido. Parabéns!")
        ];
        if (empty($sponsorName)) {
            return $this->jsonHelper->jsonEncode($response);
        }
        $collection = $this->sponsorsFactory->create()->getCollection();
        $collection->addFieldToFilter("sponser_name", $sponsorName);
        
        if ($collection->getSize() == 0) {
            $response["success"] = true;
            $response["message"] = __("Desculpe! Esse nome já está em uso, tente novamente.");
            return $this->jsonHelper->jsonEncode($response);
        }
        
        return $response;
    }
}
