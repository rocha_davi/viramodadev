<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MLM\Model\Sponsors\Source\Status as SponsorsStatus;

class SaveSponsorProductDetails implements ObserverInterface
{
    /**
     * @param \Webkul\MLM\Helper\Sponsor $sponsorHelper
     * @param \Webkul\MLM\Model\SponsorsFactory $sponsorsF
     * @param \Webkul\MLM\Helper\Data $dataHelper
     * @param \Magento\Framework\Session\SessionManagerInterface $sessionManagerInterface
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Model\SponsorsFactory $sponsorsF,
        \Webkul\MLM\Helper\Data $dataHelper,
        \Magento\Framework\Session\SessionManagerInterface $sessionManagerInterface,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->sponsorHelper = $sponsorHelper;
        $this->sessionManagerInterface = $sessionManagerInterface;
        $this->sponsorsF = $sponsorsF;
        $this->checkoutSession = $checkoutSession;
        $this->dataHelper = $dataHelper;
        $this->logger = $logger;
    }

    /**
     * Address after save event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {

            if ($this->dataHelper->isModuleEnabled()) {
                $request = $observer->getRequest();
                $product = $observer->getProduct();
                $serverData = $request->getServer();
                $referer = $serverData->get('HTTP_REFERER');
                if (strpos($referer, 'ref_id=') === false) {
                    return ;
                }
                $query = $this->getQueryParams($referer, PHP_URL_QUERY);
                $query = explode("&", $query);
                $query = array_filter($query);
                $query = array_filter($query, function ($item) {
                    return strpos($item, "ref_id=") !== false;
                });
                $query = array_values($query);
                $sponsorCode = explode("=", $query[0]);
                if (empty($sponsorCode[1])) {
                    return;
                }
                $sponsorCode = $sponsorCode[1];
                $sponsor = $this->sponsorsF->create()->getCollection()->addFieldToFilter('sponsor_code', $sponsorCode)
                    ->getFirstItem();
                $quoteId = $this->checkoutSession->getQuoteId();
                $mlmSalesData = $this->sessionManagerInterface->getMLMSponsorSales();
                if ($mlmSalesData != null) {
                    $mlmSalesData = \Zend_Json::decode($mlmSalesData);
                } else {
                    $mlmSalesData = [];
                }
                if (isset($mlmSalesData[$quoteId])) {
                    $currentQuoteData = $mlmSalesData[$quoteId];
                } else {
                    $currentQuoteData = [];
                }
                $sponsorId = $sponsor->getEntityId();
                if (isset($currentQuoteData[$sponsorId])) {
                    $sponsorProductIds = $currentQuoteData[$sponsorId];
                    $sponsorProductIds = explode(',', $sponsorProductIds);
                    $sponsorProductIds[] = $product->getId();
                    $sponsorProductIds = array_unique($sponsorProductIds);
                    $sponsorProductIds = implode(",", $sponsorProductIds);
                } else {
                    $sponsorProductIds = "{$product->getId()}";
                }
                $currentQuoteData[$sponsorId] = $sponsorProductIds;
                $mlmSalesData[$quoteId] = $currentQuoteData;
                $mlmSalesData = \Zend_Json::encode($mlmSalesData);
                $this->sessionManagerInterface->setMLMSponsorSales($mlmSalesData);
            }
        } catch (\Throwable $t) {
            $this->logger->debug($t->getMessage());
            $this->logger->debug($t->getTraceAsString());
        }
    }

    /**
     * @param string $url
     * @return string
     */
    public function getQueryParams($url)
    {
        $afterQuestionMark = explode("?", $url);
        $afterQuestionMark = isset($afterQuestionMark[1])
            ? $afterQuestionMark[1] : $afterQuestionMark[0];

        $queryString = explode("#", $afterQuestionMark);
        $queryString = $queryString[0];
        return $queryString;
    }
}
