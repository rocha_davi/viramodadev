<?php

namespace Webkul\MLM\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Webkul\MLM\Model\Sponsors\Source\Status as SponsorsStatus;

class EditCustomerDetails implements ObserverInterface
{

    private $logger;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\RequestInterface $request,
        \Webkul\MLM\Model\SponsorsFactory $sponsorsFactory,
        \Webkul\MLM\Helper\StoreConfiguration $storeConfiguration,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Helper\Data $dataHelper,
        \Webkul\MLM\Helper\AttachSponsorToParent $attachSponsorToParent,
        \Webkul\MLM\Helper\AssignMemberLevelToSponsor $assignMemberLevelToSponsor,
        \Webkul\MLM\Helper\CreateWalletIfNotExist $createWalletIfNotExist,
        \Webkul\MLM\Api\VerifySponsorInterface $verifySponsor,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->request = $request;
        $this->logger = $logger;
        $this->sponsorsFactory = $sponsorsFactory;
        $this->storeConfiguration = $storeConfiguration;
        $this->dateTime = $dateTime;
        $this->sponsorHelper = $sponsorHelper;
        $this->attachSponsorToParent = $attachSponsorToParent;
        $this->assignMemberLevelToSponsor = $assignMemberLevelToSponsor;
        $this->createWalletIfNotExist = $createWalletIfNotExist;
        $this->dataHelper = $dataHelper;
        $this->verifySponsor = $verifySponsor;
        $this->jsonHelper = $jsonHelper;
        $this->messageManager = $messageManager;
    }

    public function execute(Observer $observer)
    {
        try {
            $postData = $this->request->getParams();
            $customer = $observer->getCustomer();
            $sponsorId = $postData["sponsor_reference_id"] ?? "";
            $sponsorName = $postData["sponsor_name"] ?? "";
            
            if (strtolower($this->request->getFullActionName()) == 'customer_account_editPost' && !empty($postData['make_sponsor']) && $customer->getId()) {
              
                $sponsorVerifyData = $this->jsonHelper->jsonEncode([
                    'sponsorId' => $sponsorId
                ]);
                $response = $this->verifySponsor->verifySponsorId($sponsorVerifyData);
                $response = $this->jsonHelper->jsonDecode($response);
                $sponsorNameResponse = $this->verifySponsorName($sponsorName);
                $sponsorNameResponse = $this->jsonHelper->jsonDecode($sponsorNameResponse);
                if (!$response['success']) {
                    $message = __(
                        'The Sponsor Reference Id \'%1\' is invalid.'.
        
                        $sponsorId
                    );
                    $this->messageManager->addWarning($message);
                } elseif (!$sponsorNameResponse['success']) {
                    $message = __(
                        'The Sponsor Name \'%1\' is invalid.'.
                        $sponsorName
                    );
                    $this->messageManager->addWarning($message);
                } else {
                   
                    if ($sponsorId && $sponsorName) {
                        
                        $collection = $this->sponsorsFactory->create()->getCollection();
                        $collection->addFieldToFilter("sponsor_reference_code", $sponsorId);
                        $collection->addFieldToFilter("customer_id", $customer->getId());
                        $customerId = $customer->getId();
                        if (!$collection->getSize()) {
                            
                            $sponsorCode = $this->storeConfiguration->generateSponsorsId();
                            $model = $this->sponsorsFactory->create();
                            $parentSponsorId = $this->sponsorHelper->getParentSponsorId($sponsorId);
                            $model->setSponsorCode($sponsorCode);
                            $model->setSponsorReferenceCode($sponsorId);
                            $model->setCustomerId($customerId);
                            $model->setParentSponsorId($parentSponsorId);
                            $model->setSponserName($sponsorName);
                            $model->setStatus(SponsorsStatus::STATUS_DISABLED);
                            $model->save();
                            $sponsorId = $model->getId();
                            $this->attachSponsorToParent->execute($sponsorId);
                            $this->createWalletIfNotExist->execute($sponsorId);
                            $defaultMemberLevelId = $this->dataHelper->getDefaultMemberLevelId();
                            $this->assignMemberLevelToSponsor->execute($sponsorId, $defaultMemberLevelId);
                        } else {
                            
                            $collection = $this->sponsorsFactory->create()->getCollection();
                            $collection->addFieldToFilter("sponsor_reference_code", $sponsorId);
                            $collection->addFieldToFilter("customer_id", $customer->getId());
                            $collection->addFieldToFilter("sponser_name", $sponsorName);
                            $customerId = $customer->getId();
                            
                            if (!$collection->getSize()) {
                                $collectionData = $this->sponsorsFactory->create()->getCollection()
                                ->addFieldToFilter("sponsor_reference_code", $sponsorId)
                                ->addFieldToFilter("customer_id", $customer->getId())->getFirstItem();
                              
                                $this->sponsorsFactory->create()->setId($collectionData->getEntityId())
                                ->setSponserName($sponsorName)
                                ->save();
                            }
                        }
                    }
                }
            }
            
        } catch (\Exception $e) {
            $this->logger->critical('Error message', ['exception' => $e]);
        }
    }
    /**
     * Check for Existing Sponsor Name
     *
     * @return string
     */
    private function verifySponsorName($sponsorName)
    {
        $response = [
            "success" => false,
            "message" => __("The Sponsor Name is not Valid")
        ];
        if (empty($sponsorName)) {
            return $this->jsonHelper->jsonEncode($response);
        }
        $collection = $this->sponsorsFactory->create()->getCollection();
        $collection->addFieldToFilter("sponser_name", $sponsorName);
      
        if ($collection->getSize() == 0) {
            $response["success"] = true;
            $response["message"] = __("The Sponsor Name is Valid");
            return $this->jsonHelper->jsonEncode($response);
        }
        
        return $response;
    }
}
