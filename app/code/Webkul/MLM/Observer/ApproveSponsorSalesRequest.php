<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MLM\Model\SponsorSalesRequest\Source\Status as SponsorSalesRequestStatus;
use Webkul\MLM\Model\SponsorSalesRequestFactory;
use Webkul\MLM\Helper\DistributeSalesCommissionToSponsors;

class ApproveSponsorSalesRequest implements ObserverInterface
{
    /**
     * @param \Webkul\MLM\Helper\Sponsor $sponsorHelper
     * @param \Webkul\MLM\Model\SponsorsFactory $sponsorsF
     * @param \Webkul\MLM\Helper\Data $dataHelper
     * @param \Magento\Framework\Session\SessionManagerInterface $sessionManagerInterface
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Psr\Log\LoggerInterface $logger
     * @param SponsorSalesRequestFactory $sponsorSalesRequestF
     * @param DistributeSalesCommissionToSponsors $distributeSalesCommissionToSponsors
     */
    public function __construct(
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Model\SponsorsFactory $sponsorsF,
        \Webkul\MLM\Helper\Data $dataHelper,
        \Magento\Framework\Session\SessionManagerInterface $sessionManagerInterface,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Psr\Log\LoggerInterface $logger,
        SponsorSalesRequestFactory $sponsorSalesRequestF,
        DistributeSalesCommissionToSponsors $distributeSalesCommissionToSponsors
    ) {
        $this->sponsorHelper = $sponsorHelper;
        $this->sessionManagerInterface = $sessionManagerInterface;
        $this->sponsorsF = $sponsorsF;
        $this->checkoutSession = $checkoutSession;
        $this->dataHelper = $dataHelper;
        $this->logger = $logger;
        $this->sponsorSalesRequestF = $sponsorSalesRequestF;
        $this->distributeSalesCommissionToSponsors = $distributeSalesCommissionToSponsors;
    }

    /**
     * Address after save event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     * MLMSponsorSales
     * [
     *      [$quoteId] => [
     *           [$sponsorId] => "1,2,3" // Product Ids
     *      ]
     * ]
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            if ($this->dataHelper->isModuleEnabled()) {
                $invoice = $observer->getInvoice();
                $order = $observer->getInvoice()->getOrder();
                $orderId = $order->getId();
                $salesRequestColl = $this->sponsorSalesRequestF->create()->getCollection()
                    ->addFieldToFilter('order_id', $orderId);
                if ($salesRequestColl->getSize() < 1) {
                    return ;
                }
                $invoiceItemsSortedByProductIds = $this->getInvoiceItemsSortedByProductIds($invoice);
                if (count($invoiceItemsSortedByProductIds) == 0) {
                    return ;
                }
                foreach ($salesRequestColl as $salesReqeust) {
                    $productId = $salesReqeust->getProductId();
                    if (empty($invoiceItemsSortedByProductIds[$productId])) {
                        continue;
                    }
                    $invoiceItem = $invoiceItemsSortedByProductIds[$productId];
                    $baseRowTotal = $this->dataHelper->formatNumber($invoiceItem->getBaseRowTotal());
                    $saleAmount = $this->dataHelper->formatNumber($salesReqeust->getAmount());
                    $compareResult = bccomp($saleAmount, $baseRowTotal);
                    if ($compareResult == 0) {
                        $salesReqeust->setStatus(SponsorSalesRequestStatus::STATUS_COMPLETED);
                    } else {
                        $netAmountReceived = $salesReqeust->getAmountReceived();
                        $netAmountReceived += $baseRowTotal;
                        $netAmountReceived = $this->dataHelper->formatNumber($netAmountReceived);
                        $compareResult = bccomp($saleAmount, $netAmountReceived);
                        if ($compareResult == 1) {
                            $salesReqeust->setStatus(SponsorSalesRequestStatus::STATUS_PROCESSING);
                        } elseif ($compareResult == 0) {
                            $salesReqeust->setStatus(SponsorSalesRequestStatus::STATUS_COMPLETED);
                        }
                    }
                    $amountRecieved = $salesReqeust->getAmountReceived();
                    $amountRecieved += $baseRowTotal;
                    $salesReqeust->setAmountReceived($amountRecieved);
                    $salesReqeust->save();
                    $sponsorId = $salesReqeust->getSponsorId();
                    $this->distributeSalesCommissionToSponsors->execute($sponsorId, $baseRowTotal);
                }
            }
        } catch (\Throwable $t) {
            $this->logger->debug($t->getMessage());
            $this->logger->debug($t->getTraceAsString());
        }
    }

    /**
     * @param Invoice $invoice
     * @return array
     */
    private function getInvoiceItemsSortedByProductIds($invoice)
    {
        $products = [];
        $invoiceItems = $invoice->getItemsCollection();
        foreach ($invoiceItems as $item) {
            $products[$item->getProductId()] = $item;
        }
        return $products;
    }
}
