<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MLM\Model\SponsorSalesRequest\Source\Status as SponsorSalesRequestStatus;
use Webkul\MLM\Model\SponsorSalesRequestFactory;

class SaveSponsorSales implements ObserverInterface
{
    /**
     * @param \Webkul\MLM\Helper\Sponsor $sponsorHelper
     * @param \Webkul\MLM\Model\SponsorsFactory $sponsorsF
     * @param \Webkul\MLM\Helper\Data $dataHelper
     * @param \Magento\Framework\Session\SessionManagerInterface $sessionManagerInterface
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Psr\Log\LoggerInterface $logger
     * @param SponsorSalesRequestFactory $sponsorSalesRequestF
     */
    public function __construct(
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Model\SponsorsFactory $sponsorsF,
        \Webkul\MLM\Helper\Data $dataHelper,
        \Magento\Framework\Session\SessionManagerInterface $sessionManagerInterface,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Psr\Log\LoggerInterface $logger,
        SponsorSalesRequestFactory $sponsorSalesRequestF
    ) {
        $this->sponsorHelper = $sponsorHelper;
        $this->sessionManagerInterface = $sessionManagerInterface;
        $this->sponsorsF = $sponsorsF;
        $this->checkoutSession = $checkoutSession;
        $this->dataHelper = $dataHelper;
        $this->logger = $logger;
        $this->sponsorSalesRequestF = $sponsorSalesRequestF;
    }

    /**
     * Address after save event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     * MLMSponsorSales
     * [
     *      [$quoteId] => [
     *           [$sponsorId] => "1,2,3" // Product Ids
     *      ]
     * ]
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            if ($this->dataHelper->isModuleEnabled()) {
                $mlmSalesData = $this->sessionManagerInterface->getMLMSponsorSales();
                if (empty($mlmSalesData)) {
                    return ;
                }
                $mlmSalesData = \Zend_Json::decode($mlmSalesData);
                $order = $observer->getEvent()->getOrder();
                $quoteId = $order->getQuoteId();
                if (empty($mlmSalesData[$quoteId])) {
                    return ;
                }
                $quoteData = $mlmSalesData[$quoteId];
                $orderItemsSortedByProductIds = $this->getOrderItemsSortedByProductIds($order);
                $orderId = $order->getId();
                $storeId = $order->getStoreId();
                foreach ($quoteData as $sponsorId => $productIds) {
                    $productIdsArray = explode(",", $productIds);
                    foreach ($productIdsArray as $productId) {
                        $productId = (int)$productId;
                        if (empty($orderItemsSortedByProductIds[$productId])) {
                            continue;
                        }
                        $orderItem = $orderItemsSortedByProductIds[$productId];
                        $amount = $orderItem->getBaseRowTotal();
                        if ($amount > 0) {
                            $salesRequestData = [
                                'sponsor_id' => $sponsorId,
                                'order_id' => $orderId,
                                'amount' => $amount,
                                'amount_received' => 0.00,
                                'store_id' => $storeId,
                                'status' => SponsorSalesRequestStatus::STATUS_PENDING,
                                'product_id' => $productId,
                            ];
                            $sponsorSalesRequest = $this->sponsorSalesRequestF->create();
                            $sponsorSalesRequest->setData($salesRequestData);
                            $sponsorSalesRequest->save();
                        }
                    }
                }
                $this->sessionManagerInterface->setMLMSponsorSales(null);
            }
        } catch (\Throwable $t) {
            $this->logger->debug($t->getMessage());
            $this->logger->debug($t->getTraceAsString());
        }
    }

    /**
     * @param Order $order
     * @return array
     */
    private function getOrderItemsSortedByProductIds($order)
    {
        $orderProducts = [];
        foreach ($order->getAllVisibleItems() as $item) {
            $orderProducts[$item->getProductId()] = $item;
        }
        return $orderProducts;
    }
}
