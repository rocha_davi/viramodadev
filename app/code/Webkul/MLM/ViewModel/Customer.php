<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Webkul\MLM\Helper\ConfigConstants;

class Customer implements ArgumentInterface
{

    /**
     * @var \Webkul\MLM\Helper\Data
     */
    private $_helper;

    /**
     * Initailized Dependencies
     *
     * @param \Webkul\MLM\Helper\Data $helper
     */
    public function __construct(
        \Webkul\MLM\Helper\Data $helper
    ) {
        $this->_helper = $helper;
    }

    /**
     * Returns general configuration group
     *
     * @return string
     */
    private function _getConfigurationGroup()
    {
        $path = ConfigConstants::SECTION;
        $path .= ConfigConstants::DIR_SEPARATOR;
        $path .= ConfigConstants::CONFIGURATION_GROUP;
        return $path .= ConfigConstants::DIR_SEPARATOR;
    }

    /**
     * Check the module is enabled from backend or not.
     *
     * @return boolean
     */
    public function isMLMEnable()
    {
        $path = $this->_getConfigurationGroup();
        $path .= ConfigConstants::ENABLE;
        $enabled = $this->_helper->getConfigData($path);
        return $enabled ? true : false;
    }

    /**
     * Check the module is enabled from backend or not.
     *
     * @return string
     */
    public function cmsPageUrl()
    {
        $path = $this->_getConfigurationGroup();
        $path .= ConfigConstants::SPONSOR_DETAILS_CMS;
        $identifier = $this->_helper->getConfigData($path);
        return $identifier ? $identifier : "no-route";
    }
}
