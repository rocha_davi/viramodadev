<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

class Profile extends \Magento\Ui\Component\Listing\Columns\Column
{
    const NAME = 'filename';

    const ALT_FIELD = 'entity_id';

    /**
     * @param ContextInterface $context
     * @param StoreManagerInterface $storeManger
     * @param UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        StoreManagerInterface $storeManger,
        UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                if ($item && !empty($item)) {
                    $sponsor = new \Magento\Framework\DataObject($item);
                    $item[$fieldName . '_src'] = $sponsor->getFilename();
                    $item[$fieldName . '_alt'] = $this->getAlt($item) ?: __("No Image");
                    $item[$fieldName . '_link'] = $this->urlBuilder->getUrl(
                        'mlm/Sponsors/edit',
                        ['id' => $sponsor->getEntityId()]
                    );
                    $item[$fieldName . '_orig_src'] = $sponsor->getFilename();
                }
            }
        }
        return $dataSource;
    }

    /**
     * Get Alt
     *
     * @param array $row
     *
     * @return null|string
     */
    protected function getAlt($row)
    {
        $altField = $this->getData('config/altField') ?: self::ALT_FIELD;
        return $row[$altField] ?? null;
    }
}
