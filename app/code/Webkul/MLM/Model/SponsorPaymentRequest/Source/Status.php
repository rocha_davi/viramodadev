<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model\SponsorPaymentRequest\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Sponsors status functionality model
 *
 * @api
 * @since 3.0.0
 */
class Status implements OptionSourceInterface
{
    /**#@+
     * Sponsors Status values
     */
    const STATUS_APPROVED = 1;

    const STATUS_NOT_APPROVED = 2;

    /**#@-*/

    /**
     * Retrieve Visible Status Ids
     *
     * @return int[]
     */
    public function getVisibleStatusIds()
    {
        return [self::STATUS_APPROVED];
    }

    /**
     * Retrieve InVisible Status Ids
     *
     * @return int[]
     */
    public function getInvisibleStatusIds()
    {
        return [self::STATUS_NOT_APPROVED];
    }

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public function getOptionArray()
    {
        return [
            self::STATUS_APPROVED => __('Approved'),
            self::STATUS_NOT_APPROVED => __('Not Approved')
        ];
    }

    /**
     * Retrieve option array with empty value
     *
     * @return string[]
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}
