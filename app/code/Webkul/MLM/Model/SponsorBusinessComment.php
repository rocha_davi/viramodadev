<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model;

use Webkul\MLM\Api\Data\SponsorBusinessCommentInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Attachment
 * @inheritDoc
 */
class SponsorBusinessComment extends AbstractModel implements SponsorBusinessCommentInterface, IdentityInterface
{
   /**
     * No route page id
     */
    const NOROUTE_ENTITY_ID = 'no-route';
    
    /**
     * Sponsors cache tag
     */
    const CACHE_TAG = 'sponsor_business_comment';

    /**
     * Sponsors cache tag
     */
    const MAIN_TABLE = 'sponsor_business_comment';

    /**
     * @var string
     */
    protected $_cacheTag = 'sponsor_business_comment';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Webkul\MLM\Model\ResourceModel\SponsorBusinessComment::class
        );
    }

    /**
     * Load object data
     *
     * @param int|null $id
     * @param string $field
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRoutePlans();
        }
        return parent::load($id, $field);
    }

    /**
     * Load No-Route
     *
     * @return \Webkul\MLM\Model\SponsorBusinessComment
     */
    public function noRouteSponsorBusinessComment()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Webkul\MLM\Api\Data\SponsorsInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get sponsor code
     *
     * @return string
     */
    public function getComment()
    {
        return parent::getData(self::COMMENT);
    }

    /**
     * Set sponsor code
     *
     * @param string $Comment
     * @return $this
     */
    public function setComment($Comment)
    {
        return $this->setData(self::COMMENT, $Comment);
    }

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getSponsorId()
    {
        return parent::getData(self::SPONSOR_ID);
    }

    /**
     * Set Customer Id
     *
     * @param string $SponsorId
     * @return $this
     */
    public function setSponsorId($sponsorId)
    {
        return $this->setData(self::SPONSOR_ID, $sponsorId);
    }

    /**
     * Get createdAt
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return parent::getData(self::CREATED_AT);
    }

    /**
     * Set createdAt
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get updatedAt
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return parent::getData(self::UPDATED_AT);
    }

    /**
     * Set updatedAt
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $id);
    }
}
