<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model;

use Webkul\MLM\Api\Data\SponsorWalletTransactionInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Spoosor Wallet Transaction
 * @inheritDoc
 */
class SponsorWalletTransaction extends AbstractModel implements SponsorWalletTransactionInterface, IdentityInterface
{
   /**
     * No route page id
     */
    const NOROUTE_ENTITY_ID = 'no-route';
    
    /**
     * Sponsors cache tag
     */
    const CACHE_TAG = 'sponsor_wallet_transaction';

    /**
     * Sponsors cache tag
     */
    const MAIN_TABLE = 'sponsor_wallet_transaction';

    /**
     * @var string
     */
    protected $_cacheTag = 'sponsor_wallet_transaction';

    /**
     * Initialized Dependencies
     *
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->date = $date;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Webkul\MLM\Model\ResourceModel\SponsorWalletTransaction::class);
    }

    /**
     * Load object data
     *
     * @param int|null $id
     * @param string $field
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRoutePlans();
        }
        return parent::load($id, $field);
    }

    /**
     * Load No-Route
     *
     * @return \Webkul\MLM\Model\SponsorWalletTransaction
     */
    public function noRouteSponsorWalletTransaction()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Webkul\MLM\Api\Data\SponsorsInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get entity SponsorId
     *
     * @return string
     */
    public function getSponsorId()
    {
        return parent::getData(self::SPONSOR_ID);
    }

    /**
     * Set entity SponsorId
     *
     * @param integer $SponsorId
     * @return $this
     */
    public function setSponsorId($sponsorId)
    {
        return $this->setData(self::SPONSOR_ID, $sponsorId);
    }

    /**
     * Get entity walletId
     *
     * @return string
     */
    public function getWalletId()
    {
        return parent::getData(self::WALLET_ID);
    }

    /**
     * Set entity walletId
     *
     * @param integer $walletId
     * @return $this
     */

    public function setWalletId($walletId)
    {
        return $this->setData(self::WALLET_ID, $walletId);
    }

    /**
     * Get entity EarningType
     *
     * @return string
     */
    public function getEarningType()
    {
        return parent::getData(self::EARNING_TYPE);
    }

    /**
     * Set entity EarningType
     *
     * @param integer $EarningType
     * @return $this
     */
    public function setEarningType($earningType)
    {
        return $this->setData(self::EARNING_TYPE, $earningType);
    }

    /**
     * Get entity EarningType
     *
     * @return string
     */
    public function getWalletBalance()
    {
        return parent::getData(self::WALLET_BALANCE);
    }

    /**
     * Set entity EarningType
     *
     * @param integer $EarningType
     * @return $this
     */
    public function setWalletBalance($earningType)
    {
        return $this->setData(self::WALLET_BALANCE, $earningType);
    }

    /**
     * Get entity Amount
     *
     * @return string
     */
    public function getAmount()
    {
        return parent::getData(self::AMOUNT);
    }

    /**
     * Set entity Amount
     *
     * @param integer $Amount
     * @return $this
     */
    public function setAmount($amount)
    {
        return $this->setData(self::AMOUNT, $amount);
    }

    /**
     * Get entity Status
     *
     * @return string
     */
    public function getStatus()
    {
        return parent::getData(self::STATUS);
    }

    /**
     * Set entity Status
     *
     * @param integer $Status
     * @return $this
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get createdAt
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return parent::getData(self::CREATED_AT);
    }

    /**
     * Set createdAt
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get entity Description
     *
     * @return string
     */
    public function getDescription()
    {
        return parent::getData(self::DESCRIPTION);
    }

    /**
     * Set entity Description
     *
     * @param integer $Description
     * @return $this
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Get updatedAt
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return parent::getData(self::UPDATED_AT);
    }

    /**
     * Set updatedAt
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }
}
