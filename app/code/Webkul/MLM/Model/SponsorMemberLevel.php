<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model;

use Webkul\MLM\Api\Data\SponsorMemberLevelInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Attachment
 * @inheritDoc
 */
class SponsorMemberLevel extends AbstractModel implements SponsorMemberLevelInterface, IdentityInterface
{
   /**
     * No route page id
     */
    const NOROUTE_ENTITY_ID = 'no-route';
    
    /**
     * Sponsors cache tag
     */
    const CACHE_TAG = 'mlm_memberlevel_sponsor';

    /**
     * Sponsors cache tag
     */
    const MAIN_TABLE = 'mlm_memberlevel_sponsor';

    /**
     * @var string
     */
    protected $_cacheTag = 'mlm_memberlevel_sponsor';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Webkul\MLM\Model\ResourceModel\SponsorMemberLevel::class
        );
    }

    /**
     * Load object data
     *
     * @param int|null $id
     * @param string $field
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRoutePlans();
        }
        return parent::load($id, $field);
    }

    /**
     * Load No-Route
     *
     * @return \Webkul\MLM\Model\SponsorMemberLevel
     */
    public function noRouteSponsorMemberLevel()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Webkul\MLM\Api\Data\SponsorsInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }
}
