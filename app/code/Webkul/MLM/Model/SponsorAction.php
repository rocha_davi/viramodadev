<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model;
 
use Webkul\MLM\Api\SponsorActionInterface;
 
class SponsorAction implements SponsorActionInterface
{
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Webkul\MLM\Model\EmailNotification
     */
    protected $emailNotification;
    
    /**
     * Dependencies Initailized
     *
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Webkul\MLM\Model\EmailNotification $emailNotification
     */
    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Webkul\MLM\Helper\SponsorOperation $sponsorOperationHelper,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Helper\SubmitPaymentRequest $submitPaymentRequest,
        \Webkul\MLM\Helper\MemberLevel $memberLevelHelper,
        \Webkul\MLM\Helper\AssignMemberLevelToSponsor $assignMemberLevelToSponsor
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->sponsorOperationHelper = $sponsorOperationHelper;
        $this->memberLevelHelper = $memberLevelHelper;
        $this->submitPaymentRequest = $submitPaymentRequest;
        $this->assignMemberLevelToSponsor = $assignMemberLevelToSponsor;
        $this->sponsorHelper = $sponsorHelper;
    }

    /**
     * @param int $sponsorId
     * @param int $memberLevelId
     * @return string
     */
    public function changeMemberLevel($sponsorId, $memberLevelId)
    {
        $response = [
            "success" => true,
            "message" => __("Member level Successfully changed.")
        ];
        try {
            $this->assignMemberLevelToSponsor->execute($sponsorId, $memberLevelId);
        } catch (\Throwable $e) {
            $response["success"] = false;
            $response["message"] = $e->getMessage();
            $response["trace"] = $e->getTraceAsString();
        }
        return $this->jsonHelper->jsonEncode($response);
    }

    /**
     * @param int $sponsorId
     * @param float $amount
     * @param string $description
     * @return string
     */
    public function payToSponsor($sponsorId, $amount, $description)
    {
        $response = [
            "success" => true,
            "message" => __("Payment request successfully submitted.")
        ];
        try {
            $data['amount'] = $amount;
            $data['sponsor_id'] = $sponsorId;
            $data['description'] = $description;

            $sponsorWalletBalance = $this->sponsorHelper->getWalletBalance($sponsorId);
            if ($amount > $sponsorWalletBalance) {
                $response['success'] = false;
                $response['message'] = __("Insufficient wallet balance.");
                return $this->jsonHelper->jsonEncode($response);
            }

            $this->submitPaymentRequest->execute($data);

        } catch (\Throwable $e) {
            $response["success"] = false;
            $response["message"] = $e->getMessage();
            $response["trace"] = $e->getTraceAsString();
        }
        return $this->jsonHelper->jsonEncode($response);
    }

    /**
     * @param int $sponsorId
     * @param string $title
     * @param string $comment
     * @return string
     */
    public function addSponsorBusinessComment($sponsorId, $title, $comment)
    {
        $response = [
            "success" => true,
            "message" => __("Comment Successfylly added.")
        ];
        try {
            $data['comment'] = $comment;
            $data['title'] = $title;
            $data['sponsor_id'] = $sponsorId;
            $this->sponsorOperationHelper->createBusinessComment($data);
        } catch (\Exception $e) {
            $response["success"] = false;
            $response["message"] = $e->getMessage();
            $response["trace"] = $e->getTraceAsString();
        }
        return $this->jsonHelper->jsonEncode($response);
    }
}
