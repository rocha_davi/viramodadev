<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model\SponsorSalesRequest\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Sponsors status functionality model
 *
 * @api
 * @since 3.0.0
 */
class Status implements OptionSourceInterface
{
    /**#@+
     * Sponsors Status values
     */
    const STATUS_PENDING = 2;
    const STATUS_PROCESSING = 3;

    const STATUS_COMPLETED = 1;

    /**#@-*/

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public function getOptionArray()
    {
        return [
            self::STATUS_COMPLETED => __('Completed'),
            self::STATUS_PENDING => __('Pending'),
            self::STATUS_PROCESSING => __('Processing'),
        ];
    }

    /**
     * Retrieve option array with empty value
     *
     * @return string[]
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}
