<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model;

use Webkul\MLM\Api\Data\SponsorsInterface;
use Magento\Framework\DataObject\IdentityInterface;

/**
 * Attachment
 * @inheritDoc
 */
class Sponsors extends \Magento\Framework\Model\AbstractModel implements SponsorsInterface, IdentityInterface
{
   /**
     * No route page id
     */
    const NOROUTE_ENTITY_ID = 'no-route';
    
    /**
     * Sponsors cache tag
     */
    const CACHE_TAG = 'mlm_sponsor';

    /**
     * Sponsors cache tag
     */
    const MAIN_TABLE = 'mlm_sponsor';

    /**
     * @var string
     */
    protected $_cacheTag = 'mlm_sponsor';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Webkul\MLM\Model\ResourceModel\Sponsors::class);
    }

    /**
     * Load object data
     *
     * @param int|null $id
     * @param string $field
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRoutePlans();
        }
        return parent::load($id, $field);
    }

    /**
     * Load No-Route
     *
     * @return \Webkul\MLM\Model\Sponsors
     */
    public function noRouteSponsors()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Webkul\MLM\Api\Data\SponsorsInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get mlm_sponsorCode
     *
     * @return string
     */
    public function getSponsorCode()
    {
        return parent::getData(self::SPONSOR_CODE);
    }

    /**
     * Set sponsorCode
     *
     * @param string $sponsorCode
     * @return \Webkul\MLM\Api\Data\SponsorsInterface
     */
    public function setSponsorCode($sponsorCode)
    {
        return $this->setData(self::SPONSOR_CODE, $sponsorCode);
    }

    /**
     * Get customer id
     *
     * @return string
     */
    public function getCustomerId()
    {
        return parent::getData(self::CUSTOMER_ID);
    }

    /**
     * Set customer id
     *
     * @param string $customerId
     * @return \Webkul\MLM\Api\Data\SponsorsInterface
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return parent::getData(self::FILENAME);
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return \Webkul\MLM\Api\Data\SponsorsInterface
     */
    public function setFilename($filename)
    {
        return $this->setData(self::FILENAME, $filename);
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return parent::getData(self::STATUS);
    }

    /**
     * Set status
     *
     * @param string $status
     * @return \Webkul\MLM\Api\Data\SponsorsInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return parent::getData(self::CREATED_AT);
    }

    /**
     * Set created at
     *
     * @param string $createdAt
     * @return \Webkul\MLM\Api\Data\SponsorsInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get update at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return parent::getData(self::CREATED_AT);
    }

    /**
     * Set update at
     *
     * @param string $updateAt
     * @return \Webkul\MLM\Api\Data\SponsorsInterface
     */
    public function setUpdatedAt($updateAt)
    {
        return $this->setData(self::CREATED_AT, $updateAt);
    }

    /**
     * Get ParentSponsorId
     *
     * @return boolean
     */
    public function getParentSponsorId()
    {
        return parent::getData(self::PARENT_SPONSOR_ID);
    }

    /**
     * Set ParentSponsorId
     *
     * @param boolean $ParentSponsorId
     * @return $this
     */
    public function setParentSponsorId($parentSponsorId)
    {
        return $this->setData(self::PARENT_SPONSOR_ID, $parentSponsorId);
    }

    /**
     * Get MaxDownlineMemberLimit
     *
     * @return boolean
     */
    public function getMaxDownlineMemberLimit()
    {
        return parent::getData(self::MAX_DOWNLINE_MEMBER_LIMIT);
    }

    /**
     * Set MaxDownlineMemberLimit
     *
     * @param boolean $MaxDownlineMemberLimit
     * @return $this
     */
    public function setMaxDownlineMemberLimit($maxDownlineMemberLimit)
    {
        return $this->setData(self::MAX_DOWNLINE_MEMBER_LIMIT, $maxDownlineMemberLimit);
    }
}
