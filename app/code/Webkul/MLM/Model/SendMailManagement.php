<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model;
 
use Webkul\MLM\Api\SendMailInterface;
 
class SendMailManagement implements SendMailInterface
{
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var \Webkul\MLM\Model\EmailNotification
     */
    protected $emailNotification;
    
    /**
     * Dependencies Initailized
     *
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Webkul\MLM\Model\EmailNotification $emailNotification
     */
    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Webkul\MLM\Model\EmailNotification $emailNotification
    ) {
        $this->emailNotification = $emailNotification;
        $this->jsonHelper = $jsonHelper;
    }

   /**
    * Returns string
    *
    * @api
    * @param string $referenceId
    * @return string
    */
    public function sendReferralMail($referenceId)
    {
        $response = [
            "success" => false,
            "message" => __("The SponsorId is not Valid")
        ];
        try {
            $adminSponsorData = $this->jsonHelper->jsonDecode($referenceId, true);
            $referenceId = $adminSponsorData["sponsorId"];
            $to = $adminSponsorData["email"] ?? "test@webkul.com";
            $result = $this->emailNotification->referralEmail(
                $referenceId,
                $to
            );
            if ($result) {
                $response["success"] = true;
                $response["message"] = __("The Sponsor Reference Id is Valid");
            } else {
                $response["success"] = false;
                $response["message"] = __("The Sponsor Reference Id is not Valid");
            }
        } catch (\Exception $e) {
            $response["success"] = false;
            $response["message"] = $e->getMessage();
        }
        return $this->jsonHelper->jsonEncode($response);
    }
}
