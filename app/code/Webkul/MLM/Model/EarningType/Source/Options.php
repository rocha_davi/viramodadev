<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_MLM
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model\EarningType\Source;

use Magento\Framework\Data\OptionSourceInterface;

class Options implements OptionSourceInterface
{
    /**
     * @param \Webkul\MLM\Model\ResourceModel\EarningType\Collection $collection
     */
    public function __construct(
        \Webkul\MLM\Model\ResourceModel\EarningType\Collection $collection
    ) {
        $this->collection = $collection;
    }

    /**
     * @return array
     */
    public function getOptionsWithLabel()
    {
        $allItems = $this->collection->getItems();
        $options = [];
        foreach ($allItems as $item) {
            $options[$item->getId()] = $item->getLabel();
        }
        return $options;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->getOptionsWithLabel();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
