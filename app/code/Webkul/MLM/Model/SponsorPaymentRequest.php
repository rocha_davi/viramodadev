<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model;

use Webkul\MLM\Api\Data\SponsorPaymentRequestInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Attachment
 * @inheritDoc
 */
class SponsorPaymentRequest extends AbstractModel implements SponsorPaymentRequestInterface, IdentityInterface
{
   /**
     * No route page id
     */
    const NOROUTE_ENTITY_ID = 'no-route';
    
    /**
     * Sponsors cache tag
     */
    const CACHE_TAG = 'sponsor_payment_request';

    /**
     * Sponsors cache tag
     */
    const MAIN_TABLE = 'sponsor_payment_request';

    /**
     * @var string
     */
    protected $_cacheTag = 'sponsor_payment_request';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Webkul\MLM\Model\ResourceModel\SponsorPaymentRequest::class
        );
    }

    /**
     * Load object data
     *
     * @param int|null $id
     * @param string $field
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRoutePlans();
        }
        return parent::load($id, $field);
    }

    /**
     * Load No-Route
     *
     * @return \Webkul\MLM\Model\SponsorPaymentRequest
     */
    public function noRouteSponsorPaymentRequest()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Webkul\MLM\Api\Data\SponsorsInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get sponsor code
     *
     * @return string
     */
    public function getStatus()
    {
        return parent::getData(self::STATUS);
    }

    /**
     * Set sponsor code
     *
     * @param string $Status
     * @return $this
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getSponsorId()
    {
        return parent::getData(self::SPONSOR_ID);
    }

    /**
     * Set Customer Id
     *
     * @param string $SponsorId
     * @return $this
     */
    public function setSponsorId($sponsorId)
    {
        return $this->setData(self::SPONSOR_ID, $sponsorId);
    }

    /**
     * Set Customer Id
     *
     * @param string $Amount
     * @return $this
     */
    public function setAmount($amount)
    {
        return $this->setData(self::AMOUNT, $amount);
    }
    /**
     * Get customer id
     *
     * @return integer
     */
    public function getAmount()
    {
        return parent::getData(self::AMOUNT);
    }

    /**
     * Set Customer Id
     *
     * @param string $CommissionAmount
     * @return $this
     */
    public function setCommissionAmount($commissionAmount)
    {
        return $this->setData(self::COMMISSION_AMOUNT, $commissionAmount);
    }

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getCommissionAmount()
    {
        return parent::getData(self::COMMISSION_AMOUNT);
    }

    /**
     * Set Customer Id
     *
     * @param string $CommissionPercent
     * @return $this
     */
    public function setCommissionPercent($commissionPercent)
    {
        return $this->setData(self::COMMISSION_PERCENT, $commissionPercent);
    }

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getCommissionPercent()
    {
        return parent::getData(self::COMMISSION_PERCENT);
    }

    /**
     * Get createdAt
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return parent::getData(self::CREATED_AT);
    }

    /**
     * Set createdAt
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get updatedAt
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return parent::getData(self::UPDATED_AT);
    }

    /**
     * Set updatedAt
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $id);
    }
}
