<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model;

use Webkul\MLM\Api\Data\SponsorWalletInterface;
use Magento\Framework\DataObject\IdentityInterface;

/**
 * Attachment
 * @inheritDoc
 */
class SponsorWallet extends \Magento\Framework\Model\AbstractModel implements SponsorWalletInterface, IdentityInterface
{
   /**
     * No route page id
     */
    const NOROUTE_ENTITY_ID = 'no-route';
    
    /**
     * Sponsors cache tag
     */
    const CACHE_TAG = 'sponsor_wallet';

    /**
     * Sponsors cache tag
     */
    const MAIN_TABLE = 'sponsor_wallet';

    /**
     * @var string
     */
    protected $_cacheTag = 'sponsor_wallet';

    /**
     * Initialized Dependencies
     *
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->date = $date;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Get Wallet
     *
     * @param int $customerId
     * @return object
     */
    public function getWallet($sponsorId)
    {
        $collection = $this->getCollection()
            ->addFieldToFilter("sponsor_id", $sponsorId);
        if (!$collection->getSize()) {
            $data = [
                "sponsor_id" => $sponsorId,
                "current_balance" => 0.0,
                "total_earning" => 0.0,
            ];
            $this->setData($data)->save();
        }
        return $this->getCollection()->addFieldToFilter("sponsor_id", $sponsorId)->getFirstItem();
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Webkul\MLM\Model\ResourceModel\SponsorWallet::class);
    }

    /**
     * Load object data
     *
     * @param int|null $id
     * @param string $field
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRoutePlans();
        }
        return parent::load($id, $field);
    }

    /**
     * Load No-Route
     *
     * @return \Webkul\MLM\Model\SponsorWallet
     */
    public function noRouteSponsorWallet()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Webkul\MLM\Api\Data\SponsorsInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return parent::getData(self::CREATED_AT);
    }

    /**
     * Set created at
     *
     * @param string $createdAt
     * @return \Webkul\MLM\Api\Data\SponsorsInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get update at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return parent::getData(self::CREATED_AT);
    }

    /**
     * Set update at
     *
     * @param string $updateAt
     * @return \Webkul\MLM\Api\Data\SponsorsInterface
     */
    public function setUpdatedAt($updateAt)
    {
        return $this->setData(self::CREATED_AT, $updateAt);
    }

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getSponsorId()
    {
        return parent::getData(self::SPONSOR_ID);
    }

    /**
     * Set Customer Id
     *
     * @param string $SponsorId
     * @return $this
     */
    public function setSponsorId($sponsorId)
    {
        return $this->setData(self::SPONSOR_ID, $sponsorId);
    }

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getTotalEarning()
    {
        return parent::getData(self::TOTAL_EARNING);
    }

    /**
     * Set Customer Id
     *
     * @param string $TotalEarning
     * @return $this
     */
    public function setTotalEarning($totalEarning)
    {
        return $this->setData(self::TOTAL_EARNING, $totalEarning);
    }

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getCurrentBalance()
    {
        return parent::getData(self::CURRENT_BALANCE);
    }

    /**
     * Set Customer Id
     *
     * @param string $CurrentBalance
     * @return $this
     */
    public function setCurrentBalance($currentBalance)
    {
        return $this->setData(self::CURRENT_BALANCE, $currentBalance);
    }
}
