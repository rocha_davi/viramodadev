<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model;

use Webkul\MLM\Api\Data\SponsorsInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * @api
 * @since 1.0.0
 */
interface EmailNotificationInterface
{
    /**
     * Constants type email to be sent
     */
    const REFERRAL = 'referral';
    
    /**
     * Send email with new account related information
     *
     * @param string $sponsorId
     * @param string $type
     * @return void
     * @throws LocalizedException
     */
    public function referralEmail(
        $sponsorId,
        $to = null,
        $type = self::REFERRAL
    );

    /**
     * @param int $sponsorId
     * @param array $templateParams
     * @return void
     */
    public function sendBusinessCommentEmail(
        $sponsorId,
        $templateParams
    );
}
