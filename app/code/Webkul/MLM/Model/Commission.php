<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model;

use Webkul\MLM\Api\Data\CommissionInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Commission extends AbstractModel implements CommissionInterface, IdentityInterface
{
   /**
     * No route page id
     */
    const NOROUTE_ENTITY_ID = 'no-route';
    
    /**
     * Sponsors cache tag
     */
    const CACHE_TAG = 'mlm_commission';

    /**
     * Sponsors cache tag
     */
    const MAIN_TABLE = 'mlm_commission';

    /**
     * @var string
     */
    protected $_cacheTag = 'mlm_commission';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Webkul\MLM\Model\ResourceModel\Commission::class
        );
    }

    /**
     * Load object data
     *
     * @param int|null $id
     * @param string $field
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRoutePlans();
        }
        return parent::load($id, $field);
    }

    /**
     * Load No-Route
     *
     * @return \Webkul\MLM\Model\Commission
     */
    public function noRouteCommission()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Webkul\MLM\Api\Data\SponsorsInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }
}
