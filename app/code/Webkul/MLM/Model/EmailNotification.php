<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Mail\Template\SenderResolverInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Customer\Helper\View as CustomerViewHelper;
use Webkul\MLM\Api\Data\SponsorsInterface;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Exception\LocalizedException;
use Webkul\MLM\Model\ModuleConstants;
use Webkul\MLM\Helper\Sponsor as SponsorHelper;

/**
 * Customer email notification
 *
 */
class EmailNotification implements EmailNotificationInterface
{
    /**#@+
     * Configuration paths for email templates and identities
     */
    const XML_PATH_REFERRAL_EMAIL_TEMPLATE = 'mlm/sponsor/referral_email_template';
    const XML_PATH_REFERRAL_EMAIL_IDENTITY = 'mlm/sponsor/email_identity';

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var CustomerViewHelper
     */
    protected $customerViewHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataProcessor;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var SenderResolverInterface
     */
    private $senderResolver;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @param CustomerRegistry $customerRegistry
     * @param StoreManagerInterface $storeManager
     * @param TransportBuilder $transportBuilder
     * @param CustomerViewHelper $customerViewHelper
     * @param DataObjectProcessor $dataProcessor
     * @param ScopeConfigInterface $scopeConfig
     * @param SenderResolverInterface|null $senderResolver
     */
    public function __construct(
        Session $customerSession,
        StoreManagerInterface $storeManager,
        TransportBuilder $transportBuilder,
        CustomerViewHelper $customerViewHelper,
        DataObjectProcessor $dataProcessor,
        ScopeConfigInterface $scopeConfig,
        CustomerRepositoryInterface $customerRepository,
        SponsorHelper $sponsorHelper,
        \Psr\Log\LoggerInterface $logger,
        SenderResolverInterface $senderResolver = null
    ) {
        $this->customerRepository = $customerRepository;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->customerViewHelper = $customerViewHelper;
        $this->dataProcessor = $dataProcessor;
        $this->scopeConfig = $scopeConfig;
        $this->session = $customerSession;
        $this->sponsorHelper = $sponsorHelper;
        $this->logger = $logger;
        $this->senderResolver = $senderResolver ?: ObjectManager::getInstance()->get(SenderResolverInterface::class);
    }

    /**
     * Get customer data object
     *
     * @param int $customerId
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    private function getCustomerDataObject($customerId)
    {
        return $this->customerRepository->getById($customerId);
    }

    /**
     * Send email with new account related information
     *
     * @param string $sponsorId
     * @param string $type
     * @return void
     * @throws LocalizedException
     */
    public function referralEmail(
        $sponsorId,
        $to = null,
        $type = self::REFERRAL
    ) {
        $templateParams = [
            "referenceId" => $sponsorId
        ];
        $this->sendEmailTemplate(
            $sponsorId,
            $type,
            self::XML_PATH_REFERRAL_EMAIL_TEMPLATE,
            $to,
            $templateParams
        );
    }

    /**
     * Send email with new account related information
     *
     * @param string $sponsorId
     * @param string $type
     * @return void
     * @throws LocalizedException
     */
    public function sendEmail(
        $email,
        $subject,
        $body,
        $sponsorId
    ) {
        $templateParams = [
            "reference_id" => $sponsorId,
            "subject" => $subject,
            "body_content" => $body
        ];
        $this->sendEmailTemplate(
            $sponsorId,
            self::REFERRAL,
            self::XML_PATH_REFERRAL_EMAIL_TEMPLATE,
            $email,
            $templateParams
        );
    }

    /**
     * Send corresponding email template
     *
     * @param CustomerInterface $customer
     * @param string $template configuration path of email template
     * @param string $sender configuration path of email identity
     * @param array $templateParams
     * @param int|null $storeId
     * @param string $email
     * @return void
     * @throws \Magento\Framework\Exception\MailException
     */
    private function sendEmailTemplate(
        $sponsorId,
        $type,
        $template,
        $email,
        $templateParams
    ) {
        $customerId = $this->session->getCustomerId();
        $currentCustomerDataObject = $this->getCustomerDataObject($customerId);
        
        $storeId = $currentCustomerDataObject->getStoreId();
        $templateId = $this->scopeConfig->getValue($template, 'store', $storeId);
        
        /** @var array $from */
        $from = $this->senderResolver->resolve(
            $this->scopeConfig->getValue(self::XML_PATH_REFERRAL_EMAIL_IDENTITY, 'store', $storeId),
            $storeId
        );
        
        $transport = $this->transportBuilder->setTemplateIdentifier($templateId)
            ->setTemplateOptions(['area' => 'frontend', 'store' => $storeId])
            ->setTemplateVars($templateParams)
            ->setFrom($from)
            ->addTo(
                $email,
                $this->customerViewHelper->getCustomerName(
                    $currentCustomerDataObject
                )
            )->getTransport();
        $transport->sendMessage();
    }

    /**
     * Send corresponding email template
     *
     * @param CustomerInterface $customer
     * @param string $template configuration path of email template
     * @param string $sender configuration path of email identity
     * @param array $templateParams
     * @param int|null $storeId
     * @param string $email
     * @return void
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendBusinessCommentEmail(
        $sponsorId,
        $templateParams
    ) {
        try {
            $customerId = $this->sponsorHelper->getCustomerId($sponsorId);
            $sponsorName = $this->sponsorHelper->getSponsorName($sponsorId);
            $sponsorEmail = $this->sponsorHelper->getSponsorEmail($sponsorId);
            
            $currentCustomerDataObject = $this->getCustomerDataObject($customerId);
            $storeId = $currentCustomerDataObject->getStoreId();
            $templateId = ModuleConstants::BUSINESS_COMMENT_TEMPLATE_ID;
            
            /** @var array $from */
            $from = $this->senderResolver->resolve(
                $this->scopeConfig->getValue(self::XML_PATH_REFERRAL_EMAIL_IDENTITY, 'store', $storeId),
                $storeId
            );
            $templateParams['sponsorName'] = $sponsorName;
            $templateParams['subject'] = $templateParams['title'];
            $transport = $this->transportBuilder->setTemplateIdentifier($templateId)
                ->setTemplateOptions(['area' => 'frontend', 'store' => $storeId])
                ->setTemplateVars($templateParams)
                ->setFrom($from)
                ->addTo(
                    $sponsorEmail,
                    $sponsorName
                )->getTransport();
            $transport->sendMessage();
        } catch (\Throwable $t) {
            $this->logger->debug($t);
        }
    }
}
