<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model;

use Webkul\MLM\Api\Data\MemberLevelInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractExtensibleModel;

/**
 * Attachment
 * @inheritDoc
 */
class MemberLevel extends AbstractExtensibleModel implements MemberLevelInterface, IdentityInterface
{
   /**
     * No route page id
     */
    const NOROUTE_ENTITY_ID = 'no-route';
    
    /**
     * MemberLevel cache tag
     */
    const CACHE_TAG = 'mlm_memberlevel';

    /**
     * MemberLevel cache tag
     */
    const MAIN_TABLE = 'mlm_memberlevel';

    /**
     * @var string
     */
    protected $_cacheTag = 'mlm_memberlevel';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Webkul\MLM\Model\ResourceModel\MemberLevel::class);
    }

    /**
     * Load object data
     *
     * @param int|null $id
     * @param string $field
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRoutePlans();
        }
        if ($field == null) {
            $field = self::ENTITY_ID;
        }
        return parent::load($id, $field);
    }

    /**
     * Load No-Route
     *
     * @return \Webkul\MLM\Model\MemberLevel
     */
    public function noRouteMemberLevel()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Webkul\MLM\Api\Data\MemberLevelInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get sponsor code
     *
     * @return string
     */
    public function getLevelName()
    {
        return $this->getData(self::LEVEL_NAME);
    }

    /**
     * Set sponsor code
     *
     * @param string $LevelName
     * @return $this
     */
    public function setLevelName($levelName)
    {
        $this->setData(self::LEVEL_NAME, $levelName);
        return $this;
    }

    /**
     * Get customer id
     *
     * @return integer
     */
    public function getMaximumBusinessAmount()
    {
        return $this->getData(self::MAXIMUM_BUSINESS_AMOUNT);
    }

    /**
     * Set Customer Id
     *
     * @param string $MaximumBusinessAmount
     * @return $this
     */
    public function setMaximumBusinessAmount($maximumBusinessAmount)
    {
        $this->setData(self::MAXIMUM_BUSINESS_AMOUNT, $maximumBusinessAmount);
        return $this;
    }

    /**
     * Get BonusAmount
     *
     * @return string|null
     */
    public function getBonusAmount()
    {
        return $this->getData(self::BONUS_AMOUNT);
    }

    /**
     * Set BonusAmount
     *
     * @param string $BonusAmount
     * @return $this
     */
    public function setBonusAmount($bonusAmount)
    {
        $this->setData(self::BONUS_AMOUNT, $bonusAmount);
        return $this;
    }

    /**
     * Get Priority
     *
     * @return boolean
     */
    public function getPriority()
    {
        return $this->getData(self::PRIORITY);
    }

    /**
     * Set Priority
     *
     * @param boolean $Priority
     * @return $this
     */
    public function setPriority($Priority)
    {
        $this->setData(self::PRIORITY, $Priority);
        return $this;
    }

    /**
     * Get Priority
     *
     * @return boolean
     */
    public function getBadgeImage()
    {
        return $this->getData(self::BADGE_IMAGE);
    }

    /**
     * Set Priority
     *
     * @param boolean $Priority
     * @return $this
     */
    public function setBadgeImage($badgeImage)
    {
        $this->setData(self::BADGE_IMAGE, $badgeImage);
        return $this;
    }

    /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return parent::getData(self::CREATED_AT);
    }

    /**
     * Set created at
     *
     * @param string $createdAt
     * @return \Webkul\MLM\Api\Data\MemberLevelInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get update at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return parent::getData(self::CREATED_AT);
    }

    /**
     * Set update at
     *
     * @param string $updateAt
     * @return \Webkul\MLM\Api\Data\MemberLevelInterface
     */
    public function setUpdatedAt($updateAt)
    {
        return $this->setData(self::CREATED_AT, $updateAt);
    }

    /**
     * @inheritDoc
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * @inheritDoc
     */
    public function setExtensionAttributes(
        \Webkul\MLM\Api\Data\MemberLevelExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
