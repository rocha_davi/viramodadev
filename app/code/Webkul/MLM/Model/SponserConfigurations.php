<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MLM
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */


namespace Webkul\MLM\Model;

/**
 * SponserConfigurations Class
 */
class SponserConfigurations extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface, \Webkul\MLM\Api\Data\SponserConfigurationsInterface
{

    const NOROUTE_ENTITY_ID = 'no-route';

    const CACHE_TAG = 'webkul_mlm_sponserconfigurations';

    protected $_cacheTag = 'webkul_mlm_sponserconfigurations';

    protected $_eventPrefix = 'webkul_mlm_sponserconfigurations';

    /**
     * set resource model
     */
    public function _construct()
    {
        $this->_init(\Webkul\MLM\Model\ResourceModel\SponserConfigurations::class);
    }

    /**
     * Load No-Route Indexer.
     *
     * @return $this
     */
    public function noRouteReasons()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities.
     *
     * @return []
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG.'_'.$this->getId()];
    }

    /**
     * Set EntityId
     *
     * @param int $entityId
     * @return Webkul\MLM\Model\SponserConfigurationsInterface
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get EntityId
     *
     * @return int
     */
    public function getEntityId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set SponsorId
     *
     * @param int $sponsorId
     * @return Webkul\MLM\Model\SponserConfigurationsInterface
     */
    public function setSponsorId($sponsorId)
    {
        return $this->setData(self::SPONSOR_ID, $sponsorId);
    }

    /**
     * Get SponsorId
     *
     * @return int
     */
    public function getSponsorId()
    {
        return parent::getData(self::SPONSOR_ID);
    }

    /**
     * Set LogoImage
     *
     * @param string $logoImage
     * @return Webkul\MLM\Model\SponserConfigurationsInterface
     */
    public function setLogoImage($logoImage)
    {
        return $this->setData(self::LOGO_IMAGE, $logoImage);
    }

    /**
     * Get LogoImage
     *
     * @return string
     */
    public function getLogoImage()
    {
        return parent::getData(self::LOGO_IMAGE);
    }

    /**
     * Set LogoWidth
     *
     * @param int $logoWidth
     * @return Webkul\MLM\Model\SponserConfigurationsInterface
     */
    public function setLogoWidth($logoWidth)
    {
        return $this->setData(self::LOGO_WIDTH, $logoWidth);
    }

    /**
     * Get LogoWidth
     *
     * @return int
     */
    public function getLogoWidth()
    {
        return parent::getData(self::LOGO_WIDTH);
    }

    /**
     * Set LogoHeight
     *
     * @param int $logoHeight
     * @return Webkul\MLM\Model\SponserConfigurationsInterface
     */
    public function setLogoHeight($logoHeight)
    {
        return $this->setData(self::LOGO_HEIGHT, $logoHeight);
    }

    /**
     * Get LogoHeight
     *
     * @return int
     */
    public function getLogoHeight()
    {
        return parent::getData(self::LOGO_HEIGHT);
    }
}
