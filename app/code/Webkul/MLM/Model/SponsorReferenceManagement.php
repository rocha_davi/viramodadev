<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model;
 
use Webkul\MLM\Api\VerifySponsorInterface;
use Webkul\MLM\Helper\StoreConfiguration;
use Webkul\MLM\Model\Sponsors\Source\Status as SponsorStatus;
 
class SponsorReferenceManagement implements VerifySponsorInterface
{
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @var object \Magento\Framework\Stdlib\DateTime\DateTime
     */
    private $_dataTime;

    /**
     * @var \Webkul\MLM\Model\SponsorsFactory
     */
    protected $sponsorsFactory;

    /**
     * StoreConfiguration $storeConfiguration
     *
     * @var StoreConfiguration $storeConfiguration
     */
    protected $StoreConfiguration;
    
    /**
     * Dependencies Initailized
     *
     * @param StoreConfiguration $storeConfiguration
     * @param \Webkul\MLM\Model\SponsorsFactory $sponsorsFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $dataTime
     */
    public function __construct(
        StoreConfiguration $storeConfiguration,
        \Webkul\MLM\Model\SponsorsFactory $sponsorsFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Stdlib\DateTime\DateTime $dataTime
    ) {
        $this->_dataTime = $dataTime;
        $this->sponsorsFactory = $sponsorsFactory;
        $this->jsonHelper = $jsonHelper;
        $this->storeConfiguration = $storeConfiguration;
    }

   /**
    * Returns string
    *
    * @api
    * @param string $referenceId
    * @return string
    */
    public function verifySponsorId($referenceId)
    {
        $response = [
            "success" => false,
            "message" => __("The SponsorId is not Valid")
        ];
        try {
            $adminSponsorId = $this->storeConfiguration->getSavedAdminSponsorId();
            $sponsorData = $this->jsonHelper->jsonDecode($referenceId, true);
            $referenceId = $sponsorData["sponsorId"];
            if (empty($referenceId)) {
                return $this->jsonHelper->jsonEncode($response);
            }
            $collection = $this->sponsorsFactory->create()->getCollection();
            $collection->addFieldToFilter("sponsor_reference_code", $referenceId);
           
            $size = $collection->getSize();
            if ($size >= 0) {
                if ($adminSponsorId == $referenceId) {
                    $response["success"] = true;
                    $response["message"] = __("The Sponsor Reference Id is Valid");
                } else {
                    $collection = $this->sponsorsFactory->create()->getCollection();
                    $collection->addFieldToFilter("sponsor_code", $referenceId);
                    $sponsorCollSize = $collection->getSize();
                    $sponsorStatus = $collection->getFirstItem()->getStatus();
                    if ($size >= 0 && SponsorStatus::STATUS_ENABLED==$sponsorStatus) {
                        $response["success"] = true;
                        $response["message"] = __("The Sponsor Reference Id is Valid");
                    } else {
                        $response["success"] = false;
                        $response["message"] = __("The Sponsor Reference Id is not Valid");
                    }
                }
            } else {
                if ($adminSponsorId == $referenceId && ($size >=0)) {
                    $response["success"] = true;
                    $response["message"] = __("The Sponsor Reference Id is Valid");
                } else {
                    $response["success"] = false;
                    $response["message"] = __("The Sponsor Reference Id is not Valid");
                }
            }
        } catch (\Exception $e) {
            $response["success"] = false;
            $response["message"] = $e->getMessage();
        }
        return $this->jsonHelper->jsonEncode($response);
    }
}
