<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Model\MemberLevel;

use Webkul\MLM\Api\Data\MemberLevel\MemberCountInterface;

class MemberCount extends \Magento\Framework\Model\AbstractModel implements MemberCountInterface
{
    /**
     * @inheritDoc
     */
    public function getMemberCount()
    {
        return $this->getData(self::MEMBER_COUNT);
    }

    /**
     * @inheritDoc
     */
    public function setMemberCount($value)
    {
        $this->setData(self::MEMBER_COUNT, $value);
        return $this;
    }
}
