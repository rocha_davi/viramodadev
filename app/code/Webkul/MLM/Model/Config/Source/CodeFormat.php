<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Model\Config\Source;

class CodeFormat implements \Magento\Framework\Option\ArrayInterface
{
    const ALPHA_NUMBERIC = "alphanumeric";
    const NUMBERIC = "numeric";
    const ALPHBETIC = "alphabetic";
    
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() : array
    {
        return [
            [
                'value' => strtolower(self::ALPHA_NUMBERIC),
                'label' => __(("Alphanumeric"))
            ],
            [
                'value' => strtolower(self::NUMBERIC),
                'label' => __(("Numeric"))
            ],
            [
                'value' => strtolower(self::ALPHBETIC),
                'label' => __(("Alphabetic"))
            ]
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray() : array
    {
        return [
            strtolower(self::ALPHA_NUMBERIC) => __(("Alphanumeric")),
            strtolower(self::NUMBERIC) => __(("Numeric")),
            strtolower(self::ALPHBETIC) => __(("Alphabetic"))
        ];
    }
}
