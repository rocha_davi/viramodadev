<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Model;

abstract class ModuleConstants
{
    /**#@+
     * Constants for keys of data array
     */
    const MEMBER_LEVEL = 'memberlevel';
    const MEMBER_LEVEL_BADGE_IMAGES_DIR = 'webkul_mlm/memberlevel/badge/images';
    const BUSINESS_COMMENT_TEMPLATE_ID = 'mlm_sponsor_business_comment_email_template';
    /**#@-*/
}
