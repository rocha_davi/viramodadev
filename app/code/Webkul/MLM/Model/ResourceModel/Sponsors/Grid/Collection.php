<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */

/**
 * Sponsors model Collection
 *
 * @author     Webkul Team <support@webkul.com>
 */
namespace Webkul\MLM\Model\ResourceModel\Sponsors\Grid;

use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Search\AggregationInterface;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Webkul\MLM\Model\ResourceModel\Sponsors\Collection as SponsorsCollection;

/**
 * Collection for displaying grid of Sponsors
 */
class Collection extends SponsorsCollection implements SearchResultInterface
{
    /**
     * @var AggregationInterface
     */
    protected $aggregations;

    /**
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entity
     * @param \Psr\Log\LoggerInterface $logger
     * @param  FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $event
     * @param \Magento\Store\Model\StoreManagerInterface $store
     * @param mixed|null $mainTable
     * @param AbstractDb $eventPrefix
     * @param mixed $eventObject
     * @param mixed $resourceModel
     * @param string $model
     * @param null $connection
     * @param AbstractDb|null $resource
     *
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entity,
        \Psr\Log\LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $event,
        \Magento\Store\Model\StoreManagerInterface $store,
        $mainTable,
        $eventPrefix,
        $eventObject,
        $resourceModel,
        $model = \Magento\Framework\View\Element\UiComponent\DataProvider\Document::class,
        $connection = null,
        AbstractDb $resource = null
    ) {
        parent::__construct($entity, $logger, $fetchStrategy, $event);
        $this->_eventPrefix = $eventPrefix;
        $this->_eventObject = $eventObject;
        $this->_init($model, $resourceModel);
        $this->setMainTable($mainTable);
        $this->joinTables();
        $this->addTotalBusinessField();
        $this->addDownlineMemberField();
    }

    /**
     * @return void
     */
    public function joinTables()
    {
        $memberLevelSponsorTableName = $this->getTable("mlm_memberlevel_sponsor");
        $memberLevelTableName = $this->getTable("mlm_memberlevel");
        $customerEntityTableName = $this->getTable("customer_entity");
        $sponsorEarningTableName = $this->getTable("mlm_sponsor_earning");
        $sponsorWalletTable = $this->getTable("sponsor_wallet");
        $sponsorTableName = $this->getTable("mlm_sponsor");
        $this->getSelect()
        ->join(
            ["mlm_memberlevel_sponsor" => $memberLevelSponsorTableName],
            "mlm_memberlevel_sponsor.sponsor_id=main_table.entity_id",
            ["memberlevel_id" => "mlm_memberlevel_sponsor.memberlevel_id"]
        )
        ->join(
            ["mlm_memberlevel" => $memberLevelTableName],
            "mlm_memberlevel_sponsor.memberlevel_id=mlm_memberlevel.entity_id",
            ["level_name" => "mlm_memberlevel.level_name"]
        )
        ->join(
            ["customer_entity" => $customerEntityTableName],
            "customer_entity.entity_id=main_table.customer_id",
            [
                "email" => "customer_entity.email",
                "name" => "CONCAT(customer_entity.firstname, ' ',".
                    " IF(LENGTH(IFNULL(customer_entity.middlename, ''))>0,"
                    ." CONCAT(customer_entity.middlename, ' '), '')".
                    ", customer_entity.lastname)",
                "customer_entity_id" => "customer_entity.entity_id",
            ]
        )
        ->joinLeft(
            ["sponsor_wallet" => $sponsorWalletTable],
            "sponsor_wallet.sponsor_id=main_table.entity_id",
            ["current_balance" => "IFNULL((sponsor_wallet.current_balance), 0)"]
        )
        ;
    }

    /**
     * @return void
     */
    public function addTotalBusinessField()
    {
        $mlmSponsorEarning = $this->getTable("mlm_sponsor_earning");
        $this->getSelect()
            ->columns([
                "total_business" =>
                "(SELECT IFNULL(SUM({$mlmSponsorEarning}.amount), 0) FROM {$mlmSponsorEarning} WHERE " .
                " {$mlmSponsorEarning}.sponsor_id=main_table.entity_id AND {$mlmSponsorEarning}.amount > 0)",
            ]);
        $collectionSql = $this->getSelect()->assemble();
        $this->getSelect()->reset();
        $this->getSelect()->from(
            ["main_table" => new \Zend_Db_Expr("($collectionSql)")],
            new \Zend_Db_Expr("*")
        );
    }

    /**
     * @return void
     */
    public function addDownlineMemberField()
    {
        $sponsorSponsorTable = $this->getTable("mlm_sponsor_sponsor");
        $this->getSelect()
            ->columns([
                "downline_member" =>
                "(SELECT COUNT(*) FROM {$sponsorSponsorTable} WHERE parent_sponsor_id = main_table.entity_id)",
            ]);
        $collectionSql = $this->getSelect()->assemble();
        $this->getSelect()->reset();
        $this->getSelect()->from(
            ["main_table" => new \Zend_Db_Expr("($collectionSql)")],
            new \Zend_Db_Expr("*")
        );
    }

    /**
     * @return AggregationInterface
     */
    public function getAggregations()
    {
        return $this->aggregations;
    }

    /**
     * Get search criteria.
     *
     * @return \Magento\Framework\Api\SearchCriteriaInterface|null
     */
    public function getSearchCriteria()
    {
        return '';
    }

    /**
     * @param AggregationInterface $aggregations
     *
     * @return $this
     */
    public function setAggregations($aggregations)
    {
        $this->aggregations = $aggregations;
    }

    /**
     * Set items list.
     *
     * @param \Magento\Framework\Api\ExtensibleDataInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items = null)
    {
        return $this;
    }

    /**
     * Retrieve all ids for collection
     * Backward compatibility with EAV collection.
     *
     * @param int $limit
     * @param int $offset
     *
     * @return array
     */
    public function getAllIds($limit = null, $offset = null)
    {
        $ids = $this->_getAllIdsSelect($limit, $offset);
        return $this->getConnection()->fetchCol($ids, $this->_bindParams);
    }

    /**
     * Set search criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $search
     *
     * @return $this
     */
    public function setSearchCriteria(SearchCriteriaInterface $search = null)
    {
        return $this;
    }

    /**
     * Set total count.
     *
     * @param int $totalCount
     *
     * @return $this
     */
    public function setTotalCount($totalCount)
    {
        return $this;
    }

    /**
     * Get total count.
     *
     * @return int
     */
    public function getTotalCount()
    {
        return $this->getSize();
    }
}
