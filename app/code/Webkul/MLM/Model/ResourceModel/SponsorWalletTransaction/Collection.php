<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */

/**
 * Sponsors model Collection
 *
 * @author Webkul Team <support@webkul.com>
 */
namespace Webkul\MLM\Model\ResourceModel\SponsorWalletTransaction;

/**
 * @api
 * @since 3.0.0
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Initialize resource
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Webkul\MLM\Model\SponsorWalletTransaction::class,
            \Webkul\MLM\Model\ResourceModel\SponsorWalletTransaction::class
        );

        $this->_idFieldName = \Webkul\MLM\Model\SponsorWalletTransaction::ENTITY_ID;
    }

    /**
     * Add filter by store.
     *
     * @param int|array|\Magento\Store\Model\Store $store
     * @param bool                                 $withAdmin
     *
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        if (!$this->getFlag('store_filter_added')) {
            $this->performAddStoreFilter($store, $withAdmin);
        }
        return $this;
    }
}
