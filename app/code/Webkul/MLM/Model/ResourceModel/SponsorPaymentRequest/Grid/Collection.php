<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */

/**
 * SponsorPaymentRequest model Collection
 *
 * @author     Webkul Team <support@webkul.com>
 */
namespace Webkul\MLM\Model\ResourceModel\SponsorPaymentRequest\Grid;

use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Search\AggregationInterface;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Webkul\MLM\Model\ResourceModel\SponsorPaymentRequest\Collection as SponsorPaymentRequestCollection;

/**
 * Collection for displaying grid of SponsorPaymentRequest
 */
class Collection extends SponsorPaymentRequestCollection implements SearchResultInterface
{
    /**
     * @var AggregationInterface
     */
    protected $aggregations;

    /**
     * @param \Magento\Framework\Data\Collection\EntityFactoryInterface $entity
     * @param \Psr\Log\LoggerInterface $logger
     * @param  FetchStrategyInterface $fetchStrategy
     * @param \Magento\Framework\Event\ManagerInterface $event
     * @param \Magento\Store\Model\StoreManagerInterface $store
     * @param mixed|null $mainTable
     * @param AbstractDb $eventPrefix
     * @param mixed $eventObject
     * @param mixed $resourceModel
     * @param string $model
     * @param null $connection
     * @param AbstractDb|null $resource
     *
     */
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entity,
        \Psr\Log\LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $event,
        \Magento\Store\Model\StoreManagerInterface $store,
        $mainTable,
        $eventPrefix,
        $eventObject,
        $resourceModel,
        $model = \Magento\Framework\View\Element\UiComponent\DataProvider\Document::class,
        $connection = null,
        AbstractDb $resource = null
    ) {
        parent::__construct($entity, $logger, $fetchStrategy, $event);
        $this->_eventPrefix = $eventPrefix;
        $this->_eventObject = $eventObject;
        $this->_init($model, $resourceModel);
        $this->setMainTable($mainTable);
        $this->joinTables($this);
    }

    /**
     * @param $this $payoutCollection
     * @return void
     */
    public function joinTables($payoutCollection)
    {
        $memberLevelSponsorTableName = $payoutCollection->getTable("mlm_memberlevel_sponsor");
        $memberLevelTableName = $payoutCollection->getTable("mlm_memberlevel");
        $sponsorTableName = $payoutCollection->getTable("mlm_sponsor");
        $customerTable = $payoutCollection->getTable("customer_entity");
        $payoutCollection->addFilterToMap('status', 'main_table.status');
        $payoutCollection->getSelect()
            ->join(
                ["mlm_memberlevel_sponsor" => $memberLevelSponsorTableName],
                "mlm_memberlevel_sponsor.sponsor_id=main_table.sponsor_id",
                ["memberlevel_id" => "mlm_memberlevel_sponsor.memberlevel_id"]
            )
            ->join(
                ["mlm_memberlevel" => $memberLevelTableName],
                "mlm_memberlevel_sponsor.memberlevel_id=mlm_memberlevel.entity_id",
                ["level_name" => "mlm_memberlevel.level_name"]
            )
            ->join(
                ["mlm_sponsor" => $sponsorTableName],
                "mlm_sponsor.entity_id=main_table.sponsor_id",
                ["customer_id" => "mlm_sponsor.customer_id"]
            )
            ->join(
                ["customer_entity" => $customerTable],
                "mlm_sponsor.customer_id=customer_entity.entity_id",
                [
                    "name" => "CONCAT(customer_entity.firstname, ' ',".
                    " IF(LENGTH(IFNULL(customer_entity.middlename, ''))>0,"
                    ." CONCAT(customer_entity.middlename, ' '), '')".
                    ", customer_entity.lastname)",
                    "customer_entity_id" => "customer_entity.entity_id",
                ]
            );
            $collectionSql = $payoutCollection->getSelect()->assemble();
            $payoutCollection->getSelect()->reset();
            $payoutCollection->getSelect()->from(
                ["main_table" => new \Zend_Db_Expr("($collectionSql)")],
                new \Zend_Db_Expr("*")
            );
    }

    /**
     * @return AggregationInterface
     */
    public function getAggregations()
    {
        return $this->aggregations;
    }

    /**
     * Get search criteria.
     *
     * @return \Magento\Framework\Api\SearchCriteriaInterface|null
     */
    public function getSearchCriteria()
    {
        return '';
    }

    /**
     * @param AggregationInterface $aggregations
     *
     * @return $this
     */
    public function setAggregations($aggregations)
    {
        $this->aggregations = $aggregations;
    }

    /**
     * Set items list.
     *
     * @param \Magento\Framework\Api\ExtensibleDataInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items = null)
    {
        return $this;
    }

    /**
     * Retrieve all ids for collection
     * Backward compatibility with EAV collection.
     *
     * @param int $limit
     * @param int $offset
     *
     * @return array
     */
    public function getAllIds($limit = null, $offset = null)
    {
        $ids = $this->_getAllIdsSelect($limit, $offset);
        return $this->getConnection()->fetchCol($ids, $this->_bindParams);
    }

    /**
     * Set search criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $search
     *
     * @return $this
     */
    public function setSearchCriteria(SearchCriteriaInterface $search = null)
    {
        return $this;
    }

    /**
     * Set total count.
     *
     * @param int $totalCount
     *
     * @return $this
     */
    public function setTotalCount($totalCount)
    {
        return $this;
    }

    /**
     * Get total count.
     *
     * @return int
     */
    public function getTotalCount()
    {
        return $this->getSize();
    }
}
