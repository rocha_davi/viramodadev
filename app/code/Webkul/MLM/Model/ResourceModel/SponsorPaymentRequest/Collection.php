<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_MLM
 * @author     Webkul
 * @copyright  Copyright (c) Webkul (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */

/**
 * Sponsors model Collection
 *
 * @author     Webkul Team <support@webkul.com>
 */
namespace Webkul\MLM\Model\ResourceModel\SponsorPaymentRequest;

/**
 * @api
 * @since 3.0.0
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Initialize resource
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Webkul\MLM\Model\SponsorPaymentRequest::class,
            \Webkul\MLM\Model\ResourceModel\SponsorPaymentRequest::class
        );
        $this->_idFieldName = \Webkul\MLM\Model\SponsorPaymentRequest::ENTITY_ID;
        $this->addFilterToMap(
            'entity_id',
            'main_table.entity_id'
        );
    }

    /**
     * Add filter by store.
     *
     * @param int|array|\Magento\Store\Model\Store $store
     * @param bool                                 $withAdmin
     *
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        if (!$this->getFlag('store_filter_added')) {
            $this->performAddStoreFilter($store, $withAdmin);
        }
        return $this;
    }
}
