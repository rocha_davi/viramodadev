<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MLM
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */


namespace Webkul\MLM\Model;

/**
 * SponsorBanner Class
 */
class SponsorBanner extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface, \Webkul\MLM\Api\Data\SponsorBannerInterface
{

    const NOROUTE_ENTITY_ID = 'no-route';

    const CACHE_TAG = 'webkul_mlm_sponsorbanner';

    protected $_cacheTag = 'webkul_mlm_sponsorbanner';

    protected $_eventPrefix = 'webkul_mlm_sponsorbanner';

    /**
     * set resource model
     */
    public function _construct()
    {
        $this->_init(\Webkul\MLM\Model\ResourceModel\SponsorBanner::class);
    }

    /**
     * Load No-Route Indexer.
     *
     * @return $this
     */
    public function noRouteReasons()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities.
     *
     * @return []
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG.'_'.$this->getId()];
    }

    /**
     * Set EntityId
     *
     * @param int $entityId
     * @return Webkul\MLM\Model\SponsorBannerInterface
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get EntityId
     *
     * @return int
     */
    public function getEntityId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set SponsorId
     *
     * @param int $sponsorId
     * @return Webkul\MLM\Model\SponsorBannerInterface
     */
    public function setSponsorId($sponsorId)
    {
        return $this->setData(self::SPONSOR_ID, $sponsorId);
    }

    /**
     * Get SponsorId
     *
     * @return int
     */
    public function getSponsorId()
    {
        return parent::getData(self::SPONSOR_ID);
    }

    /**
     * Set BannerImage
     *
     * @param string $bannerImage
     * @return Webkul\MLM\Model\SponsorBannerInterface
     */
    public function setBannerImage($bannerImage)
    {
        return $this->setData(self::BANNER_IMAGE, $bannerImage);
    }

    /**
     * Get BannerImage
     *
     * @return string
     */
    public function getBannerImage()
    {
        return parent::getData(self::BANNER_IMAGE);
    }

    /**
     * Set BannerLink
     *
     * @param string $bannerLink
     * @return Webkul\MLM\Model\SponsorBannerInterface
     */
    public function setBannerLink($bannerLink)
    {
        return $this->setData(self::BANNER_LINK, $bannerLink);
    }

    /**
     * Get BannerLink
     *
     * @return string
     */
    public function getBannerLink()
    {
        return parent::getData(self::BANNER_LINK);
    }

    /**
     * Set Sortorder
     *
     * @param int $sortorder
     * @return Webkul\MLM\Model\SponsorBannerInterface
     */
    public function setSortorder($sortorder)
    {
        return $this->setData(self::SORTORDER, $sortorder);
    }

    /**
     * Get Sortorder
     *
     * @return int
     */
    public function getSortorder()
    {
        return parent::getData(self::SORTORDER);
    }
}
