<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Controller\Adminhtml\Commission;

class MassDelete extends \Webkul\MLM\Controller\Adminhtml\Commission
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectF->create();
        $collection     = $this->massActionFilter->getCollection($this->commissionCollF->create());
        $commissionDel = 0;
        foreach ($collection->getAllIds() as $commissionId) {
            if (!empty($commissionId)) {
                try {
                    $this->commissionF->create()->load($commissionId)->delete();
                    $commissionDel++;
                } catch (\Throwable $exception) {
                    $this->messageManager->addError($exception->getMessage());
                }
            }
        }
        if ($commissionDel) {
            $this->messageManager->addSuccess(__("A total of %1 record(s) were deleted.", $commissionDel));
        }
        return $resultRedirect->setPath("mlm/commission/index");
    }
}
