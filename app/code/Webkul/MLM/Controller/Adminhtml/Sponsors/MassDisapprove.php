<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Controller\Adminhtml\Sponsors;

use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Webkul\MLM\Model\ResourceModel\Sponsors\CollectionFactory;
use Webkul\MLM\Model\Sponsors\Source\Status as SponsorStatus;

class MassDisapprove extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Webkul_MLM::sponsorlist';

    /**
     * @var \Magento\Backend\App\Action\Context
     */
    protected $context;

    /**
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $filter;

    /**
     * @param Context $context
     * @param Filter $filter
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        \Magento\Backend\Model\View\Result\RedirectFactory $resultRedirectFactory
    ) {
        parent::__construct($context);
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->redirectResultFactory = $resultRedirectFactory;
    }

    /**
     * Execute action.
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     *
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        foreach ($collection as $model) {
            $this->changeStatus($model);
        }
        
        $this->messageManager->addSuccess(
            __(
                'A total of %1 record(s) have been disapproved.',
                $collection->getSize()
            )
        );
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->redirectResultFactory->create();
        return $resultRedirect->setPath('mlm/sponsors/index');
    }

    /**
     * Changes status
     *
     * @param object $sponsor
     * @return void
     */
    private function changeStatus($sponsor)
    {
        $sponsor->setStatus(SponsorStatus::STATUS_DISABLED)->save();
    }
}
