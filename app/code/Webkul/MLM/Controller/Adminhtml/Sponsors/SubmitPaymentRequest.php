<?php
/**
 * Webkul Software.
 *
 * PHP version 7.0+
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul <support@webkul.com>
 * @copyright Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html ASL Licence
 * @link      https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Controller\Adminhtml\Sponsors;

use Magento\Framework\Exception\LocalizedException;

class SubmitPaymentRequest extends \Magento\Backend\App\Action
{
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Webkul\MLM\Model\SponsorAction $sponsorAction
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Psr\Log\LoggerInterface $logger,
        \Webkul\MLM\Model\SponsorAction $sponsorAction
    ) {
        parent::__construct($context);

        $this->jsonHelper = $jsonHelper;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->logger = $logger;
        $this->sponsorAction = $sponsorAction;
    }

    /**
     * @return \Magetno\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $response = [
            "success" => false,
        ];
        try {
            $wholeData = $this->getRequest()->getPostValue();
            $sponsorId =  $wholeData['sponsorId'];
            $amount = $wholeData['amount'];
            $description = $wholeData['description'];
            if ($amount != null) {
                $response = $this->sponsorAction->payToSponsor($sponsorId, $amount, $description);
                $response = $this->jsonHelper->jsonDecode($response);
            } else {
                $response = [
                    "success" => false,
                    "message" => __('Please fill amount to pay.')
                ];
            }
            
        } catch (\Throwable $e) {
            $response['message'] = $e->getMessage();
            $response['trace'] = $e->getTraceAsString();
        }
        $resultJson->setData($response);
        return $resultJson;
    }
}
