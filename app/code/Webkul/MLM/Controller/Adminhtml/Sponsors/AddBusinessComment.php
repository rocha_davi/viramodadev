<?php
/**
 * Webkul Software.
 *
 * PHP version 7.0+
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul <support@webkul.com>
 * @copyright Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html ASL Licence
 * @link      https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Controller\Adminhtml\Sponsors;

use Magento\Framework\Exception\LocalizedException;

class AddBusinessComment extends \Magento\Backend\App\Action
{
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Webkul\MLM\Api\SponsorActionInterface $sponsorAction
     * @param \Webkul\MLM\Model\EmailNotificationInterface $emailNotification
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Psr\Log\LoggerInterface $logger,
        \Webkul\MLM\Api\SponsorActionInterface $sponsorAction,
        \Webkul\MLM\Model\EmailNotificationInterface $emailNotification
    ) {
        parent::__construct($context);

        $this->jsonHelper = $jsonHelper;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->logger = $logger;
        $this->sponsorAction = $sponsorAction;
        $this->emailNotification = $emailNotification;
    }

    /**
     * @return \Magetno\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $response = [
            "success" => false,
        ];
        try {
            $wholeData = $this->getRequest()->getPostValue();
            $sponsorId =  $wholeData['sponsorId'];
            $title = $wholeData['title'];
            $comment = $wholeData['comment'];
            if ($title == null || $comment == null) {
                $response = [
                    "success" => false,
                    "message" => __('Please fill all the details!')
                ];
                $resultJson->setData($response);
                return $resultJson;
            }
            $emailCopy = (bool)((int)($wholeData['email_copy'] ?? false));
            $response = $this->sponsorAction->addSponsorBusinessComment($sponsorId, $title, $comment);
            if ($emailCopy) {
                $templateVars['title'] = $title;
                $templateVars['comment'] = $comment;
                $this->emailNotification->sendBusinessCommentEmail($sponsorId, $templateVars);
            }
            $response = $this->jsonHelper->jsonDecode($response);
        } catch (\Throwable $e) {
            $response['message'] = $e->getMessage();
            $response['trace'] = $e->getTraceAsString();
        }
        
        return $resultJson;
    }
}
