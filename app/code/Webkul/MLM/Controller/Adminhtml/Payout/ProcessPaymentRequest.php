<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Controller\Adminhtml\Payout;

use Magento\Backend\App\Action\Context;
use Webkul\MLM\Model\ResourceModel\SponsorPaymentRequest\CollectionFactory;
use Webkul\MLM\Model\SponsorPaymentRequest\Source\Status as PaymentRequestStatus;

class ProcessPaymentRequest extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Webkul_MLM::payout';

    /**
     * @var \Magento\Backend\App\Action\Context
     */
    protected $context;

    /**
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    protected $filter;

    /**
     * @param Context $context
     * @param Filter $filter
     */
    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        \Magento\Backend\Model\View\Result\RedirectFactory $resultRedirectFactory,
        \Webkul\MLM\Helper\ProcessPaymentRequest $processPaymentRequest
    ) {
        parent::__construct($context);
        $this->collectionFactory = $collectionFactory;
        $this->redirectResultFactory = $resultRedirectFactory;
        $this->processPaymentRequest = $processPaymentRequest;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        try {
            if (!$this->_authorization->isAllowed(static::ADMIN_RESOURCE)) {
                return $this->resultForwardFactory->create()->forward('denied');
            }
            $request = $this->getRequest();
            $paymentID = $request->getParam('id', false);
            $paymentRequest = $this->collectionFactory->create()
                ->addFieldToFilter("entity_id", $paymentID)
                ->getFirstItem();
            $status = $paymentRequest->getStatus();
            if (PaymentRequestStatus::STATUS_APPROVED == $status) {
                $this->messageManager->addWarning(__('Payment request has already been approved.'));
            } else {
                $this->processPaymentRequest->execute($paymentRequest);
                $this->messageManager->addSuccessMessage(__('Payment request has been successfully processed.'));
            }

        } catch (\Throwable $e) {
            $message = $e->getMessage();
            if (!empty($message)) {
                $this->messageManager->addErrorMessage($message);
            }
        }
        $resultRedirect = $this->redirectResultFactory->create();
        return $resultRedirect->setPath('mlm/payout/index');
    }
}
