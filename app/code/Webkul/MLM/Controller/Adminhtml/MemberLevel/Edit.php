<?php

/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_MLM
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Controller\Adminhtml\MemberLevel;

use Webkul\MLM\Model\ModuleConstants;

class Edit extends \Webkul\MLM\Controller\Adminhtml\MemberLevel
{
    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');
        $model = $this->memberLevelF->create();
        $pageTitle = __("Add Member Level");
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This member level no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
            $imageMapStr = $model->getBadgeImage();
            $imageUploaderData = $this->filesystemHelper->getBadgeImageUploaderData($imageMapStr);
            $model->setBadgeImage($imageUploaderData);
            $pageTitle = __("Edit %1", $model->getLevelName());
        }
        $this->_coreRegistry->register(ModuleConstants::MEMBER_LEVEL, $model);
        $resultPage = $this->_initAction();
        $resultPage->getConfig()->getTitle()->prepend($pageTitle);
        return $resultPage;
    }
}
