<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Controller\Adminhtml\MemberLevel;

class MassDelete extends \Webkul\MLM\Controller\Adminhtml\MemberLevel
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectF->create();
        $collection     = $this->massActionFilter->getCollection($this->memlevCollF->create());
        $memlevDeleted = 0;
        foreach ($collection->getAllIds() as $memlevId) {
            if (!empty($memlevId)) {
                try {
                    if ($this->validateMemberLevelForDeletion($memlevId)) {
                        $this->memberLevelRepository->deleteById($memlevId);
                        $memlevDeleted++;
                    } else {
                        $model = $this->memberLevelF->create()->load($memlevId);
                        $message = __(
                            'Unable to delete the \'%1.\''.
                            ' Please first un-assign the member level from all the sponsors.',
                            $model->getLevelName()
                        );
                        $this->messageManager->addWarning($message);
                    }
                } catch (\Throwable $exception) {
                    $this->messageManager->addError($exception->getMessage());
                }
            }
        }
        if ($memlevDeleted) {
            $this->messageManager->addSuccess(__("A total of %1 record(s) were deleted.", $memlevDeleted));
        }
        return $resultRedirect->setPath("mlm/memberlevel/index");
    }
}
