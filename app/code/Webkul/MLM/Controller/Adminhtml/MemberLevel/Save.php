<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_MLM
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Controller\Adminhtml\MemberLevel;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Webkul\MLM\Controller\Adminhtml\MemberLevel
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $data['entity_id'];
            $model = $this->memberLevelF->create()->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This member level no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            $badgeImageData = $data['badge_image'];
            $data['badge_image'] =
                $this->filesystemHelper->getBadgeImageMap(
                    $badgeImageData[0]['file'],
                    $badgeImageData[0]['name']
                );
            unset($data['form_key']);
            if (empty($data['entity_id'])) {
                unset($data['entity_id']);
            }
            $model->setData($data);
            
            try {
                $this->memberLevelRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the member level.'));
                $this->dataPersistor->clear('memberlevel');
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e);
            }

            $this->dataPersistor->set('memberlevel', $data);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
