<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Controller\Adminhtml\MemberLevel;

class Delete extends \Webkul\MLM\Controller\Adminhtml\MemberLevel
{
    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('entity_id');
        if ($id) {
            try {
                $model = $this->memberLevelF->create();
                $model->load($id);
                if ($this->validateMemberLevelForDeletion($id)) {
                    $model->delete();
                    $this->messageManager->addSuccessMessage(__('You deleted the Member level.'));
                } else {
                    $message = __(
                        'Unable to delete the \'%1.\''.
                        ' Please first un-assign the member level from all the sponsors.',
                        $model->getLevelName()
                    );
                    $this->messageManager->addError($message);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['entity_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Member level to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
