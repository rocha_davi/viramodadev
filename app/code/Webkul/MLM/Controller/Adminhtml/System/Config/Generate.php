<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Controller\Adminhtml\System\Config;

use Magento\Framework\App\Action\HttpPostActionInterface as HttpGetActionInterface;

class Generate extends \Webkul\MLM\Controller\Adminhtml\Sponsors implements HttpGetActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Webkul_MLM::configuration';

    /**
     * Sponsors grid
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        try {
            $flag = $this->storeConfiguration->generateSponsorIdForAdmin();
            $response = [
                'success' => $flag ? true : false,
                "message" => __("Could not generate new id. Please remove the child sponsors and try again.")
            ];
        } catch (\Exception $e) {
            $response = ["success" => false, 'error' => true, 'message' => $e->getMessage()];
        }

        $this->_actionFlag->set('', self::FLAG_NO_POST_DISPATCH, true);
        return $resultJson->setData($response);
    }
}
