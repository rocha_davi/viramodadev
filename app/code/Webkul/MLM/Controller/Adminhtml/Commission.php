<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Controller\Adminhtml;

use Psr\Log\LoggerInterface;
use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Webkul\MLM\Api\Data\CommissionInterfaceFactory;

abstract class Commission extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Webkul_MLM::managemlm';

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Webkul\MLM\Helper\Data $helper
     * @param \Webkul\MLM\Helper\StoreConfiguration $storeConfiguration
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param CommissionInterfaceFactory $CommissionF
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\Filesystem\Driver\File $fileDriver
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Webkul\MLM\Api\CommissionRepositoryInterface $CommissionRepository
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Webkul\MLM\Helper\Data $helper,
        \Webkul\MLM\Helper\Filesystem $filesystemHelper,
        \Webkul\MLM\Helper\StoreConfiguration $storeConfiguration,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        CommissionInterfaceFactory $commissionF,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\Filesystem\Driver\File $fileDriver,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Backend\Model\View\Result\RedirectFactory $resultRedirectF,
        \Magento\Ui\Component\MassAction\Filter $massActionFilter,
        \Webkul\MLM\Model\ResourceModel\Commission\CollectionFactory $commissionCollF
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->logger = $logger;
        $this->helper = $helper;
        $this->storeConfiguration = $storeConfiguration;
        $this->commissionF = $commissionF;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->fileDriver = $fileDriver;
        $this->filesystem = $filesystem;
        $this->fileUploaderFactory = $fileUploaderFactory;
        $this->dataPersistor = $dataPersistor;
        $this->date = $date;
        $this->storeManager = $storeManager;
        $this->filesystemHelper = $filesystemHelper;
        $this->resultRedirectF = $resultRedirectF;
        $this->massActionFilter = $massActionFilter;
        $this->commissionCollF = $commissionCollF;
        try {
            $this->mediaDirectory = $filesystem->getDirectoryWrite(
                \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
            );
        } catch (\Magento\Framework\Exception\FileSystemException $e) {
            $this->mediaDirectory = null;
        }
        parent::__construct($context);
    }

    /**
     * Check for is allowed.
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_MLM::managemlm');
    }

    /**
     * Init layout, menu and breadcrumb
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        $resultPage = $this->resultPageFactory->create();
        $this->setActiveMenu($resultPage);
        return $this->addBreadCrumbs($resultPage);
    }

    /**
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function setActiveMenu($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE);
        return $resultPage;
    }

    /**
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function addBreadCrumbs($resultPage)
    {
        $resultPage->addBreadcrumb(__('MLM'), __('MLM'));
        $resultPage->addBreadcrumb(__('Manage MLM'), __('Manage MLM'));
        return $resultPage;
    }
}
