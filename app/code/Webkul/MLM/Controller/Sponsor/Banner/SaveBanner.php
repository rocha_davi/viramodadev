<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Controller\Sponsor\Banner;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Customer\Model\AuthenticationInterface;
use Magento\Customer\Model\Customer\Mapper;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\Phrase;
use Magento\Framework\App\Filesystem\DirectoryList;
use Webkul\MLM\Helper\SubmitPaymentRequest;

class SaveBanner extends AbstractAccount implements CsrfAwareActionInterface, HttpPostActionInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var Validator
     */
    protected $formKeyValidator;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var AuthenticationInterface
     */
    private $authentication;

    /**
     * @var Mapper
     */
    private $customerMapper;

    /**
     * @var Sponsor Payment Request
     */
    private $sponsorPaymentRequest;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param Validator $formKeyValidator
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        Validator $formKeyValidator,
        SubmitPaymentRequest $submitPaymentRequest,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Webkul\MLM\Model\SponsorBannerFactory $sponsorConfiguration
    ) {
        parent::__construct($context);
        $this->session = $customerSession;
        $this->customerSession = $customerSession;
        $this->date = $date;
        $this->submitPaymentRequest = $submitPaymentRequest;
        $this->customerRepository = $customerRepository;
        $this->formKeyValidator = $formKeyValidator;
        $this->sponsorHelper = $sponsorHelper;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_filesystem = $filesystem;
        $this->sponsorConfiguration = $sponsorConfiguration;
    }

    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(
        RequestInterface $request
    ): ?InvalidRequestException {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/*/email');

        return new InvalidRequestException(
            $resultRedirect,
            [new Phrase('Invalid Form Key. Please refresh the page.')]
        );
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return null;
    }

    /**
     * Send Referral mail
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $validFormKey = $this->formKeyValidator->validate($this->getRequest());
        $customerId = $this->customerSession->getCustomerId();
        if (!$this->sponsorHelper->isCustomerApprovedSponsor($customerId)) {
            return $this->resultRedirectFactory->create()->setPath(
                '*/*/becomesponsor',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
        if ($validFormKey && $this->getRequest()->isPost()) {
            try {
                $wholeData = $this->getRequest()->getParams();
                if ($wholeData['banner_id'] != null) {
                    $model  = $this->sponsorConfiguration->create();
                    $model->addData($wholeData)->setEntityId($wholeData['banner_id']);
                    $id = $model->save()->getEntityId();
                    $resultLogo = $this->saveLogoFile($id);
                } else {
                    $model  = $this->sponsorConfiguration->create();
                    $model->addData($wholeData);
                    $id = $model->save()->getEntityId();
                    $resultLogo = $this->saveLogoFile($id);
                }
            
                $this->messageManager->addSuccess(__('The Details are saved successfully.'));

                
                
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
  
        }

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/sponsor/banner');
        return $resultRedirect;
    }

    public function saveLogoFile($id)
    {
        try {
            $customerId = $this->customerSession->getCustomerId();
            $files = $this->getRequest()->getFiles()->toArray();
            
            if ($files['banner_image']['error'] != 4) {
                $uploader = $this->_fileUploaderFactory->create(['fileId' => 'banner_image']);
     
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'svg', 'png']);
                
                $uploader->setAllowRenameFiles(false);
                
                $uploader->setFilesDispersion(false);
        
                $path = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)
                
                ->getAbsolutePath('mlm/banner/'.$customerId.'/'.$id.'/');
                
                $resultLogo = $uploader->save($path);
                if ($resultLogo['file']) {
                    $imageData= $resultLogo['file'];
                }
                if ($imageData) {
                    $collection  = $this->sponsorConfiguration->create()->getCollection()
                    ->addFieldToFilter('entity_id', $id)->getFirstItem();
                    $model  = $this->sponsorConfiguration->create();
                    $model->setBannerImage($imageData)
                        ->setEntityId($collection->getEntityId())->save();
                }
            }
            
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
        }
    }
}
