<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Controller\Sponsor;

use Magento\Framework\App\Action\Action;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey\Validator as FormKeyValidator;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\App\RequestInterface;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Backend\Model\Url as BackendUrl;
use Webkul\MLM\Model\Sponsors\Source\Status as SponsorsStatus;

class BecomesponsorPost extends Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $_formKeyValidator;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var  \Webkul\MLM\Model\Sponsors
     */
    protected $sponsorsFactory;

    /**
     * @var CustomerUrl
     */
    protected $customerUrl;

    /**
     * @var BackendUrl
     */
    protected $backendUrl;

    /**
     * StoreConfiguration
     *
     * @var \Webkul\MLM\Helper\StoreConfiguration $storeConfiguration
     */
    protected $storeConfiguration;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param FormKeyValidator $formKeyValidator
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param CustomerUrl $customerUrl
     * @param BackendUrl $backendUrl
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        FormKeyValidator $formKeyValidator,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\MLM\Model\SponsorsFactory $sponsorsFactory,
        \Webkul\MLM\Helper\StoreConfiguration $storeConfiguration,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Webkul\MLM\Helper\Data $dataHelper,
        CustomerUrl $customerUrl,
        BackendUrl $backendUrl,
        \Webkul\MLM\Api\VerifySponsorInterface $verifySponsor,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Webkul\MLM\Helper\AttachSponsorToParent $attachSponsorToParent,
        \Webkul\MLM\Helper\AssignMemberLevelToSponsor $assignMemberLevelToSponsor,
        \Webkul\MLM\Helper\CreateWalletIfNotExist $createWalletIfNotExist
    ) {
        $this->_customerSession = $customerSession;
        $this->_formKeyValidator = $formKeyValidator;
        $this->_date = $date;
        $this->sponsorsFactory = $sponsorsFactory;
        $this->storeConfiguration = $storeConfiguration;
        $this->customerUrl = $customerUrl;
        $this->backendUrl = $backendUrl;
        $this->sponsorHelper = $sponsorHelper;
        $this->dataHelper = $dataHelper;
        $this->verifySponsor = $verifySponsor;
        $this->jsonHelper = $jsonHelper;
        $this->attachSponsorToParent = $attachSponsorToParent;
        $this->assignMemberLevelToSponsor = $assignMemberLevelToSponsor;
        $this->createWalletIfNotExist = $createWalletIfNotExist;
        parent::__construct($context);
    }

    /**
     * Retrieve customer session object.
     *
     * @return \Magento\Customer\Model\Session
     */
    protected function _getSession()
    {
        return $this->_customerSession;
    }

    /**
     * Check customer authentication.
     *
     * @param RequestInterface $request
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->customerUrl->getLoginUrl();

        if (!$this->_customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }

        return parent::dispatch($request);
    }

    /**
     * BecomesponsorPost action.
     *
     * @return \Magento\Framework\Controller\Result\RedirectFactory
     */
    public function execute()
    {
        $hasError = false;
        /**
         * @var \Magento\Framework\Controller\Result\Redirect
         */
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath(
                '*/*/becomesponsor',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
        $customerId = $this->_getSession()->getCustomerId();
        
        if (!$this->getRequest()->isPost()) {
            return $this->resultRedirectFactory->create()->setPath(
                '*/*/becomesponsor',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
        
        try {
            $sponsorId = $this->getRequest()->getParam("sponsorcode");
            $sponsorName = $this->getRequest()->getParam("sponsorname");
            $response = $this->verifyReferenceId();
            $sponsorNameResponse = $this->verifySponsorName();
            if (!$response['success']) {
                $message = __(
                    'O código de referência \'%1\' é inválido.',
                    $sponsorId
                );
                $this->messageManager->addError($message);
                return $this->resultRedirectFactory->create()->setPath(
                    '*/*/becomesponsor',
                    ['_secure' => $this->getRequest()->isSecure()]
                );
            } elseif (!$response['success']) {
                $message = __(
                    'Nome de Consultor \'%1\' é inválido.',
                    $sponsorName
                );
                $this->messageManager->addError($message);
                return $this->resultRedirectFactory->create()->setPath(
                    '*/*/becomesponsor',
                    ['_secure' => $this->getRequest()->isSecure()]
                );
            }
            
            if ($sponsorId && $sponsorName) {
                $collection = $this->sponsorsFactory->create()->getCollection();
                $collection->addFieldToFilter("sponsor_reference_code", $sponsorId);
                $collection->addFieldToFilter("customer_id", $customerId);
                if (!$collection->getSize()) {
                    $sponsorCode = $this->storeConfiguration->generateSponsorsId();
                    $model = $this->sponsorsFactory->create();
                    $parentSponsorId = $this->sponsorHelper->getParentSponsorId($sponsorId);
                    $model->setSponsorCode($sponsorCode);
                    $model->setSponsorReferenceCode($sponsorId);
                    $model->setCustomerId($customerId);
                    $model->setParentSponsorId($parentSponsorId);
                    $model->setSponserName($sponsorName);
                    $model->setStatus(SponsorsStatus::STATUS_DISABLED);
                    $model->save();
                    $sponsorId = $model->getId();
                    $this->attachSponsorToParent->execute($sponsorId);
                    $this->createWalletIfNotExist->execute($sponsorId);
                    $defaultMemberLevelId = $this->dataHelper->getDefaultMemberLevelId();
                    $this->assignMemberLevelToSponsor->execute($sponsorId, $defaultMemberLevelId);
                    $this->messageManager->addSuccess(__(
                        'Pedido enviado com sucesso, aguardando aprovação do Administrador'
                    ));
                }
            }
        } catch (\Throwable $e) {
            $this->messageManager->addError($e->getMessage());
        }

        return $this->resultRedirectFactory->create()->setPath(
            '*/*/becomesponsor',
            ['_secure' => $this->getRequest()->isSecure()]
        );
    }

    /**
     * Check for Existing Sponsor Code
     *
     * @return string
     */
    private function verifyReferenceId()
    {
        $sponsorId = $this->getRequest()->getParam("sponsorcode");
        $sponsorVerifyData = $this->jsonHelper->jsonEncode([
            'sponsorId' => $sponsorId
        ]);
        $response = $this->verifySponsor->verifySponsorId($sponsorVerifyData);
        $response = $this->jsonHelper->jsonDecode($response);
        
        return $response;
    }
    /**
     * Check for Existing Sponsor Name
     *
     * @return string
     */
    private function verifySponsorName()
    {
        $response = [
            "success" => false,
            "message" => __("Nome de Consultor não é válido.")
        ];
        $sponsorName = $this->getRequest()->getParam("sponsorname");
        if (empty($sponsorName)) {
            return $this->jsonHelper->jsonEncode($response);
        }
        $collection = $this->sponsorsFactory->create()->getCollection();
        $collection->addFieldToFilter("sponser_name", $sponsorName);
        if ($collection->getSize() == 0) {
            $response["success"] = true;
            $response["message"] = __("Parabéns! O seu nome de consultor(a) está correto!");
            return $this->jsonHelper->jsonEncode($response);
        }
        
        return $response;
    }
}
