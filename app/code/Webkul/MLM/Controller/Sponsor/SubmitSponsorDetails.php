<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Controller\Sponsor;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Customer\Model\AuthenticationInterface;
use Magento\Customer\Model\Customer\Mapper;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\Phrase;
use Magento\Framework\App\Filesystem\DirectoryList;
use Webkul\MLM\Helper\SubmitPaymentRequest;

class SubmitSponsorDetails extends AbstractAccount implements CsrfAwareActionInterface, HttpPostActionInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var Validator
     */
    protected $formKeyValidator;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var AuthenticationInterface
     */
    private $authentication;

    /**
     * @var Mapper
     */
    private $customerMapper;

    /**
     * @var Sponsor Payment Request
     */
    private $sponsorPaymentRequest;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param Validator $formKeyValidator
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        Validator $formKeyValidator,
        SubmitPaymentRequest $submitPaymentRequest,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Framework\Filesystem $filesystem,
        \Webkul\MLM\Model\SponserConfigurationsFactory $sponsorConfiguration
    ) {
        parent::__construct($context);
        $this->session = $customerSession;
        $this->customerSession = $customerSession;
        $this->date = $date;
        $this->submitPaymentRequest = $submitPaymentRequest;
        $this->customerRepository = $customerRepository;
        $this->formKeyValidator = $formKeyValidator;
        $this->sponsorHelper = $sponsorHelper;
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->_filesystem = $filesystem;
        $this->sponsorConfiguration = $sponsorConfiguration;
    }

    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(
        RequestInterface $request
    ): ?InvalidRequestException {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/*/email');

        return new InvalidRequestException(
            $resultRedirect,
            [new Phrase('Chave de formulário inválida. Por favor atualize a página.')]
        );
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return null;
    }

    /**
     * Send Referral mail
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $validFormKey = $this->formKeyValidator->validate($this->getRequest());
        $customerId = $this->customerSession->getCustomerId();
        if (!$this->sponsorHelper->isCustomerApprovedSponsor($customerId)) {
            return $this->resultRedirectFactory->create()->setPath(
                '*/*/becomesponsor',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
        if ($validFormKey && $this->getRequest()->isPost()) {
            try {
                $wholeData = $this->getRequest()->getParams();
               
                $resultLogo = $this->saveLogoFile();
                $imageWidth = $imageHeight = 0;
                $imageData = '';
                $productsIds;
                if (isset($wholeData['logo_width'])) {
                    $imageWidth =  $wholeData['logo_width'];
                }
                if (isset($wholeData['logo_height'])) {
                    $imageHeight =  $wholeData['logo_height'];
                }
                if (isset($wholeData['product_selection'])) {
                    $productsIds = json_encode($wholeData['product_selection']);
                }
                if ($resultLogo['file']) {
                    $imageData = $resultLogo['file'];
                }
                $modelData  = $this->sponsorConfiguration->create()->getCollection()
                ->addFieldToFilter('sponsor_id', $customerId)->getFirstItem();
                
                if ($modelData->getEntityId()) {
                    
                    $model  = $this->sponsorConfiguration->create();
                    if ($imageData != null) {
                        $model->setLogoImage($imageData);
                    }
                    if ($productsIds != null) {
                        $model->setProductIds($productsIds);
                    }
                    $model
                    ->setLogoWidth($imageWidth)
                    ->setLogoHeight($imageHeight)
                    ->setEntityId($modelData->getEntityId())
                    ->save();
                    $this->messageManager->addSuccess(__('Os detalhes foram atualizados com sucesso.'));

                } else {
                    $model  = $this->sponsorConfiguration->create();
                    if ($imageData != null) {
                        $model->setLogoImage($imageData);
                    }
                    if ($productsIds != null) {
                        $model->setProductIds($productsIds);
                    }
                    $model->setSponsorId($customerId);
                    
                    $model->setLogoWidth($imageWidth);
                    $model->setLogoHeight($imageHeight);
                    
                    $model->save();
                    $this->messageManager->addSuccess(__('Os detalhes foram salvos com sucesso.'));

                }
                
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
  
        }

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/*/sponsordetails');
        return $resultRedirect;
    }

    public function saveLogoFile()
    {
        try {
            $customerId = $this->customerSession->getCustomerId();
            $files = $this->getRequest()->getFiles()->toArray();
            // print_r($files);exit;
            if ($files['logo_file']['error'] != 4) {
                $uploader = $this->_fileUploaderFactory->create(['fileId' => 'logo_file']);
     
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'svg', 'png']);
                
                $uploader->setAllowRenameFiles(false);
                
                $uploader->setFilesDispersion(false);
        
                $path = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)
                
                ->getAbsolutePath('mlm/'.$customerId.'/');
                
                $resultlogo = $uploader->save($path);
                return $resultlogo;
            }
            
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
        }
    }
}
