<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Controller\Sponsor;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Customer\Model\AuthenticationInterface;
use Magento\Customer\Model\Customer\Mapper;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\Phrase;
use Webkul\MLM\Helper\SubmitPaymentRequest;

class Submitrequest extends AbstractAccount implements CsrfAwareActionInterface, HttpPostActionInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var Validator
     */
    protected $formKeyValidator;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var AuthenticationInterface
     */
    private $authentication;

    /**
     * @var Mapper
     */
    private $customerMapper;

    /**
     * @var Sponsor Payment Request
     */
    private $sponsorPaymentRequest;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param Validator $formKeyValidator
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        Validator $formKeyValidator,
        SubmitPaymentRequest $submitPaymentRequest,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper
    ) {
        parent::__construct($context);
        $this->session = $customerSession;
        $this->customerSession = $customerSession;
        $this->date = $date;
        $this->submitPaymentRequest = $submitPaymentRequest;
        $this->customerRepository = $customerRepository;
        $this->formKeyValidator = $formKeyValidator;
        $this->sponsorHelper = $sponsorHelper;
    }

    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(
        RequestInterface $request
    ): ?InvalidRequestException {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/*/email');

        return new InvalidRequestException(
            $resultRedirect,
            [new Phrase('Chave de formulário inválida. Atualize a página.')]
        );
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return null;
    }

    /**
     * Send Referral mail
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $validFormKey = $this->formKeyValidator->validate($this->getRequest());
        $customerId = $this->customerSession->getCustomerId();
        if (!$this->sponsorHelper->isCustomerApprovedSponsor($customerId)) {
            return $this->resultRedirectFactory->create()->setPath(
                '*/*/becomesponsor',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
        if ($validFormKey && $this->getRequest()->isPost()) {
            try {
                $wholeData = $this->getRequest()->getParams();
                if (isset(
                    $wholeData["requestedamount"]
                ) && isset(
                    $wholeData["description"]
                ) && $wholeData["requestedamount"] != ""
                    && $wholeData["description"] != "") {
                        $wholeData["commission_rate"] = 0;
                        $wholeData["commission_percent"] = 0;
                        $wholeData["amount"] = $wholeData["requestedamount"];
                    $sponsorId = $wholeData['sponsor_id'];
                    $paymentMethodTitle = $this->sponsorHelper->getPaymentMethodTitle($sponsorId);
                    if (!$paymentMethodTitle) {
                        $this->messageManager->addError(
                            __("Defina um método de pagamento padrão para o processamento de pagamentos.")
                        );
                        return $resultRedirect->setPath('*/*/payment');
                    }
                    $amount = $wholeData["amount"];
                    $walletBalance = $this->sponsorHelper
                        ->getWalletBalance($wholeData['sponsor_id']);
                    if ($amount > $walletBalance) {
                        $this->messageManager->addError(__("Saldo de carteira insuficiente."));
                        return $resultRedirect->setPath('*/*/paymentrequest');
                    }
                    $this->submitPaymentRequest->execute($wholeData);
                    $this->messageManager->addSuccess(__('O pedido de pagamento foi enviado!'));
                } else {
                    $this->messageManager->addError(__("Por favor, preencha todos os campos obrigatórios."));
                    return $resultRedirect->setPath('*/*/paymentrequest');
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Não podemos enviar pedidos.'));
                return $resultRedirect->setPath('*/*/paymentrequest');
            }

            $this->session->setCustomerFormData($this->getRequest()->getPostValue());
        }

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/*/wallet');
        return $resultRedirect;
    }

    /**
     * Get customer data object
     *
     * @param int $customerId
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    private function getCustomerDataObject($customerId)
    {
        return $this->customerRepository->getById($customerId);
    }
}
