<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Controller\Sponsor;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Webkul\MLM\Helper\Data as MLMDataHelper;

/**
 * Webkul MLM Sponsor codeverify controller.
 */
class Codeverify extends Action
{
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_jsonHelper;

    /**
     * @var \Webkul\MLM\Model\ResourceModel\Sponsors\CollectionFactory
     */
    protected $_sponsorsCollectionFactory;

    /**
     * StoreConfiguration
     *
     * @var \Webkul\MLM\Helper\StoreConfiguration $storeConfiguration
     */
    protected $storeConfiguration;

    /**
     * SponsorReferenceManagement
     *
     * @var \Webkul\MLM\Model\SponsorReferenceManagement $sponsorReferenceManagement
     */
    protected $sponsorReferenceManagement;

    /**
     * Initialize dependencies
     *
     * @param Context $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Webkul\MLM\Model\ResourceModel\Sponsors\CollectionFactory $sponsorsCollectionFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Webkul\MLM\Helper\StoreConfiguration $storeConfiguration,
        \Webkul\MLM\Model\SponsorReferenceManagement $sponsorReferenceManagement,
        \Webkul\MLM\Model\ResourceModel\Sponsors\CollectionFactory $sponsorsCollectionFactory
    ) {
        $this->_jsonHelper = $jsonHelper;
        $this->storeConfiguration = $storeConfiguration;
        $this->sponsorReferenceManagement = $sponsorReferenceManagement;
        $this->_sponsorsCollectionFactory = $sponsorsCollectionFactory;
        parent::__construct($context);
    }

    /**
     * Verify sponsoe Code exists or not
     *
     * @return \Magento\Framework\App\Response\Http
     */
    public function execute()
    {
        $sponsorcode = trim($this->getRequest()->getParam("sponsorcode", ""));
        $data["sponsorId"] = $sponsorcode;
        $response = $this->sponsorReferenceManagement->verifySponsorId(
            $this->_jsonHelper->jsonEncode($data)
        );
        $returnedData = $this->_jsonHelper->jsonDecode($response);
        if ($returnedData["success"]) {
            $this->getResponse()->representJson($this->_jsonHelper->jsonEncode(0));
        } else {
            $this->getResponse()->representJson($this->_jsonHelper->jsonEncode(1));
        }
    }
}
