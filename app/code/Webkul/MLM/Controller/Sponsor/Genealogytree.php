<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Controller\Sponsor;

class Genealogytree extends Referral
{
    /**
     * Sponsors Genealogy Tree
     *
     * @return object
     */
    public function execute()
    {
        $customerId = $this->customerSession->getCustomerId();
        if (!$this->sponsorHelper->isCustomerApprovedSponsor($customerId)) {
            return $this->resultRedirectFactory->create()->setPath(
                '*/*/becomesponsor',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Árvore Genealógica'));
        return $resultPage;
    }
}
