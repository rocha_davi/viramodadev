<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MLM\Controller\Sponsor;

use Webkul\MLM\Helper\StoreConfiguration;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;

class Paymentrequest extends AbstractAccount implements HttpGetActionInterface
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var StoreConfiguration
     */
    protected $storeConfiguration;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        StoreConfiguration $storeConfiguration,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->storeConfiguration = $storeConfiguration;
        $this->customerSession = $customerSession;
        $this->sponsorHelper = $sponsorHelper;
    }

    /**
     * Sponsors Dashboard
     *
     * @return object
     */
    public function execute()
    {
        $customerId = $this->customerSession->getCustomerId();
        if (!$this->sponsorHelper->isCustomerApprovedSponsor($customerId)) {
            return $this->resultRedirectFactory->create()->setPath(
                '*/*/becomesponsor',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Solicitação de Retirada'));
        return $resultPage;
    }
}
