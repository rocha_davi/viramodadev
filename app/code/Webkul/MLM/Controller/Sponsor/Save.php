<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MLM
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MLM\Controller\Sponsor;

use Webkul\MLM\Model\EmailNotification;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Customer\Model\AuthenticationInterface;
use Magento\Customer\Model\Customer\Mapper;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Controller\AbstractAccount;
use Magento\Framework\Phrase;

class Save extends AbstractAccount implements CsrfAwareActionInterface, HttpPostActionInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var Validator
     */
    protected $formKeyValidator;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var AuthenticationInterface
     */
    private $authentication;

    /**
     * @var Mapper
     */
    private $customerMapper;

    /**
     * @var EmailNotification
     */
    private $emailNotification;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param Validator $formKeyValidator
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        Validator $formKeyValidator,
        EmailNotification $emailNotification,
        \Webkul\MLM\Helper\Sponsor $sponsorHelper
    ) {
        parent::__construct($context);
        $this->session = $customerSession;
        $this->customerSession = $customerSession;
        $this->emailNotification = $emailNotification;
        $this->customerRepository = $customerRepository;
        $this->formKeyValidator = $formKeyValidator;
        $this->sponsorHelper = $sponsorHelper;
    }

    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(
        RequestInterface $request
    ): ?InvalidRequestException {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/*/email');

        return new InvalidRequestException(
            $resultRedirect,
            [new Phrase('Chave de formulário inválida. Atualize a página.')]
        );
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return null;
    }

    /**
     * Send Referral mail
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $customerId = $this->customerSession->getCustomerId();
        if (!$this->sponsorHelper->isCustomerApprovedSponsor($customerId)) {
            return $this->resultRedirectFactory->create()->setPath(
                '*/*/becomesponsor',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $validFormKey = $this->formKeyValidator->validate($this->getRequest());
        
        if ($validFormKey && $this->getRequest()->isPost()) {
            try {
                $wholeData = $this->getRequest()->getParams();
                
                $email = $wholeData["email"];
                $subject = $wholeData["subject"];
                $body = $wholeData["body"];
                $sponsorId = $wholeData["sponsorId"];
                $this->emailNotification->sendEmail(
                    $email,
                    $subject,
                    $body,
                    $sponsorId
                );
                $this->messageManager->addSuccess(__('E-mail enviado!.'));
                return $resultRedirect->setPath('*/*/email');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Não podemos enviar referências.'));
            }

            $this->session->setCustomerFormData($this->getRequest()->getPostValue());
        }

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/*/email');
        return $resultRedirect;
    }

    /**
     * Get customer data object
     *
     * @param int $customerId
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    private function getCustomerDataObject($customerId)
    {
        return $this->customerRepository->getById($customerId);
    }
}
