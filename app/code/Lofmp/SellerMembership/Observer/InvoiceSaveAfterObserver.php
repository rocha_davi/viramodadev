<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_SellerMembership
 * @copyright  Copyright (c) 2018 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lofmp\SellerMembership\Observer;

use Magento\Framework\Event\ObserverInterface;
use Lof\MarketPlace\Model\Seller;

class InvoiceSaveAfterObserver implements ObserverInterface
{

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $_customerFactory;

    /**
     * @var \Lofmp\Vendors\Model\VendorFactory
     */
    protected $_sellerFactory;
    
    /**
     * @var \Lofmp\SellerMembership\Model\TransactionFactory
     */
    protected $_transactionFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;
    /**
     * @var \Lofmp\SellerMembership\Model\Membership
     */
    protected $membership;

    protected $helper;

    public function __construct(
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Lof\MarketPlace\Model\SellerFactory $sellerFactory,
        \Lofmp\SellerMembership\Model\Membership $membership,
        \Lofmp\SellerMembership\Model\TransactionFactory $transactionFactory,
        \Lof\MarketPlace\Helper\Data $helper,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        $this->helper = $helper;
        $this->membership = $membership;
        $this->_transactionFactory = $transactionFactory;
        $this->_sellerFactory = $sellerFactory;
        $this->_customerFactory = $customerFactory;
        $this->_date = $date;
    }

    /**
     * Add the notification if there are any seller awaiting for approval. 
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $invoice = $observer->getInvoice();
        $order = $invoice->getOrder();
       
        /*Return if the invoice is not paid*/
        if ($invoice->getState() != \Magento\Sales\Model\Order\Invoice::STATE_PAID) {
            return;
        }

        /*Buy membership package*/
        $this->processBuyMembership($invoice);
    }

    /**
     * Process buy membership transaction.
     *
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     */
    public function processBuyMembership(\Magento\Sales\Model\Order\Invoice $invoice)
    {
        $order = $invoice->getOrder();
       
        $customerId = $order->getCustomerId();
       
        if (!$customerId) {
            return;
        }

        $customer = $this->_customerFactory->create();
        $customer->load($customerId);

        if (!$customer->getId()) {
            return;
        }
       
        $seller = $this->_sellerFactory->create();
        $seller->load($customer->getId(),'customer_id');
         
        if (!$seller->getId()) {
            return;
        }
        
        /*Return if the transaction for the invoice is already exist.*/
        $trans = $this->_transactionFactory->create()->getCollection()
            ->addFieldToFilter(
                'additional_info',
                ['like' => 'invoice|'.$invoice->getId()]
            );
       
        if ($trans->count()) {
            return;
        }

        foreach ($invoice->getAllItems() as $item) {
            $orderItem = $item->getOrderItem();
            if ($orderItem->getParentItemId()) {
                continue;
            }
           
            if ($orderItem->getProductType() != 'seller_membership') {
                continue;
            }

            $product = $orderItem->getProduct();
            if (!$product) {
                continue;
            }
 
            $membershipOption = $orderItem->getProductOptions();
            
            if (!is_array($membershipOption)) {
                $membershipOption = unserialize($membershipOption);
            }
            $membershipOption = $membershipOption['info_buyRequest']['membership'];
            $membershipOption = explode("|",$membershipOption['duration']);
            $relatedGroupId = $product->getData('seller_group');
            $duration = $membershipOption[0];
            $durationUnit = $membershipOption[1];
            $price = $membershipOption[2];

            if (!$relatedGroupId || !$duration || !$durationUnit) {
                continue;
            }

            $time = '';
            $duration = $duration * $item->getQty();

            switch ($durationUnit) {
                case 'day':
                    $time = "+$duration days";
                    break;
                case 'week':
                    $duration = $duration * 7;
                    $time = "+$duration days";
                    break;
                case 'month':
                    $time = "+$duration months";
                    break;
                case 'year':
                    $time = "+$duration years";
                    break;
            }
            
            $membership = $this->membership->getCollection()->addFieldToFilter('seller_id',$seller->getId())->getFirstItem();
           
            if ($seller->getGroupId() == $relatedGroupId) {
                /*Renew the current package*/
                $currentTime = $membership->getData('expiration_date');
                if (!$currentTime) {
                    $currentTime = $this->_date->date();
                }
            } else {
                /*Upgrade to new package*/
                $currentTime = $this->_date->date();
            }
            $expiryTime = strtotime($currentTime.$time);
            $date = date('Y-m-d h:i:s A', $expiryTime);
       
            $membership->setGroupId($relatedGroupId);
            $membership->setExpirationDate($date);
            $membership->setStatus(1);
            $membership->setDuration($duration.' '.$durationUnit);
            $membership->setPrice($price);
            $membership->setName($item->getName());
            $membership->save();

            $trans = $this->_transactionFactory->create();
            $trans->setData([
                'seller_id' => $seller->getId(),
                'package' => $item->getName(),
                'amount' => $item->getBaseRowTotal(),
                'duration' => $duration,
                'duration_unit' => $durationUnit,
                'additional_info' => 'invoice|'.$invoice->getId().'||item|'.$item->getId(),
                'created_at' => $this->_date->timestamp(),
            ]);
            $trans->save();

            $seller->setGroupId($relatedGroupId);
            $seller->save();
        }
    }
}
