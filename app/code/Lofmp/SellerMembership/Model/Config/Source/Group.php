<?php
/**
 * Venustheme
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Venustheme.com license that is
 * available through the world-wide-web at this URL:
 * http://www.venustheme.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Venustheme
 * @package    Lofmp_SellerMembership
 * @copyright  Copyright (c) 2016 Venustheme (http://www.venustheme.com/)
 * @license    http://www.venustheme.com/LICENSE-1.0.html
 */
namespace Lofmp\SellerMembership\Model\Config\Source;

class Group extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    protected $group;

    /**
     * @param \Lofmp\SellerMembership\Model\Category
     */
    public function __construct(
      \Lof\MarketPlace\Model\Group $group
      ){
        $this->group = $group;
    }

    public function getAllOptions()
    {
        $options = array();
        foreach ($this->group->getCollection()->addFieldToFilter('status', '1') as $key => $group) {
            $options[] = [
                'label' => $group->getData('name'),
                'value' => $group->getData('group_id')
            ];
        } 
        return $options;
    }
}