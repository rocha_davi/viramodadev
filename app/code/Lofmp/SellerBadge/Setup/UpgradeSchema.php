<?php declare(strict_types=1);
namespace Lofmp\SellerBadge\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (version_compare($context->getVersion(), '0.0.17', '<')) {
            $installer->getConnection()->addColumn(
                $installer->getTable('lofmp_sellerbadge_badge'),
                'actions_serialized',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'size' => '2M',
                    'nullable' => false,
                    'default' => '',
                    'comment' => 'Actions',
                ]
            );
        }
        $installer->endSetup();
    }
}
