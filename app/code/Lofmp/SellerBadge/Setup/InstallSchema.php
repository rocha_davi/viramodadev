<?php

namespace Lofmp\SellerBadge\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Create table 'badge'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('lofmp_sellerbadge_badge'))
            ->addColumn('sb_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'SellerBadge id')
            ->addColumn('sb_created_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT], 'Created at')
            ->addColumn('sb_updated_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE], 'Updated at')
            ->addColumn('sb_name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Name')
            ->addColumn('sb_description', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'Description')
            ->addColumn('sb_image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 200, [], 'Image Badge')
            ->addColumn('sb_rank', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 8, [], 'Rank')
            ->addColumn('sb_status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, [], 'Status');
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'seller badge manage'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('lofmp_sellerbadge_seller_badge_manager'))
            ->addColumn('sbm_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true], 'SellerBadge id')
            ->addColumn('sbm_created_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT], 'Created at')
            ->addColumn('sbm_updated_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE], 'Updated at')
            ->addColumn('sbm_seller_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 8, [], 'Seller ID')
            ->addColumn('sbm_badge_id', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 8, [], 'Badge ID')
            ->addColumn('sbm_name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Seller Name')
            ->addColumn('sbm_email', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, [], 'Seller Email')
            ->addColumn(
                'actions_serialized',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '2M',
                ['nullable' => false],
                'Actions'
            )
            ->addColumn('sbm_status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, [], 'Seller Status');
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
