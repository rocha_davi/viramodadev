<?php

namespace Lofmp\SellerBadge\Controller\Adminhtml\Badge;

class ProcessImport extends \Lofmp\SellerBadge\Controller\Adminhtml\Badge
{
    public function execute()
    {
        try {
            $desFolder = $this->_dir->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR);
            $uploader = $this->_uploaderFactory->create(array('fileId' => 'import_file'));
            $uploader->setAllowRenameFiles(true);
            $uploader->setAllowedExtensions(['csv', 'txt']);
            $uploader->setFilesDispersion(true);
            $uploader->setAllowCreateFolders(true);
            $result = $uploader->save($desFolder);
            $fullPath = $result['path'].$result['file'];

            $importHandler = $this->_objectManager->create('Lofmp\SellerBadge\Model\Badge\ImportHandler');
            $count = $importHandler->importFromCsvFile($fullPath);
            $this->messageManager->addSuccess(__('Csv file has been imported : %1 rows processed', $count));
        }
        catch(\Exception $ex) {
            $this->messageManager->addError($ex->getMessage());
        }
        $this->_redirect('*/*/index');
    }
}
