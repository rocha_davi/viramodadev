<?php

namespace Lofmp\SellerBadge\Controller\Adminhtml\Badge;

class PosGrid extends \Lofmp\SellerBadge\Controller\Adminhtml\Badge
{
    public function execute()
    {
        $sb_id = $this->getRequest()->getParam('sb_id');
        $model = $this->_badgeFactory->create();
        $model->load($sb_id);
        $this->_coreRegistry->register('current_badge', $model);
        $resultLayout = $this->_resultLayoutFactory->create();
        return $resultLayout;
    }
}
