<?php

namespace Lofmp\SellerBadge\Controller\Adminhtml\Badge;

class Edit extends \Lofmp\SellerBadge\Controller\Adminhtml\Badge
{
    public function execute()
    {
        $sb_id = $this->getRequest()->getParam('sb_id');
        $model = $this->_badgeFactory->create();
        if ($sb_id) {
            $model->load($sb_id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This badge no longer exists.'));
                $this->_redirect('adminhtml/*/');
                return;
            }
        }
        else {
            //$model->badgeDefaultData();
        }
        $this->_coreRegistry->register('current_badge', $model);
        if (isset($sb_id)) {
            $breadcrumb = __('Edit Badge');
        } else {
            $breadcrumb = __('New Badge');
        }
        $this->_initAction()->_addBreadcrumb($breadcrumb, $breadcrumb);
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Badges'));
        $this->_view->getPage()->getConfig()->getTitle()->prepend($model->getId() ? __("Edit Badge '%1'", $model->getSbName()) : __('New Badge'));
        $this->_view->renderLayout();
    }
}
