<?php

namespace Lofmp\SellerBadge\Controller\Adminhtml\Badge;

class Import extends \Lofmp\SellerBadge\Controller\Adminhtml\Badge
{
    public function execute()
    {
        $this->_initAction();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Import badges'));
        $this->_view->renderLayout();
    }
}
