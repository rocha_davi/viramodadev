<?php

namespace Lofmp\SellerBadge\Controller\Adminhtml\Badge;

class Delete extends \Lofmp\SellerBadge\Controller\Adminhtml\Badge
{
    public function execute()
    {
        if ($sb_id = $this->getRequest()->getParam('sb_id')) {
            try {
                $model = $this->_badgeFactory->create();
                $model->setId($sb_id);
                $model->delete();
                $this->messageManager->addSuccess(__('You deleted the badge.'));
                $this->_redirect('*/*/index');
                return;
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_redirect('adminhtml/*/edit', ['sb_id' => $sb_id]);
                return;
            }
        }
        $this->messageManager->addError(__('We can\'t find the badge to delete.'));
        $this->_redirect('*/*/index');
    }
}
