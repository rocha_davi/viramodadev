<?php

namespace Lofmp\SellerBadge\Controller\Adminhtml\Badge;

class Index extends \Lofmp\SellerBadge\Controller\Adminhtml\Badge
{
    public function execute()
    {
        $this->_initAction();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('Badges'));
        $this->_view->renderLayout();
    }
}
