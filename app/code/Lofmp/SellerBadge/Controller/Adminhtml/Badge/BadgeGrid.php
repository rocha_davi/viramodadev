<?php

namespace Lofmp\SellerBadge\Controller\Adminhtml\Badge;

class BadgeGrid extends \Lofmp\SellerBadge\Controller\Adminhtml\Badge
{
    public function execute()
    {
        $this->_view->loadLayout(true);
        $this->_view->renderLayout();
    }
}
