<?php

namespace Lofmp\SellerBadge\Controller\Adminhtml\Badge;

class NewAction extends \Lofmp\SellerBadge\Controller\Adminhtml\Badge
{
    public function execute()
    {
        $this->_redirect('*/*/Edit');
    }
}
