<?php

namespace Lofmp\SellerBadge\Controller\Adminhtml\Badge;

class Save extends \Magento\Backend\App\Action
{
    protected $_coreRegistry;
    protected $_badgeFactory;
    protected $_uploaderFactory;
    protected $_resultLayoutFactory;
    protected $_dir;
    protected $_objectManager;
    protected $_stdTimezone;
    protected $_filesystem;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\User\Model\UserFactory $userFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Lofmp\SellerBadge\Model\BadgeFactory $badgeFactory,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Framework\File\UploaderFactory $uploaderFactory,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Framework\Stdlib\DateTime\Timezone $stdTimezone,
        \Magento\Framework\Filesystem $filesystem
    ) {

        $this->_objectManager = $context->getObjectManager();
        parent::__construct($context);
        $this->_stdTimezone = $stdTimezone;
        $this->_filesystem = $filesystem;
        

        $this->_coreRegistry = $coreRegistry;
        $this->_badgeFactory = $badgeFactory;
        $this->_resultLayoutFactory = $resultLayoutFactory;
        $this->_dir = $dir;
        $this->_uploaderFactory = $uploaderFactory;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Lof_MarketPlace::Badge_save');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $dateTimeNow = $this->_stdTimezone->date()->format('Y-m-d H:i:s');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_objectManager->create('Lofmp\SellerBadge\Model\Badge');

            $id = $this->getRequest()->getParam('sb_id');

            if ($id) {
                $model->load($id);
            }

             /** @var \Magento\Framework\Filesystem\Directory\Read $mediaDirectory */
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
            $mediaDirectory = $fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
            
            $mediaFolder = 'lofmp/sellerbadge/' ;
            $path = $mediaDirectory->getAbsolutePath($mediaFolder);

            // Delete, Upload Image
            $imagePath = $mediaDirectory->getAbsolutePath($model->getImage());

            if(isset($data['sb_image']['delete']) && file_exists($imagePath)){
                unlink($imagePath);
                $data['sb_image'] = '';
            }
            if(isset($data['sb_image']) && is_array($data['sb_image'])){
                unset($data['sb_image']);
            }

             if (isset($data['rule']['actions'])) {
                $data['actions'] = $data['rule']['actions'];
            } 
            if (isset($data['rule']['conditions'])) {
                $data['conditions'] = $data['rule']['conditions'];
            } 
            unset($data['rule']); 
            
            if($image = $this->uploadImage('sb_image')){
                $data['sb_image'] = $image;
            }
            $data['sb_created_at'] = $dateTimeNow;
            // init model and set data
            $model->loadPost($data);
           
            try {
                $model->save();
            
                $this->messageManager->addSuccess(__('You saved this Badge.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['sb_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Badge.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['sb_id' => $this->getRequest()->getParam('sb_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
     public function uploadImage($fieldId = 'image')
    {
        $resultRedirect = $this->resultRedirectFactory->create();


        if (isset($_FILES[$fieldId]) && $_FILES[$fieldId]['name']!='') 
        {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $uploader = $objectManager->create('Magento\Framework\File\Uploader',array('fileId' => $fieldId));
            
            $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
            $mediaDirectory = $fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);

            $mediaFolder = 'lof/sellerbadge/';
            try {
                $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png', 'svg')); 
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);
                $result = $uploader->save($mediaDirectory->getAbsolutePath($mediaFolder)
                    );
                return $mediaFolder.$result['name'];
            } catch (\Exception $e) {
                $this->_logger->critical($e);
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['sb_id' => $this->getRequest()->getParam('sb_id')]);
            }
        }
        return;
    }
}