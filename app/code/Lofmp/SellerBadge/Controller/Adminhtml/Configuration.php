<?php

namespace Lofmp\SellerBadge\Controller\Adminhtml;

abstract class Configuration extends \Magento\Backend\App\AbstractAction
{
    public function execute()
    {
        $this->_redirect('adminhtml/system_config/edit', ['section' => 'sellerbadge']);
    }
    protected function _isAllowed()
    {
        return true;
    }
}
