<?php

namespace Lofmp\SellerBadge\Controller\Adminhtml;

abstract class SellerBadge extends \Magento\Backend\App\AbstractAction
{
    protected $_coreRegistry;
    protected $_resultPageFactory;
    protected $_badgeFactory;
    protected $_sellerBadgeFactory;

    /**
     * [__construct]
     * @param \Magento\Backend\App\Action\Context          $context            
     * @param \Magento\Framework\Registry                  $coreRegistry       
     * @param \Magento\Framework\View\Result\PageFactory   $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Lofmp\SellerBadge\Model\SellerBadgeFactory $sellerBadgeFactory,
        \Lofmp\SellerBadge\Model\BadgeFactory $badgeFactory
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_badgeFactory = $badgeFactory;
        $this->_sellerBadgeFactory = $sellerBadgeFactory;
    }

    protected function _initAction()
    {
        $this->_view->loadLayout();
        return $this;
    }

    protected function _isAllowed()
    {
        return true;
    }
}
