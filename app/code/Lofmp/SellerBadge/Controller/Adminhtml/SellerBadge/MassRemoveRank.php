<?php

namespace Lofmp\SellerBadge\Controller\Adminhtml\SellerBadge;

class MassRemoveRank extends \Lofmp\SellerBadge\Controller\Adminhtml\SellerBadge
{
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        $arrSbmId = $data['sbm_id'];
        $mode = $data['mode'];

        // - get array sbm_id by sbm_seller_id
        $badge = $this->_badgeFactory->create();
        $exSbmId = implode(',', $arrSbmId);

        // - remove all rank by array seller id
        $listSellerId = $badge->getListSellerId($exSbmId);
        $exSellerId = implode(',', $listSellerId);

        if ($mode == 'remove_all') {
            $badge->removeRankBySellerId($exSellerId);
        } else {
            $badgeId = $data['mode'];
            $badge->removeRank($badgeId, $exSellerId);
        }

        $this->messageManager->addSuccess(__('Rank removed from seller.'));
        $this->_redirect('sellerbadge/sellerbadge/index');

    }

}
