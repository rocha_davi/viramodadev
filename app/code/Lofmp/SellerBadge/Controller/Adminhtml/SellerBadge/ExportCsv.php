<?php 
namespace Lofmp\SellerBadge\Controller\Adminhtml\SellerBadge;

use Magento\Framework\App\Filesystem\DirectoryList;

class ExportCsv extends \Lofmp\SellerBadge\Controller\Adminhtml\SellerBadge {

    public function execute(){
        
        try{

            $csv = $this->_view->getLayout()->createBlock('\Lofmp\SellerBadge\Block\SellerBadge\Grid')->getCsv();

            $date = $this->_objectManager->create('\Magento\Framework\Stdlib\DateTime\DateTime')->date('Y-m-d_H-i-s');

            return $this->_objectManager->get('\Magento\Framework\App\Response\Http\FileFactory')->create(
                'lofmp_sellerbadge_' . $date . '.csv',
                $csv,
                DirectoryList::VAR_DIR,
                'application/csv'
            );

        }catch(\Exception $e){
            $this->messageManager->addError(__('An error occurred : '.$e->getMessage()));
            $this->_redirect('*/*/index');
        }

    }

}