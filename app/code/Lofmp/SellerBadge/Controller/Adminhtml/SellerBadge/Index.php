<?php

namespace Lofmp\SellerBadge\Controller\Adminhtml\SellerBadge;

class Index extends \Lofmp\SellerBadge\Controller\Adminhtml\SellerBadge
{
    /**
     * @return void
     */
    public function execute()
    {
        $this->_initAction();
        $this->_view->getPage()->getConfig()->getTitle()->prepend(__('SellerBadge Manager'));
        $this->_view->renderLayout();
    }
}
