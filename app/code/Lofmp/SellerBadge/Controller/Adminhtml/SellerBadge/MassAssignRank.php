<?php

namespace Lofmp\SellerBadge\Controller\Adminhtml\SellerBadge;

class MassAssignRank extends \Lofmp\SellerBadge\Controller\Adminhtml\SellerBadge
{
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        $arrSbmId = $data['sbm_id'];
        $mode = $data['mode'];
        $badge = $this->_badgeFactory->create();
        // get list seller
        $exSbmId = implode(',', $arrSbmId);
        $listSeller = $badge->getListSeller($exSbmId);

        // get list badgeId
        $arrSbmBadgeId = $badge->getListBadgeId();
        if ($mode == 'assign_all') {
            $results = array();
            foreach( $arrSbmBadgeId as $sbmBadgeId ){
                foreach($listSeller as $seller){
                    $tmp = array(
                        'sbm_name' => $seller['sbm_name'],
                        'sbm_email' => $seller['sbm_email'],
                        'sbm_status' => 1,
                        'sbm_badge_id' => $sbmBadgeId['value'],
                        'sbm_seller_id' => $seller['sbm_seller_id']
                    );
                    array_push($results,$tmp);
                }
            }
            $badge->assignAllRank($results);
        } else {
            $results = array();
            $badgeId = $data['mode'];
            foreach($listSeller as $seller){
                $tmp = array(
                    'sbm_name' => $seller['sbm_name'],
                    'sbm_email' => $seller['sbm_email'],
                    'sbm_status' => 1,
                    'sbm_badge_id' => $badgeId,
                    'sbm_seller_id' => $seller['sbm_seller_id']
                );
                array_push($results,$tmp);
            }
            $badge->assignRank($results);
        }

        $this->messageManager->addSuccess(__('Rank associated to seller.'));
        $this->_redirect('sellerbadge/sellerbadge/index');

    }

}
