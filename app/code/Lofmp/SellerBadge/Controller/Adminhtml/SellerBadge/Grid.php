<?php

namespace Lofmp\SellerBadge\Controller\Adminhtml\SellerBadge;

class Grid extends \Lofmp\SellerBadge\Controller\Adminhtml\SellerBadge
{
    /**
     * @return void
     */
    public function execute()
    {
        $resultLayout = $this->_resultLayoutFactory->create();
        $block = $resultLayout->getLayout()->getBlock('sellerbadge_sellerbadge_grid_container');
        $block->setUseAjax(true);
        return $resultLayout;

    }
}
