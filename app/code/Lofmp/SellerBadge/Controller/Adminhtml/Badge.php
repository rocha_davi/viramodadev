<?php
namespace Lofmp\SellerBadge\Controller\Adminhtml;
abstract class Badge extends \Magento\Backend\App\AbstractAction
{
    protected $_coreRegistry;
    protected $_badgeFactory;
    protected $_uploaderFactory;
    protected $_resultLayoutFactory;
    protected $_dir;
    protected $_objectManager;
    protected $_stdTimezone;
    protected $_filesystem;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\User\Model\UserFactory $userFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Lofmp\SellerBadge\Model\BadgeFactory $badgeFactory,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Framework\File\UploaderFactory $uploaderFactory,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Framework\Stdlib\DateTime\Timezone $stdTimezone,
        \Magento\Framework\Filesystem $filesystem
    ) {

        $this->_objectManager = $context->getObjectManager();
        parent::__construct($context);
        $this->_stdTimezone = $stdTimezone;
        $this->_filesystem = $filesystem;
        

        $this->_coreRegistry = $coreRegistry;
        $this->_badgeFactory = $badgeFactory;
        $this->_resultLayoutFactory = $resultLayoutFactory;
        $this->_dir = $dir;
        $this->_uploaderFactory = $uploaderFactory;
    }

    protected function _initAction()
    {
        $this->_view->loadLayout();
        return $this;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}
