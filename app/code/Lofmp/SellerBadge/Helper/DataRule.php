<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_SellerBadge
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lofmp\SellerBadge\Helper;

use Magento\Catalog\Model\Product;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Convert\DataObject as ObjectConverter;
use Magento\Customer\Api\CustomerRepositoryInterface;

Class DataRule extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $sellerCollectionFactory;
     /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;
     /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Collection
     */
    protected $_collection;
       /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_session;
     /**
     * @var \Lofmp\SellerBadge\Model\Badge
     */
    protected $badge;
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Lofmp\SellerBadge\Model\EmailFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
    */


    public function __construct(
    \Magento\Framework\App\Helper\Context $context, 
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    CustomerRepositoryInterface $customerRepository,
    \Magento\Sales\Model\ResourceModel\Order\Collection $collection,
    \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
    \Lof\MarketPlace\Model\ResourceModel\Seller\CollectionFactory $sellerCollectionFactory,
    \Magento\Quote\Model\QuoteFactory $quoteFactory,
    \Magento\Checkout\Model\Cart $cart,
    \Magento\Checkout\Model\Session $session,
    \Lofmp\SellerBadge\Model\ResourceModel\Badge\Collection $badge,
    \Lofmp\SellerBadge\Model\SellerBadge $seller_badge,
    \Magento\Customer\Model\Session $customerSession
    ) {
        parent::__construct($context);
        $this->cart = $cart;
        $this->storeManager = $storeManager;
        $this->customerSession              = $customerSession;
        $this->sellerCollectionFactory     = $sellerCollectionFactory;
        $this->quoteFactory = $quoteFactory;
        $this->productCollectionFactory     = $productCollectionFactory;
        $this->_collection          = $collection;
        $this->customerRepository = $customerRepository;
        $this->_session             =  $session;
        $this->badge  = $badge;
        $this->seller_badge = $seller_badge;
    }

    public function getStore($storeId = '')
    {
        $store = $this->storeManager->getStore($storeId);
        return $store;
    }

     public function getCustomer()
    {   
        $customer = $this->customerSession->getCustomer();
        return $customer;
    }

    /**
     * Get rule by store && customer group id
     * @param  string $store
     * @param  string $customerGroupId
     * @return Lofmp\SellerBadge\Model\ResourceModel\Earning\Rule\Collection
     */
    public function getRules()
    {
        $badge = $this->badge;
 
        return $badge;
    }
       /**
     * 
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getProductCollection()
    {
      $collection = $this->productCollectionFactory->create();
      
      $collection->addMinimalPrice()
      ->addFinalPrice()
      ->addTaxPercents()
      ->addUrlRewrite()
      ->addStoreFilter(); 
 
      return $collection;
    }
      /**
     * 
     * @return \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    public function getSellerCollection()
    {
      $collection = $this->sellerCollectionFactory->create();
      
   
      return $collection;
    }

    public function getSellerBadge($sellerId) {
        return  $this->seller_badge->getCollection()->addFieldToFilter('sbm_seller_id',$sellerId);
    }

    public function getArrayBadge($sellerId) {
        $data = array();
        if($this->getSellerBadge($sellerId)) {
            foreach ($this->getSellerBadge($sellerId) as $key => $badge) {
                $data[] = $badge->getData('sbm_badge_id');
            }
        }
        return $data;
    }

    public function getRuleSellers($sellerId)
    { 
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance ();
        $seller = $objectManager->create('Lof\MarketPlace\Model\Seller');
        $storeId = $this->storeManager->getStore()->getId();
        $collection = $this->getSellerCollection();
        $seller = $seller->load($sellerId);
        $today = (new \DateTime())->format('Y-m-d');
        $product = $this->getProductCollection()->addFieldToFilter('seller_id',$sellerId)->addFieldToFilter('approval',2);
        $rules = $this->getRules()->addFieldToFilter('sb_status',1);
    

        $commissionRules = $rules;
        foreach($commissionRules as $commissionRule) { 
            $collection->getSelect()->reset(\Magento\Framework\DB\Select::WHERE);         
            $conditions = json_decode($commissionRule->getData('actions_serialized'));  
               
            $conditions = ((array)$conditions)['conditions'];
            $where = '';

            foreach ($conditions as $key => $_condition) {
                $attribute = ((array)$_condition)['attribute'];
                $operator = ((array)$_condition)['operator'];
                $value = ((array)$_condition)['value'];
                if($operator == '==') {
                    $operator = '=';
                }
                /*if($attribute == 'product_count') {
                    if($attribute $operator count($product->getData())) {*/
                        $where .= ' AND '.$attribute.' '.$operator.' '.$value;
                  /*  }
                }   */ 
            }  
           // $conditions->collectValidatedAttributes($collection);   
           
            $collection->getSelect()->where('seller_id = '.$sellerId.$where);
  
            if(count($collection->getData())>0 && (count($this->getArrayBadge($sellerId)) == 0 || !in_array($commissionRule->getData('sb_id'),$this->getArrayBadge($sellerId)))) {
                return $commissionRule;
            } 
        }           
        
         
    }
}
?>