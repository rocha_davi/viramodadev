<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_SellerBadge
 * @copyright  Copyright (c) 2017 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lofmp\SellerBadge\Model\Rule;

class Combine extends \Magento\Rule\Model\Condition\Combine
{
    /**
     * @var \Magento\CatalogRule\Model\Rule\Condition\ProductFactory
     */
    protected $_productFactory;

    /**
     * @param \Magento\Rule\Model\Condition\Context $context
     * @param \Magento\CatalogRule\Model\Rule\Condition\ProductFactory $conditionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Rule\Model\Condition\Context $context,
        \Magento\CatalogRule\Model\Rule\Condition\ProductFactory $conditionFactory,
        array $data = []
    ) {
        $this->_productFactory = $conditionFactory;
        parent::__construct($context, $data);
        $this->setType('Lof\SellerBadge\Model\Rule\Combine');
    }

    public function sellerAttribute()
    {
        return  array(
            'seller_id' => __('Seller Id'), 
            'group'     => __('Group'),
            'company'   => __('Company'),
            'company_locality' => __('Company Locality'),
            'address' => __('Address'),
            'city' => __('City'),
            'country' => __('Country'),
            'postcode' => __('Zip/Postal Code'),
            'region' => __('State/Province'),
            'telephone' => __('Phone Number')
            );
    }
     public function sellerUsage()
    {
        return  array(
            'lifetime_sales' => __('Lifetime Sales'), 
            'product_count'     => __('Number of Products'),
            'sale'   => __('Number of Completed Sales'),
            'total_sold' => __('Total amount sold'),
            'duration_of_vendor' => __('Years as Seller')
            );
    }
    /**
     * @return array
     */
    public function getNewChildSelectOptions()
    {
        $productAttributes = $this->sellerAttribute();
        $sellerUsages = $this->sellerUsage();
        $attributes = [];
        $usages = [];
        foreach ($productAttributes as $code => $label) {
            $attributes[] = [
                'value' => 'Lofmp\SellerBadge\Model\Rule\Condition\Seller|' . $code,
                'label' => $label,
            ];
        }
        foreach ($sellerUsages as $key => $value) {
             $usages[] = [
                'value' => 'Lofmp\SellerBadge\Model\Rule\Condition\Usage|' . $key,
                'label' => $value,
            ];
        }
        $conditions = parent::getNewChildSelectOptions();
        $conditions = array_merge_recursive(
            $conditions,
            [
                [
                    'value' => 'Magento\CatalogRule\Model\Rule\Condition\Combine',
                    'label' => __('Conditions Combination'),
                ],
                ['label' => __('Seller Attribute'), 'value' => $attributes]
            ],
            [
                [
                    'value' => 'Magento\CatalogRule\Model\Rule\Condition\Combine',
                    'label' => __('Conditions Combination'),
                ],
                ['label' => __('Seller Usage'), 'value' => $usages]
            ]

        );
        return $conditions;
    }

    /**
     * @param array $productCollection
     * @return $this
     */
    public function collectValidatedAttributes($productCollection)
    {
        foreach ($this->getConditions() as $condition) {
            /** @var Product|Combine $condition */
            $condition->collectValidatedAttributes($productCollection);
        }
        return $this;
    }
}
