<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Lofmp\SellerBadge\Model\Source;


class Attributes extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * Options array
     *
     * @var array
     */
    protected $_options = null;
    
    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions($blankOption = true)
    {
        if ($this->_options === null) {
            $this->_options = [];
            $om = \Magento\Framework\App\ObjectManager::getInstance();
  
            $attributeCollection = $om->create('Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection');
            $attributeCollection->addFieldToFilter('frontend_input',['in' => ['select', 'multiselect']])
                ->addVisibleFilter();
            $attributeCollection->getSelect()->where('source_model is NULL');
            foreach($attributeCollection as $attribute){
                $this->_options[] = ['label' => $attribute->getAttributeCode(), 'value' => $attribute->getAttributeCode()];
            }
        }
        return $this->_options;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray($blankOption = true)
    {
        $_options = [];
        foreach ($this->getAllOptions($blankOption) as $option) {
            $_options[$option['value']] = $option['label'];
        }
        return $_options;
    }
    
    
    /**
     * Get options as array
     *
     * @return array
     * @codeCoverageIgnore
     */
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}
