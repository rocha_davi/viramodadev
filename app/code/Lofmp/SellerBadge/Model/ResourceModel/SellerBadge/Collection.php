<?php

namespace Lofmp\SellerBadge\Model\ResourceModel\SellerBadge;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * [_construct]
     * @return [type]
     */
    protected function _construct()
    {
        $this->_init('Lofmp\SellerBadge\Model\SellerBadge', 'Lofmp\SellerBadge\Model\ResourceModel\SellerBadge');
    }

    public function getListBadgeIDBySellerID($sellerId)
    {
        $sellerId = addslashes($sellerId);
        $this->getSelect()->reset()->from($this->getMainTable(), ['sbm_badge_id'])->where("sbm_seller_id = " . $sellerId);
        return $this->getConnection()->fetchCol($this->getSelect());
    }

    public function getIdFromSellerBadge($badgeId, $sellerId)
    {
        $connection = $this->getConnection();
        $select = $connection
            ->select()->distinct(true)
            ->from($this->getMainTable(), array('sbm_badge_id'))
            ->where('sbm_seller_id = ' . $sellerId);
        return $connection->fetchOne($select);
    }

    public function checkExist($badgeId, $sellerId)
    {
        $connection = $this->getConnection();
        $table = $this->getMainTable();
        $sql = ' SELECT * FROM ' . $table . ' WHERE sbm_seller_id =' . $sellerId . ' and sbm_badge_id = ' . $badgeId;
        return $connection->fetchAll($sql);
    }

    public function assignAllRank($dataTable)
    {
        $connection = $this->getConnection();
        $table = $this->getMainTable();
        $query_values = array();
        $sql = 'INSERT INTO ' . $table . ' (
            `sbm_name`,
            `sbm_email`,
            `sbm_status`,
            `sbm_badge_id`,
            `sbm_seller_id`,
            `sbm_updated_at`,
            `sbm_created_at`
        ) VALUES ';

        foreach($dataTable as $row) {
            if(empty($this->checkExist($row["sbm_badge_id"], $row["sbm_seller_id"]))) {
                $query_values[] = '("'.$row["sbm_name"].'", "'.$row["sbm_email"].'", '.$row["sbm_status"].', '.$row["sbm_badge_id"].','.$row["sbm_seller_id"].', NOW(), NOW())';
            }
        }
        // echo "<pre>"; print_r($sql . implode(',',$query_values)); die;
        return $connection->query($sql . implode(',',$query_values));
    }

    public function getListSeller($arrSbmId)
    {
        $connection = $this->getConnection();
        $table = $this->getMainTable();
        $sql = ' SELECT DISTINCT sbm_seller_id, sbm_name, sbm_email FROM ' . $table . ' WHERE `sbm_id` IN (' . $arrSbmId .')';
        $results = $connection->fetchAll($sql);

        $listSeller = array();
        foreach($results as $result){
            $listSeller[] = array(
                'sbm_seller_id' => $result['sbm_seller_id'],
                'sbm_name' => $result['sbm_name'],
                'sbm_email' => $result['sbm_email'],
            );
        }
        // echo "<pre>"; print_r($listSeller); die;
        return $listSeller;
    }

    public function assignRank($dataTable)
    {
        $connection = $this->getConnection();
        $table = $this->getMainTable();
        $query_values = array();
        $sql = 'INSERT INTO ' . $table . ' (
            `sbm_name`,
            `sbm_email`,
            `sbm_status`,
            `sbm_badge_id`,
            `sbm_seller_id`,
            `sbm_updated_at`,
            `sbm_created_at`
        ) VALUES ';

        foreach($dataTable as $row) {
            if(empty($this->checkExist($row["sbm_badge_id"], $row["sbm_seller_id"]))) {
                $query_values[] = '("'.$row["sbm_name"].'", "'.$row["sbm_email"].'", '.$row["sbm_status"].', '.$row["sbm_badge_id"].','.$row["sbm_seller_id"].', NOW(), NOW())';
            }
        }
        if(empty($query_values)){
            return false;
        } else {
            $connection->query($sql . implode(',',$query_values));
            return true;
        }
        return true;
    }

    public function deleteSellerBadge($badgeId, $exSellerId)
    {
        $connection = $this->getConnection();
        $table = $this->getMainTable();
        $sql = 'DELETE FROM ' . $table . ' WHERE `sbm_badge_id` = ' . $badgeId . ' and `sbm_seller_id` IN( ' . $exSellerId . ')';
        return $connection->query($sql);
    }

    public function deleteBadgeBySellerId($arrSellerId)
    {
        $connection = $this->getConnection();
        $table = $this->getMainTable();
        $sql = 'DELETE FROM ' . $table . ' WHERE `sbm_seller_id` IN (' . $arrSellerId .')';

        return $connection->query($sql);
    }

    public function getListSellerId($arrSbmId)
    {
        $connection = $this->getConnection();
        $table = $this->getMainTable();
        $sql = ' SELECT DISTINCT sbm_seller_id FROM ' . $table . ' WHERE `sbm_id` IN (' . $arrSbmId .')';
        $results = $connection->fetchAll($sql);

        $listSellerId = array();
        foreach($results as $result){
            $listSellerId[] = $result['sbm_seller_id'];
        }
        return $listSellerId;
    }

}
