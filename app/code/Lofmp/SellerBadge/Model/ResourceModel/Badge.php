<?php

namespace Lofmp\SellerBadge\Model\ResourceModel;

class Badge extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('lofmp_sellerbadge_badge', 'sb_id');
    }
}
