<?php

namespace Lofmp\SellerBadge\Model\ResourceModel;

class SellerBadge extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('lofmp_sellerbadge_seller_badge_manager', 'sbm_id');
    }

}
