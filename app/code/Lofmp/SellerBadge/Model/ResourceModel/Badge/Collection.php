<?php

namespace Lofmp\SellerBadge\Model\ResourceModel\Badge;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Resource collection initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Lofmp\SellerBadge\Model\Badge', 'Lofmp\SellerBadge\Model\ResourceModel\Badge');
    }


    public function toOptionArray()
    {
        $options = array();
        $this->addFieldToFilter('sb_status', 1);
        $this->setOrder('sb_name', 'ASC');
        $collection = $this->load();

        foreach($collection as $item)
        {
            $options[] = array(
                'label'=> $item->getsb_name(),
                'value' => $item->getId()
            );
        }
        return $options;
    }

    public function getPathIconByID($badgeId)
    {
        $badgeId = addslashes($badgeId);
        $this->getSelect()->reset()->from($this->getMainTable(), ['sb_image'])->where("sb_id = ".$badgeId);
        return $this->getConnection()->fetchCol($this->getSelect());
    }

}
