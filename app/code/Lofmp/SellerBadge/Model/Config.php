<?php

namespace Lofmp\Supplier\Model;

class Config
{
    protected $_scopeConfig;
    protected $_moduleManager;

    /*
     * @var \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\Module\Manager $moduleManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ){
        $this->_scopeConfig = $scopeConfig;
        $this->_moduleManager = $moduleManager;
    }

    public function getSetting($path, $storeId = 0)
    {
        return $this->_scopeConfig->getValue('sellerbadge/'.$path, 'store', $storeId);
    }

    public function getGlobalSetting($path, $storeId = 0)
    {
        return $this->_scopeConfig->getValue($path, 'store', $storeId);
    }

    public function getBarcodeAttribute()
    {
        return $this->_scopeConfig->getValue('sellerbadge/general/barcode_attribute');
    }

    public function getExtendedCostMethod()
    {
        return $this->_scopeConfig->getValue('sellerbadge/order_product/extended_cost_method');
    }

    public function updateProductCostAfterReception()
    {
        return $this->_scopeConfig->getValue('sellerbadge/order_product/update_cost');
    }

    public function isErpIsInstalled()
    {
        return $this->_moduleManager->isEnabled('Lofmp_Erp');
    }

}