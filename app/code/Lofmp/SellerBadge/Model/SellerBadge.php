<?php

namespace Lofmp\SellerBadge\Model;

class SellerBadge extends \Magento\Framework\Model\AbstractModel
{
    protected $_sellerBageCollectionFactory;
    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\Db $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        // \Lofmp\SellerBadge\Model\ResourceModel\SellerBadge\CollectionFactory $sellerBageCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        // $this->_sellerBageCollectionFactory = $sellerBageCollectionFactory;
    }
    
    protected function _construct()
    {
        $this->_init('Lofmp\SellerBadge\Model\ResourceModel\SellerBadge');
    }

    public function loadBySellerBadge($badgeId, $sellerId)
    {
        // $sellerBadge = $this->_sellerBageCollectionFactory->create();
        // $id = $sellerBadge->getIdFromSellerBadge($badgeId, $sellerId);
        // return $this->load($id);

        return '';
    }

}