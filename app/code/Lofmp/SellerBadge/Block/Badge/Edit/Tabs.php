<?php

namespace Lofmp\SellerBadge\Block\Badge\Edit;

class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    protected $_registry;

    /**
     * [__construct]
     * @param \Magento\Backend\Block\Template\Context  $context     
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder 
     * @param \Magento\Backend\Model\Auth\Session      $authSession 
     * @param \Magento\Framework\Registry              $registry    
     * @param array                                    $data        
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_registry = $registry;
        parent::__construct($context, $jsonEncoder, $authSession, $data);
    }
    
    /**
     * [_construct]
     * @return [void]
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('page_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Badge Info'));
    }
    
    /**
     * [_beforeToHtml]
     * @return [html]
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'main_section',
            [
                'label' => __('Main Badge'),
                'title' => __('Main Badge'),
                'content' => $this->getLayout()->createBlock('Lofmp\SellerBadge\Block\Badge\Edit\Tab\Main')->toHtml(),
                'active' => true
            ]
        );

        $this->_eventManager->dispatch('lofmpsellerbadge_edit_tabs', ['badge' => $this->getBadge(), 'tabs' => $this, 'layout' => $this->getLayout()]);

        return parent::_beforeToHtml();
    }

    /**
     * [getBadge]
     * @return [object]
     */
    protected function getBadge()
    {
        return $this->_registry->registry('current_badge');
    }
}
