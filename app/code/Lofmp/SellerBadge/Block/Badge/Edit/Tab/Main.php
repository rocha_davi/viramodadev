<?php

namespace Lofmp\SellerBadge\Block\Badge\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var \Magento\Framework\Locale\ListsInterface
     */
    protected $_LocaleLists;
    protected $_countryLists;
    protected $_ruleActions;

    /**
     * @var \Magento\Backend\Block\Widget\Form\Renderer\FieldsetFactory
     */
    protected $rendererFieldsetFactory;    

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Framework\Locale\ListsInterface $localeLists
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Framework\Locale\ListsInterface $localeLists,
        \Magento\Rule\Block\Actions $ruleActions,
        \Magento\Backend\Block\Widget\Form\Renderer\FieldsetFactory $rendererFieldsetFactory,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        array $data = []
    ) {
        $this->rendererFieldsetFactory = $rendererFieldsetFactory; 
        $this->_LocaleLists = $localeLists;
        $this->_countryLists = $countryCollectionFactory;
        $this->_ruleActions = $ruleActions;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form fields
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @return \Magento\Backend\Block\Widget\Form
     */
    protected function _prepareForm()
    {
        
        /** @var $model \Magento\User\Model\User */
        $model = $this->_coreRegistry->registry('current_badge');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('badge_');

        $mainFieldset = $form->addFieldset('base_fieldset', ['legend' => __('Main')]);

        if ($model->getId()) {
            $mainFieldset->addField('sb_id', 'hidden', ['name' => 'sb_id']);
        }

        $mainFieldset->addField(
            'sb_status',
            'select',
            [
                'name' => 'sb_status',
                'label' => __('Status'),
                'id' => 'sb_status',
                'title' => __('Status'),
                'class' => 'input-select',
                'options' => ['1' => __('Enable'), '0' => __('Disable')]
            ]
        );

        $mainFieldset->addField(
            'sb_name',
            'text',
            [
                'name' => 'sb_name',
                'label' => __('Name'),
                'id' => 'sb_name',
                'title' => __('Name'),
                'required' => true
            ]
        );
        $mainFieldset->addField(
            'sb_rank',
            'text',
            [
                'name' => 'sb_rank',
                'label' => __('Rank'),
                'id' => 'sb_rank',
                'title' => __('Rank'),
                'required' => true
            ]
        );
        $mainFieldset->addField(
            'sb_image',
            'image',
            [
                'name' => 'sb_image',
                'label' => __('Image'),
                'title' => __('Image'),
                'disabled' => false
            ]
        );

        $mainFieldset->addField(
            'sb_description',
            'textarea',
            [
                'name' => 'sb_description',
                'label' => __('Description'),
                'id' => 'sb_description',
                'title' => __('Description'),
                'class' => '',
                'required' => false
            ]
        );
        $this->addProductConditions($form,$model);

        $data = $model->getData();
        $form->setValues($data);

        $this->setForm($form);

        return parent::_prepareForm();
    }
     protected function addProductConditions(\Magento\Framework\Data\Form $form, \Lofmp\SellerBadge\Model\Badge $model, $fieldsetId = 'actions_fieldset', $formName = 'edit_form') {  
        // commission_product_conditions_fieldset
        $id = $this->getRequest()->getParam('id');
        if (!$model) {   
            $model = $this->getRuleFactory()->create();
            $model->load($id);
        }


        $newChildUrl = $this->getUrl(
        'sellerbadge/promo_quote/newActionHtml/form/commission_actions_fieldset',
        ['form_namespace' => $formName]
        );

        $renderer = $this->rendererFieldsetFactory->create()->setTemplate(
        'Magento_CatalogRule::promo/fieldset.phtml'
        )->setNewChildUrl(
        $newChildUrl
        );
        
        $fieldset = $form->addFieldset(
            $fieldsetId,
            [
            'legend' => __(
                'Conditions (don\'t add conditions if rule is applied to all products)'
                )
            ]
            )->setRenderer(
            $renderer
            );
          
            $fieldset->addField(
                'actions',
                'text',
                [
                'name'           => 'apply_to',
                'label'          => __('Apply To'),
                'title'          => __('Apply To'),
                'required'       => true,
                'data-form-part' => $formName
                ]
            )->setRule(
                $model
            )->setRenderer(
                $this->_ruleActions
        );               

    }

}
