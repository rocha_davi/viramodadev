<?php

namespace Lofmp\SellerBadge\Block\Badge\Renderer;

use Magento\Framework\DataObject;
use Magento\Store\Model\StoreManagerInterface;

class Icon extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * @param \Magento\Backend\Block\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
    }

    public function render(DataObject $row)
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');

        // Result $mediaUrl = http://example.com/pub/media/
        $mediaUrl =  $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        $path = $mediaUrl.$row->getsb_image();

    	$html = [];
        try {

            $html = '<img alt="Icon Item" class="badge-icon" title="Badge icon" src="'.$path.'" width="50px" height="50px" >';

            return $html;
        }
        catch(\Exception $ex)
        {
            return '<font color="red">'.$ex->getMessage().'</font>';
        }
    }
}