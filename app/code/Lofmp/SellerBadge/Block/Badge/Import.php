<?php
namespace Lofmp\SellerBadge\Block\Badge;

class Import extends \Magento\Backend\Block\Template
{
    protected $_template = 'Badge/Import.phtml';

    /**
     * [getSubmitUrl]
     * @return [url]
     */
    public function getSubmitUrl()
    {
        return $this->getUrl('*/*/ProcessImport');
    }

    /**
     * [getFormKey]
     * @return [key]
     */
    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }
    
}