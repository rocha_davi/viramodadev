<?php
namespace Lofmp\SellerBadge\Block\Badge;
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    protected $_registry = null;
    
    /**
     * [__construct]
     * @param \Magento\Backend\Block\Widget\Context $context 
     * @param \Magento\Framework\Registry           $registry
     * @param array                                 $data    
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_registry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * [_construct]
     * @return [void]
     */
    protected function _construct()
    {
        $this->_objectId = 'sb_id';
        $this->_controller = 'badge';
        $this->_blockGroup = 'Lofmp_SellerBadge';
        parent::_construct();
       if ($this->_isAllowedAction('Lofmp_SellerBadge::badge_save')) { 
            $this->buttonList->update('save', 'label', __('Save Product'));
            $this->buttonList->add(
                'saveandcontinue',
                [
                    'label' => __('Save and Continue Edit'),
                    'class' => 'save',
                    'data_attribute' => [
                        'mage-init' => [
                            'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                        ],
                    ]
                ],
                -100
            );

        }else {
            $this->buttonList->remove('save');
        }
        if ($this->_isAllowedAction('Lofmp_SellerBadge::badge_delete')) {
            $this->buttonList->update('delete', 'label', __('Delete Badge'));
        } else {
            $this->buttonList->remove('delete');
        }

    }
    
    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('sellerbadge/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '{{tab_id}}']);
    }
    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
    /**
     * [getHeaderText]
     * @return [string]
     */
    public function getHeaderText()
    {
        if ($this->_registry->registry('current_badge')->getId()) {
            $name = $this->escapeHtml($this->_registry->registry('current_badge')->getSbName());
            return __("Edit Badge '%1'", $name);
        } else {
            return __('New Badge');
        }
    }
}
