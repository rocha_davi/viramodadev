<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Landofcoder
 * @package    Lofmp_SellerBadge
 *
 * @copyright  Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lofmp\SellerBadge\Block\Seller;

use Lofmp\SellerBadge\Model\Config;

class Badge extends \Magento\Framework\View\Element\Template
{

 
    /**
     * @var \Lofmp\SellerBadge\Model\SellerBadge
     */
    protected $seller_badge;

    protected $helper;

    protected $badge;

    protected $seller;
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Lof\MarketPlace\Helper\Data $helper,
        \Magento\Framework\Registry $registry,
        \Lofmp\SellerBadge\Model\SellerBadge $seller_badge,
        \Lofmp\SellerBadge\Model\Badge $badge,
        \Lof\MarketPlace\Model\Seller $seller,
        \Lof\MarketPlace\Model\SellerProduct $sellerProduct
    ) {
        $this->seller_badge = $seller_badge;
        $this->badge = $badge;
        $this->sellerProduct = $sellerProduct;
        $this->helper = $helper;
        $this->seller = $seller;
        $this->_coreRegistry  = $registry;
        parent::__construct($context);
    }
    public function countProduct($seller_id) {
        $product = $this->sellerProduct->getCollection()->addFieldToFilter('seller_id',$seller_id);
        return count($product);
    }
    public function getSeller() {
        return $this->seller;
    }
    public function getSellerBadge() {
        return  $this->seller_badge;
    }

    public function showBadge($seller_id) {
        $badge = $this->seller_badge->getCollection()->addFieldToFilter('sbm_seller_id',$seller_id);
        return $badge;
    }

    public function getBadge($badge_id) {
        return $this->badge->getCollection()->addFieldToFilter('sb_id',$badge_id)->getFirstItem();
    }
      public function getCurrentSeller()
    {
        $seller = $this->_coreRegistry->registry('current_seller');
        if ($seller) {
            $this->setData('current_seller', $seller);
        }
        return $seller;
    }
}
