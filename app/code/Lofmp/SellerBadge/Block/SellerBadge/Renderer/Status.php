<?php

namespace Lofmp\SellerBadge\Block\SellerBadge\Renderer;


class Status extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    /**
     * @param \Magento\Backend\Block\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
    }

    public function render(\Magento\Framework\DataObject $row)
    {
        switch ($row->getsbm_status()) {
            case '1':
                return 'Approved';
                break;
            case '2':
                return 'Pedding';
                break;
            default:
                return 'Default';
                break;
        }
        
    }

}