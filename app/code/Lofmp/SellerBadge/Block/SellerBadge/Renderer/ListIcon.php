<?php

namespace Lofmp\SellerBadge\Block\SellerBadge\Renderer;

use Magento\Framework\DataObject;

class ListIcon extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer
{
    protected $_badgeCollectionFactory;
    protected $_sellerBageCollectionFactory;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Lofmp\SellerBadge\Model\ResourceModel\Badge\CollectionFactory $badgeCollectionFactory,
        \Lofmp\SellerBadge\Model\ResourceModel\SellerBadge\CollectionFactory $sellerBageCollectionFactory,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_badgeCollectionFactory = $badgeCollectionFactory;
        $this->_sellerBageCollectionFactory = $sellerBageCollectionFactory;
    }

    /**
     * [render description]
     * @param  DataObject $row
     * @return [type]         
     */
    public function render(DataObject $row)
    {
        $html = '';
        try {
            $sellerBadge = $this->_sellerBageCollectionFactory->create();
            $badges = $sellerBadge->getListBadgeIDBySellerID($row->getsbm_seller_id());

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            // Result $mediaUrl = http://example.com/pub/media/
            $mediaUrl =  $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

            foreach ($badges as $badgeId) {
                $badgeCollection = $this->_badgeCollectionFactory->create();
                $icon = $badgeCollection->getPathIconByID($badgeId);
                $path = $mediaUrl.$icon[0];
                $html .= '<img alt="Icon Item" class="badge-icon" title="Badge icon" src="'.$path.'" width="50px" height="50px" >';
            }

            return $html;
        }
        catch(\Exception $ex)
        {
            return '<font color="red">'.$ex->getMessage().'</font>';
        }

    }
}