<?php

namespace Lofmp\SellerBadge\Block\SellerBadge\Filter;

use Magento\Backend\Block\Context;
use Magento\Framework\DB\Helper;

class Status extends \Magento\Backend\Block\Widget\Grid\Column\Filter\Select
{
    public function __construct(
        Context $context,
        Helper $resourceHelper,
        array $data = []
    ) {
        parent::__construct($context, $resourceHelper, $data);
    }

    /**
     * Get options
     *
     * @return array
     */
    protected function _getOptions()
    {
        $options = array();
        $options[] = array('value' => '1', 'label' => 'Approved');
        $options[] = array('value' => '2', 'label' => __('Pedding'));
        return $options;
    }
}
