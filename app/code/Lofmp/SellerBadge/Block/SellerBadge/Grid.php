<?php

namespace Lofmp\SellerBadge\Block\SellerBadge;

use Magento\Backend\Block\Widget\Grid\Column;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $moduleManager;
    protected $_collectionFactory;
    protected $_websiteFactory;

    protected $_badgeCollectionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Store\Model\WebsiteFactory $websiteFactory
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Store\Model\WebsiteFactory $websiteFactory,
        \Lofmp\SellerBadge\Model\ResourceModel\SellerBadge\Collection $sellerBadgeCollection,
        \Lofmp\SellerBadge\Model\ResourceModel\Badge\CollectionFactory $badgeCollectionFactory,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    )
    {
        $this->_badgeCollectionFactory = $badgeCollectionFactory;
        $this->_sellerBadgeCollection = $sellerBadgeCollection;
        $this->_websiteFactory = $websiteFactory;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * Class constructor
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('sellerBadgeGrid');
        $this->setDefaultSort('sbm_id');
        $this->setDefaultDir('desc');
        $this->setTitle(__('SellerBage Manager'));
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
    }

    protected function addAdditionnalFilter(&$collection)
    {
        return $this;
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        try {
            $collection = $this->_sellerBadgeCollection->load();
            $collection->getSelect()->group('sbm_seller_id');

            $this->setCollection($collection);
            $this->addAdditionnalFilter($collection);
            parent::_prepareCollection();
            return $this;
        } catch (Exception $e) {
            echo $e->getMessage();
            die;
        }

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        // $this->addColumn('sbm_id', ['header' => __('ID'), 'index' => 'sbm_id']);
        $this->addColumn('sbm_badge_id',
            [
                'header' => __('Badges'),
                'index' => 'sbm_badge_id',
                'renderer' => '\Lofmp\SellerBadge\Block\SellerBadge\Renderer\ListIcon',
                'filter' => false,
                'sortable' => false,
            ]
        );
        $this->addColumn('sbm_name', ['header' => __('Seller Name'), 'index' => 'sbm_name']);
        $this->addColumn('sbm_email', ['header' => __('Seller Email'), 'index' => 'sbm_email']);

        $this->addColumn('sbm_status',
            [
                'header' => __('Status'),
                'index' => 'sbm_status',
                'renderer' => '\Lofmp\SellerBadge\Block\SellerBadge\Renderer\Status',
                'filter' => '\Lofmp\SellerBadge\Block\SellerBadge\Filter\Status',
                'filter_condition_callback' => array($this, '_filterHasUrlConditionCallback')
            ]
        );

        // $this->addColumn('sbm_update_at', ['header' => __('Update At'), 'index' => 'sbm_update_at', 'type' => 'date']);
        $this->addColumn('sbm_created_at', ['header' => __('Created At'), 'index' => 'sbm_created_at', 'type' => 'date']);

        $this->_eventManager->dispatch('lofmpsellerbadge_sellerbadge_grid_preparecolumns', ['grid' => $this]);


        /*{{CedAddGridColumn}}*/

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        $this->addExportType('sellerbadge/sellerbadge/ExportCsv', __('CSV'));

        return parent::_prepareColumns();
    }


    protected function _filterHasUrlConditionCallback($collection, $column)
    {
        $filterroleid = $column->getFilter()->getValue();
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
        $this->getCollection()->addFieldToFilter('sbm_status', array('eq' => $filterroleid));
        return false;
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        // return $this->getUrl('sellerBadge/sellerBadge/Grid');
        return $this->getUrl('sellerbadge/sellerbadge/index', ['_current' => true]);
    }

    /**
     * @param \Magento\Catalog\Model\Product|\Magento\Framework\Object $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return false;
    }

    /**
     * [_prepareMassaction]
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('sbm_id');
        $this->getMassactionBlock()->setFormFieldName('sbm_id');

        $badgeCollection = $this->_badgeCollectionFactory->create();
        $badgeOption = $badgeCollection->toOptionArray();

        $modes = [];
        $modes[] = ['label' => 'Remove All Ranks', 'value' => 'remove_all'];

        $arr = array_merge($modes, $badgeOption);

        $this->getMassactionBlock()->addItem(
            'removeRank',
            [
                'label' => __('Remove Rank to Seller'),
                'url' => $this->getMassActionUrl('MassRemoveRank'),
                'additional' => [
                    'mode' => [
                        'name' => 'mode',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Remove Rank'),
                        'values' => $arr,
                    ],
                ]

            ]
        );

        $modes = [];
        $modes[] = ['label' => 'Assign All Ranks', 'value' => 'assign_all'];
        $arr = array_merge($modes, $badgeOption);
        $this->getMassactionBlock()->addItem(
            'assignRank',
            [
                'label' => __('Assign Rank to Seller'),
                'url' => $this->getMassActionUrl('MassAssignRank'),
                'additional' => [
                    'mode' => [
                        'name' => 'mode',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => __('Assign Rank'),
                        'values' => $arr,
                    ],
                ]

            ]
        );

        return $this;
    }

    protected function getMassActionUrl($action)
    {
        return $this->getUrl('*/*/' . $action);
    }

    public function getRowClass($row)
    {
        return 'sellerbadge_badge_' . $row->getfake_id();
    }

    protected function _prepareMassactionColumn()
    {
        $columnId = 'massaction';
        $massactionColumn = $this->getLayout()
            ->createBlock('Magento\Backend\Block\Widget\Grid\Column')
            ->setData(
                [
                    'index' => $this->getMassactionIdField(),
                    'filter_index' => $this->getMassactionIdFilter(),
                    'type' => 'massaction',
                    'name' => $this->getMassactionBlock()->getFormFieldName(),
                    'is_system' => true,
                    'header_css_class' => 'col-select',
                    'column_css_class' => 'col-select',
                    'use_index' => 1,
                ]
            );

        if ($this->getNoFilterMassactionColumn()) {
            $massactionColumn->setData('filter', false);
        }

        $massactionColumn->setSelected($this->getMassactionBlock()->getSelected())->setGrid($this)->setId($columnId);

        $this->getColumnSet()->insert(
            $massactionColumn,
            count($this->getColumnSet()->getColumns()) + 1,
            false,
            $columnId
        );
        return $this;
    }

}
