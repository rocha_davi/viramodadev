<?php
namespace Lofmp\SellerBadge\Block\SellerBadge;

class Header extends \Magento\Backend\Block\Template
{
    protected $_template = 'sellerbadge/header.phtml';

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Lofmp\SellerBadge\Model\ResourceModel\SellerBadge\CollectionFactory $sellerBadgeCollectionFactory,
        array $data = []
    )
    {
        parent::__construct($context, $data);

        $this->_sellerBadgeCollectionFactory = $sellerBadgeCollectionFactory;
    }

    public function getSellerBadges()
    {
        return $this->_sellerBadgeCollectionFactory->create();
    }
}