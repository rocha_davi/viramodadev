<?php
namespace Lofmp\SellerBadge\Block;

class Badge extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * @var \Magento\User\Model\ResourceModel\User
     */
    protected $_resourceModel;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        
        $this->addData(
            [
                \Magento\Backend\Block\Widget\Container::PARAM_CONTROLLER => 'badge',
                \Magento\Backend\Block\Widget\Grid\Container::PARAM_BLOCK_GROUP => 'Lofmp_SellerBadge',
                \Magento\Backend\Block\Widget\Grid\Container::PARAM_BUTTON_NEW => __('Add New Badge'),
                \Magento\Backend\Block\Widget\Container::PARAM_HEADER_TEXT => __('Badges'),
            ]
        );
        parent::_construct();
        $this->_addNewButton();
        // $this->_addImportButton();
    }

    protected function _addImportButton()
    {
        $this->addButton(
            'import',
            [
                'label' => __('Import'),
                'onclick' => 'setLocation(\'' . $this->getUrl('*/*/import') . '\')',
                'class' => 'action-secondary'
            ]
        );
    }

}
