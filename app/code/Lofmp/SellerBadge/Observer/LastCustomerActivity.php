<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lof_FollowUpEmail
 * @copyright  Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */
namespace Lofmp\SellerBadge\Observer;

use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Customer log observer.
 */
class LastCustomerActivity implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\ResourceConnection 
     */
    protected $_resource;
    /**
     * @var helper
     */
    protected $helper_rule;


    protected $helper;

     public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Framework\App\ResourceConnection $resource,
        \Lofmp\SellerBadge\Helper\DataRule $helper_rule,
        \Lof\MarketPlace\Helper\Data $helper,
        \Magento\Store\Model\StoreManagerInterface $storeManager
        ) {
        $this->helper = $helper;
        $this->_storeManager    = $storeManager;
        $this->quoteRepository  = $quoteRepository;
        $this->_resource        = $resource;
        $this->helper_rule           = $helper_rule;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
            $customer = $observer->getCustomer();
         $seller_id = $this->helper->getSellerId();
          
    }
  
}
