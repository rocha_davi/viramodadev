<?php
/**
 * LandOfCoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Venustheme.com license that is
 * available through the world-wide-web at this URL:
 * http://www.venustheme.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   LandOfCoder
 * @package    Lofmp_Rma
 * @copyright  Copyright (c) 2016 Venustheme (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */



namespace Lofmp\Rma\Controller\MarketPlace\Rma;

use Magento\Framework\Controller\ResultFactory;

class SaveMessage extends \Magento\Framework\App\Action\Action
{
    public function __construct(
        \Magento\Framework\Registry $registry,
        \Lofmp\Rma\Api\Repository\RmaRepositoryInterface $rmaRepository,
        \Lofmp\Rma\Api\Repository\MessageRepositoryInterface $messageRepository,
        \Lof\MarketPlace\Helper\Data     $helper,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Event\ManagerInterface               $eventManager,
        \Magento\Framework\App\Action\Context $context
    ) {
        $this->registry             = $registry;
        $this->helper               = $helper;
        $this->rmaRepository        = $rmaRepository;
        $this->messageRepository     = $messageRepository;
        $this->resultFactory        = $context->getResultFactory();
         $this->eventManager         = $eventManager;
        $this->customerSession      = $customerSession;

        parent::__construct($context);
    }


    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        try {
             $data = $this->getRequest()->getParams();
            $id = $data['id'];
            
            $customerId = $this->customerSession->getCustomer()->getId();
            $objectManager       = \Magento\Framework\App\ObjectManager::getInstance ();
            $seller = $objectManager->create ( 'Lof\MarketPlace\Model\Seller' )->load($customerId,'customer_id');
            $rma = $this->rmaRepository->getById($id);
            $rma->setStatusId($data['status_id'])
                ->setReturnAddress($data['return_address']);
            $this->rmaRepository->save($rma);
            if (!$this->registry->registry('current_rma')) {
                $this->registry->register('current_rma', $rma);
            }
           
            if ((isset($data['reply']) && $data['reply'] != '') ||(!empty($_FILES['attachment']) && !empty($_FILES['attachment']['name'][0]))) {
                      $message = $this->messageRepository->create();
                      $message->setRmaId($rma->getId())
                            ->setText($data['reply']);
                        if (isset($data['internalcheck']) ) {
                                   $message->setInternal($data['internalcheck'])
                                    ->setIsCustomerNotified(false)
                                    ->setSellerId($seller->getData('seller_id'));
                            }
                        else{
                                $message->setInternal(0)
                                    ->setIsCustomerNotified(true)
                                    ->setSellerId($seller->getData('seller_id'));
                                   
                            }
                            $this->messageRepository->save($message);

                            $rma->setLastReplyName($seller->getData('name'))
                                ;
                            $this->rmaRepository->save($rma);

                            $this->eventManager->dispatch(
                                'rma_add_message_after',
                                ['rma'=> $rma, 'message' => $message, 'user' => $seller, 'params' => $data]
                            );
                }
                $this->eventManager->dispatch('rma_update_rma_after', ['rma' => $rma, 'user' => $seller]);
                        
                        $this->messageManager->addSuccess(__('Your message was successfuly added'));
            
            
            return $resultRedirect->setPath('*/rma/view', ['id' => $rma->getId(), '_nosid' => true]);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
            return $resultRedirect->setPath('*/*/index');
        }
    }

}

