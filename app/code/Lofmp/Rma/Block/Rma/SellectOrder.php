<?php
/**
 * Landofcoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://landofcoder.com/license
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   Landofcoder
 * @package    Lofmp_Rma
 * @copyright  Copyright (c) 2016 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lofmp\Rma\Block\Rma;

class SellectOrder extends \Magento\Framework\View\Element\Html\Link
{
    

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession, 
       \Magento\Framework\Api\SearchCriteriaBuilder       $searchCriteriaBuilder,
        \Magento\Framework\Api\SortOrderBuilder            $sortOrderBuilder,
        \Magento\Sales\Api\OrderRepositoryInterface                   $orderRepository,
        \Lof\MarketPlace\Model\SellerFactory $sellerFactory,
        \Lof\MarketPlace\Model\Order $orders,
        \Lof\MarketPlace\Model\Orderitems $orderitems,
        \Lofmp\Rma\Helper\Data    $rmaHelper,
        \Lofmp\Rma\Helper\Help                                $Helper,
        array $data = []
    ) {
        $this->sortOrderBuilder      = $sortOrderBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->orderRepository       = $orderRepository;
        $this->rmaHelper             = $rmaHelper;
        $this->helper                = $Helper;
        $this->sellerFactory         = $sellerFactory;
        $this->orders                = $orders;
        $this->orderitems            = $orderitems;
        $this->customerSession       = $customerSession;
         $this->context              = $context;
        parent::__construct($context, $data);
    }

    public function getOrderList() {
        $customer_id = $this->customerSession->getCustomerId();
        $allowedStatuses = $this->helper->getConfig($store = null,'rma/policy/allow_in_statuses');
        $allowedStatuses = explode(',', $allowedStatuses); 
        $orders = $this->orders->getCollection()->addFieldToFilter('customer_id',(int)$customer_id)->addFieldToFilter('status', ['in' => $allowedStatuses]);

        if(count($this->rmaHelper->getAllowOrderId()) > 0) {
            $orders->addFieldToFilter('order_id', $this->rmaHelper->getAllowOrderId(), 'in');
        }

        return $orders;
    }
    public function getOrderById($order_id) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance ();
        $order = $objectManager->get('Magento\Sales\Model\Order')->load($order_id);
        return $order;
    }
    public function GetItemInfos($order_id, $seller_id)
    {
            $items ='';
            $orderitems = $this->orderitems->getCollection()->addFieldToFilter('seller_id',(int)$seller_id)->addFieldToFilter('order_id',$order_id);
           foreach($orderitems as $orderitem){
               $items .= $orderitem->getProductName().'(qty : '.(int)$orderitem['product_qty'].'), ';
           }
           return $items;
        
    }
         /**
     * @param int $orderId
     * @return string
     */
    public function getOrderUrl($orderId)
    {
        return  $this->context->getUrlBuilder()->getUrl('sales/order/view', ['order_id' => $orderId]);
    }
    /**
     * @return string
     */
    public function getCreateRmaUrl($order_id,$seller_id)
    {
        return $this->context->getUrlBuilder()->getUrl('returns/rma/new/',['seller_id' => $seller_id,'order_id' => $order_id]);
    }
     
    /**
     * @return int
     */
    public function getSeller($seller_id)
    {
        return $this->sellerFactory->create()->load($seller_id);
    }
    /**
     * @return int
     */
    public function getReturnPeriod()
    {
        return $this->helper->getConfig($store = null,'rma/policy/return_period');
    }
     /**
     * @return boolean
     */
     public function IsItemsQtyAvailable($order){
        $items = $order->getAllItems();
        foreach ($items as  $item) {
            if($item->getData('base_row_total') <=0||$item->getData('product_type') == 'bundle')  continue; 
                      if($this->rmaHelper->getItemQuantityAvaiable($item)>0){
                         return true;
                      }
                  }  
        return false;    
        }
   
    /**
     * Prepare layout for change buyer
     *
     * @return Object
     */
    public function _prepareLayout() {
        $this->pageConfig->getTitle ()->set(__('Sellect Order'));
        return parent::_prepareLayout ();
    }
    

}