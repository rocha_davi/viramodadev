<?php


namespace Lofmp\Rma\Block\Marketplace;

    

  use Lofmp\Rma\Model\Rma;

class Grid extends \Magento\Framework\View\Element\Template
{

    public function __construct(
                \Magento\Customer\Model\Session $customerSession,
                \Lof\MarketPlace\Model\SellerFactory $sellerFactory,
                \Lofmp\Rma\Model\ResourceModel\Rma\Collection $RmaCollection,
                \Lofmp\Rma\Model\ResourceModel\Item\Collection $ItemCollection,
                 \Magento\Framework\View\Element\Template\Context $context,
                 \Lof\MarketPlace\Helper\Data $helper,
                 \Lofmp\Rma\Model\Status $statusFactory ,
                 array $data = []
            )
    {       
        $this->_RmaCollection = $RmaCollection;
         $this->_ItemCollection = $ItemCollection;
        $this->customerSession = $customerSession;
        $this->_sellerFactory = $sellerFactory;
        $this->helper         = $helper;
        $this->status         = $statusFactory;
        parent::__construct($context);
    } 

    public function GetSellerRma(){
    	 $sellerId = $this->helper->getSellerId();
         
    	 $Seller_rma = $this->_RmaCollection->addFieldToFilter('seller_id',$sellerId)->getData();

    	 return $Seller_rma;        
    }
    public function GetStatusname($id){
         $status =  $this->status->load($id);
         return $status->getName();
    }

}
