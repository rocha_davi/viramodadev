<?php
/**
 * LandOfCoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Venustheme.com license that is
 * available through the world-wide-web at this URL:
 * http://www.venustheme.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   LandOfCoder
 * @package    Lofmp_Rma
 * @copyright  Copyright (c) 2016 Venustheme (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */



namespace Lofmp\Rma\Block\Marketplace\Rma;

class View extends \Magento\Framework\View\Element\Template
{
    public function __construct(
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository,
        \Lofmp\Rma\Helper\Data         $rmaHelper,
        \Lofmp\Rma\Model\Status $statusFactory ,
        \Lof\MarketPlace\Model\Orderitems $orderitems,
        \Lof\MarketPlace\Model\Order $orders,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        $this->addressRenderer = $addressRenderer;
        $this->groupRepository = $groupRepository;
         $this->rmaHelper             = $rmaHelper;
        $this->orderitems = $orderitems;
        $this->orders = $orders;
        $this->request =  $context->getRequest();
        $this->registry                      = $registry;
        $this->status         = $statusFactory;
        $this->context = $context;
        parent::__construct($context, $data);
    }
    
     public function getOrder() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance ();
        $order = $objectManager->get('Magento\Sales\Model\Order')->load($this->getOrderId());
        return $order;
    }
      public function getOrderId() {
        return $this->getRma()->getOrderId();
    }
     public function getFormattedAddress()
    { 
        if($this->getOrder()->getShippingAddress()) {
            return $this->addressRenderer->format($this->getOrder()->getShippingAddress(), 'html');
        } else {
            return;
        }
    }

    /**
     * @param \Lofmp\Rma\Model\Rma    $rma
     *
     * @return bool
     */
    public function allowCreateCreditmemo($order_id,$seller_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance ();
        $order = $objectManager->get('Magento\Sales\Model\Order')->load($order_id);
        $orderitemsCollection = $this->orderitems->getCollection();
         $orderitems = $orderitemsCollection->addFieldToFilter('order_id',$order_id)
        ->addFieldToFilter('seller_id',$seller_id);
        $k=0;
       foreach ($orderitems as $orderitem) {
           # code...
            if($orderitem->getData('qty_invoiced')-$orderitem->getData('qty_refunded') > 0){
                   $k = 1;
                 }
       }
        if($k==0){
            return false;
        }
       
        if (!$order->canCreditmemo()) {
            return false;
        }
        


        return true;
    }


    public function getBillingAddress() {
        return $this->addressRenderer->format($this->getOrder()->getBillingAddress(), 'html');
    }

      public function getOrderDate() {
        return $this->formatDate(
            $this->getOrderAdminDate($this->getOrder()->getCreatedAt()),
            \IntlDateFormatter::MEDIUM,
            true
        );
    }
    /**
     * Get order store name
     *
     * @return null|string
     */
    public function getOrderStoreName()
    {
        if ($this->getOrder()) {
            $storeId = $this->getOrder()->getStoreId();
            if ($storeId === null) {
                $deleted = __(' [deleted]');
                return nl2br($this->getOrder()->getStoreName()) . $deleted;
            }
            $store = $this->_storeManager->getStore($storeId);
            $name = [$store->getWebsite()->getName(), $store->getGroup()->getName(), $store->getName()];
            return implode('<br/>', $name);
        }

        return null;
    }

    /**
     * Return name of the customer group.
     *
     * @return string
     */
    public function getCustomerGroupName()
    {
        if ($this->getOrder()) {
            $customerGroupId = $this->getOrder()->getCustomerGroupId();
            try {
                if ($customerGroupId !== null) {
                    return $this->groupRepository->getById($customerGroupId)->getCode();
                }
            } catch (NoSuchEntityException $e) {
                return '';
            }
        }

        return '';
    }
    public function getSellerId($productid){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load ( $productid, 'entity_id' );
        return $product->getSellerId();
    }
    public function getSeller($productid) {
         $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $sellerDatas = $objectManager->get ( 'Lof\MarketPlace\Model\Seller' )->load ( $this->getSellerId($productid), 'seller_id' );

        return $sellerDatas;
    }
    public function getOrderItems($product_id) {
        $orderitems = $this->orderitems->getCollection()->addFieldToFilter('seller_id',$this->getSellerId($product_id))->addFieldToFilter('order_id',$this->getOrderId())->addFieldToFilter('product_id',$product_id)->getFirstItem();
        return $orderitems;
    }
    
    /**
     * @return \Lofmp\Rma\Model\Rma
     */
    public function getRma()
    {
        if ($this->registry->registry('current_rma') && $this->registry->registry('current_rma')->getId()) {
            return $this->registry->registry('current_rma');
        }
    } 
    public function getQtyAvailable($item)
    {
        return $this->rmaHelper->getItemQuantityAvaiable($item);
    }
    public function getQtyRequest($item)
    {
        return $this->rmaHelper->getQtyReturnedRma($item,$this->getRma()->getId());
    }
    public function getRmaItemData($item)
    {
        return $this->rmaHelper->getRmaItemData($item,$this->getRma()->getId());
    }
     
    public function getAttachmentUrl($Uid){
        $this->context->getUrlBuilder()->getUrl('rma/attachment/download',['uid' => $Uid]);
    }
    public function GetStatusname($id){
         $status =  $this->status->load($id);
         return $status->getName();
    }
    public function getRmaDate() {
        return $this->formatDate(
            $this->getOrderAdminDate($this->getRma()->getCreatedAt()),
            \IntlDateFormatter::MEDIUM,
            true
        );
    }

    

}