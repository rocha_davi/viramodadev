<?php


namespace Lofmp\Rma\Api\Data;

interface OrderStatusHistorySearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{


    /**
     * Get OrderStatusHistory list.
     * @return \Lofmpmp\Rma\Api\Data\OrderStatusHistoryInterface[]
     */
    public function getItems();

    /**
     * Set history_id list.
     * @param \Lofmpmp\Rma\Api\Data\OrderStatusHistoryInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}