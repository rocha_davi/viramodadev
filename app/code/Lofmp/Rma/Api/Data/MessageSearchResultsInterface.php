<?php
/**
 * LandOfCoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Venustheme.com license that is
 * available through the world-wide-web at this URL:
 * http://www.venustheme.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   LandOfCoder
 * @package    Lofmp_Rma
 * @copyright  Copyright (c) 2016 Venustheme (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */



namespace Lofmp\Rma\Api\Data;

/**
 * Interface for messages search results.
 */
interface MessageSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get messages list.
     *
     * @return \Lofmp\Rma\Api\Data\MessageInterface[]
     */
    public function getItems();

    /**
     * Set messages list.
     *
     * @param array $items Array of \Lofmp\Rma\Api\Data\MessageInterface[]
     * @return $this
     */
    public function setItems(array $items);
}
