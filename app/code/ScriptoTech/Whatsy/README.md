

    ``scriptotech/module-whatsy``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
In the modern age of online communication using whatsapp

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/ScriptoTech`
 - Enable the module by running `php bin/magento module:enable ScriptoTech_Whatsy`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require scriptotech/module-whatsy`
 - enable the module by running `php bin/magento module:enable ScriptoTech_Whatsy`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - enable_whatsy (whatsy/general/enable_whatsy)

 - phone_no (whatsy/general/phone_no)

 - whatsy_location (whatsy/general/whatsy_location)


## Specifications

 - Cache
	- whsy - whsy_cache_tag > ScriptoTech\Whatsy\Model\Cache\Whsy

 - Helper
	- ScriptoTech\Whatsy\Helper\Whatsapp


## Attributes



