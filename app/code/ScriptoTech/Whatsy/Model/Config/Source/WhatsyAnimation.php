<?php
namespace ScriptoTech\Whatsy\Model\Config\Source;



class WhatsyAnimation implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'Normal', 'label' => __('Normal')],
            ['value' => 'blink', 'label' => __('Blink')],

        ];
    }
}
