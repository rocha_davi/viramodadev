<?php
/**
 * Copyright © 2021 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace ScriptoTech\Whatsy\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

use Magento\Framework\View\Asset\Repository;
use Magento\Framework\App\RequestInterface;
class Whatsapp extends AbstractHelper
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Repository $assetRepo,
        RequestInterface $request
    ) {
        $this->_storeManager = $storeManager;
        $this->assetRepo = $assetRepo;
        $this->request = $request;
        parent::__construct($context);
    }

    public function getWhatsyStatus()
    {

        return $this->scopeConfig->getValue(
            'whatsy/general/enable_whatsy',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }


    public function getWhatsylocation()
    {

        return $this->scopeConfig->getValue(
            'whatsy/general/whatsy_location',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }


    public function getWhatsyPhone()
    {

        return $this->scopeConfig->getValue(
            'whatsy/general/phone_no',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getWhatsyposition()
    {

        return $this->scopeConfig->getValue(
            'whatsy/general/whatsy_position',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getwhatsyanimation()
    {

        return $this->scopeConfig->getValue(
            'whatsy/general/whatsy_animation',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }





}

