#!/bin/bash -x
BUILD_NUMBER=$(date +%Y-%m-%d-%H-%M-%S)

echo "sou o usuario $(whoami)"

DIRECTORY=`dirname $0`
BRANCH=$(echo $DIRECTORY | awk -F "/" '{print $4}')

echo "Deploy automatico da branch $BRANCH iniciado em $(date +%Y-%m-%d-%H-%M-%S)"

echo "Baixando o novo codigo da branch $BRANCH"
rm -Rf /wwwroot/viramoda/temporario/*
rm -Rf /wwwroot/viramoda/$BRANCH/releases/$BUILD_NUMBER
mkdir -p /wwwroot/viramoda/temporario/
cd /wwwroot/viramoda/temporario/
rm -Rf /wwwroot/viramoda/$BRANCH/releases/viramoda
git clone -b $BRANCH git@bitbucket.org:thiago_covre/viramoda.git

echo "Movendo o codigo novo para o release $BUILD_NUMBER"
mv /wwwroot/viramoda/temporario/viramoda  /wwwroot/viramoda/$BRANCH/releases/$BUILD_NUMBER
cd /wwwroot/viramoda/$BRANCH/releases/$BUILD_NUMBER

echo "criando o novo link simbolico"
rm -Rf /wwwroot/viramoda/$BRANCH/releases/$BUILD_NUMBER/var 
rm -Rf /wwwroot/viramoda/$BRANCH/releases/$BUILD_NUMBER/pub/media
rm -Rf /wwwroot/viramoda/$BRANCH/releases/$BUILD_NUMBER/app/etc/env.php
rm -Rf /wwwroot/viramoda/$BRANCH/releases/$BUILD_NUMBER/sitemap.xml

ln -s /wwwroot/viramoda/$BRANCH/shared/etc/env.php		/wwwroot/viramoda/$BRANCH/releases/$BUILD_NUMBER/app/etc/env.php
ln -s /wwwroot/viramoda/$BRANCH/shared/etc/sitemap.xml	/wwwroot/viramoda/$BRANCH/releases/$BUILD_NUMBER/sitemap.xml
ln -s /wwwroot/viramoda/$BRANCH/shared/var				/wwwroot/viramoda/$BRANCH/releases/$BUILD_NUMBER/var
ln -s /wwwroot/viramoda/$BRANCH/shared/media/			/wwwroot/viramoda/$BRANCH/releases/$BUILD_NUMBER/pub/media


echo "rodando o composer install"
composer install

echo "apagando dados estaticos"
sudo rm -rf ./generated/* ./var/cache/* ./var/view_preprocessed/*
sudo chmod -R 777 ./var/cache ./pub/static  ./pub/media ./generated/

echo "inciando o deploy do mangento"
sudo chmod +x bin/magento
sudo chmod +x deploy.sh
sudo su -s /bin/bash -c "./bin/magento deploy:mode:set production --skip-compilation" www-data

echo "inciando o deploy do mangento => setup:upgrade "
sudo su -s /bin/bash -c "./bin/magento setup:upgrade" www-data

echo "inciando o deploy do mangento => setup:di:compile "
sudo su -s /bin/bash -c "./bin/magento setup:di:compile" www-data

echo "inciando o deploy do mangento => setup:static-content:deploy "
sudo su -s /bin/bash -c "./bin/magento setup:static-content:deploy -f -j $(lscpu -b -eCPU | tail -n +2 | wc -l) pt_BR en_US" www-data

echo "acertando as permissoes dos arquivos"
sudo chmod -R 777 ./var/cache ./pub/static  ./pub/media ./generated/

echo "inciando o deploy do mangento => cache:flush "
sudo su -s /bin/bash -c "./bin/magento cache:flush" www-data

echo "deploy do mangento => finalizado "
echo "apagando o a versao atual e ativando a nova versao na branch $BRANCH "
rm -Rf /wwwroot/viramoda/$BRANCH/current
ln -s  /wwwroot/viramoda/$BRANCH/releases/$BUILD_NUMBER /wwwroot/viramoda/$BRANCH/current

echo "limpando os caches"
echo "limpando os caches da cloudflare"
/wwwroot/viramoda/$BRANCH/current/bin/magento cache:clean

if [ $HOSTNAME == 'admin' ]; then

	echo "limpando os .git"
	find . -type d -name .git -exec rm -Rf {} \;

	echo "criando pacote para o s3"
	echo "compactando...."	
	tar -czf /wwwroot/viramoda/$BRANCH/releases/$BUILD_NUMBER.tar.gz /wwwroot/viramoda/$BRANCH/releases/$BUILD_NUMBER/

	echo "Copiando o release $BRANCH/$BUILD_NUMBER para o s3 " 
	echo "subindo para s3://viramoda-deploy/$BRANCH/"	
	aws s3 mv --region us-east-2 /wwwroot/viramoda/$BRANCH/releases/$BUILD_NUMBER.tar.gz s3://viramoda-deploy/$BRANCH/

	if [ $BRANCH == 'master' ]; then
		aws s3 sync --region us-east-2 --delete  /wwwroot/viramoda/master/current/pub/static s3://static.vira.moda
		curl -s -X POST "https://api.cloudflare.com/client/v4/zones/567c0ffda7cd765d073cf13c9da32b0f/purge_cache" -H "X-Auth-Email: contato@neocantra.com" -H "X-Auth-Key: cfd02b83e015a95251d8069d5d47c34e005b7" -H "Content-Type: application/json" --data '{"purge_everything":true}';		
	fi

	if [ $BRANCH == 'develop' ]; then
		curl -s -X POST "https://api.cloudflare.com/client/v4/zones/d2c3ae4fcbe8e4cede32c1f6229be9a1/purge_cache" -H "X-Auth-Email: contato@neocantra.com" -H "X-Auth-Key: cfd02b83e015a95251d8069d5d47c34e005b7" -H "Content-Type: application/json" --data '{"purge_everything":true}';
	fi

	if [ $BRANCH == 'staging' ]; then
		curl -s -X POST "https://api.cloudflare.com/client/v4/zones/d2c3ae4fcbe8e4cede32c1f6229be9a1/purge_cache" -H "X-Auth-Email: contato@neocantra.com" -H "X-Auth-Key: cfd02b83e015a95251d8069d5d47c34e005b7" -H "Content-Type: application/json" --data '{"purge_everything":true}';
	fi
fi

echo "Reiniciando os servi�os"
php -r "opcache_reset();"
sudo service php7.3-fpm restart
sudo nginx -t || exit 0
sudo service nginx restart
sudo service varnish restart

echo "Removendo versões antigas"
cd /wwwroot/viramoda/$BRANCH/releases/
ls -t1 | grep -Ev 'current|shared|releases' | tail -n +3 | xargs rm -Rf

echo "Deploy automatico da branch $BRANCH que se iniciou em $BUILD_NUMBER foi finalizado em $(date +%Y-%m-%d-%H-%M-%S)"
